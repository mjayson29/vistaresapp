<?php

use Illuminate\Database\Seeder;

use App\GeneralPage;
use App\GeneralPageItem;

class GeneralPageSeeder extends Seeder
{

	private $pages = [
		[
			'id' => 1,
			'name' => 'Terms and Condition',
			'description' => '',
			'slug_code' => 'terms_and_condition'
		],
		[
			'id' => 2,
			'name' => 'Privacy Policy',
			'description' => '',
			'slug_code' => 'privacy_policy'
		],
        [
            'id' => 3,
            'name' => 'Introduction',
            'description' => '',
            'slug_code' => 'introduction'
        ],
	];


	private $page_items = [
		[
			'general_page_id' => 1,
			'content' => 'Lorem ipsum dolor sit amet, expetenda urbanitas sed ut, ex molestie accusata sed. Elitr debitis persecuti vix at, feugait iudicabit moderatius ea eam. Pro facilisi mandamus ea, eu ullum velit nonumy vim. Populo scaevola salutatus per at, pro animal contentiones cu. Te malorum facilisi expetenda vel, est eu exerci labores reprehendunt.',
		],
		
		[
			'general_page_id' => 2,
			'content' => 'Lorem ipsum dolor sit amet, expetenda urbanitas sed ut, ex molestie accusata sed. Elitr debitis persecuti vix at, feugait iudicabit moderatius ea eam. Pro facilisi mandamus ea, eu ullum velit nonumy vim. Populo scaevola salutatus per at, pro animal contentiones cu. Te malorum facilisi expetenda vel, est eu exerci labores reprehendunt.',
		],
        [
            'general_page_id' => 3,
            'content' => 'Lorem ipsum dolor sit amet, expetenda urbanitas sed ut, ex molestie accusata sed. Elitr debitis persecuti vix at, feugait iudicabit moderatius ea eam. Pro facilisi mandamus ea, eu ullum velit nonumy vim. Populo scaevola salutatus per at, pro animal contentiones cu. Te malorum facilisi expetenda vel, est eu exerci labores reprehendunt.',
        ],
        [
            'general_page_id' => 3,
            'content' => 'Lorem ipsum dolor sit amet, expetenda urbanitas sed ut, ex molestie accusata sed. Elitr debitis persecuti vix at, feugait iudicabit moderatius ea eam. Pro facilisi mandamus ea, eu ullum velit nonumy vim. Populo scaevola salutatus per at, pro animal contentiones cu. Te malorum facilisi expetenda vel, est eu exerci labores reprehendunt.',
        ],
        [
            'general_page_id' => 3,
            'content' => 'Lorem ipsum dolor sit amet, expetenda urbanitas sed ut, ex molestie accusata sed. Elitr debitis persecuti vix at, feugait iudicabit moderatius ea eam. Pro facilisi mandamus ea, eu ullum velit nonumy vim. Populo scaevola salutatus per at, pro animal contentiones cu. Te malorum facilisi expetenda vel, est eu exerci labores reprehendunt.',
        ],

	];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	/** Populate GeneralPage Items */
    	$this->populatePages();
    }

    /**
     * Populate items
	 *
     */
    private function populatePages() 
    {

    	/** Erase all general page 
    	* and general page item
    	**/
    	\DB::table('general_pages')->truncate();
    	\DB::table('general_page_items')->truncate();


    	foreach ($this->pages as $key => $page) {
    		/** Create page */
    		GeneralPage::create($page);
    	}

    	foreach ($this->page_items as $key => $page_item) {
    		/** Create page item */
    		GeneralPageItem::create($page_item);
    	}

    }
}
