<?php

use Illuminate\Database\Seeder;
use PRAXXYS\Admin\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	app()['cache']->forget('spatie.permission.cache');
        
        $default = [
            [
                'name' =>'index',
                'description' => 'Index '
            ],
            [
                'name' =>'create',
                'description' => 'Create '
            ],
            [
                'name' =>'store',
                'description' => 'Store '
            ],
            [
                'name' =>'edit',
                'description' => 'Edit '
            ],
            [
                'name' =>'show',
                'description' => 'Show '
            ],
            [
                'name' =>'destroy',
                'description' => 'Destroy '
            ],
        ];

        $default_2 = [
        	[
                'name' =>'index',
                'description' => 'Index '
        	]
        ];

        $permissions_2 = [
        	'projects' => [],
        	'building-layouts' => [],
        ];

        $permissions = [
        	'rooms' => [],
        	'exchange_rates' => [],
        	'amenities' => [],
        	'sold-out-projects' => [],
        	'commercial-areas' => [],
        	'sold-out-projects' => [],
        	'home-banner' => [],
        	'general-page-item' => [],
        	'project' => [],
        	'building-layout' => [],
        	'floor-layout' => [],
        	'gallery' => [],
        	'units' => [],
        	
        ];

        foreach ($permissions as $name => $permissionArray) {
        	foreach ($permissionArray as $item) {
        		$item['name'] = $name . '.' . $item['name'];
        		$this->command->info('Creating permission '. $item['name']);
        		Permission::create($item + ['system' => true]);
        	}

    	    foreach ($default as $item) {
    			$expandName = title_case(str_replace('_', ' ', $name));
    			$item['name'] = $name . '.' . $item['name'];
    			$item['description'] .= $expandName;

    			$this->command->info('Creating permission ' . $item['description']);

    			Permission::create($item + ['system' => true]);
    		}
        }

        foreach ($permissions_2 as $name => $permissionArray) {
        	foreach ($permissionArray as $item) {
        		$item['name'] = $name . '.' . $item['name'];
        		$this->command->info('Creating permission '. $item['name']);
        		Permission::create($item + ['system' => true]);
        	}

    	    foreach ($default_2 as $item) {
    			$expandName = title_case(str_replace('_', ' ', $name));
    			$item['name'] = $name . '.' . $item['name'];
    			$item['description'] .= $expandName;

    			$this->command->info('Creating permission ' . $item['description']);

    			Permission::create($item + ['system' => true]);
    		}
        }

        
    }
}
