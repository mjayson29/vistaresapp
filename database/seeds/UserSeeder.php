<?php

use Illuminate\Database\Seeder;

use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	\DB::table('users')->truncate();

    	echo "Creating user for shared credentials";
    	echo "\n";

    	/** Create user */
    	$userData = new User;

    	$userData->id = 1;
    	$userData->name = 'Shared Credentials';
    	$userData->email = 'vista-shared-credentials@praxxys.ph';
    	$userData->password = bcrypt('password');

    	/** Save user */
    	$userData->save();

    }
}
