<?php

use Illuminate\Database\Seeder;

use App\ExchangeRate;

class ExchangeRateSeeder extends Seeder
{
	private $rates = [
		[
			'name' => 'US DOLLAR',
			'exchange_rate_name' => 'USD',
			'amount' => 52
		],
		[
			'name' => 'EURO',
			'exchange_rate_name' => 'EURO',
			'amount' => 59	
		],
		[
			'name' => 'JAPAN',
			'exchange_rate_name' => 'YEN',
			'amount' => 0.47
		],
		[
			'name' => 'SRI LANKA',
			'exchange_rate_name' => 'RUPEE',
			'amount' => 0.30
		]
	];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->populateExchangeRates();
    }

    /**
     * Populating of exchange rates
     * 
     */
    
    private function populateExchangeRates()
    {
    	/** Truncate exchange rates in table */
    	\DB::table('exchange_rates')->truncate();

    	foreach ($this->rates as $key => $rate) {
			
			/** Insert rate */
    		ExchangeRate::create($rate);

    	}
    }
}
