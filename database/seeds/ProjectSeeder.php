<?php

use Illuminate\Database\Seeder;

use App\Project;

class ProjectSeeder extends Seeder
{


	private $projects = [
		[
			'name' => 'Plumeria Heights',
			'description' => 'Lorem ipsum dolor sit amet, expetenda urbanitas sed ut, ex molestie accusata sed. Elitr debitis persecuti vix at, feugait iudicabit moderatius ea eam. Pro facilisi mandamus ea, eu ullum velit nonumy vim. Populo scaevola salutatus per at, pro animal contentiones cu. Te malorum facilisi expetenda vel, est eu exerci labores reprehendunt.',
			'address' => 'Sample Address',
			'latitude' => '14.576377',
			'longitude' => '121.085114',
			'image_path' => 'projects/plumeriafacade.jpg',
			'footer' => 'Li Europan lingues Ma quande lingues coalesce, li grammatica del resultant lingue es'
		],

		[
			'name' => 'Bradbury Heights',
			'description' => 'Lorem ipsum dolor sit amet, expetenda urbanitas sed ut, ex molestie accusata sed. Elitr debitis persecuti vix at, feugait iudicabit moderatius ea eam. Pro facilisi mandamus ea, eu ullum velit nonumy vim. Populo scaevola salutatus per at, pro animal contentiones cu. Te malorum facilisi expetenda vel, est eu exerci labores reprehendunt.',
			'address' => 'Sample Address',
			'latitude' => '14.576377',
			'longitude' => '121.085114',
			'image_path' => 'projects/BradburyHeights.jpeg',
			'footer' => 'Li Europan lingues Ma quande lingues coalesce, li grammatica del resultant lingue es'
		],

		[
			'name' => 'Hawthrone Heights',
			'description' => 'Lorem ipsum dolor sit amet, expetenda urbanitas sed ut, ex molestie accusata sed. Elitr debitis persecuti vix at, feugait iudicabit moderatius ea eam. Pro facilisi mandamus ea, eu ullum velit nonumy vim. Populo scaevola salutatus per at, pro animal contentiones cu. Te malorum facilisi expetenda vel, est eu exerci labores reprehendunt.',
			'address' => 'Sample Address',
			'latitude' => '14.576377',
			'longitude' => '121.085114',
			'image_path' => 'projects/HawthorneHeights.jpeg',
			'footer' => 'Li Europan lingues Ma quande lingues coalesce, li grammatica del resultant lingue es'
		],

	];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		/** Seed Projects */
	   	$this->populateProjects();

    }

    /**
     * Populating of projects
     * 
     */
    private function populateProjects()
    {

    	/** Truncate projects in table */
    	\DB::table('projects')->truncate();

    	foreach ($this->projects as $key => $project) {
			
			/** Insert project */
    		Project::create($project);

    	}


    }

}
