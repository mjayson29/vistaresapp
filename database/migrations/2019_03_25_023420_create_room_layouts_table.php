<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomLayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_layouts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('unit_id')->unsigned()->index();
            $table->string('name');
            $table->float('x_coordinates');
            $table->float('y_coordinates');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_layouts');
    }
}
