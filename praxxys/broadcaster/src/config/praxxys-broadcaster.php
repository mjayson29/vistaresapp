<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Broadcaster Config
    |--------------------------------------------------------------------------
    |
    | This section defines a custom guard, custom password broker, and auth 
    | provider. It also provides defaults for websocket authentication
    */

    'auth' => [
        'websockets' => [
            'token-key' => 'token',
            'cache-store' => 'file',
            'cache-lifetime' => 1,
            'endpoint' => '/praxxys-broadcaster/auth'
        ],
    ],

    # Comment this out to set your private user channel:

    // 'channels' => [
    //     'private' => [
    //         'users' => App\User::class
    //     ],
    // ],

    'laravel' => [
        'auth' => [
            'guards' => [
                'websocket' => [
                    'driver' => 'websocket',
                    'provider' => 'none'
                ]
            ],
            'providers' => [
                'none' => [
                    'model' => 'none'
                ],
            ],

        ],
        'broadcasting' => [
            'connections' => [
                'praxxys-broadcaster' => [
                    'driver' => 'praxxys-broadcaster',
                    'key' => env('BROADCASTER_APP_KEY'),
                    'app_id' => env('BROADCASTER_APP_ID'),
                    'options' => [
                        'host' => env('BROADCASTER_APP_HOST'),
                        'port' => env('BROADCASTER_APP_PORT'),
                        'scheme' => env('BROADCASTER_APP_SCHEME')
                    ],                
                ]
            ]
        ],
    ]

];
