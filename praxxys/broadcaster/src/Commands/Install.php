<?php

namespace PRAXXYS\Broadcaster\Commands;

use Illuminate\Console\Command;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'praxxys-broadcaster:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs PRAXXYS Broadcaster';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo PHP_EOL;
        $this->info("Installing PRAXXYS Broadcaster...");
        echo PHP_EOL;
        echo PHP_EOL;

        $this->info("Publishing vendor files...");
        echo PHP_EOL;
        $this->call('vendor:publish', [
            '--provider' => 'PRAXXYS\\Broadcaster\\BroadcasterServiceProvider'
        ]);
        echo PHP_EOL;

        $this->info("Writing setting keys to .env...");
        echo PHP_EOL;

        file_put_contents(
            $this->laravel->environmentFilePath(),
            file_get_contents($this->laravel->environmentFilePath()) .
'
BROADCASTER_APP_ID=
BROADCASTER_APP_KEY=
BROADCASTER_APP_PORT=6001
BROADCASTER_APP_HOST=http://localhost
BROADCASTER_APP_SCHEME=http

'
        );
        echo PHP_EOL;
        echo PHP_EOL;
        $this->info("Done installing PRAXXYS Broadcaster!");

    }
}
