
	<script src="{{ PRXBroadcaster::vendor_path() }}/js/praxxys-broadcaster.min.js"></script>
	<script>

		<?php $guard = isset($guard) ? $guard : 'web' ?>

	    @if(PRXBroadcaster::token($guard))
	    var echo = new Echo({!! PRXBroadcaster::echoJson($guard) !!});

	    @foreach(PRXBroadcaster::channels('private') as $model)

	    @if(get_class(Auth::guard($guard)->user()) === $model)
	    echo.private('{{ PRXBroadcaster::dotClass($model) }}.{{ Auth::guard($guard)->id() }}').notification(function (notification) {
	        console.log(notification.type);
	    });
	    @endif

	    @endforeach

	    @endif
	</script>
