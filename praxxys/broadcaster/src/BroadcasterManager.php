<?php

namespace PRAXXYS\Broadcaster;

use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class BroadcasterManager
{
    /**
     * Config
     *
     * @var array
     */
    protected $config;

    /**
     * Broadcast token
     *
     * @var array
     */
    protected $token;

    /**
     * Token lifetime
     *
     * @var array
     */
    protected $tokenTTL;

    /**
     * Registered channels
     *
     * @var array
     */
    protected $channels = [];

    /**
     * Create a new broadcaster manager instance.
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->config = $config;
        
        $this->channels = $this->config['channels'];
        $this->cache = Cache::store(array_get($this->config, 'auth.websockets.cache-store'));
        $this->tokenTTL = array_get($this->config, 'auth.websockets.cache-lifetime');



    }

    /**
     * Return token or create one if not yet set
     *
     * @param string
     * @return string
     */
    public function token($guard = 'web') {

        if(empty($this->token)) {
            $this->makeToken($guard);
        }

        return $this->token;

    }

    /**
     * Return settings in json format (for Laravel Echo)
     *
     * @return json
     */
    public function echoJson($guard = 'web') {

        # Shortcircuit if no user
        if(!$this->getUser($guard)) {
            return;
        }        

        $config = array_get($this->config, 'laravel.broadcasting.connections.praxxys-broadcaster');

        return json_encode([
            'broadcaster' => 'socket.io',
            'host' => $config['options']['host'] . (empty($config['options']['port']) ?: ':' . $config['options']['port']),
            'key' => $config['key'],
            'auth' => [
                'headers' => [
                     $this->config['auth']['websockets']['token-key'] => $this->token()
                ]
            ]

        ]);
    }

    /**
     * Register channels with current broadcast driver
     *
     * @param string
     * @return sring
     */
    public function registerChannels() {
        foreach($this->config['channels'] as $type => $models) {
            switch($type) {
                case 'private':
                    foreach($models as $model)
                    Broadcast::channel(
                        $this->dotClass($model) . '.{id}',
                        function ($user, $id) {
                            return (int) $user->id === (int) $id;
                        }
                    );
                    break;
            }
        }
    }

    /**
     * Get channels
     *
     * @param string
     * @return sring
     */
    public function channels($type) {
        return $this->channels[$type];
    }

    /**
     * Return class path in dot notation
     *
     * @param string
     * @return sring
     */
    public static function dotClass($class) {
        return str_replace ("\\", ".", $class);
    }

    /**
     * Create and cache token
     *
     * @return void
     */
    protected function makeToken($guard) {

        # Shortcircuit if no user
        if(! $user = $this->getUser($guard)) {
            return;
        }

        $this->token = bin2hex(random_bytes(16));

        $this->cache->put(
            $this->token,
            $user->id,
            $this->tokenTTL
            );
    }

    /**
     * Get current user
     *
     * @return void
     */
    protected function getUser($guard) {
        return Auth::guard($guard)->user();
    }

    /**
     * Get vendor path
     *
     * @return void
     */
    public function vendor_path() {
        return '/vendor/praxxys/broadcaster';
    }


}
