<?php

namespace PRAXXYS\Broadcaster\Auth;

use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Auth\GenericUser;

use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class WebSocketUserProvider implements UserProvider
{
    /**
     * Cache repository
     *
     * @var \Illuminate\Cache\Repository
     */
    protected $cache;

    /**
     * Cache lifetime
     *
     * @var int
     */
    protected $lifetime;

    /**
     * The token key
     *
     * @var string
     */
    protected $tokenKey;

    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct() {

        # Set configuration options
        $this->tokenKey = config('praxxys-broadcaster.auth.websockets.token-key');
        $this->cache = Cache::store(config('praxxys-broadcaster.auth.websockets.cache-store'));
        $this->lifetime = config('praxxys-broadcaster.auth.websockets.cache-lifetime');
    }

    protected function setToken($credentials) {

        if(empty($credentials)) {
            throw new AccessDeniedHttpException;
        }

        $this->token = $credentials[$this->tokenKey];
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed  $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier) {
        throw new NotAcceptableHttpException('Method is not implemented.');
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed   $identifier
     * @param  string  $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token) {
        throw new NotAcceptableHttpException('Method is not implemented.');    
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  string  $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token) {
        throw new NotAcceptableHttpException('Method is not implemented.');
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials) {
        
        $this->setToken($credentials);

        if($result = $this->cache->get($this->token)) {
            return new GenericUser((array) ['id' => $result, 'token' => $this->token]);
        }

        return;

    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  array  $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials) {
        return $user->token === $credentials[$this->tokenKey];
    }
}
