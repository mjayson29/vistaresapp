<?php

namespace PRAXXYS\Broadcaster\Auth;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Auth\GuardHelpers;

class WebSocketGuard implements Guard
{
    use GuardHelpers;

    /**
     * The token key.
     *
     * @var string
     */
    protected $tokenKey;

    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Create a new authentication guard.
     *
     * @param  \Illuminate\Contracts\Auth\UserProvider  $provider
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(WebSocketUserProvider $provider, Request $request)
    {
        $this->request = $request;
        $this->provider = $provider;
        $this->tokenKey = config('praxxys-broadcaster.auth.websockets.token-key');        
    }

    /**
     * Get the currently authenticated user.
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function user()
    {
        // If we've already retrieved the user for the current request we can just
        // return it back immediately. We do not want to fetch the user data on
        // every call to this method because that would be tremendously slow.
        if (! is_null($this->user)) {
            return $this->user;
        }

        $user = null;

        $token = $this->request->header($this->tokenKey);

        if (! empty($token)) {
            $user = $this->provider->retrieveByCredentials(
                [$this->tokenKey => $token]
            );
        }

        return $this->user = $user;
    }

    /**
     * Validate a user's credentials.
     *
     * @param  array  $credentials
     * @return bool
     */
    public function validate(array $credentials = [])
    {
        if (empty($credentials[$this->tokenKey])) {
            return false;
        }

        $credentials = [$this->tokenKey => $credentials[$this->tokenKey]];

        if ($this->provider->retrieveByCredentials($credentials)) {
            return true;
        }

        return false;
    }
    
    /**
     * Set the current request instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;

        return $this;
    }
}