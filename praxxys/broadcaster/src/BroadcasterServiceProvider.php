<?php

namespace PRAXXYS\Broadcaster;

use PRAXXYS\Broadcaster\Auth\WebSocketGuard;
use PRAXXYS\Broadcaster\Auth\WebSocketUserProvider;
use PRAXXYS\Broadcaster\Http\Middleware\BroadcasterMiddleware;
use PRAXXYS\Broadcaster\Broadcaster;
use PRAXXYS\Broadcaster\BroadcasterManager;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Broadcasting\BroadcastManager;
use Pusher\Pusher;

class BroadcasterServiceProvider extends ServiceProvider
{
 
    /**
     * Register commands array
     *
     * @var array
     */
    protected $commands = [
        'PRAXXYS\Broadcaster\Commands\Install'
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(BroadcastManager $broadcastManager)
    {
        # Register new broadcast driver
        $broadcastManager->extend('praxxys-broadcaster', function ($app, array $config) {
            return new Broadcaster(new Pusher(
                $config['key'], null,
                $config['app_id'], $config['options'])
            );
        });
        # Register Guard
        Auth::extend('websocket', function ($app, $name, array $config) {
            return new WebSocketGuard(new WebSocketUserProvider(), $app->request);
        });

        # register alias
        $this->app->singleton('praxxys-broadcaster', function ($app) {
            return new BroadcasterManager($app['config']->get('praxxys-broadcaster'));
        });

        # register middleware
        $this->app->router->aliasMiddleware(
            'praxxys-broadcaster', 
            BroadcasterMiddleware::class
        );

        # load routes
        $this->loadRoutesFrom(__DIR__.'/Http/Routes/web.php');
        # load views
        $this->loadViewsFrom(__DIR__.'/resources/views', 'PRXBroadcaster');

        # publish config
        $this->publishes([
            __DIR__.'/config/' => config_path(),
        ], 'praxxys-broadcaster-config');
        # publish public assets
        $this->publishes([
            __DIR__.'/resources/assets' => public_path('vendor/praxxys/broadcaster'),
        ], 'praxxys-broadcaster-assets');

        # Register config
        if(config('praxxys-broadcaster.laravel')) {
            foreach(config('praxxys-broadcaster.laravel') as $mainKey => $mainSection) {
                config([$mainKey => array_merge_recursive($mainSection, config($mainKey))]);
            }
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        # Register commands
        $this->commands($this->commands);

    }
}
