<?php

/*
|--------------------------------------------------------------------------
| Broadcasting Routes
|--------------------------------------------------------------------------
*/

Route::post(config('praxxys-broadcaster.auth.websockets.endpoint'), 
	\PRAXXYS\Broadcaster\Http\Controllers\BroadcastController::class.'@authenticate')
		->middleware('auth:websocket');

