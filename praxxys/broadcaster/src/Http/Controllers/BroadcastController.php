<?php

namespace PRAXXYS\Broadcaster\Http\Controllers;

use PRAXXYS\Broadcaster\Facades\PRXBroadcaster;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Broadcast;

class BroadcastController extends Controller
{
    /**
     * Authenticate the request for channel access.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request) {

        # Set custom broadcast driver
        config(["broadcasting.default" => 'praxxys-broadcaster']);

        # Set broadcast permissions
        PRXBroadcaster::registerChannels();

    	return Broadcast::auth($request);
    }
}
