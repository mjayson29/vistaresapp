let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */


// Compile app.js
mix.js('src/Resources/Assets/js/app.js', 'src/Resources/Assets/assets/js/app.min.js');
mix.js('src/Resources/Assets/js/main.js', 'src/Resources/Assets/assets/js/main.min.js');
mix.js('src/Resources/Assets/js/main-public.js', 'src/Resources/Assets/assets/js/main-public.min.js');

// Concatenate css
mix.styles(
	[
		'node_modules/datatables.net-bs/css/dataTables.bootstrap.css',
		'node_modules/dropzone/dist/dropzone.css',
		'node_modules/bootstrap3/dist/css/bootstrap.css',
		'node_modules/bootstrap-toggle/css/bootstrap-toggle.css',
		'node_modules/admin-lte/dist/css/AdminLTE.css',
		'node_modules/flatpickr/dist/flatpickr.min.css',
		'node_modules/select2/dist/css/select2.css',
		'node_modules/fullcalendar/dist/fullcalendar.css',
		'css/default-skin.css'
	], 'src/Resources/Assets/assets/css/admin.min.css');

// Copy fonts

mix.copy('node_modules/bootstrap3/dist/fonts', 'src/Resources/Assets/assets/fonts');