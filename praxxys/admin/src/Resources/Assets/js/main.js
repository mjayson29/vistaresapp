$('document').ready(function() {
	main.init();
});

var main = {

	dataTables: null,
	actionBtn: null,
	deleteBtn: null,
	fullCalendar: null,
	locationPicker: null,
	flatpickr: null,
	editorField: null,
	select2: null,
	bootstrapToggle: null,
	dropzone: null,
	dzContainer: null,
	dzDeleteContainer: null,

	init: function() {

		this.setupSelectors();

		this.alertStatusMessage();
		this.setupSelect2();
		this.setupBootstrapToggle2();
		this.setupDropzone();

		this.setupNavigation();

		this.setupPRXDataTables();
		this.onDataTableUpdate();

		this.setupButtons();
		this.setupCKEditor();
		this.setupFlatPicker();
		this.setupLocationPicker();
		this.setupFullCalendar();

	},

	setupSelectors: function() {
		main.dataTables = $('.dataTables');
		main.actionBtn = $('.actionBtn');
		main.deleteBtn = $('.deleteBtn');
		main.fullCalendar = $('.fullCalendar');
		main.locationPicker = $('.locationPicker');
		main.flatpickr = $('.flatpickr');
		main.editorField = $('textarea.editorField');
		main.select2 = $('.select2');
		main.bootstrapToggle = $('.bootstrap-toggle');
		main.dropzone = $('.dropzone');
		main.dzContainer = $('.dzContainer');
		main.dzDeleteContainer = $('.dzDeleteContainer');
	},

	// Setup Full Calendar
	setupFullCalendar: function() {

	    main.fullCalendar.each(function() {

    		$this = $(this);

	    	$this.fullCalendar({
	    		defaultView: 'agendaWeek',
	    		allDaySlot: false,
		    	header: {
		    		left: 'prev,next today',
		    		center: 'title',
		    		right: 'month,agendaWeek,agendaDay'
		    	},
		    	//defaultDate: '2018-03-12',
		    	eventLimit: true,
		    	events: systemVars.fullCalendars[$this.data('name')],
		    });

	    });

	},

	// Setup location picker
	setupLocationPicker: function() {

		if(main.locationPicker.length) {

			// Load google maps api
			$.getScript('https://maps.google.com/maps/api/js?key=AIzaSyBKese9Nxt27v9smmicyvJ037PwXfGbe3E&sensor=false&libraries=places', function() {

				require('jquery-locationpicker');

				main.locationPicker.each(function() {
					
					var $this = $(this),
						lat = $this.find('.locationPicker__lat').val() || 14.634106,
						long = $this.find('.locationPicker__long').val() || 121.041755;


					// WIP: Use location name for edit method
					$this.find('.locationPicker__map')
						.width(300)
						.height(300)
						.css({
							'margin': '20px 0'
						})
						.locationpicker({
							location: {
								latitude: lat,
								longitude: long
							},
							radius: 1,
							inputBinding: {
								latitudeInput: $this.find('.locationPicker__lat'),
								longitudeInput: $this.find('.locationPicker__long'),
								//radiusInput: $('#us2-radius'),
								locationNameInput: $('.locationPicker__search')
							},
							enableAutocomplete: true,
							autocompleteOptions: {
								//types: ['(cities)'],
								componentRestrictions: {country: 'ph'}
							}

						});

				});


			});
		}
	},

	setupFlatPicker: function() {
		const $this = this;
		
		main.flatpickr.each(function() {
			/* Set default value base on the data value attr */
            const elem = $(this);
            let value = elem.data('value');
            let type = elem.data('type');

            let dateFormat = 'Y-m-d';
            let timeOnly = false;
            let includeTime = false;

			switch (type) {
				case 'date':
						dateFormat = 'Y-m-d';
					break;
				case 'time':
						dateFormat = 'H:i';
						timeOnly = true;
						includeTime = true;
					break;
				case 'date-time':
						dateFormat = 'Y-m-d H:i';
						includeTime = true;
					break;
				default:
						dateFormat = 'Y-m-d H:i';
					break;
			}

        	$(this).flatpickr({
                dateFormat: dateFormat,
				enableTime: includeTime,
				noCalendar: timeOnly,
                defaultDate: value ? value : new Date(),
                onReady: function(selectedDates, dateStr, instance) {
                	elem.data('value', dateStr);
                },
                
                onChange: function(selectedDates, dateStr, instance) {
                	elem.data('value', dateStr);
			    },
            });
		});
	},

	setupCKEditor: function() {

		main.editorField.each( function() {
		ClassicEditor
		        .create( $(this)[0] )
		        .catch( error => {
		            console.error( error );
		        } );

		});
	},


	setupNavigation: function() {

		// Set active nav
		$('.main-sidebar li > a.nav-link').each(function() {
			if((window.location.href).indexOf($(this).prop('href')) !== -1) {
				$(this).parents('li').addClass('active');
				//console.log(window.location.href + ' | ' + $(this).prop('href'));
			}
		});

		// Remove blank navs
		$('.main-sidebar li.nav-item').each(function() {
			if(!$(this).find('a.nav-link').length) {
				$(this).remove();
			}
		});

		// Remove blank sections
		$('.main-sidebar li.header').each(function() {
			if(!$(this).next('li.treeview').length) {
				$(this).remove();
			}
		});

		// Show nav
		$('.main-sidebar').fadeIn();

	},

	alertStatusMessage: function() {
		if(systemVars.status.title !== null) {

			swal({
			  	title: systemVars.status.title,
			  	text: systemVars.status.message,
			  	type: systemVars.status.type,
				confirmButtonColor: '#E0C34C',
			});

		}
	},

	onDataTableUpdate: function() {
		main.dataTables.each(function() {
			$(this).on('draw.dt search.dt page.dt responsive-display.dt', function() {

				setTimeout(function() {
	        		main.actionBtn.off();
	        		main.deleteBtn.off();
					main.setupButtons();
        		}, 500);
        		
			});

		});
	},

	setupButtons: function() {

		main.actionBtn.on('click', function(e) {

			var $this = $(this),
				message = $this.data('message'),
				method = $this.data('method') ? $this.data('method') : 'POST',
				getOrPost = $this.data('isget') ? 'GET' : 'POST',
				action = $this.data('action');

				e.preventDefault();

				swal({
					title: 'Are you sure?',
					text: message,
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#d33',
					cancelButtonColor: '#ccc',
					confirmButtonText: 'Yes!'
				})

				.then(function() {

					$('<form method="'+ getOrPost +'" action="' + action +'"><input type="hidden" name="_method" value="' + method + '"><input type="hidden" name="_token" value="' + systemVars.token + '"></form>')
					.appendTo(document.body)
					.submit();

				});


		});

		main.deleteBtn.on('click', function(e) {

			var $this = $(this),
				message = $this.data('message'),
				action = $this.data('action');

				e.preventDefault();

				swal({
					title: 'Are you sure?',
					text: message,
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#d33',
					cancelButtonColor: '#ccc',
					confirmButtonText: 'Yes, delete it!'

				}).then((result) => {
					if (result.value) {

						$('<form action="' + action + '" method="post">' +
						       '<input type="hidden" name="_method" value="DELETE">' +
						       '<input type="hidden" name="_token" value="' + systemVars.token + '">' +
						  '</form>').appendTo('body').submit();

					}
				})

		});
	},

	setupPRXDataTables: function() {


		main.dataTables.each(function() {

			var $this = $(this),
				events = [],
				options = {
					responsive: true,
					autoWidth: false,
				};

			// Add datatable systemVars source if defined
			if(systemVars.datatables !== undefined && systemVars.datatables[$this.data('name')]) {
				
				options.data = systemVars.datatables[$this.data('name')].data;
				options.columns = systemVars.datatables[$this.data('name')].headers;
			}

			/* Add sorting of columns */
			const sort = $(this).data('sort') ? $(this).data('sort') : 'asc';
			const column = $(this).data('column') ? $(this).data('column') : 0;

			options.order = [[ column, sort ]];

			// Initialize table actions
			if($this.hasClass('table__actions')) {
			
				var actions = $('<center></center>');

				options.columns.push({
					title: 'Actions'
				});

				// Bind viewable
				if($this.hasClass('table__viewable')) {

					// Append html edit button to actions
					actions.append('<a href="#" class="btn__view btn btn-primary"><i class="fa fa-eye"></i></a>');

					// Push event into array
					events.push({
						name: 'draw.dt search.dt page.dt responsive-display.dt',
						func: function(e) {
							e.find('.btn__view').off().on('click', function(event) {

								event.preventDefault();

									// row variable checks for responsive mode
								var row =  $(this).parents('li').length ? $(this).closest('li') : $(this).closest('tr'),
									data = $(e).DataTable().row(row).data(),
									route = $(e).data('route') ? $(e).data('route') : window.location.href;
									id = data[0];

								var link = route + '/' + id;

								window.location.href = link;
							});
						}
					});
				}

				// Bind editable
				if($this.hasClass('table__editable')) {

					// Append html edit button to actions
					actions.append('<a href="#" class="btn__edit btn btn-primary"><i class="far fa-edit"></i></a>');

					// Push event into array
					events.push({
						name: 'draw.dt search.dt page.dt responsive-display.dt',
						func: function(e) {
							e.find('.btn__edit').off().on('click', function(event) {

								event.preventDefault();

									// row variable checks for responsive mode
								var row =  $(this).parents('li').length ? $(this).closest('li') : $(this).closest('tr'),
									data = $(e).DataTable().row(row).data(),
									route = $(e).data('route') ? $(e).data('route') : window.location.href;
									id = data[0];

								var link = route + '/' + id + '/edit';

								window.location.href = link;
							});
						}
					});
				}

				// Bind editable
				if($this.hasClass('table__deletable')) {

					// Append html edit button to actions
					actions.append('<a href="#" class="btn__delete btn btn-danger"><i class="fa fa-trash-alt"></i></a>');

					// Push event into array
					events.push({
						name: 'draw.dt search.dt page.dt responsive-display.dt',
						func: function(e) {
							e.find('.btn__delete').off().on('click', function(event) {

								event.preventDefault();

									// row variable checks for responsive mode
								var row =  $(this).parents('li').length ? $(this).closest('li') : $(this).closest('tr'),
									data = $(e).DataTable().row(row).data(),
									route = $(e).data('route') ? $(e).data('route') : window.location.href;
									id = data[0];

								var action = route + '/' + id;

								
								swal({
									title: 'Are you sure?',
									text: "Please confirm if you wish to delete this item.",
									type: 'warning',
									showCancelButton: true,
									confirmButtonColor: '#d33',
									//cancelButtonColor: '#d33',
									confirmButtonText: 'Yes, delete it!'
								}).then((result) => {
									if (result.value) {

										$('<form action="' + action + '" method="post">' +
										       '<input type="hidden" name="_method" value="DELETE">' +
										       '<input type="hidden" name="_token" value="' + systemVars.token + '">' +
										  '</form>').appendTo('body').submit();

									}
								})

							});
						}
					});
				}



				// Add html actions to new column
				options.columnDefs = [{
		            targets: -1,
		            data: null,
		            width: '200px',
		            "defaultContent": actions.prop('outerHTML')
		        }];

			}


			// Bind events
			events.forEach(function(event) {
				$this.on(event.name, function() { return event.func($this); });
			});

			// Initialize data table
			$(this).DataTable(options);
		
		});
	},

	setupSelect2: function() {
		main.select2.each(function() {
			const elem = $(this);
			const tags = elem.data('tags');

			if (tags) {
				elem.val(elem.data('value'))
				.select2({
					tags: tags ? true : false,
					tokenSeparators: [','],
				});
			} else {
				elem.select2();
			}
		});
	},

	setupBootstrapToggle2: function() {
			
		main.bootstrapToggle.bootstrapToggle();
	},

	setupDropzone: function() {

		main.dropzone.each(function() {
			var $this = $(this),
				target = $this.find('.dzTarget'),
				action = $this.data('action');
				parentID = $this.data('parent-id') !== undefined ? $this.data('parent-id') : 0;
			
	        // TODO: On delete, remove the deleted file ID on the file variable holder



			$this.dropzone({
				url: action,
				headers: { 
					'X-CSRF-TOKEN': systemVars.token
				},
				params: {
					parentID: parentID
				},
				acceptedFiles: 'image/*',
				init: function() {
					this.on('success', function(file, data) {

						// add "," + value to string
						var fileArray = $.grep([target.val(), data.id], Boolean).join(",");

			            target.val(fileArray);

			            // build to container
			            var container = 
			            	$('<li></li>')
			            	.addClass('ui-state-default')
			            	.css('display', 'none')
			            	.data('id', data.id);

			            // build inner div and add to container
			            $('<div></div>')
			            	.addClass('dzImage')
			            	.css('background-image', 'url(' + data.path + ')')
			            	.appendTo(container);

			            
			            // add container to main and show
			            container
			            	.appendTo('.dzContainer')
			            	.show('fade');

			            main.dzContainer.sortable('refresh');
						//main.utils.dropzone.setImageOrder();

		        	});

				}
			});
	    });

	    main.dzContainer.each(function() {
			var $this = $(this);

			$this.sortable({
				revert: true,
				update: function(a, b){

					var entityName = $this.data('entity'),
						action = $this.data('action');

					var $sorted = b.item;

					var $previous = $sorted.prev();
					var $next = $sorted.next();

					if ($previous.length > 0) {
						main.utils.dropzone.postImageOrder({
							//parentId: $sorted.data('parentid'),
							type: 'moveAfter',
							entityName: entityName,
							id: $sorted.data('id'),
							positionEntityId: $previous.data('id')
						}, action);
					} else if ($next.length > 0) {
						main.utils.dropzone.postImageOrder({
							//parentId: $sorted.data('parentid'),
							type: 'moveBefore',
							entityName: entityName,
							id: $sorted.data('id'),
							positionEntityId: $next.data('id')
						}, action);
					} else {
						console.error('Something wrong!');
					}
				},
			});

	    });

	    main.dzDeleteContainer.each(function() {
			var $this = $(this),
				action = $this.data('action');

			$this.droppable({
				drop: function(event, ui) {
					swal({
						title: 'Delete Image',
						text: 'Are you sure you want to delete the image?',
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#d33',
						cancelButtonColor: '#ccc',
						confirmButtonText: 'Yes!'
					})

					.then(function() {

						main.utils.dropzone.postImageDelete({
							id: ui.draggable.data('id')
						}, action);

						ui.draggable.remove();


					});
				}
			});

	    });

	},
	utils: {
		dropzone: {
			postImageDelete: function(requestData, action){
				$.ajax({
					'url': action,
					'type': 'POST',
					'data': requestData,
					'headers': {
						'X-CSRF-TOKEN': systemVars.token
					},
					'success': function(data) {
						if (data.success) {
							console.log('Deleted!');
						} else {
							console.error(data.errors);
						}
					},
					'error': function(){
						console.error('Something wrong!');
					}
				});
			},
			postImageOrder: function(requestData, action){
				$.ajax({
					'url': action,
					'type': 'POST',
					'data': requestData,
					'headers': {
						'X-CSRF-TOKEN': systemVars.token
					},
					'success': function(data) {
						if (data.success) {
							console.log('Saved!');
						} else {
							console.error(data.errors);
						}
					},
					'error': function(){
						console.error('Something wrong!');
					}
				});
			}
		}
	}
};