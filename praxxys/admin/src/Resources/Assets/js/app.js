window.$ = window.jQuery = require('jquery');

require("jquery-ui");
require("jquery-ui/ui/widgets/draggable");
require("jquery-ui/ui/widgets/droppable");
require("jquery-ui/ui/widgets/sortable");

require('bootstrap3');
require('fastclick');
require('jquery-slimscroll');
require("admin-lte");
require("jquery-ui");
require('select2');
require('bootstrap-toggle');
require('datatables.net-bs');

window.ClassicEditor = require('@ckeditor/ckeditor5-build-classic');
window.Vue = require('vue');
window.flatpickr = require("flatpickr");
window.swal = require('sweetalert2');

window.Dropzone = require('dropzone');
window.Dropzone.autoDiscover = false;

import 'promise-polyfill/src/polyfill';
import 'fullcalendar';
