    <!-- Content Wrapper. Contains page content -->
    <div id="admin" class="content-wrapper">

        @yield('content')

    </div>
    <!-- /.content-wrapper -->