    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar" style="display: none;">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                @include('PRXAdmin::includes.navigation-panel')    
            </ul>
            <!-- /.sidebar-menu -->
        </section>
    <!-- /.sidebar -->
    </aside>