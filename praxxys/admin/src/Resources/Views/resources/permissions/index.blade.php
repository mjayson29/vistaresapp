@extends('PRXAdmin::master')

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Roles and Permissions
      <small>Manage Permissions of Roles</small>
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
     <div class="row margin-bottom">
      <div class="col-md-12">
      </div>
      <!-- /.col -->
    </div>
    <div class="row">

      <div class="col-xs-12">
        <div class="box">

          <div class="box-header">
            <h3 class="box-title">Roles</h3>
          </div>

         <div class="box-body">
            <table class="table table-bordered table-striped dataTables dt-responsive nowrap" width="100%">
              <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Description</th>
                <th>Actions</th>
              </tr>
              </thead>
              <tbody>
              @foreach($roles as $role)
              <tr>
                <td>{{ $role->id }}</td>
                <td>{{ $role->name }}</td>
                <td>{{ $role->description }}</td>
                <td>
                  <a href="{{ route('permissions.show', $role->id) }}" class="btn btn-xs btn-primary">
                    <i class="fa fa-eye"></i>
                  </a>
                </td>
              </tr>
              @endforeach
              </tbody>
              <tfoot>
              </tfoot>
            </table>
          </div>


        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
@stop