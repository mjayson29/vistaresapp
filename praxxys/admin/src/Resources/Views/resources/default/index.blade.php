    @extends('PRXAdmin::master')
    @section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>{{ $subtitle }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-list"></i> {{ $title }}</a></li>
            <li class="active">{{ $subtitle }}</li>
        </ol>
    </section>

<!-- Main content -->
    <section class="content">

        <div class="row margin-bottom">
            @if(PRXAdmin::auth()->can("$name.create"))
            <div class="col-md-12">
                <a href="{{ route("admin.$name.create") }}" class="btn btn-primary">
                    <i class="fa fa-plus"></i> Add {{ $title }}
                </a>
            </div>
            @endif
            <!-- /.col -->
        </div>
        <!-- /.row -->      

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ $title }} {{ $subtitle }}</h3>
                    </div>
                    <div class="box-body">
                        @component('PRXAdmin::components.datatable', ['name' => $name])
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- /.content -->

    @stop
