    @extends('PRXAdmin::master')
    @section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $item->getName() }}
            <small>{{ $subtitle }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route("admin.$name.index") }}"><i class="far fa-edit"></i> {{ $title }}</a></li>
            <li class="active">{{ $subtitle }}</li>
        </ol>
    </section>

<!-- Main content -->
    <section class="content">

        @if(PRXAdmin::auth()->can("$name.edit") ||  PRXAdmin::auth()->can("$name.destroy"))
        <div class="row margin-bottom">
            <div class="col-md-12">
                @if(PRXAdmin::auth()->can("$name.edit"))
                <a href="{{ route("admin.$name.edit", $item->id) }}" class="btn btn-primary">
                    <i class="far fa-edit"></i> Edit
                </a>
                @endif
                @if(PRXAdmin::auth()->can("$name.destroy"))
                <button data-action="{{ route("admin.$name.destroy", $item->id) }}" class="deleteBtn btn btn-danger">
                    <i class="fa fa-trash"></i> Delete
                </button>
                @endif
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->      
        @endif


        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_main" data-toggle="tab">Main</a></li>
                        {{-- Loop has_many --}}
                        @foreach($has_many as $name => $data)
                        <li><a href="#tab_{{ $name }}" data-toggle="tab">{{ $data['label'] }}</a></li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_main">
                            <div class="row">
                                
                            
                            <div class="col-md-6">
                            @foreach($has_one as $name => $data)

                                @if($item->$name)
                                <div class="form-group">
                                    <label for="{{ $name }}">{{ $data['label'] }}</label>
                                    <p>
                                        @if(Route::has("{$item->$name::getBaseRoute()}.show"))
                                        <a href="{{ route("{$item->$name::getBaseRoute()}.show", $item->$name->id) }}">{{ $item->$name->getName() }}</a>
                                        @else

                                        {{ $item->$name->getName() }}
                                        @endif
                                    </p>
                                </div>
                                @endif

                            @endforeach

                            {{-- Has One Data --}}
                            @foreach($belongs_to as $name => $data)

                                @switch($data['type'])

                                @case('select')

                                <div class="form-group">
                                    <label for="{{ $name }}">{{ $data['label'] }}</label>
                                    <p>
                                        @if(Route::has("{$item::getBelongsToBaseRoute($name)}.show"))
                                        <a href="{{ route("{$item::getBelongsToBaseRoute($name)}.show", $item->$name) }}">{{ $item::getBelongsToByField($name, $item)}}</a>
                                        @else
                                        {{ $item::getBelongsToByField($name, $item)}}
                                        @endif
                                    </p>
                                </div>

                                    @break

                                @endswitch

                            @endforeach



                            {{-- Render Enum Fields --}}
                            @foreach($enum_fields as $name => $data)

                                <div class="form-group">
                                    <label for="{{ $name }}">{{ $data['label'] }}</label>
                                    <p>{{ $item::renderEnum($data['collection'], $item->$name) }}</p>
                                </div>

                            @endforeach
                            
                            </div>


                            <div class="col-md-6">
                            @foreach($fields as $name => $data)
                                
                                @switch($data['type'])

                                @case('text')
                                @case('email')
                                @case('date')
                                @case('time')
                                @case('datetime')
                                @case('textarea')
                                @case('number')
                                @case('decimal')

                                <div class="form-group">
                                    <label for="{{ $name }}">{{ $data['label'] }}</label>
                                    <p>{{ $item->$name }}</p>
                                </div>


                                    @break

                                @case('boolean')
                                    <div class="form-group">
                                        <p><b>{{ $data['label'] }}</b></p>
                                        <input name="{{ $name}}" type="checkbox" class="form-control bootstrap-toggle" data-toggle="toggle" data-on="Enabled" data-off="Disabled" data-width="100" data-height="35" @if($item->$name) checked @endif disabled> 
                                    </div>

                                    @break


                                @case('image')
                                    <div class="form-group">
                                        <label for="{{ $name }}">{{ $data['label'] }}</label>
                                    </div>                                            
                                    <div class="form-group">
                                        <img src="{{ PRXAdmin::storage()->getCachedImage('square-medium',basename($item->$name)) }}" alt="No Image">
                                    </div>

                                    @break

                                @case('publicImage')

                                    <div class="form-group">
                                        <label for="{{ $name }}">{{ $data['label'] }}</label>
                                    </div>                                            
                                    <div class="form-group">
                                        <img src="/storage/{{ $item->$name }}" alt="No Image" width="130">
                                    </div>

                                    @break

                                @case('locationPicker')

                                    <div class="form-group locationPicker">

                                        <label for="{{ $name . '-location-picker-search' }}">{{ $data['label'] }}</label>
                                        
                                        <input 
                                            name="{{ $name . '_address' }}"
                                            type="text"
                                            class="form-control locationPicker__search"
                                            id="{{ $name . '-location-picker-search' }}"
                                            placeholder="Search address..." 
                                            readonly="readonly" 
                                            value="{{ $item->{$name . '-address'} }}">

                                        <div class="locationPicker__map"></div>

                                        <input 
                                            name="{{ $name . '_latitude' }}"
                                            class="locationPicker__lat"
                                            type="hidden"
                                            value="{{ $item->{$name . '_latitude'} }}"
                                            placeholder="Longitude" 
                                            readonly>

                                        <input 
                                            name="{{ $name . '_longitude' }}"
                                            class="locationPicker__long"
                                            type="hidden"
                                            value="{{ $item->{$name . '_longitude'} }}"
                                            placeholder="Longitude" 
                                            readonly>


                                    </div>
                                    @break

                                @endswitch

                            @endforeach
                            </div>
                            </div>

                        </div>
                        <!-- /.tab-pane -->
                        @foreach($has_many as $name => $data)
                        <div class="tab-pane" id="tab_{{ $name }}">
                            <div class="row margin-bottom">
                                @if(PRXAdmin::auth()->can("$name.create"))
                                <div class="col-md-12">
                                    <a href="{{ route("admin.$name.create") . "?{$item::getPrimaryKey()}={$item->id}&{$item::getPrimaryKey()}_value={$item->getName()}" }}" class="btn btn-primary">
                                        <i class="fa fa-plus"></i> Add {{ $data['label'] }}
                                    </a>
                                </div>
                                @endif
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->      
                            <div class="row">
                                <div class="col-md-12">
                                    @component('PRXAdmin::components.datatable', ['name' => $name])
                                    @endcomponent
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>

    </section>

    <!-- /.content -->

    @stop
