@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Administrators
        <small>Edit User {{ $admin->renderFullname() }}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user-circle"></i> Administrators</a></li>
        <li class="active">Edit</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Edit Administrator</h3>
                </div>
                <div class="box-body">

                    <!-- form start -->
                    <form role="form" method="post" action="{{ route('admin-users.update', $admin->id) }}" 
                        enctype="multipart/form-data">

                        {{ method_field('PUT') }}

                        @include('PRXAdmin::resources.admin-users.details')
                        
                    </form>

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop