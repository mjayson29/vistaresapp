{{ csrf_field() }}
@include('PRXAdmin::includes.form-errors')

<div class="box-body">
    <div class="form-group">
        <label for="firstName">First Name</label>
        <input value="{{ isset($admin) ? $admin->first_name : old('first_name') }}" name="first_name" type="name" class="form-control" id="FirstName" placeholder="First Name">
    </div>
    <div class="form-group">
        <label for="LastName">Last Name</label>
        <input value="{{ isset($admin) ? $admin->last_name : old('last_name') }}" name="last_name" type="name" class="form-control" id="LastName" placeholder="Last Name">
    </div>
    <div class="form-group">
        <label for="Email">Email</label>
        <input value="{{ isset($admin) ? $admin->email : old('email') }}" name="email" type="email" class="form-control" id="Email" placeholder="Email">
    </div>

    @isset ($admin)
    <div>
        <img src="{{ $admin->avatar() }}" class="thumbnail">
    </div>
    @endisset

    <div class="form-group">
        <label for="Avatar">Avatar</label>
        <input type="file" id="Avatar" name="file">
    </div>
</div>
<!-- /.box-body -->

<div class="box-footer">
    <button type="submit" class="btn btn-primary">Submit</button>
</div>