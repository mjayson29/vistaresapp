    @extends('PRXAdmin::master')
    @section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Roles
            <small>Create</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-address-card"></i> Roles</a></li>
            <li class="active">Create</li>
        </ol>

    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Create Role</h3>
                    </div>
                    <div class="box-body">
                        
                        @include('PRXAdmin::includes.form-errors')

                        <!-- form start -->
                        <form role="form" method="post" action="{{ route('roles.store') }}" 
                            enctype="multipart/form-data">

                            {{ csrf_field() }}

                            <div class="box-body">
                                <div class="form-group">
                                    <label for="RoleName">Role Name</label>
                                    <input name="name" type="name" class="form-control" id="RoleName" placeholder="Role Name" required>
                                </div>
                                <div class="form-group">
                                    <label for="Description">Description</label>
                                    <input name="description" type="type" class="form-control" id="Description" placeholder="Description" required>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>

                    </div>
                    <!-- /.box-body -->
                 </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->    
    </section>
    <!-- /.content -->
    @stop