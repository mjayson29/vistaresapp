    @extends('PRXAdmin::master')
    @section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Roles
            <small>&raquo; {{ $role->name }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('roles.index') }}"><i class="fa fa-address-card"></i> Roles</a></li>
            <li class="active">{{ $role->name }}</li>
        </ol>

    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <!-- Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#admins_tab" data-toggle="tab">
                                <h5>
                                    <b>Admins</b>
                                </h4>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">

                        <!-- Tab Pane -->
                        <div class="tab-pane active edit-icon-wrapper" id="admins_tab">
                            <div class="box-body">

                                <form method="post" action="{{ route('admin.roles.addUser') }}" style="margin-bottom: 25px;">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="role_name" value="{{ $role->name }}">
                                    <select class="select2" name="id" required>
                                        @foreach($all_admins as $admin)
                                        <option value="{{ $admin->id }}">{{ $admin->renderFullname() }}</option>
                                        @endforeach
                                    </select>
                                    <button class="btn-primary btn"><i class="fa fa-plus"></i> Add User</button>
                                </form>



                                <table class="paging dataTables table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($admins as $admin)
                                        <tr>
                                            <td>{{ $admin->id }}</td>
                                            <td>{{ $admin->renderFullname() }}</td>
                                            <td>{{ $admin->email }}</td>
                                            <td>
                                                <center>
                                                    <a href="{{ route('admin-users.edit', $admin->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>
                                                    <a href="#" class="btn btn-xs btn-danger actionBtn"
                                                        data-action="{{ route('admin.roles.removeUser', [$role->id, $admin->id]) }}"
                                                        >Remove User</a>
                                                </center>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->

                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>
    </section>
    <!-- /.content -->
    @stop