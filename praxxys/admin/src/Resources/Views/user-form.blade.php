<!DOCTYPE html>
<html>
	<head>

		@include('PRXAdmin::includes.head')
		@include('PRXAdmin::includes.css')

	</head>

	<body class="{{ config('praxxys-admin.theme') }} hold-transition login-page">

		@yield('content')

	</body>
</html>