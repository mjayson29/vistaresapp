@extends('PRXAdmin::user-form')
@section('content')

<div class="login-box">
    <div class="login-logo">
        <b>{{ config('praxxys-admin.header.main') }}</b>{{ config('praxxys-admin.header.sub') }}
    </div>

    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <form action="{{ route('admin.processLogin') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="form-control" placeholder="Email" value="{{ old('email') }}" required name="email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif

            </div>
            <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" placeholder="Password" required name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif

            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <div class="row" style="padding-top:20px;">
            <div class="col-xs-12">
                <a href="{{ route('admin.password.request') }}">I forgot my password</a><br>
          </div>
        </div>
    </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

@stop
