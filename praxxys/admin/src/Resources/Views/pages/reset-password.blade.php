@extends('PRXAdmin::user-form')
@section('content')

<div class="login-box">
    <div class="login-logo">
        <b>{{ config('praxxys-admin.titleStyled.main') }}</b>{{ config('praxxys-admin.titleStyled.sub') }}
    </div>

    <!-- /.login-logo -->
    <div class="login-box-body">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

        <p class="login-box-msg">Register a new password</p>
        <form action="{{ route('admin.password.request') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">

                <input type="hidden" name="token" value="{{ $token }}">

                <input type="email" class="form-control" placeholder="Email" value="{{ old('email') }}" required name="email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif

            </div>
            <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" placeholder="Password" required name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif

            </div>
            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input type="password" class="form-control" name="password_confirmation" required placeholder="Repeat Password">
                <span class="glyphicon glyphicon-rotation-lock form-control-feedback"></span>

                @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
                @endif
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Reset</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
    </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

@stop
