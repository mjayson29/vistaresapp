<?php

namespace PRAXXYS\Admin;

use PRAXXYS\Admin\Helpers\SecurityHelper;
use PRAXXYS\Admin\Helpers\StorageHelper;
use PRAXXYS\Admin\Helpers\ViewHelper;
use PRAXXYS\Admin\Helpers\AuthHelper;
use PRAXXYS\Admin\Helpers\RouterHelper;
use PRAXXYS\Admin\Helpers\ImageHelper;

class AdminManager
{
    /**
     * Config
     *
     * @var array
     */
    protected $config;

    /**
     * SecurityHelper instance
     *
     * @var \PRAXXYS\Admin\Helpers\SecurityHelper
     */
    protected $securityHelper;

    /**
     * StorageHelper instance
     *
     * @var \PRAXXYS\Admin\Helpers\StorageHelper
     */
    protected $storageHelper;

    /**
     * ViewHelper instance
     *
     * @var \PRAXXYS\Admin\Helpers\ViewHelper
     */
    protected $viewHelper;

    /**
     * AuthHelper instance
     *
     * @var \PRAXXYS\Admin\Helpers\AuthHelper
     */
    protected $authHelper;

    /**
     * RouterHelper instance
     *
     * @var \PRAXXYS\Admin\Helpers\RouterHelper
     */
    protected $routerHelper;

    /**
     * ImageHelper instance
     *
     * @var \PRAXXYS\Admin\Helpers\ImageHelper
     */
    protected $imageHelper;

    /**
     * Create a new admin manager instance.
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * Create and return view helper
     *
     * @return void
     */
    public function view() {
        if(! $this->viewHelper) {
            $this->viewHelper = new ViewHelper($this->config);
        }
        return $this->viewHelper;
    }

    /**
     * Create and return auth helper
     *
     * @return void
     */
    public function auth() {
        if(! $this->authHelper) {
            $this->authHelper = new AuthHelper($this->config);
        }
        return $this->authHelper;
    }

    /**
     * Create and return auth helper
     *
     * @return void
     */
    public function router() {
        if(! $this->routerHelper) {
            $this->routerHelper = new RouterHelper($this->config);
        }
        return $this->routerHelper;
    }

    /**
     * Create and return security helper
     *
     * @return void
     */
    public function security() {
        if(! $this->securityHelper) {
            $this->securityHelper = new SecurityHelper($this->config);
        }
        return $this->securityHelper;
    }

    /**
     * Create and return security helper
     *
     * @return void
     */
    public function storage() {
        if(! $this->storageHelper) {
            $this->storageHelper = new StorageHelper($this->config);
        }
        return $this->storageHelper;
    }

    /**
     * Create and return image helper
     *
     * @return void
     */
    public function image() {
        if(! $this->imageHelper) {
            $this->imageHelper = new ImageHelper(config('image'));
        }
        return $this->imageHelper;
    }

}
