<?php

namespace PRAXXYS\Admin;

use PRAXXYS\Admin\AdminManager;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Blade;
use PRAXXYS\Admin\Facades\PRXAdmin;

class AdminServiceProvider extends ServiceProvider
{

    protected $commands = [
        'PRAXXYS\Admin\Commands\Install',
        'PRAXXYS\Admin\Commands\Seed',
        'PRAXXYS\Admin\Commands\CreateSecurityResource',
        'PRAXXYS\Admin\Commands\SeedSecurityResources',
        'PRAXXYS\Admin\Commands\CreatePermission',
        'PRAXXYS\Admin\Commands\MakeResource'
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        # register alias
        $this->app->singleton('praxxys-admin', function ($app) {
            return new AdminManager($app['config']->get('praxxys-admin'));
        });

        if(config('praxxys-admin.security.default-routes')) {
            # include routes
            $this->loadRoutesFrom(__DIR__.'/Http/Routes/web.php');
        }
        # include admin
        $this->loadViewsFrom(__DIR__.'/Resources/Views', 'PRXAdmin');
        # include migrations
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        
        # publish public assets
        $this->publishes([
            __DIR__.'/Resources/Assets' => public_path('vendor/praxxys/admin'),
        ], 'praxxys-admin.assets');
        # publish migrations
        $this->publishes([
            __DIR__ . '/Database/Migrations/' => database_path('migrations/'),
        ], 'praxxys-admin.migrations');

        # publish view file
        $this->publishes([
            __DIR__.'/Resources/Views' => resource_path('/views/vendor/praxxys/admin'),
        ], 'praxxys-admin.views');

        # publish master file
        $this->publishes([
            __DIR__.'/Resources/Views/master.blade.php' => resource_path('/views/vendor/praxxys/admin/master.blade.php'),
        ], 'praxxys-admin.views.master');

        # publish config
        $this->publishes([
            __DIR__.'/config/' => config_path(),
        ], 'praxxys-admin.config');
    
        # register middleware
        $this->app->router->aliasMiddleware(
            'prxadmin-public', 
            \PRAXXYS\Admin\Http\Middleware\PublicMiddleware::class
        );
        # register middleware
        $this->app->router->aliasMiddleware(
            'prxadmin', 
            \PRAXXYS\Admin\Http\Middleware\AdminMiddleware::class
        );
        $this->app->router->aliasMiddleware(
            'role', 
            \Spatie\Permission\Middlewares\RoleMiddleware::class
        );
        $this->app->router->aliasMiddleware(
            'permission', 
            \Spatie\Permission\Middlewares\PermissionMiddleware::class
        );

        # register config
        if(config('praxxys-admin.laravel')) {
            foreach(config('praxxys-admin.laravel') as $mainKey => $mainSection) {
                config([$mainKey => array_merge_recursive($mainSection, config($mainKey))]);
            }
            # Register additional config
            config(['imagecache.paths' => 
                array_merge(
                    config('imagecache.paths'),
                    # Expand array paths
                    array_map(function($value) {
                        return PRXAdmin::storage()->full_image_path() . '/' . $value;
                    }, config('praxxys-admin.cache.paths'))
                    )
                ]);
            config(['imagecache.route' => config('praxxys-admin.cache.images.route')]);

        }

        # Check security: file permissions
        if(App::environment('production') && config('praxxys-admin.security.file-permissions')) {
            PRXAdmin::security()->checkFilePermissions();
        }



        Blade::if('admincan', function ($permission) {
            return PRXAdmin::auth()->can($permission);
        });

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        # Register commands
        $this->commands($this->commands);

        $this->app->register(
            \Spatie\Permission\PermissionServiceProvider::class
        );

        $this->app->register(
            \Rutorika\Sortable\SortableServiceProvider::class
        );

        $this->app->register(
            \Intervention\Image\ImageServiceProvider::class
        );

        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('Image', Intervention\Image\Facades\Image::class);

    }
}
