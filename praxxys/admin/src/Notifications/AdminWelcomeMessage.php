<?php

namespace PRAXXYS\Admin\Notifications;

use PRAXXYS\Admin\Models\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AdminWelcomeMessage extends Notification
{
    use Queueable;

    /**
     * Admin
     *
     * @var \PRAXXYS\Admin\Models\Admin
     */
    protected $admin;

    /**
     * Admin token
     *
     * @var string
     */

    protected $token;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Admin $admin, $token)
    {
        $this->admin = $admin;
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject(config('app.name') . ": An Admin account has been created for you.")
                    ->greeting("Hello {$this->admin->first_name} {$this->admin->last_name},")
                    ->line('You have been invited to be an Admin at ' . config('app.name') . '.')
                    ->line('Kindly click on the link below to activate your account.')
                    ->action('Activate My Account', route('admin.password.reset', $this->token))
                    ->line('Thank you!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
