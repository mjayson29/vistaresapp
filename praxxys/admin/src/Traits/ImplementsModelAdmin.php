<?php

namespace PRAXXYS\Admin\Traits;

use PRAXXYS\Admin\Facades\PRXAdmin;

trait ImplementsModelAdmin
{
    /**
     * Get name of model
     *
     * @return string
     */
    public function getName() {
        $name = $this->name;

        if ($this->name) {
            $name = basename(str_replace('\\', '/', get_class($this)));
        }

        return $name;
    }

    /**
     * Get base resource route by convention
     *
     * @return string
     */
    public static function getBaseRoute() {
        $path = explode('\\', __CLASS__);
        return 'admin.' . strtolower(str_plural(snake_case(array_pop($path))));
    }


    /**
     * Get name of primary key
     *
     * @return string
     */
    public static function getPrimaryKey() {
        $path = explode('\\', __CLASS__);
        return strtolower(snake_case(array_pop($path))) . '_id';
    }

    /**
     * Get enum fields
     *
     * @return string
     */
    public static function getEnumFields() {
        return method_exists(__CLASS__, 'enum_fields') ? self::enum_fields() : [];
    }

    /**
     * Get has_one relation
     *
     * @return string
     */
    public static function getHasOne() {
        return method_exists(__CLASS__, 'has_one') ? self::has_one() : [];
    }


    /**
     * Get belongs_to relation
     *
     * @return string
     */
    public static function getBelongsTo() {
        return method_exists(__CLASS__, 'belongs_to') ? self::belongs_to() : [];
    }

    /**
     * Get has_many relation
     *
     * @return string
     */
    public static function getHasMany() {
        return method_exists(__CLASS__, 'has_many') ? self::has_many() : [];
    }

    /**
     * Get has one base route
     *
     * @return string
     */
    public static function getBelongsToBaseRoute($field) {
        if(method_exists(__CLASS__, 'belongs_to')) {
            $path = explode('\\', get_class(self::belongs_to()[$field]['collection']->first()));
            return 'admin.' . strtolower(str_plural(snake_case(array_pop($path))));
        }

        return false;

    }


    /**
     * Get belongs_to relation by field
     *
     * @param string $field 
     * @param Collection $model 
     * 
     * @return Collection
     */
    public static function getBelongsToByField($field, $model) {

        return method_exists(__CLASS__, 'belongs_to') ? self::belongs_to()[$field]['collection']->find($model->$field)->getName() : [];
    }

    /**
     * Get has_many relation by field
     *
     * @param string $name
     * @param Collection $model 
     * 
     * @return Collection
     */
    public static function getHasManyByName($name, $model) {

        return method_exists(__CLASS__, $name) ? $model->$name() : [];

    }

    /**
     * filter enum array
     *
     * @param string $name
     * @param Collection $model 
     * 
     * @return array
     */
    public static function filterEnum($list, $id) {

        $array = 
            array_filter($list, function($var) use ($id) {
                return $var['id'] === $id;
            });

        return reset($array);

    }

    /**
     * Render enum value
     *
     * @param string $name
     * @param Collection $model 
     * 
     * @return string
     */
    public static function renderEnum($list, $id) {

        return self::filterEnum($list, $id)['name'];

    }

    /**
     * Render HTML enum label
     *
     * @param string $name
     * @param Collection $model 
     * 
     * @return string
     */
    public static function renderEnumLabel($list, $id) {

        $element = self::filterEnum($list, $id);
        $class = array_key_exists('class', $element) ? $element['class'] : 'bg-yellow';

        return '<small class="label '  . $class . '">' . $element['name'] . ' </small>';

    }
}