<?php

namespace PRAXXYS\Admin\Auth;

use PRAXXYS\Admin\Facades\PRXAdmin;
use Illuminate\Support\Facades\Route;

class RouteGuard
{

    /**
     * Request variable
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Action array
     *
     * @var array
     */
    protected $action;

    /**
     * List of resource methods
     *
     * @var array
     */
    protected $resourceMethods = ['index', 'create', 'store', 'show', 'edit', 'update', 'destroy'];

    /**
     * Create a new route guard instance
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Authorize route via user permissions
     *
     * @return mixed
     */
    public function authorizeRequest($request) {

        $this->action = PRXAdmin::router()->getActionFromRequest($request);

        if(!$this->authorize()) {
            abort(403, 'You are not authorized to perform this action.');
        }

        return true;
    }

    /**
     * Authorize route via route name
     *
     * @return bool
     */
    public function authorizeRoute($name) {

        $this->action = PRXAdmin::router()->getActionFromRouteName($name);

        return $this->authorize();
    }
    
    /**
     * Authorize route via user permissions
     *
     * @return bool
     */
    protected function authorize() {

        if($this->action && $this->checkAction()) {
            return true;
        }

        return false;
    }

    /**
     * Check currrent action
     *
     * Executes checks in sequential order and short circuits when a match is found.
     *
     * @return bool
     */
    protected function checkAction() {
        return $this->checkSuperAdmin() || $this->checkUnguard() || $this->checkResource() || $this->checkRoute();
    }

    /**
     * Check resource
     *
     * @return bool
     */
    protected function checkUnguard() {
        if(isset($this->action['controller']::$unguard)) {

            $unguard = $this->action['controller']::$unguard;
            $method = $this->action['method'];

            if($unguard === true) {
                return $unguard;
            }

            if(is_array($unguard)) {
                foreach ($unguard as $unguarded_method) {
                    if($unguarded_method === $method) {
                        return true;
                    }
                }
            }

        }

        return false;
    }

    /**
     * Check super admin
     *
     * @return bool
     */
    protected function checkSuperAdmin() {
        return PRXAdmin::auth()->user()->hasRole('super-admin');
    }

    /**
     * Check resource
     *
     * @return bool
     */
    protected function checkResource() {
        
        # Check resource
        if(isset($this->action['controller']::$guardResource)) {
            
            $resource = $this->action['controller']::$guardResource;
            
            # Check method resource
            if(in_array(
                $this->action['method'],
                $this->resourceMethods)) {

                    $method = $this->action['method'];

                    # flatten store/update to create/edit since these are same
                    switch($this->action['method']) {
                        case 'store':
                            $method = 'create';
                            break;
                        case 'update':
                            $method = 'edit';
                            break;
                    }

                    $permission = $resource . '.' . $method;

                    return PRXAdmin::auth()->can($permission);

                
            }
        }

        return false;

    }

    /**
     * Check route
     *
     * @return bool
     */
    protected function checkRoute() {

        # Iterate guard array
        $name = false;
        $method = $this->action['method'];
        
        if(isset($this->action['controller']::$guardResource)) {

            $resource = $this->action['controller']::$guardResource;

            # Check resource
            $permission = $resource . '.' . $method;
            return PRXAdmin::auth()->can($permission);
        }


        return false;
    }

}
