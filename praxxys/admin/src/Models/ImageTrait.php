<?php

namespace PRAXXYS\Admin\Models;

use PRAXXYS\Admin\Facades\PRXAdmin;

class ImageTrait
{
    public function renderPath() {
        return PRXAdmin::storage()->path . '/' . $this->path;
    }

    public function renderSmallThumb() {
    	return '/images/cache/product-small/' . basename($this->path);
    }


}
