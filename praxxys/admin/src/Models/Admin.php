<?php

namespace PRAXXYS\Admin\Models;

use PRAXXYS\Admin\Traits\ImplementsModelAdmin;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

use Laravolt\Avatar\Facade as Avatar;

use Illuminate\Foundation\Auth\User as Authenticatable;


class Admin extends Authenticatable
{
    use ImplementsModelAdmin, Notifiable, SoftDeletes, HasRoles;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Set guard name for model
     *
     * @var array
     */
    protected $guard_name = 'praxxys-admin';
    
    /**
     * Remove mass assignment protection
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * Return user full name
     *
     * @return string
     */
    public function renderFullname() {
        return $this->first_name . ' ' . $this->last_name;
    }
    
    /**
     * Generate user avatar
     *
     * @return string
     */
    public function avatar() {

        /* Get background color from Config */
        $background = '#f39c12';
        $config = config('praxxys-admin.avatars.background') ;
        if ($config) { $background = $config; }

        /* Get text color from Config */
        $color = '#fff';
        $config = config('praxxys-admin.avatars.color') ;
        if ($config) { $color = $config; }

        $border = '0';
        $config = config('praxxys-admin.avatars.border') ;
        if ($config || $config !== 0) { $border = $config; }

        return $this->admin_avatar
                    ? $this->admin_avatar->medium_thumb()
                    : Avatar::create($this->renderFullname())->setBackground($background)->setForeground($color)->setBorder($border, $color);
    }

    /**
     * Get Avatar Model
     *
     * @return string
     */
    public function admin_avatar() {
        return $this->hasOne(AdminAvatar::class);
    }

    /**
     * Get all user roles as csv
     *
     * @return string
     */
    public function renderRoles() {
        return implode(', ', $this->getRoleNames()->all());
    }

    public function getName() {
        return $this->renderFullname();
    }

    public function renderName() {
        return $this->renderFullname();
    }

    public function renderView() {
        return route('admin-users.edit', $this->id);
    }
}
