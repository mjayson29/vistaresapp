<?php

namespace PRAXXYS\Admin\Models;

use PRAXXYS\Admin\Models\Admin;
use PRAXXYS\Admin\Facades\PRXAdmin;

use Illuminate\Database\Eloquent\Model;

class AdminAvatar extends Model
{

    /**
     * Remove mass assignment protection
     *
     * @var array
     */
	protected $guarded = ['id'];

    /**
     * Eloquent belongs to
     *
     * @return \PRAXXYS\Admin\Models\Admin
     */
    public function admin() {

        $model = config('praxxys-admin.security.admin-model');

    	return $this->belongsTo($model);
    }

    /**
     * Get avatar path
     *
     * @return string
     */
    public function path() {
    	return PRXAdmin::storage()->getImage($this->path);
    }

    /**
     * Get small thumbnail
     *
     * @return string
     */
    public function small_thumb() {
    	return PRXAdmin::storage()->getCachedImage('square-tiny', basename($this->path));
    }

    /**
     * Get medium thumbnail
     *
     * @return string
     */
    public function medium_thumb() {
        return PRXAdmin::storage()->getCachedImage('square-medium', basename($this->path));
    }

    /**
     * Get large thumbnail
     *
     * @return string
     */
    public function large_thumb() {
        return PRXAdmin::storage()->getCachedImage('square-large', basename($this->path));
    }
}
