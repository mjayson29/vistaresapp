<?php

namespace PRAXXYS\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminLog extends Model
{
    protected $guarded = [];

    public static function renderDataTable() {
    	return DB::table('admin_logs')
					->join('admins', 'admin_logs.admin_id', '=', 'admins.id')
    				->select(
    					'admin_logs.id',
    					DB::raw('CONCAT(admins.first_name, " ", admins.last_name, admin_logs.message) as message'),
    					'admin_logs.created_at')
    				->get();
    }
}
