<?php

namespace PRAXXYS\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminTableSeeder extends Seeder
{
    public function run()
	{

        $model = config('praxxys-admin.security.admin-model');

        $users = [
            [
                'id' => '1', 
                'first_name' => 'Default', 
                'last_name' => 'Admin', 
                'email' => 'admin@praxxys.ph', 
                'password' => 'password'
            ],            
        ];

        foreach ($users as $user) {

            $vars = [
                'id' => $user['id'],

                'first_name' => $user['first_name'],
                'last_name' => $user['last_name'],

                'email' => $user['email'],
                'password' => Hash::make($user['password']),

                'notifiable' => 1,
                'is_verified' => 1,
                'verify_token' => bin2hex(random_bytes(64)),

                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            $admin = $model::create($vars);
            $admin->assignRole('super-admin');

            
        }              

    }
}