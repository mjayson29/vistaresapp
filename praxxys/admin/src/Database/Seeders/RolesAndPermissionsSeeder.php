<?php

namespace PRAXXYS\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use PRAXXYS\Admin\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    public function run()
	{
    	// Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create super admin role
        $role = Role::create(['name' => 'super-admin', 'description' => 'Omnipotent. Omniscient', 'system' => true]);

        // create user role
        $role = Role::create(['name' => 'user', 'description' => 'Regular user', 'system' => true]);

    }
}