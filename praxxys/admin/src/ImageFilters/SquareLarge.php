<?php

namespace PRAXXYS\Admin\ImageFilters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class SquareLarge implements FilterInterface
{
    public function applyFilter(Image $image)
    {

        $x = 250;
        $y = 250;

        return $image->fit($x, $y)->orientate();

    }
}
