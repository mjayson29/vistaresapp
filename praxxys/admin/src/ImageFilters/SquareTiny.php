<?php

namespace PRAXXYS\Admin\ImageFilters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class SquareTiny implements FilterInterface
{
    public function applyFilter(Image $image)
    {

        $x = 50;
        $y = 50;

        return $image->fit($x, $y)->orientate();

    }
}
