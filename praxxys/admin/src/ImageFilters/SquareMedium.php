<?php

namespace PRAXXYS\Admin\ImageFilters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class SquareMedium implements FilterInterface
{
    public function applyFilter(Image $image)
    {

        $x = 100;
        $y = 100;

        return $image->fit($x, $y)->orientate();

    }
}
