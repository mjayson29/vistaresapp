<?php

namespace PRAXXYS\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use PRAXXYS\Admin\Facades\PRXAdmin;

use PRAXXYS\Admin\Models\AdminLog;


class AdminLogController extends Controller
{
    public function index() {

		 $logs = PRXAdmin::view()->toDataTable(AdminLog::renderDataTable(), [
                'message' => 'Message',
                'created_at' => 'Date',
		    ]);

		PRXAdmin::view()->pushJSVar([
		    'datatables' => [
		        'logs' => $logs
		    ]
		]);

    	return view('PRXAdmin::resources.admin-logs.index');
    }

    public static function log($message, $id = null) {

        # Short circuit if logging is disabled
        if(!config('praxxys-admin.enable_log')) {
            return;
        }

    	AdminLog::create([
    		'admin_id' => $id ?: PRXAdmin::auth()->id(),
    		'message' => $message,
    		'action' => Route::currentRouteAction()
    	]);
    }
}
