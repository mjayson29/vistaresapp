<?php

namespace PRAXXYS\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request as RequestFacade;
use PRAXXYS\Admin\Facades\PRXAdmin;
use PRAXXYS\Admin\Http\Requests\ResourceFormRequest;
use Exception;

abstract class ResourceController extends Controller
{
    /**
     * Get Resource Name
     *
     * @return void
     */
    public function getName() {
        return static::$guardResource;
    }
    /**
     * Get Resource Title
     *
     * @return void
     */
    protected function getTitle() {        
        return ucwords(str_replace('_', ' ', $this->getName()));
    }

    /**
     * Get Resource Model
     *
     * @return void
     */
    public function getModel() {
        return $this->model;
    }

    /**
     * Get Summary Fields
     *
     * @return void
     */
    public function getSummaryFields() {
        return $this->model::$summary_fields;
    }

    /**
     * Check model or fail
     *
     * @return void
     */
    public function checkModelOrFail() {
        if(!isset($this->model)) {
            throw new Exception('Missing $model for ' . get_class($this));
        }
    }

    /**
     * Check model or fail
     *
     * @return void
     */
    public function checkGuardResourceOrFail() {
        if(!isset(static::$guardResource)) {
            throw new Exception('Missing $guardResource for ' . get_class($this));
        }
    }

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {

        $this->checkModelOrFail();
        $this->checkGuardResourceOrFail();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $items = $this->getModel()::all();

        PRXAdmin::view()->pushJSVar(
            [
                'datatables' => [
                    $this->getName() => PRXAdmin::view()->toDataTable($items, $this->getSummaryFields())
                ]
            ]);

        return view("PRXAdmin::resources.default.index", $this->viewVars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("PRXAdmin::resources.default.create",
            $this->viewVars() + [
                'fields' => $this->model::$editable_fields,
                'belongs_to' => $this->model::getBelongsTo(),
                'enum_fields' => $this->model::getEnumFields(),
                'input' => RequestFacade::input()
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \PRAXXYS\Admin\Http\Requests\ResourceFormRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ResourceFormRequest $request) {  

        $obj = $this->writeModel($request, new $this->model);

        PRXAdmin::view()->flash("You have successfully created a new {$this->getTitle()}");

        #return redirect()->route("admin.{$this->getName()}.index");

        PRXAdmin::auth()->log(" created '{$obj->getName()}' in {$this->getTitle()} .");

        return PRXAdmin::router()->back(2);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->model::findOrFail($id);

        $array = [];
        foreach($this->model::getHasMany() as $name => $data) {
            $class = $data['class'];
            $collection = $class::where($this->model::getPrimaryKey(), $id)->get();
            $summary_fields = $class::$summary_fields;

            $array[$name] = PRXAdmin::view()->toDataTable($collection, $summary_fields);
        }

        PRXAdmin::view()->pushJSVar(
            [
                'datatables' => $array
            ]);

        $model = $this->model;

        return view("PRXAdmin::resources.default.show",
            $this->viewVars() + [
                'fields' => $model::$editable_fields,
                'item' => $item,
                'has_one' => $model::getHasOne(),
                'belongs_to' => $model::getBelongsTo(),
                'has_many' => $model::getHasMany(),
                'enum_fields' => $model::getEnumFields(),
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $item = $this->model::findOrFail($id);

        return view("PRXAdmin::resources.default.edit",
            $this->viewVars() + [
                'fields' => $this->model::$editable_fields,
                'item' => $item,
                'belongs_to' => $this->model::getBelongsTo(),
                'enum_fields' => $this->model::getEnumFields(),

            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \PRAXXYS\Admin\Http\Requests\ResourceFormRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */    
    public function update(ResourceFormRequest $request, $id) {

        $obj = $this->writeModel($request, $this->model::findOrFail($id));

        $slug = $obj->getName();

        PRXAdmin::view()->flash("You have successfully edited $slug");

        PRXAdmin::auth()->log(" updated '{$obj->getName()}' in {$this->getTitle()} .");

        #return redirect()->route("admin.{$this->getName()}.index");
        return PRXAdmin::router()->back(2);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        
        $obj = $this->model::find($id);
        $name = $obj->name;

        $obj->delete();

        $slug = $obj->getName();


        PRXAdmin::view()->flash("You have successfully deleted $slug");

        PRXAdmin::auth()->log(" deleted '{$obj->getName()}' in {$this->getTitle()} .");

        return redirect()->route("admin.{$this->getName()}.index");
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id) {
        
        $obj = $this->model::withTrashed()->find($id);
        $name = $obj->name;

        $obj->restore();

        $slug = $obj->getName();


        PRXAdmin::view()->flash("You have successfully deleted $slug");

        PRXAdmin::auth()->log(" deleted '{$obj->getName()}' in {$this->getTitle()} .");

        return redirect()->route("admin.{$this->getName()}.index");
    }

    /**
     * Compose view variables based on route name
     *
     * @return void
     */
    protected function viewVars() {

        $route = explode('.', Route::currentRouteName());

        return [
       
            'name' => $this->getName(),
            'title' => $this->getTitle(),
            'subtitle' => ucfirst($route[2]),
        ];
    }

    /**
     * Compose view variables based on route name
     *
     * @return Model
     */
    protected function writeModel($request, $obj)
    {
        $model = $this->model;

        $fields = $model::$editable_fields;
        $belongs_to = $model::getBelongsTo();
        $enum_fields = $model::getEnumFields();

        $method = PRXAdmin::router()->getActionFromRequest($request)['method'];

        foreach ($belongs_to as $name => $data) {

            switch($data['type']) {
                case 'select':
                    $obj->$name = $request->input($name);
                    break;
            }
        }

        foreach ($enum_fields as $name => $data) {

            if($method === 'store' && array_key_exists('hideOnCreate', $data) && $data['hideOnCreate']) {
               continue;
            }

            $obj->$name = $request->input($name);
        }

        foreach ($fields as $name => $data) {
                
            if($method === 'update' && array_key_exists('readonly', $data) && $data['readonly']) {
               continue;
            }

            switch($data['type']) {
                case 'text':
                case 'email':
                case 'textarea':
                case 'ckeditor':
                case 'date':
                case 'time':
                case 'datetime':
                case 'number':
                case 'decimal':
                    $obj->$name = $request->input($name);
                    break;

                case 'boolean':
                    $obj->$name = $request->input($name) ? true : false;
                    break;

                case 'image':
                    if($request->file($name)) {                    

                        # Delete old image if exists
                        if($obj->$name) {
                            PRXAdmin::storage()->deleteImage($obj->$name);
                        }

                        $path = PRXAdmin::storage()->storeImage($request->file($name), 'uploads');
                        $obj->$name = $path;
                    }

                    break;

                case 'publicImage' :

                    if($request->file($name)){

                        if($obj->$name){
                            \Storage::delete('public/'. $obj->$name);
                        }

                        
                        $var = $request->file($name)->store('public-images', 'public');

                        $obj->$name = $var;


                    }
                    break;
                
                case 'locationPicker':
                    
                    $address = $name . '_address';
                    $latitude = $name . '_latitude';
                    $longitude = $name . '_longitude';

                    $obj->$address = $request->input($address);
                    $obj->$latitude = $request->input($latitude);
                    $obj->$longitude = $request->input($longitude);
                    break;
            }
        }

        $obj->save();

        return $obj;

    }
}
