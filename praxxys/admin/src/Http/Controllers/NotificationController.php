<?php

namespace PRAXXYS\Admin\Http\Controllers;

use Illuminate\Http\Request;
use PRAXXYS\Admin\Facades\PRXAdmin;

use Carbon\Carbon;

class NotificationController extends Controller
{

    public static $unguard = true;

    public function index() {
    	// return view('admin.notifications.index', [
    	// 	'notifications' => \Auth::user()->notifications->sortByDesc('created_at'),
    	// 	'renderer' => new NotificationsControllerHelper
    	// ]);
    }

    public function read() {
		PRXAdmin::auth()->user()->unreadNotifications()->update(['read_at' => Carbon::now()]);
    }

    public function getUnread() {
    	return PRXAdmin::auth()->user()->unreadNotifications;
    }

}

// class NotificationsControllerHelper
// {
// 	public function renderType($type) {
// 		switch($type) {
// 			case 'App\Notifications\AdminBankDeposit':
// 				return 'Order via Bank Deposit';
// 			case 'App\Notifications\AdminUserRegistered':
// 				return 'New User Registration';
// 			case 'App\Notifications\InvoicePaid':
// 				return 'Order via Paypal';
// 			case 'App\Notifications\NotifyUploadedPaymentReference':
// 				return 'Deposit Slip Uploaded';
// 		}
// 	}
// }