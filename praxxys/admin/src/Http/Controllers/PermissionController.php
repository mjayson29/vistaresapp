<?php

namespace PRAXXYS\Admin\Http\Controllers;

use Illuminate\Http\Request;

use PRAXXYS\Admin\Models\Permission;
use PRAXXYS\Admin\Models\Role;

class PermissionController extends Controller
{
    public static $guardResource = 'permissions';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();

        return view('PRXAdmin::resources.permissions.index', [
            'roles' => $roles,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $permissions = Permission::all();
        $arrPermissionNames = [];

        foreach ($permissions as $permission) {
            $name = explode('.', $permission->name)[0];

            if (!in_array($name, $arrPermissionNames)) {
                array_push($arrPermissionNames, $name);
            }            
        }

        $arrPermissionNames = array_unique($arrPermissionNames);

        $arrPermissions = [];


        foreach ($arrPermissionNames as $name) {

            $where = Permission::where('name', 'like', $name . '%')->get();

            array_push($arrPermissions, $where);
        }

        return view('PRXAdmin::resources.permissions.show', [
            'role' => $role,
            'permissionGroups' => $arrPermissions,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $ids = $request->input(['permissions']);
    
        $role->permissions()->sync($ids);

        foreach ($role->users as $admin) {
            $admin->removeRole($role);

            if(!$admin->hasRole($role)) {
                $admin->assignRole($role);
            }
        }

        \PRXAdmin::view()->flash('You have successfully updated the permissions of Role #' . $role->id . ' ' . $role->name);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function fetchPermissions()
    {
        $permissions = Permission::all();
        $arrPermissionNames = [];

        foreach ($permissions as $permission) {
            $name = explode('.', $permission->name)[0];

            if (!in_array($name, $arrPermissionNames)) {
                array_push($arrPermissionNames, $name);
            }            
        }

        $arrPermissionNames = array_unique($arrPermissionNames);

        $arrPermissions = [];


        foreach ($arrPermissionNames as $name) {

            $where = Permission::where('name', 'like', $name . '%')->get();

            array_push($arrPermissions, $where);
        }

        return response()->json([
            'permissions' => $arrPermissions,
        ]);
    }
}
