<?php

namespace PRAXXYS\Admin\Http\Controllers;

use PRAXXYS\Admin\Facades\PRXAdmin;
use PRAXXYS\Admin\Models\Role;
use PRAXXYS\Admin\Models\Permission;
use PRAXXYS\Admin\Models\Admin;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Set guard resource parameter
     *
     * @var string
     */
    public static $guardResource = 'roles';

    /**
     * Admin model class
     *
     * @var string
     */
    protected $adminClass = null;

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        $this->adminClass = PRXAdmin::auth()->adminClass();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
    	return view('PRXAdmin::resources.roles.index',[
    		'roles' => Role::all(),
    	]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {      

        $role = Role::find($id);

        # Get permissions except for already owned permission
        $role_permissions = $role->permissions->pluck('id');
        $permissions = Permission::whereNotIn('id', $role_permissions)->get();

        # Get admins except for logged in user
        $role_admins = $role->users->pluck('id');
        $admins = $this->adminClass::whereNotIn('id', $role_admins)->get();


        return view('PRXAdmin::resources.roles.show',[
            'role' => $role,
            'admins' => $this->adminClass::role($role->name)->get(),
            'all_permissions' => $permissions,
            'all_admins' => $admins
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('PRXAdmin::resources.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validateStore($request);

        # Handle request

        $role = Role::create($request->all());

        # Flash message
        PRXAdmin::view()->flash(
            'You have successfully created a new Role!'
            );

        PRXAdmin::auth()->log(" created new role '{$role->name}'.");

        return redirect()->route('roles.index');
    }

    /**
     * Add user to role
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function addUser(Request $request)
    {

        $admin = $this->adminClass::find($request->id);
        $role = $request->role_name;

        # Execute only when role does not exist
        if(!$admin->hasRole($role)) {
            $admin->assignRole($role);
        }

        PRXAdmin::view()->flash();

        PRXAdmin::auth()->log(" added user '{$admin->renderFullname()}'' to role '{$role}'.");

        return redirect()->back();
    }

    /**
     * Remove role from user
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function removeUser(Request $request, Role $role, Admin $admin)
    {

        $admin->removeRole($role);

        PRXAdmin::view()->flash();

        PRXAdmin::auth()->log(" removed user '{$admin->renderFullname()}' from role '{$role->name}'.");


        return redirect()->back();
    }


    /**
     * Add permission to role
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function addPermission(Request $request)
    {

        $role = Role::find($request->role_id);

        # Execute only when permission does not exist        
        if(!$role->hasPermissionTo($request->permission_name)) {
            $role->givePermissionTo($request->permission_name);
        }

        PRXAdmin::view()->flash();

        PRXAdmin::auth()->log(" added permission '{$request->permission_name}' to role '{$role->name}'.");

        return redirect()->back();
    }

    /**
     * Remove permission
     *
     * @param  \Illuminate\Http\Request $request
     * @param string $name
     * @return \Illuminate\Http\Response
     */
    public function removePermission(Request $request, $id, $name)
    {
        $role = Role::find($id);
        $role->revokePermissionTo($name);

        PRXAdmin::view()->flash();

        PRXAdmin::auth()->log(" removed permission '$name' from role '{$role->name}'.");

        return redirect()->back();
    }

    /**
     * Validate request
     *
     * @return void
     */
    public function validateStore($request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
        ]);
    }


}
