<?php

namespace PRAXXYS\Admin\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use PRAXXYS\Admin\Facades\PRXAdmin;

use PRAXXYS\Admin\Http\Controllers\Controller;

class AdminAuthController extends Controller
{

	use AuthenticatesUsers;

    public function showLogin() {

    	if(PRXAdmin::auth()->check()) {
    		return redirect('admin');
    	}

    	return view('PRXAdmin::pages.login');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout', 'showLogin', 'login']]);
    }
    
    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request) {
          return $request->only($this->username(), 'password');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {

        PRXAdmin::auth()->log(" logged in from {$request->ip()}.", $user->id);

        foreach(config('praxxys-admin.default-redirects') as $role => $route) {
            if($user->hasRole($role)) {
                $this->redirectTo = route($route);
            }
        }
    }

    public function guard() {

        return PRXAdmin::auth()->guard();

    }
}
