<?php

namespace PRAXXYS\Admin\Http\Controllers\Auth;

use PRAXXYS\Admin\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

use PRAXXYS\Admin\Facades\PRXAdmin;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;

use PRAXXYS\Admin\Models\Admin;

class AdminResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $token = null)
    {
        return view('PRXAdmin::pages.reset-password')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function sendResetResponse($response)
    {
        PRXAdmin::view()->flash('You have successfully reset your password!');
        return redirect($this->redirectPath())
                            ->with('status', trans($response));
    }

    public function guard()
    {
        return PRXAdmin::auth()->guard();
    }

    public function broker()
    {
        return Password::broker(PRXAdmin::auth()->guardName());
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        $this->validate($request, $this->rules(), $this->validationErrorMessages());

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );

        $admin = Admin::where('email', $request->email)->first();

        if($response == Password::PASSWORD_RESET) {
            PRXAdmin::auth()->log(" successfully reset password from {$request->ip()}.", $admin->id);
        } else {
            PRXAdmin::auth()->log(" FAILED to reset password from {$request->ip()}.", $admin->id);            
        }

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == Password::PASSWORD_RESET
                    ? $this->sendResetResponse($response)
                    : $this->sendResetFailedResponse($request, $response);
    }

}
