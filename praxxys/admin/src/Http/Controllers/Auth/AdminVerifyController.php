<?php

namespace PRAXXYS\Admin\Http\Controllers\Auth;

use PRAXXYS\Admin\Http\Controllers\Controller;
use PRAXXYS\Admin\Models\Admin;
use PRAXXYS\Admin\Facades\PRXAdmin;

use Illuminate\Http\Request;

class AdminVerifyController extends Controller
{

    /**
     * Set guard resource parameter
     *
     * @var string
     */
    public static $guardResource = 'admin-users';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showVerifyForm(string $code)
    {
        return $this->getAdminByCode($code)
            ? redirect()->route('admin.password.request', $code)
            : $this->returnFail();
    }
    
    /**
     * Process admin verification
     *
     * @return \Illuminate\Http\Response
     */
    public function verifyEmail($code) {
        

        if($admin = $this->getAdminByCode($code)) {
            
            $admin->is_verified = true;
            $admin->save();


            PRXAdmin::view()->flash('You have successfully activated your account!');
            
            return redirect()->route('admin.login');;

        }

        return $this->returnFail();

    }

    /**
     * Check admin by code and return if exists
     *
     * @return mixed
     */
    protected function getAdminByCode($code)  {        
        return Admin::where('verify_token', $code)->where('is_verified', false)->first();
    }

    /**
     * Flash fail
     *
     * @return void
     */
    protected function returnFail()  {        
        PRXAdmin::view()->flash(
            'There was a problem with your request. You might already be verified.',
            'Error',
            'error');

        return redirect()->route('admin.login');

    }


}
