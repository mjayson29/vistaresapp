<?php

namespace PRAXXYS\Admin\Http\Controllers\Auth;

use PRAXXYS\Admin\Facades\PRXAdmin;

use PRAXXYS\Admin\Http\Controllers\Controller;
use PRAXXYS\Admin\Notifications\ResetPasswordMessage;

use PRAXXYS\Admin\Models\Admin;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;

use Illuminate\Http\Request;


class AdminForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('PRXAdmin::pages.send-password-reset');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        $admin = Admin::where('email', $request->email)->first();

        # Create password reset
        $token = Password::broker(PRXAdmin::auth()->guardName())->createToken($admin);

        # Dispatch notification
        $admin->notify(new ResetPasswordMessage($token));

        # Flash message
        PRXAdmin::view()->flash(
            'Instructions to reset your password have been sent to your email.'
            );


        PRXAdmin::auth()->log(" initiated password reset from {$request->ip()}.", $admin->id);

        return redirect()->route('admin.login');

    }

    public function guard()
    {
        return PRXAdmin::auth()->guard();
    }

    public function broker() {
        return Password::broker(PRXAdmin::auth()->guardName());
    }

    /**
     * Validate the email for the given request.
     *
     * @param \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email|exists:admins']);
    }


}
