<?php

namespace PRAXXYS\Admin\Http\Controllers;

use PRAXXYS\Admin\Models\AdminAvatar;
use Illuminate\Http\Request;

class AdminAvatarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $parentID = $request->input('parentID');

        $path = $request->file('file')->store('avatars', PRXAdmin::storage()->path());

        $image = AdminAvatar::create(['path' => $path, 'admin_id' => $parentID]);

        return response()->json(['id' => $image->id, 'path' => $image->renderPath()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \PRAXXYS\Admin\Models\AdminAvatar  $adminAvatar
     * @return \Illuminate\Http\Response
     */
    public function show(AdminAvatar $adminAvatar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \PRAXXYS\Admin\Models\AdminAvatar  $adminAvatar
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminAvatar $adminAvatar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \PRAXXYS\Admin\Models\AdminAvatar  $adminAvatar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminAvatar $adminAvatar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \PRAXXYS\Admin\Models\AdminAvatar  $adminAvatar
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminAvatar $adminAvatar)
    {
        //
    }

    /**
     * Remove the specified image from storage and clear cache.
     *
     * @param  \PRAXXYS\Admin\Models\AdminAvatar  $adminAvatar
     * @return \Illuminate\Http\Response
     */
    protected function deleteImage($path)
    {
        PRXAdmin::storage()->delete($path);
    }


}
