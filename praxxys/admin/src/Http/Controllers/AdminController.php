<?php

namespace PRAXXYS\Admin\Http\Controllers;

use PRAXXYS\Admin\Models\Admin;
use PRAXXYS\Admin\Models\AdminAvatar;
use PRAXXYS\Admin\Facades\PRXAdmin;
use Illuminate\Support\Facades\Password;
use PRAXXYS\Admin\Notifications\AdminWelcomeMessage;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Set guard resource parameter
     *
     * @var string
     */
    public static $guardResource = 'admin-users';

    public static $guard = [
        'admin-users' => [
            'restore',
        ]
    ];

    /**
     * Admin model class
     *
     * @var string
     */
    protected $adminClass = null;

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        $this->adminClass = PRXAdmin::auth()->adminClass();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = $this->adminClass::all();
        $archiveAdmins = $this->adminClass::onlyTrashed()->get();

        return view('PRXAdmin::resources.admin-users.index', [
            'admins' => $this->adminClass::all(),
            'archiveAdmins' => $archiveAdmins,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('PRXAdmin::resources.admin-users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validateStore($request);

        # Handle request
        $data = $request->except('file') + [
            'password' => PRXAdmin::auth()->generateToken()
        ];
        
        # Begin DB Transaction
        DB::beginTransaction();

        $admin = $this->adminClass::create($data);


        # Process avatar if exists
        if($request->hasFile('file')) {
            $path = PRXAdmin::storage()->storeImage($request->file, 'admin-avatar');

            $admin_avatar = AdminAvatar::create([
                'path' => $path,
                'admin_id' => $admin->id
            ]);
        }


        # Flash message
        PRXAdmin::view()->flash(
            'You have successfully created a new Admin! An email message has been sent to the user for verification.'
            );

        # Create password reset
        $token = Password::broker(PRXAdmin::auth()->guardName())->createToken($admin);

        # Dispatch notification
        $admin->notify(new AdminWelcomeMessage($admin, $token));

        # Begin DB Transaction

        PRXAdmin::auth()->log(" created new user '{$admin->renderFullname()}'.");

        DB::commit();

        return redirect()->route('admin-users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = $this->adminClass::withTrashed()->findOrFail($id);

        return view('PRXAdmin::resources.admin-users.edit', [
            'admin' => $admin,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateUpdate($request);

        $admin = $this->adminClass::withTrashed()->findOrFail($id);

        DB::beginTransaction();

        # Process avatar if exists
        if($request->hasFile('file')) {

            # Delete previous
            $this->deleteAvatar($admin);

            $path = PRXAdmin::storage()->storeImage($request->file, 'admin-avatar');

            $admin_avatar = AdminAvatar::create([
                'path' => $path,
                'admin_id' => $admin->id
            ]);
        }

        # Fetch vars
        $vars = $request->except(['file']);

        # Update admin
        $admin->update($vars);


        # Flash message
        PRXAdmin::view()->flash(
            'You have successfully updated ' . $admin->renderFullname() . ' !'
            );

        DB::commit();

        PRXAdmin::auth()->log(" updated user '{$admin->renderFullname()}'.");

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = Admin::find($id);

        PRXAdmin::view()->flash(
            'You have successfully deleted the ' . $admin->renderFullname() . ' !'
            );

        $admin->delete();

        return redirect()->back();
    }

    public function restore($id)
    {
        $admin = Admin::withTrashed()->find($id);

        $admin->restore();

        PRXAdmin::view()->flash(
            'You have successfully resotred the ' . $admin->renderFullname() . ' !'
            );

        return redirect()->back();
    }

    /**
     * Validate request
     *
     * @return void
     */
    public function validateStore($request)
    {
        $this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|unique:admins',
            'image' => 'nullable|mimes:jpeg,bmp,png'
        ]);
    }

    /**
     * Validate request
     *
     * @return void
     */
    public function validateUpdate($request)
    {
        $this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
        ]);
    }

    /**
     * Delete avatar
     *
     * @param \PRAXXYS\Admin\Models\Admin $admin
     * @return void
     */
    public function deleteAvatar($admin) {
        if($admin->admin_avatar) {
            PRXAdmin::storage()->deleteImage($admin->admin_avatar->path);
            $admin->admin_avatar->delete();
        }
    }


}
