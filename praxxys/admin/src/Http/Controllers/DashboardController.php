<?php

namespace PRAXXYS\Admin\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{

	public static $unguard = true;

	/**
     * Default dashboard route.
     *
     * @return void
     */
    public function index() {
    	return view('PRXAdmin::pages.dashboard');
    }
}
