<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::group(
	[
		
		'prefix' => 'admin',
		'middleware' => ['web'],
		'namespace' => '\PRAXXYS\Admin\Http\Controllers'

	], function () {

	/*
	|--------------------------------------------------------------------------
	| Auth Routes
	|--------------------------------------------------------------------------
	*/

	Route::group(['namespace' => 'Auth', 'middleware' => 'prxadmin-public'], function () {

		Route::get('login', 'AdminAuthController@showLogin')->name('admin.login');
		Route::post('login/process', 'AdminAuthController@login')->name('admin.processLogin');

	    Route::get('password/reset', 'AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
	    Route::post('password/email', 'AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
	    Route::get('password/reset/{token}', 'AdminResetPasswordController@showResetForm')->name('admin.password.reset');
	    Route::post('password/reset', 'AdminResetPasswordController@reset');
		Route::get('logout', 'AdminAuthController@logout')->name('admin.logout');

		Route::get('verify-email/{code}', 'AdminVerifyController@showVerifyForm')->name('admin.verify-email');

	});

	/*
	|--------------------------------------------------------------------------
	| Authenticated Routes
	|--------------------------------------------------------------------------
	*/

	Route::group(['middleware' => ['prxadmin']], function () {

		Route::get('', 'DashboardController@index')->name('admin.dashboard');
		Route::post('logout', 'Auth\AdminAuthController@logout')->name('admin.logout');
		
		/*
		|--------------------------------------------------------------------------
		| Images
		|--------------------------------------------------------------------------
		*/

		Route::get(config('praxxys-admin.cache.images.route').'/{template}/{filename}',
			'ImageCacheController@getResponse')
		->name('imagecache')
		->where(array('filename' => '[ \w\\.\\/\\-\\@\(\)]+'));

		/*
		|--------------------------------------------------------------------------
		| Notifications
		|--------------------------------------------------------------------------
		*/

		Route::get('notifications', 'NotificationController@index')->name('admin.notifications');	
		Route::get('notifications/get-unread', 'NotificationController@getUnread');	
		Route::get('notifications/read', 'NotificationController@read');	

		/*
		|--------------------------------------------------------------------------
		| Resources
		|--------------------------------------------------------------------------
		*/

		Route::resource('admin-users', 'AdminController');
		Route::post('admin-users/{id}', 'AdminController@restore')->name('admin-users.restore');
		Route::resource('roles', 'RoleController');

		/*
		|--------------------------------------------------------------------------
		| Roles and Permissions
		|--------------------------------------------------------------------------
		*/

		Route::post('roles/users/add', 'RoleController@addUser')->name('admin.roles.addUser');
		Route::post('roles/{role}/users/remove/{admin}', 'RoleController@removeUser')->name('admin.roles.removeUser');

		Route::post('roles/permissions/add', 'RoleController@addPermission')->name('admin.roles.addPermission');
		Route::post('roles/{id}/permissions/remove/{name}', 'RoleController@removePermission')->name('admin.roles.removePermission');

		Route::get('permissions', 'PermissionController@index')->name('permissions.index');
        Route::get('permissions/{role}', 'PermissionController@show')->name('permissions.show');
        Route::post('permissions/{role}', 'PermissionController@update')->name('permissions.update');

		/*
		|--------------------------------------------------------------------------
		| Admin Logs
		|--------------------------------------------------------------------------
		*/

		Route::get('admin-logs', 'AdminLogController@index')->name('admin-logs.index');


	});


});


