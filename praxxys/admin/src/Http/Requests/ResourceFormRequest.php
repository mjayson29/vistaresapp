<?php

namespace PRAXXYS\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rule;
use Exception;

class ResourceFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];
        $fields = Request::route()->controller->model::$editable_fields;

        foreach($fields as $name => $data) {

            $defaultRules = [];

            # When set to false, disables checking of required flag
            $requiredFlag = true;

            if(array_key_exists('rules', $data)) {
                switch(gettype($data['rules'])) {

                    case 'array':
                        $defaultRules = $data['rules'];
                        break;
                    case 'string':
                        $defaultRules = explode('|', $data['rules']);
                        break;
                    default:
                        throw new Exception('Invalid rule for [' . $name . '] passed in ' .
                            Request::route()->controller->model);

                }
            }

            switch($data['type']) {

                case 'ckeditor':
                    # Workaround for CKEditor required
                    if(array_key_exists('required', $data) && $data['required']) {
                        array_push($defaultRules, Rule::notIn(['<p>&nbsp;</p>']));
                    }
                    break;

                case 'image':
                    # Append image if `image` is set and method is create
                    if(Request::hasFile($name)) {
                        array_push($defaultRules, 'image');
                    }
                    break;

                case 'locationPicker':
                    # Custom rule for required:

                    if(array_key_exists('required', $data) && $data['required']) {
                        $rules[$name . '_address'] = ['required'];
                    }

                    $requiredFlag = false;
                    break;

            }

            # Check if required flag is on
            if($requiredFlag) {
                # Append required if `required` is set
                if(array_key_exists('required', $data) && $data['required']) {
                    array_push($defaultRules, 'required');
                }

            }



            if(!empty($defaultRules)) {
                $rules[$name] = $defaultRules;
            }

        }

        return $rules;
    }

}
