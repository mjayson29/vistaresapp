<?php

namespace PRAXXYS\Admin\Http\Middleware;

use Closure;
use PRAXXYS\Admin\Facades\PRXAdmin;
use Illuminate\Support\Facades\Route;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (
            PRXAdmin::auth()->check() &&
            (
                PRXAdmin::auth()->authorizeRequest($request)
                )
            ) {

            $this->assembleJSVars();

            PRXAdmin::router()->pushHistory($request);

            return $next($request);
        }

        return redirect('/admin/login');
    }

    /**
     * Pass js variables to view helper
     *
     * @return void
     */
    protected function assembleJSVars() {

        # Build global view js variables
        PRXAdmin::view()->pushJSVar([
            'userID' => PRXAdmin::auth()->id(),
            'pageID' => Route::currentRouteName(),
            'token' => csrf_token(),
            'jsPath' => PRXAdmin::view()->js_path(),
            'cssPath' => PRXAdmin::view()->css_path(),
            'mediaPath' => PRXAdmin::view()->media_path(),
            'status' => 
                session('status_message') ?: [
                    'title' => null,
                    'type' => null,
                    'message' => null,
                ]
        ]);

    }

}
