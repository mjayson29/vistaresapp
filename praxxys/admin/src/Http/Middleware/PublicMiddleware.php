<?php

namespace PRAXXYS\Admin\Http\Middleware;

use Closure;
use PRAXXYS\Admin\Facades\PRXAdmin;


class PublicMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $this->assembleStatus();
        
        return $next($request);
    }

    /**
     * Pass js status message to view helper
     *
     * @return void
     */
    protected function assembleStatus() {

        # Build global view js variables
        PRXAdmin::view()->pushJSVar([
            'status' => 
                session('status_message') ?: [
                    'title' => null,
                    'type' => null,
                    'message' => null,
                ]
        ]);

    }

}
