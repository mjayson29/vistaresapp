<?php

namespace PRAXXYS\Admin\Helpers;

use PRAXXYS\Admin\Facades\PRXAdmin;

class ViewHelper
{
    /**
     * Config
     *
     * @return void
     */
    protected $config;

    /**
     * Javascript variables for export
     *
     * @return void
     */
    protected $jsVars = [];

    /**
     * Create a new view helper instance.
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * Prepend JS variable
     *
     * @param array
     * @return void
     */
    public function prependJSVar(array $array) {
        $this->jsVars = $array + $this->jsVars;
    }
    
    /**
     * Append JS variable
     *
     * @param array
     * @return void
     */
    public function pushJSVar(array $array) {
        $this->jsVars = $this->jsVars + $array;
    }

    /**
     * Returns "active" css class on route match
     *
     * @param string
     * @return string
     */
    public static function checkActiveNav($menu_item) {

        # Check if has sub-items
        if(array_key_exists('items', $menu_item)) {

            $route = preg_quote($menu_item['route']);

            return !preg_match(".{$route}.", \Request::route()->getName()) ?: 'active';
        }

        return \Request::route()->getName() !== $menu_item['route'] ?: 'active';

    }

    /**
     * Return JS Variables
     *
     * @return json
     */
    public function jsVars() {
        return json_encode($this->jsVars);
    }

    /**
     * Get vendor path
     *
     * @return void
     */
    public function assets_path() {
        return array_get($this->config, 'assets.path');
    }

    /**
     * Get css path
     *
     * @return void
     */
    public function css_path() {
        return $this->assets_path();
    }

    /**
     * Get js path
     *
     * @return void
     */
    public function js_path() {
        return $this->assets_path();
    }

    /**
     * Get media path
     *
     * @return void
     */
    public function media_path() {
        return $this->assets_path() .'/media';
    }

    /**
     * Flash message to session
     *
     * @return void
     */
    public function flash(
        $message = 'The operation has successfully executed',
        $title = 'Success!', 
        $type = 'success') {

        session()->flash('status_message', [
            'title' => $title,
            'message' => $message,
            'type' => $type,

        ]);
    }

    /**
     * Convert collection to model
     *
     * @param Illuminate\Database\Eloquent\Collection $items
     * @param string $summary_fields
     * @return void
     */
    public function toDataTable($items, $summary_fields) {


        # Insert ID to summary fields
        $summary_fields = array_merge(['id' => 'ID'], $summary_fields);

        $headers = [];
        $data = [];

        # Build headers
        foreach($summary_fields as $field => $field_data) {

            $title = is_array($field_data) ? $field_data['title'] : $field_data;

            array_push($headers, [
                'title' => $title
            ]);
        }

        # Build data
        foreach($items as $item) {

            $array = [];

            # Convert either stdclass or eloquent object to array
            # If data is from query builder or raw DB query, it will be rendered as stdClass
            if(get_class($item) === 'stdClass') {
                $item = (array) $item;
                $item_fields = $item;
            } else {
                $item_fields = $item->toArray();
            }

            foreach ($summary_fields as $key => $value) {

                # Process render relationship

                $relation = explode('.', $key);
                if(count($relation) > 1) {
    
                    # Or check if first level function
                    if(strpos($relation[1], '()') !== false) {
                        $function = str_replace('()', '', $relation[1]);
                        array_push($array, $item->{$relation[0]}->{$function}());
                    } else {
                        array_push($array, $item->{$relation[0]}->{$relation[1]});
                    }

                }

                # Or check if first level function
                elseif(strpos($key, '()') !== false) {
                    $function = str_replace('()', '', $key);
                    array_push($array, $item->{$function}());
                }
                # Otherwise, process normally
                elseif(array_key_exists($key, $item_fields)) {

                    $content = $item[$key];

                    # Expand content if type is different
                    if(is_array($value)) {
                        switch($value['type']) {
                            case 'image':
                                $content = '<img src="'. PRXAdmin::storage()->getCachedImage('square-medium', basename($item[$key])) .'">';
                                break;

                            default:
                                $content = $item[$key];
                                break;
                        }
                    }

                    array_push($array, $content);

                }
            }

            array_push($data, $array);

        }

        return [
            'headers' => $headers,
            'data' => $data
        ];

    }

}
