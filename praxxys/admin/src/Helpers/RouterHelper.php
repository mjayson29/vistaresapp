<?php

namespace PRAXXYS\Admin\Helpers;

use PRAXXYS\Admin\Facades\PRXAdmin;
use Illuminate\Support\Facades\Route;

class RouterHelper
{

    /**
     * Pushes current url to history
     *
     * @param \Illuminate\Http\Request
     * @return void
     */
    public function pushHistory($request) {

        $limit = 5;

        $history = session()->has('history') ? session('history') : [];
        $currentPath = request()->path();

        if(
            # Don't push history if refresh button is pressed
            (empty($history) || $history[0] !== $currentPath) &&
            # Don't push if  route is image
            strpos($currentPath, config('praxxys-admin.cache.images.route')) === false)
        {
            
            array_unshift($history, $currentPath);
            session(['history' => array_splice($history, 0, $limit)]);            
        }
    }

    /**
     * Redirects back x depth
     *
     * @param int
     * @return void
     */
    public function back($depth) {
        return redirect(session('history')[$depth]);
    }


    /**
     * Check route or abort. Aborts if not found
     * @return string
     */
    public function getURLByName($name) {

        return Route::has($name) ? route($name) : abort(500, "Route $name does not exist.");

    }

    /**
     * Get route from request. Aborts if not found
     *
     * @param string
     * @return Illuminate\Routing\Route;
     */
    public function getRouteByName($name) {
        return Route::getRoutes()->getByName($name) ?: abort(500, "Route $name does not exist.");
    }

    /**
     * Get route from request
     *
     * @return mixed
     */
    public function getActionFromRequest($request) {

        $action = $request->route()->getAction();
        
        # Check if action has controller
        if(
            is_array($action) &&
            array_key_exists('controller', $action)
            ) {

            $action = explode('@', $action['controller']);

            return ['controller' => $action[0], 'method' => $action[1]];
        }

        return false;

    }

    /**
     * Get route from request
     *
     * @param string
     * @return mixed
     */
    public function getActionFromRouteName($name) {
        
        $route = PRXAdmin::router()->getRouteByName($name);
        
        if($action = $route->getAction()) {        

            # Check if action has controller
            if(
                is_array($action) &&
                array_key_exists('controller', $action)
                ) {

                $action = explode('@', $action['controller']);

                return ['controller' => $action[0], 'method' => $action[1]];
            }
        }

        return false;

    }

    /**
     * Get page icon from route settings in config
     *
     * @return void
     */
    public function getPageIcon() {

        function recursive_array_search($needle, $haystack, $prevArray = null) {
            foreach($haystack as $value) {
                if (is_array($value)) {
                    $nextArray = recursive_array_search($needle, $value, $value);
                    if ($nextArray) {
                        return $nextArray;
                    }
                }
                else if($value==$needle) {
                    return $prevArray;
                }
            }
            return false;
        }


        $routeName = Route::currentRouteName();

        $hit = recursive_array_search($routeName, config('praxxys-admin.navigation'));

        return $hit['icon'];

    }


}
