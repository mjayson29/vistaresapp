<?php

namespace PRAXXYS\Admin\Helpers;

use Illuminate\Support\Facades\Auth;
use PRAXXYS\Admin\Facades\PRXAdmin;
use PRAXXYS\Admin\Auth\RouteGuard;
use PRAXXYS\Admin\Http\Controllers\AdminLogController;

class AuthHelper
{
    /**
     * Current request
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Config
     *
     * @return void
     */
    protected $config;

    /**
     * Guard name
     *
     * @var string
     */
    protected $guardName;

    /**
     * Active guard
     *
     * @var \Illuminate\Auth\SessionGuard
     */
    protected $guard;

    /**
     * Route guard
     *
     * @var \PRAXXYS\Admin\Auth\RouteGuard
     */
    protected $routeGuard;

    /**
     * Create a new view helper instance.
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->config = $config;
        $this->guardName = array_get($this->config, 'security.guard');
        $this->guard = Auth::guard($this->guardName);
        $this->routeGuard = new RouteGuard();
    }

    /**
     * Return Model
     *
     * @return String
     */
    public function adminClass() {
        return array_get($this->config, 'security.admin-model') ?: \PRAXXYS\Admin\Models\Admin::class;
    }

    /**
     * Dynamically call the default driver instance.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return $this->guard()->{$method}(...$parameters);
    }

    /**
     * Return guard
     *
     * @return \Illuminate\Auth\SessionGuard
     */
    public function guard() {
        return $this->guard;
    }

    /**
     * Return guard name
     *
     * @return string
     */
    public function guardName() {
        return $this->guardName;
    }

    /**
     * Authorize permission alias
     *
     * @param string $permission
     * @return bool
     */
    public function can($permission) {
        return $this->authorizePermission($permission);
    }

    /**
     * Authorize user request
     *
     * @return bool
     */
    public function authorizeRequest($request) {

        return $this->routeGuard->authorizeRequest($request);
        
    }

    /**
     * Authorize route via route name
     *
     * @return bool
     */
    public function authorizeRoute($name) {

        return $this->routeGuard->authorizeRoute($name);
        
    }

    /**
     * Generate secure token
     *
     * @param int
     * @return string
     */
    public function generateToken($length = 32) {

        return bin2hex(random_bytes($length));
        
    }

    /**
     * Log admin action
     *
     * @param string $message
     * @param int $id
     * @return void
     */
    public function log($message, $id = null) {

        AdminLogController::log($message, $id);

    }

    /**
     * Authorize permission
     *
     * @param string $permission
     * @return bool
     */
    protected function authorizePermission($permission) {

        $admin = PRXAdmin::auth()->user();

        return $admin->hasRole('super-admin') || ($admin->can($permission));
    }

}
