<?php

namespace PRAXXYS\Admin\Helpers;

use Intervention\Image\ImageManager;
use Intervention\Image\ImageCache;

use Closure;

class ImageHelper extends ImageManager
{
    /**
     * Create new cached image and run callback
     * (requires additional package intervention/imagecache)
     *
     * @param Closure $callback
     * @param integer $lifetime
     * @param boolean $returnObj
     * @param \Illuminate\Cache\Repository $store
     *
     * @return Image
     */
    public function cache(Closure $callback, $lifetime = null, $returnObj = false, $store = null)
    {
        if (class_exists('Intervention\\Image\\ImageCache')) {
            // create imagecache
            $imagecache = new ImageCache($this, $store);

            // run callback
            if (is_callable($callback)) {
                $callback($imagecache);
            }

            return $imagecache->get($lifetime, $returnObj);
        }

        throw new \Intervention\Image\Exception\MissingDependencyException(
            "Please install package intervention/imagecache before running this function."
        );
    }


}
