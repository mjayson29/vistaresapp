<?php

namespace PRAXXYS\Admin\Helpers;

use PRAXXYS\Admin\Facades\PRXAdmin;

class SecurityHelper
{
    /**
     * Current file key
     *
     * @var string
     */
    protected $fileKey;

    /**
     * Create a new security helper instance.
     *
     * @return void
     */
    protected $files = [
        '.env' => '640',
        'config/praxxys-admin.php' => '640'
    ];

    /**
     * Create a new security helper instance.
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * Checks file permissions
     *
     * @return void
     */
    public function checkFilePermissions() {
        foreach($this->files as $file => $permissions) {

            # Increment file pointer
            $this->fileKey = $file;

            # Iterate files and check permissions
            array_map(
                function($value, $expected) {
                    ($value & $expected) ? $this->throwPermissionError() : null;
                },
                $this->parseFilePermissions($file),
                $this->parseExpectedPermissions($permissions)
            );
        }
    }

    /**
     * Parses file permissions and returns split array
     *
     * @return array
     */
    protected function parseFilePermissions($path) {
        return str_split(decoct(fileperms(base_path(). '/' . $path) & 0777));
    }

    /**
     * Parses expected file permissions parameter and return a
     * bitwise not (reversal) of the permissions in split array
     *
     * @return void
     */
    protected function parseExpectedPermissions($permissions) {
        return str_split(decoct((octdec('0' . $permissions)) ^ 0777));
    }

    /**
     * Throws a 500 server error
     *
     * @return void
     */
    protected function throwPermissionError() {
        $expectedPermission = $this->files[$this->fileKey];

        abort(500, "Security: file permissions error! {$this->fileKey} should be at least $expectedPermission");
    }
}
