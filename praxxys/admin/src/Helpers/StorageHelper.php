<?php

namespace PRAXXYS\Admin\Helpers;

use PRAXXYS\Admin\Facades\PRXAdmin;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;

use Intervention\Image\ImageCache;

class StorageHelper
{
    /**
     * Config
     *
     * @var void
     */
    protected $config;

    /**
     * Driver used for storage
     *
     * @var void
     */
    protected $driver;

    /**
     * Create a new view helper instance.
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->config = $config;
        $this->driver = $this->config('storage.driver');
    }

    /**
     * Config getter
     *
     * @param string $config
     * @return mixed
     */
    protected function config($config) {
        return array_get($this->config, $config);
    }

    /**
     * Get path to storage
     *
     * @return string
     */
    public function path() {
        return $this->config('security.storage-path');
    }

    /**
     * Get path to image storage
     *
     * @return string
     */
    public function image_path() {
        return $this->config('images.storage-path') . '/';
    }

    /**
     * Get full path to image storage
     *
     * @return string
     */
    public function full_image_path() {
        return Storage::disk($this->driver)->path(config('praxxys-admin.images.storage-path'));
    }

    /**
     * Store file in image path
     *
     * @param Illuminate\Http\UploadedFile $file
     * @param string $path
     * @return string
     */
    public function storeImage(UploadedFile $file, $path) {
        return $file->store($this->image_path() . $path, $this->driver);
    }

    /**
     * Get file path
     *
     * @param string $path
     * @return string
     */
    public function getImage($path) {
        return Storage::disk($this->driver)->url($path);
    }

    /**
     * Get cached image
     *
     * @param string $path
     * @return string
     */
    public function getCachedImage($filter, $file) {
        return '/admin/' . config('praxxys-admin.cache.images.route') . '/' . $filter . '/' . $file;
    }

    /**
     * Delete file
     *
     * @param string $path
     * @return void
     */
    public function delete($path) {
        Storage::disk($this->driver)->delete($path);
    }

    /**
     * Delete image
     *
     * @param string $path
     * @return void
     */
    public function deleteImage($path) {

        # flush caches
        foreach(config('imagecache.templates') as $template) {
            $this->flushImageCache($path, $template);
        }

        # delete
        $this->delete($path);

    }

    protected function flushImageCache($path, $template) {
        $cache = Cache::store(config('praxxys-admin.cache.images.store'));
        $imageCache = new ImageCache(PRXAdmin::image(), $cache);
        $image = $imageCache->make(Storage::disk($this->driver)->path($path))->filter(new $template);

        $cache->forget($image->checksum());
    }

}
