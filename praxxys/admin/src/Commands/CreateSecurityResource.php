<?php

namespace PRAXXYS\Admin\Commands;

use Illuminate\Console\Command;
use PRAXXYS\Admin\Models\Permission;
use PRAXXYS\Admin\Models\Role;

class CreateSecurityResource extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'praxxys-admin:security.generate {name} {description?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a PRAXXYS Admin security resource';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    /**
     * Default permissions to create
     *
     * @var array
     */
    protected $default_permissions = 
        [
            'index' => 'View all ',
            'show' => 'View individual ',
            'create' => 'Create new ',
            'edit' => 'Edit ',
            'destroy' => 'Delete ',
            'restore' => 'Restore ',
        ];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $description = $this->argument('description') ? $this->argument('description') : 'Manages ' . $name;
;

        echo PHP_EOL;
        echo "Creating Permission...";
        echo PHP_EOL;
        echo PHP_EOL;

        $role = Role::create([
            'system' => true,
            'name' => $name,
            'description' => $description
        ]);

        echo "Created role {$role->name}.";
        echo PHP_EOL;

        foreach($this->default_permissions as $permission => $description) {
            $permission = Permission::create([
                'system' => true,
                'name' => $name . '.' . $permission,
                'description' => $description . $name,
            ]);

            echo "Created permission {$permission->name}.";
            echo PHP_EOL;

            $role->givePermissionTo($permission->name);
            echo "Assigned {$permission->name} to {$role->name}.";
            echo PHP_EOL;

        }
        
        echo PHP_EOL;
        echo "Done creating resource {$name}..";
        echo PHP_EOL;
        echo PHP_EOL;

    }
}
