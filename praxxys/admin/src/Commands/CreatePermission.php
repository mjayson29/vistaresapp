<?php

namespace PRAXXYS\Admin\Commands;

use Illuminate\Console\Command;
use PRAXXYS\Admin\Models\Permission;
use PRAXXYS\Admin\Models\Role;

class CreatePermission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'praxxys-admin:security.create-permission {name} {description?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a PRAXXYS Admin security permission';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $description = $this->argument('description') ? $this->argument('description') : 'Permission to ' . $name;
;

        echo PHP_EOL;
        echo "Creating Permission...";
        echo PHP_EOL;
        echo PHP_EOL;

        $permission = Permission::create([
            'system' => true,
            'name' => $name,
            'description' => $description,
        ]);

        echo "Created permission {$permission->name}.";
        echo PHP_EOL;

        
        echo PHP_EOL;
        echo "Done creating permission {$name}..";
        echo PHP_EOL;
        echo PHP_EOL;

    }
}
