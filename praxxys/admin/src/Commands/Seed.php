<?php

namespace PRAXXYS\Admin\Commands;

use Illuminate\Console\Command;

class Seed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'praxxys-admin:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seeds PRAXXYS Admin Seeders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo PHP_EOL;
        $this->info("Seed PRAXXYS Admin...");
        echo PHP_EOL;
        echo PHP_EOL;

        $this->info('Creating default security resources...');
        echo PHP_EOL;
        foreach(config('praxxys-admin.security.resources') as $entity => $model) {
            $this->call('praxxys-admin:security.generate', [
                'name' => $entity
            ]);
        }

        $this->info("Seeding initial roles...");
        $this->call('db:seed', [
            '--class' => 'PRAXXYS\\Admin\\Database\\Seeders\\RolesAndPermissionsSeeder'
        ]);
        echo "Done.";
        echo PHP_EOL;
        echo PHP_EOL;

        $this->info("Seeding initial users...");
        $this->call('db:seed', [
            '--class' => 'PRAXXYS\\Admin\\Database\\Seeders\\AdminTableSeeder'
        ]);
        echo "Done.";
        echo PHP_EOL;
        echo PHP_EOL;
        $this->info("PRAXXYS Admin installation complete. Go build something awesome!");


    }
}
