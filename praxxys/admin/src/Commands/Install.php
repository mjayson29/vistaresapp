<?php

namespace PRAXXYS\Admin\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'praxxys-admin:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs PRAXXYS Admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Test database connection
        try {
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            $this->error("Could not connect to the database.  Please check your configuration.");
            exit();
        }

        echo PHP_EOL;
        $this->info("Installing PRAXXYS Admin...");
        echo PHP_EOL;
        echo PHP_EOL;

        $this->info("Publishing vendor files...");
        echo PHP_EOL;
        $this->call('vendor:publish', [
            '--provider' => 'PRAXXYS\\Admin\\AdminServiceProvider',
        ]);

        $this->call('vendor:publish', [
            '--provider' => 'Intervention\\Image\\ImageServiceProviderLaravel5',
        ]);

        echo PHP_EOL;

        echo PHP_EOL;
        $this->call('praxxys-broadcaster-admin:install');
        echo PHP_EOL;

        # Cache prefix generator
        $rand = rand(100, 999);
        file_put_contents($this->laravel->environmentFilePath(),
            file_get_contents($this->laravel->environmentFilePath()) .
"
BROADCASTER_APP_ENABLED=FALSE

CACHE_PREFIX=prxadmin-$rand

"
        );

        # Refresh config from newly published files
        app('config')->set(
            'praxxys-admin',
             app('files')->getRequire('config/praxxys-admin.php')
        );
 
        app('config')->set(
            'permission',
             app('files')->getRequire('config/permission.php')
        );

        if ($this->confirm('Do you wish run database operations? [y,n]')) {
            echo "Running migrations...";
            echo PHP_EOL;
            $this->call('migrate');            
            echo PHP_EOL;

            $this->info("Seeding initial roles...");
            $this->call('db:seed', [
                '--class' => 'PRAXXYS\\Admin\\Database\\Seeders\\RolesAndPermissionsSeeder'
            ]);
            echo "Done.";
            echo PHP_EOL;
            echo PHP_EOL;

            $this->info("Seeding initial users...");
            $this->call('db:seed', [
                '--class' => 'PRAXXYS\\Admin\\Database\\Seeders\\AdminTableSeeder'
            ]);
            echo "Done.";
            echo PHP_EOL;
            echo PHP_EOL;
            $this->info("PRAXXYS Admin installation complete. Go build something awesome!");


        }

    }
}
