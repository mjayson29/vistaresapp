<?php

namespace PRAXXYS\Admin\Commands;

use Illuminate\Console\Command;

class SeedSecurityResources extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'praxxys-admin:security.seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seeds PRAXXYS Admin security resources';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('Creating default security resources...');
        echo PHP_EOL;

        foreach(config('praxxys-admin.security.resources') as $entity => $model) {
            $this->call('praxxys-admin:security.generate', [
                'name' => $entity
            ]);
        }

    }
}
