<?php

namespace PRAXXYS\Admin\Commands;

use Illuminate\Console\Command;

class MakeResource extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'praxxys-admin:make-resource {model}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates a PRXAdmin resource';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $model = $this->argument('model');
        $migration = str_plural(snake_case($model));

        $model_path = app_path("{$model}.php");
        $controller_path = app_path("Http/Controllers/{$model}Controller.php");
        $migration_path = base_path("database/migrations/" . date('Y_m_d_His') . "_create_{$migration}_table.php");


        # Check if files exist
        if(file_exists($model_path) || file_exists($controller_path) || file_exists($migration_path)) {
            echo PHP_EOL;
            $this->error('**WARNING!**');
            if (!$this->confirm('One or more files already exist. Do you wish to overwrite them? [y,n]')) {

                $this->info("Aborting...");
                exit();

            }

        }

        echo PHP_EOL;
        $this->info("Creating resource...");
        echo PHP_EOL;
        echo PHP_EOL;

        $this->info("Creating model...");
        file_put_contents($model_path, $this->makeModel($model));
        $this->info("Done.");
        echo PHP_EOL;

        $this->info("Creating controller...");
        file_put_contents($controller_path, $this->makeController($model, $migration));
        $this->info("Done.");
        echo PHP_EOL;

        $this->info("Creating migration...");
        file_put_contents($migration_path, $this->makeMigration($migration));
        $this->info("Done.");

        echo PHP_EOL;
        $this->info("All done!");


    }


    /**
     * Make model content
     *
     * @param string $model
     * @return string
     */
    protected function makeModel($model) {
        return
'<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use PRAXXYS\Admin\Traits\ImplementsModelAdmin;

class ' . $model . ' extends Model
{

    use ImplementsModelAdmin;

    public static $summary_fields = [
        \'name\' => \'Name\',
    ];

    /**
     * Set editable fields
     *
     * @var array
     */ 
    public static $editable_fields = [
        \'name\' => [
            \'type\' => \'text\',
            \'label\' => \'Name\',
            \'required\' => true,
        ],
    ];


    /**
     * Has one fields
     *
     * @var array
     */ 

   /* * * * * * * * * * * * * * * * * *
    * Sample belongs_to implementation   *
    * * * * * * * * * * * * * * * * * * 

    public static function belongs_to() {
        return [
            \'level_id\' => [
                \'label\' => \'Level\',
                \'collection\' => Level::all(),
                \'type\' => \'select\',
            ],
        ];

    }


    public function level() {
        return $this->belongsTo(Level::class);
    }
    */

   /**
     * Has many fields
     *
     * @var array
     */ 

   /* * * * * * * * * * * * * * * * * *
    * Sample has_many implementation   *
    * * * * * * * * * * * * * * * * * * 

    public static function has_many() {
        return [
            \'positions\' => [
                \'label\' => \'Positions\',
                \'class\' => Position::class
            ]
        ];
    }

    public function positions() {
        return $this->hasMany(Position::class);
    }
    */


}
';
    }

    /**
     * Make controller content
     *
     * @param string $model
     * @param string $migration
     * @return string
     */
    protected function makeController($model, $migration) {
        return
'<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PRAXXYS\Admin\Http\Controllers\ResourceController;

use App\\'. $model . ';

class ' . $model . 'Controller extends ResourceController
{
    # This line enables the resource authentication system:
    public static $guardResource = \'' . $migration . '\';

    # Link the Class Model so the controller can reference it:
    public $model = ' . $model . '::class;

}
';

    }


    /**
     * Make migration content
     *
     * @param string $model
     * @param string $migration
     * @return string
     */
    protected function makeMigration($migration) {
        return
'<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create' . studly_case($migration) . 'Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(\'' . $migration . '\', function (Blueprint $table) {

            $table->increments(\'id\');
            $table->string(\'name\');

            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(\'' . $migration . '\');
    }
}
';

    }
}
