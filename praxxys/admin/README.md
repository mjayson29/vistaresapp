
![](https://trello-attachments.s3.amazonaws.com/5ae98c8e97260e2d70964385/5ae98c9ca347edfed4051c5f/2f4338682fc78f990a685c39913874a1/praxxys-admin-300px.png)

# PRAXXYS Admin

## What's new in version 0.2.4

### BREAKING: Refactor usage of `has_one` and `belongs_to`

The usage of `has_one` and `belongs_to` has been refactored. See example below. Once you run `composer update`, be sure to find and replace all instances of `has_one` with `belongs_to`:

```php
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use PRAXXYS\Admin\Traits\ImplementsModelAdmin;

use App\TripExpense;
use App\TripLog;
use PRXAdmin;

class Trip extends Model
{

    use ImplementsModelAdmin;

    public static $summary_fields = [
        'name' => 'Name',
    ];

    /**
     * Set editable fields
     *
     * @var array
     */ 
    public static $editable_fields = [
        'name' => [
            'type' => 'text',
            'label' => 'Name',
            'required' => true,
        ],
    ];

    /**
     * Has one fields
     *
     * @var array
     */ 
    public static function has_one() {
        return [
            'trip_log' => [
                'label' => 'Trip Log'
            ]
        ];

    }

    public function trip_Log() {
        return $this->hasOne(TripLog::class);
    }

    /**
     * Belongs to fields
     *
     * @var array
     */ 
    public static function belongs_to() {
        return [
            'driver_id' => [
                'label' => 'Driver',
                'collection' => Driver::all(),
                'type' => 'select',
            ],
            'vehicle_id' => [
                'label' => 'Vehicle',
                'collection' => Vehicle::all(),
                'type' => 'select',
            ],
        ];

    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }

    /**
     * Set enum fields
     *
     * @var array
     */ 
    public static function enum_fields() {
        return [
            'type' => [
                'label' => 'Type',
                'collection' => self::$types,
            ],
        ];
    }

    public static $statuses = [
        [
            'id' => 0,
            'name' => 'Pending',
            'class' => 'bg-yellow'
        ],
        [
            'id' => 1,
            'name' => 'Approved',
            'class' => 'bg-green'
        ],
        [
            'id' => 2,
            'name' => 'Completed',
            'class' => 'bg-blue'
        ],
    ];

    public function renderStatus() {

        return self::renderEnumLabel(self::$statuses, $this->status);

    }

    public static $types = [
        [
            'id' => 1,
            'name' => 'Client Meeting'
        ],
        [
            'id' => 2,
            'name' => 'Company Activity'
        ],
        [
            'id' => 3,
            'name' => 'Personal Trip'
        ],
    ];

    public function renderType() {

        return self::renderEnum(self::$types, $this->type);

    }

   /**
     * Has many fields
     *
     * @var array
     */ 

    public static function has_many() {
        return [
            'trip_expenses' => [
                'label' => 'Trip Expenses',
                'class' => TripExpense::class
            ],
        ];
    }

    public function trip_expenses() {
        return $this->hasMany(TripExpense::class);
    }

}
```

### Asset refactoring

[e3ccb682](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/e3ccb682bd0cacd6ac64bca39595c3dec3aac0d8), [324db3b5](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/324db3b54fd8f1700885543f49a4eeaa72bdde47), [2670cb18](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/2670cb184fa7a7445a51ec78214876bc60b48230), [d2c405b2](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/d2c405b241a958e927d0262f81b7cb0728e628de) &raquo; [@jd](https://gitlab.praxxys.ph/jd)

All assets are now loaded via NPM. Check `webpack.mix.js` for more info.

```php
# All app dependecies go in `app.js`
mix.js('js/app.js', 'src/Resources/Assets/js/app.min.js');

# All custom scripts go in `main.js`
mix.js('js/main.js', 'src/Resources/Assets/js/main.min.js');

# All public scripts (unauthenticated pages, such as login) go in `main-public.js`
mix.js('js/main-public.js', 'src/Resources/Assets/js/main-public.min.js');

# CSS is pulled directloy from `node_modules`
mix.styles(
    [
        'node_modules/datatables.net-bs/css/dataTables.bootstrap.css',
        'node_modules/dropzone/dist/css/dropzone.css',
        'node_modules/bootstrap3/dist/css/bootstrap.css',
        'node_modules/bootstrap-toggle/css/bootstrap-toggle.css',
        'node_modules/admin-lte/dist/css/AdminLTE.css',
        'node_modules/flatpickr/dist/flatpickr.min.css',
        'node_modules/select2/dist/css/select2.css',
        'node_modules/fullcalendar/dist/fullcalendar.css',
        'css/default-skin.css'
    ], 'src/Resources/Assets/css/admin.min.css');

# Fonts required for admin are copied to the assets folder
mix.copy('node_modules/bootstrap3/dist/fonts', 'src/Resources/Assets/fonts');
```

--

### Make Resource command

[d881524e](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/d881524edadb9b851b0d8afcd351702f1b42c6dd), [97bebca4](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/97bebca4f4a75fad2f6f527236feee86876f6be6) &raquo; [@jd](https://gitlab.praxxys.ph/jd)

PRAXXYS Admin now comes with a handy command to create resources on the fly. No need to manually create them!

```bash
# Create a resource named Test Results
php artisan praxxys-admin:make-resource TestResults
```

This will automatically generate the following:

- `app/TestResult.php`
- `app/Http/Controllers/TestResultController`
- `database/migrations/yyyy_mm_dd_his_create_test_results_table.php`

--

### Input field changes

- CKEditor (HTML editor field) [90d432e8](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/90d432e8f0789aa943091df9f6e6e79cf3ac8e34), [e64c6def](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/e64c6def2b43345a521764f6f727710499254e0d) &raquo; [@allen.timon](https://gitlab.praxxys.ph/allen.timon)
- FlatPicker (Date/Time/DateTime) [3680c91f](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/3680c91f39a1bcd4d6f74b6dae6a4d3f114f4a9b), [07927e88](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/07927e88c8bc0f6f88be3d57af970686b98d09a8), [bc20169d](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/bc20169d4121830e73a373b5bec6e2e5b21e24d6), [456ac95d](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/456ac95d79882a761ef01e2e985ed3ba42ae403f) &raquo; [@allen.timon](https://gitlab.praxxys.ph/allen.timon)
- Location Picker [a578398b](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/a578398b9a92c4f688a51905ce232d1b5a82d575), [263454e5](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/263454e59a24657ff8f3f437c044694aa4f7ae8d) &raquo; [@jd](https://gitlab.praxxys.ph/jd)
- Added `readonly` attribute [37dbc8de](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/37dbc8dec36e80389d2d274938698b7f94940440), [7c0882b6](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/7c0882b6c702464eddaa1ce5a38d21899bf34da6), [0f0b877e](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/0f0b877edab0dc4dd9c37c6d3b1f80cca12e2c18) &raquo; [@allen.timon](https://gitlab.praxxys.ph/allen.timon)
- Number field [78baae0c](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/78baae0cc98dbe1314ad5b4ecce2f1870d9f4ae6) &raquo; [@allen.timon](https://gitlab.praxxys.ph/allen.timon)
- Decimal field [ac7c21af](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/ac7c21af544790eab3bf23f515eed7c94eb90aa7) &raquo; [@jd](https://gitlab.praxxys.ph/jd)
- Boolean field [e066a47e](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/e066a47ef3f29c68a6c68fe34c014e467a31c538) &raquo; [@allen.timon](https://gitlab.praxxys.ph/allen.timon)
- Enum field [4492d276](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/4492d276e904e4df6c5a8d9d78adb8f3535cf056) &raquo; [@jd](https://gitlab.praxxys.ph/jd)
- Public Image field [dcbdff78](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/dcbdff785dfe18b8120a3cde4af9b059f183624f) &raquo; [@jd](https://gitlab.praxxys.ph/jd)

--

### Bug Fixes and Enhancements

- Button events not binding after DataTable redraw [250b3f66](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/250b3f6619a22c9b90e0d3ff23aaa229aefb3db9), [83543492](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/835434925d029516dcee97627e4fcac9f8f97139) &raquo; [@jd](https://gitlab.praxxys.ph/jd)
- Sticky forms [e528551b](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/e528551b35feb316c1cdcbb03ff441ca5916b0ef) &raquo; [@allen.timon](https://gitlab.praxxys.ph/allen.timon)
- Refactor `ResourceFormRequest` logic [bedbcc6a](https://gitlab.praxxys.ph/laravel/praxxys-admin/commit/bedbcc6aff44eb3d0e3ffbef9659a65b20c413fe) &raquo; [@jd](https://gitlab.praxxys.ph/jd)


## What's new in version 0.2.3

PRAXXYS Admin now supports logging of admin actions out of the box!

By default, controllers that extend `ResourceController` will automatically log create, edit and delete actions. If you have a custom controller method, logging is quite simple:

```php
use PRAXXYS\Admin\Facades\PRXAdmin;

PRXAdmin::auth()->log(" did an action");

```

This will log an action directly in the Logs section of the system.

You won't need to do anything if you install from scratch, but if you update via `composer update`, you will need to add the following lines the config:


```php
# `/config/praxxys-admin`

[
    'navigation' => [
        
        'Security' => [
            ...

            'Logs' => [
                'icon' => 'fa fa-clipboard-list',
                'route' => 'admin-logs.index',
            ]

        ],
    ],

    'enable_log' = true,

]



```

You will also need to run the following command in order to deploy the `admin_logs` database table:

```bash
php artisan vendor:publish --tag=praxxys-admin.migrations
php artisan migrate
```


## Overview

This package aims to provide a drop-in admin solution to Laravel, which allows users to quickly and seamlessly create awesome and secure CRUD interfaces with only a few lines of code.

## Features

- Expressive frond-end framework (powered by AdminLTE)
- Robust user authentication engine with advanced roles and permissions
- Eloquent on steroids: Rapid creation of CRUD views with zero lines of front-end code
- Rapid creation of user navigation via configuration
- Built-in easy-to-use image caching
- Built-in broadcasting engine via `PRAXXYS Broadcaster` and `PRAXXYS Broadcaster Admin`
- Advanced eloquent-to-datatable converter
- Comprehensive configuration
- And many more!

## Installation

Since PRAXXYS Admin is hosted on handy-dandy private GitLab server, you will need to define the dependecies in your config.

You can do it via:

- Adding the definition to `~/.composer/config.json`
- Or directly in your project's `composer.json`

~~~json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "ssh://git@gitlab.praxxys.ph:52222/laravel/praxxys-admin.git"
        },
        {
            "type": "vcs",
            "url": "ssh://git@gitlab.praxxys.ph:52222/laravel/praxxys-broadcaster-admin.git"
        },
        {
            "type": "vcs",
            "url": "ssh://git@gitlab.praxxys.ph:52222/laravel/praxxys-broadcaster.git"
        },
    ]
}
~~~

##### Note: Before you begin, make sure you have already configured your `.env` file, specifically the database section, correctly.

~~~bash
# Install package 
composer require praxxys/admin

# Install into Laravel
php artisan praxxys-admin:install
~~~


## Running

Getting it up and running is as simple as runing `php artisan serve` and running `http://localhost:8000/admin` in a browser.

The default super-admin `admin@praxxys.ph` with password `password` is automatically seeded so you can use that to login.


## Using the Resource CRUD Engine

PRAXXYS Admin is equiped with a powerful resource CRUD engine that allows you to get up and running with your data models with only a few lines of code.

Let's say you have a migration that looks like this:

~~~php
    Schema::create('clients', function (Blueprint $table) {
        $table->increments('id');

        $table->string('name');
        $table->date('date_registered');
        $table->string('img_path');

        $table->timestamps();
        $table->softDeletes();
    });

~~~

You may unlock the power of the engine in a few simple steps.

### Model

On your model, add the `ImplementsModelAdmin` trait.

~~~php

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PRAXXYS\Admin\Traits\ImplementsModelAdmin;

class Client extends Model
{
    use ImplementsModelAdmin;
}
~~~

Next, add summary fields to indicate which fields should show up on the index DataTable route.

~~~php
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PRAXXYS\Admin\Traits\ImplementsModelAdmin;

use App\Project;

class Client extends Model
{
    use ImplementsModelAdmin;

    public static $summary_fields = [
        'name' => 'Name',
        'img_path' => [
            'title' => 'Logo',
            'type' => 'image'
        ]
    ];
}
~~~

Then, add the editable fields you wish to make editable.

~~~php
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PRAXXYS\Admin\Traits\ImplementsModelAdmin;

use App\Project;

class Client extends Model
{
    use ImplementsModelAdmin;

    public static $summary_fields = [
        'name' => 'Name',
        'img_path' => [
            'title' => 'Logo',
            'type' => 'image'
        ]
    ];

    /**
     * Set editable fields
     *
     * @var array
     */ 
    public static $editable_fields = [
        'name' => [
            'type' => 'text',
            'label' => 'Name',
            'required' => true,
            'rules' => 'max:5'
        ],

        'date_registered' => [
            'type' => 'date',
            'label' => 'Date Registered',
            'required' => true,
            'rules' => 'date'
        ],

        'img_path' => [
            'type' => 'image',
            'label' => 'Logo',
            'required' => true
        ],
    ];
}
~~~

### Routes (web.php)

On your web.php, declare the resource route for the newly created resource:

~~~php

<?php

/*
|--------------------------------------------------------------------------
| Authenticated Routes
|--------------------------------------------------------------------------
|
*/

Route::name('admin.')
    ->prefix('admin')
    ->middleware('prxadmin')
    ->group(function() {

        # Declare client resource
        Route::resource('clients', 'ClientController');
        
    });
~~~

### Controller

On the controller, extend the `ResourceController` provided by the PRAXXYS Admin package, instead of the default `Controller`.

~~~php

<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use PRAXXYS\Admin\Http\Controllers\ResourceController;

class ClientController extends ResourceController
{

    # This line enables the resource authentication system:
    public static $guardResource = 'clients';
    
    # Link the Class Model so the controller can reference it:
    public $model = Client::class;

    
}


~~~

### Navigation

Once everything is in place, add the resource to the navigation panel by adding it to the config.

In `config/praxxys-admin.php`:

~~~php
<?php


   /*
    |--------------------------------------------------------------------------
    | Admin Navigation Sections
    |--------------------------------------------------------------------------
    */

    'navigation' => [
        
        ...
       
        'Resources' => [

            'Manage Resources' => [
                'icon' => 'fa fa-clipboard-list',

                'items' => [
                    'Clients' => [
                        'route' => 'admin.clients.index',
                        'icon' => 'fa fa-handshake',
                    ],
                ],
            ],

        ],


    ],

~~~

And that's it! Try it out now.

## Relationships

PRAXXYS Admin is equiped with the ability to handle `has_one` and `has_many` relationships.

Let's revisit the `Client` model:

~~~php
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PRAXXYS\Admin\Traits\ImplementsModelAdmin;

use App\Project;

class Client extends Model
{
    use ImplementsModelAdmin;

    ...
    
    /**
     * Has many fields
     *
     * @var array
     */ 
    public static function has_many() {
        return [
            'projects' => [
                'label' => 'Projects',
                'class' => Project::class
            ]
        ];
    }
    
    /**
     * Render the client status
     *
     * @return string
     */ 
     public function renderStatus() {
        switch($this->status) {
            case 0:
                return 'Pending';
        }
     }

}
~~~

Next, on the `Project` Model, define the has_one reciprocal relationship:

~~~php

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PRAXXYS\Admin\Traits\ImplementsModelAdmin;

use App\Client;

class Project extends Model
{

    use ImplementsModelAdmin;

    /**
     * Set summary fields for `index` method display
     *
     * @var array
     */ 
    public static $summary_fields = [
        'name' => 'Name',
        
        # Relationship properties in keys are accessible via dot notation:
        'client.name' => 'Client Name',
        
        # So are public methods on the relation:
        'client.renderStatus()' => 'Client Status',
        
        'description' => 'Description',
        'start_date' => 'Date Started',
        
        # You can also call internal methods on the model:
        'renderStatus()' => 'Status',
        'due_date' => 'Date Due'
    ];

    /**
     * Set editable fields
     *
     * @var array
     */ 
    public static $editable_fields = [
        'name' => [
            'type' => 'text',
            'label' => 'Name',
            'required' => true
        ],
        'description' => [
            'type' => 'textarea',
            'label' => 'Description',
            'required' => true
        ],
        'content' => [
            'type' => 'textarea',
            'label' => 'Content',
            'required' => true
        ],
        'start_date' => [
            'type' => 'date',
            'label' => 'Start Date',
            'required' => true
        ],
        'due_date' => [
            'type' => 'date',
            'label' => 'Due Date',
            'required' => true
        ],
    ];

    /**
     * Belongs to fields
     *
     * @var array
     */ 
    public static function belongs_to() {
        return [
            'client_id' => [
                'label' => 'Client',
                'collection' => Client::all(),
                'type' => 'select',
            ],
        ];

    }
    
    /**
     * Render status
     *
     * @return string
     */ 
    public function renderStatus() {
        switch($this->status) {
            case 0:
                return 'Active';
        }
    }
}
~~~

### Polymorphic $summary_fields

Take note that `$summary_fields` accepts the following values as keys, for example:

`string`:`date_activated` - this will bind to an internal property on the model

`function`:`renderStatus()` - this will bind to an internal method on the model

`relation property`:`client.name` - this will bind to a property on the relationship

`relation function`:`client.renderStatus()` - this will bind to a method on the relationship

And that's it!

## Roles and Permissions

PRAXXYS Admin was built with roles and permissions in mind. First, let's do a walkthrough of the overall process for better understanding.

### What is a resource?

A resource is composed of a model and routes to:

1. show all (index)
2. create
3. store
4. edit
5. update
6. destroy

These routes need to be individually protected since, for example, a user may be able to create and view but not edit a particular resource.

PRAXXYS Admin provides a very easy way to manage these resources.

### Setting up a security resource

All users may have roles, which in turn have permissions. Users must posses permissions in order to access guarded routes.

To quickly create a CRUD role and permissions for a model, use this command:

`php artisan praxxys-admin:security.generate {name} {description?}`

Let's try it out for the `Clients` model we created earlier.

~~~bash
php artisan praxxys-admin:security.generate clients "Clients Manager"
~~~

This command automatically creates a `Clients Manager` role and permissions for all associated CRUD routes.

For non-CRUD routes, PRAXXYS Admin offers a convenient way to create permissions out-of-the-box:

`php artisan praxxys-admin:security.create-permission {name} {description?}`

A common example would be to create a permission entry for the `dashboard` route. To do so:

~~~bash
php artisan praxxys-admin:security.create-permission clients.dashboard
~~~

As you may have noticed, the permissions naming follows the `modelname.method` convention.

After creating setting up the permissions, make sure to add them to the desired user via:

`Access Control` > `Roles` > `View Role` > `Admins` > `Add User`

and

`Access Control` > `Roles` > `View Role` > `Permissions` > `Add Permissions`

### Enabling the security resource

By default, all routes are protected by the Route Guard for maximum security. Hence, you have to explicitly enable a route in order for it to function.

##### Note: Due to the nature of the Route Guard, closure based routes will no longer work. Make sure to use a controller for all of your routes!

To enable a security resource, one must set one of the following on the controller:

#### Guard Resource

The first way to enable a security resource is to set the `guardResource` static property on the controller:

`public static $guardResource = 'name_of_resource'`

##### Note: Since the `$guardResource` property is a critical property for the package, please follow the convention that it should match the name of the database table of the model.


#### Unguard

By setting the `unguard` static property on your controller, you can control the routes that do not require permissions to access.

##### Unguard all methods on controller

`public static $unguard = true;`

##### Unguard specific methods

`public static $unguard = ['dashboard', 'showReports'];`

#### Example

~~~php

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PRAXXYS\Admin\Http\Controllers\ResourceController;

class ClientController extends ResourceController
{
    # This will disable route protection on ALL methods:
    # public static $unguard = true;

    # This will disable route protection for specific methdos:
    public static $unguard = [
        'dashboard',
        'showReports'
    ];


    # This will guard all methods within the controller:
    public static $guardResource = 'clients';

    
    public function dashboard() {
        return view('dashboard')
    }

    public function showReports() {
        return view('show-reports')
    }
    
}
~~~

## Helpful features

The PRAXXYS Admin package is packed with a lot of helpful features. Browse through the features below.

### Extendable configuration

The configuration is very robust and extendable should you wish to do so.

Some common features include but are not limited to:

#### Navigation

~~~php
<?php
   /*
    |--------------------------------------------------------------------------
    | Admin Navigation Sections
    |--------------------------------------------------------------------------
    */

    'navigation' => [
        
        # Main Header
        'Security' => [
                
            # Sub Header
            'Access Control' => [
            
                # Icon class (see Fontawesome)
                'icon' => 'fa fa-shield-alt',

                # Actual Items
                'items' => [
                
                    # Item Label
                    'Administrators' => [
                    
                        # Item Route
                        'route' => 'admin-users.index',
                        # Item Icon
                        'icon' => 'fa fa-user-circle',
                    ],
                    'Roles' => [
                        'route' => 'roles.index',
                        'icon' => 'fa fa-address-card',
                    ],
                ],
            ],

        ],
    ],
~~~

### PRXAdmin Global Facade

A major feature of the package is the PRXAdmin facade which contains a significant portion of the package's functionality.

#### Auth

`PRXAdmin::auth()`

Since PRAXXYS Admin has its own set of guards, user providers, password reset providers, among others, authentication must be accessed via the `PRXAdmin::auth()` instead of the `Auth::` facade that comes with Laravel. For example:

`PRXAdmin::auth()->user()` instead of `Auth::user()`
`PRXAdmin::auth()->check()` instead of `Auth::check()`


To check if a user has a specific permission you can use:

`PRXAdmin::auth()->can('clients.dashboard')`

#### Router

`PRXAdmin::router()`

The router helper provides a number of helpful methods. Particularly, it stores route history for the possibility to redirect back to previous pages. For example from a controller method:

~~~php
public function store(Request $request) {

    # After storing your request, go back 2 pages.

    return PRXAdmin::router()->back(2);

}
~~~

#### Storage

`PRXAdmin::storage()`

The storage helper provides methods to help with file paths and file storage. Notably, it provides methods to automate image caching and manipulation.

For example, see the `deleteImage` and `storeImage` in action:

~~~php
if($request->file($name)) {                    

    # Delete old image if exists
    if($obj->$name) {
        PRXAdmin::storage()->deleteImage($obj->$name);
    }

    $path = PRXAdmin::storage()->storeImage($request->file($name), 'uploads');
    $obj->$name = $path;
}
~~~

##### Important! The folders in which the image caching engine searches files are defined in `config/praxxys-admin.php` under `cache.paths`. Make sure you add your folders properly or you will encounter `file not found` errors.

By default, all PRAXXYS Admin images are not accessible to authenticated users. This means that images are not physically accessible via direct url as they are sandboxed by the application. To display images on the front-end:

~~~php
PRXAdmin::storage()->getCachedImage('square-medium', basename('path_to_image))
~~~

Did you notice the `square-medium` directive? PRAXXYS Admin provides image filters that manipulate and cache your image quickly and easily. There are four default filters included, namely:

`square-tiny` - 50px x 50px, cropped at center

`square-medium` - 100px x 100px, cropped at center

`square-tiny` - 250px x 250px, cropped at center

`normal` - actual image

### View

`PRXAdmin::view()`

Perhaps this helper is the one you'll use the most. It provides tons of helpful functionality to get more work done.

#### Flash

Need to flash a message to the front-end? The `flash` method provides a convenient way to do so:

~~~php
# PRXAdmin::view()->flash($message, $title, $type);

PRXAdmin::view()->flash('You have successfully created an item!', 'Success!', 'success');
~~~

The type argument corresponds to the `SweetAlert` type, which can be `warning`, `error`, `success`, `info`, and `question`.

#### prependJSVar and pushJSVar

Need to pass json data to your front-end javascript? No problem! We've got the method for you:

~~~php
PRXAdmin::view()->pushJSVar([
    'users' => User::all()->toArray(),
    'settings' => [
        'alerts' => true
    ]
]);
~~~

This will be rendered in json under the `systemVars` global javascript array.

To access:

~~~javascript
var users = systemVars.users;
users.forEach(function(user) {

    var message = 'User email is ' + user.email;
    console.log(message);

    if(systemVars.settings.alerts) {
        alert(message);
    }

});
~~~

#### toDataTable

The view helper provides an awesome way of implementing datatables with little to no code. A full example follows:

~~~php
# PRXAdmin::view()->toDataTable($eloquent_collection, $summary_fields);

$clients = PRXAdmin::view()->toDataTable(Client::all(), [
        'name' => 'Name',
        'img_path' => [
            'title' => 'Logo',
            'type' => 'image'
        ]
    ]);

PRXAdmin::view()->pushJSVar([
    'datatables' => [
        'clients' => $clients
    ]
]);
~~~

This will format the `clients` data and output it to the `systemVars` global variable in a way that can easily be assimilated by the front-component:

~~~html
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Clients Data Table</h3>
            </div>
            <div class="box-body">
                @component('PRXAdmin::components.datatable', ['name' => 'clients'])
                @endcomponent
            </div>
        </div>
    </div>
</div>
~~~

The front-end component automatically renders permissions so you don't need to worry about the view/edit/delete and create buttons appearing where they should not.

And that's it!

## Feature and Merge Requests

We aim to use this package for all of our admin backends. As such, it is the responsibility of everyone to contribute. Any contribution, no matter how big or small is very much appreciated!

If you have ideas, create an issue and tag it as `enhancement`.

If you have any bug fixes, the right way to go about is to file an issue, then:

~~~bash
git clone ssh://git@gitlab.praxxys.ph:52222/laravel/praxxys-admin.git
cd praxxys-admin

git checkout -b issue-number-issue-description

# Once done with your changes

git commit -am 'Your changes'
git push origin HEAD
~~~

Then submit your merge request into the main `0.2` branch.

### Thank you very much!