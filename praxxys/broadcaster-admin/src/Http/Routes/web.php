<?php


Route::group(
	[	
		'prefix' => str_replace('.', '/', config('praxxys-broadcaster-admin.routePrefix')),
		'middleware' => [ 'web', config('praxxys-broadcaster-admin.middleware') ?: 'prxadmin' ],
		'namespace' => '\PRAXXYS\BroadcasterAdmin\Http\Controllers'
	], function () {


	# Get Notifications
	Route::get(
		'get',
		'NotificationController@get'	
		)
		->name(config('praxxys-broadcaster-admin.routePrefix') . '.get');


	# Read Notifications
	Route::get(
		'read',
		'NotificationController@read'
		)
		->name(config('praxxys-broadcaster-admin.routePrefix') . '.read');


	# View Notifications
	Route::get(
		'view',
		'NotificationController@view'
		)
		->name(config('praxxys-broadcaster-admin.routePrefix') . '.view');

});