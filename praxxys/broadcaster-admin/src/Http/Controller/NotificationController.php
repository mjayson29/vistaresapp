<?php

namespace PRAXXYS\BroadcasterAdmin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

use App\Http\Controllers\Controller;

class NotificationController extends Controller
{

    /**
     * Model to be notified
     *
     * @var \Illuminate\Auth\Authenticatable
     */
    protected $model;
 
    /**
     * Default guard
     *
     * @var \Illuminate\Auth\SessionGuard
     */
    protected $guard;

    /**
     * Creates a new notification controller
     *
     * @return void
     */
    public function __construct() {
        $this->model = config('praxxys-broadcaster-admin.model');
        $this->guard = config('praxxys-broadcaster-admin.guard');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view() {

        return view(config('praxxys-broadcaster-admin.views.view'), [
            'notifications' => Auth::guard($this->guard)->user()->notifications->sortByDesc('created_at'),
        ]);

    }

    /**
     * Mark notifications as read
     *
     * @return void
     */
    public function read() {
        $user = Auth::guard($this->guard)->user();
        $user->unreadNotifications()->update(['read_at' => Carbon::now()]);
    }

    /**
     * Get user notifications
     *
     * @return \Illuminate\Notifications\Notification
     */
    public function get() {
        return Auth::guard($this->guard)->user()->unreadNotifications;
    }

}