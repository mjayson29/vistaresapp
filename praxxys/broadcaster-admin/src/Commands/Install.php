<?php

namespace PRAXXYS\BroadcasterAdmin\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'praxxys-broadcaster-admin:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs PRAXXYS Broadcaster Admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $is_production = App::environment('production');

        echo PHP_EOL;
        $this->info("Installing PRAXXYS Broadcaster Admin...");
        echo PHP_EOL;
        echo PHP_EOL;

        $this->info("Publishing vendor files...");
        echo PHP_EOL;
        $this->call('vendor:publish', [
            '--provider' => 'PRAXXYS\\BroadcasterAdmin\\BroadcasterAdminServiceProvider'
        ]);

        $this->info("Installing PRAXXYS Broadcaster...");
        echo PHP_EOL;
        $this->call('praxxys-broadcaster:install');
        echo PHP_EOL;

        if (!$is_production || $this->confirm('Do you wish run notification migration? [y,n]')) {
            echo "Running migrations...";
            echo PHP_EOL;
            $this->call('migrate');            
            echo PHP_EOL;
        }

        echo PHP_EOL;
        $this->info("Done installing PRAXXYS Broadcaster Admin!");

    }
}
