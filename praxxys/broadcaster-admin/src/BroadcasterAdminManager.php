<?php

namespace PRAXXYS\BroadcasterAdmin;

use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class BroadcasterAdminManager
{
    /**
     * Config
     *
     * @var array
     */
    protected $config;

    /**
     * Create a new broadcaster manager instance.
     *
     * @param array $config
     * @return void
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * Render notification
     *
     * @param string $type
     * @return string
     */
    public function render(string $type) {
        return $this->config['notifications'][$type];
    }

    /**
     * Return guard
     *
     * @return \Illuminate\Auth\SessionGuard
     */
    public function guard() {
        return Auth::guard($this->config['guard']);
    }

}
