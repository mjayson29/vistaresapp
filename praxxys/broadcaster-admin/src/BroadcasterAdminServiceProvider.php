<?php

namespace PRAXXYS\BroadcasterAdmin;

use PRAXXYS\BroadcasterAdmin\BroadcasterAdminManager;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class BroadcasterAdminServiceProvider extends ServiceProvider
{
 
    /**
     * Register commands array
     *
     * @var array
     */
    protected $commands = [
        'PRAXXYS\BroadcasterAdmin\Commands\Install'
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        # register alias
        $this->app->singleton('praxxys-broadcaster-admin', function ($app) {
            return new BroadcasterAdminManager($app['config']->get('praxxys-broadcaster-admin'));
        });

        # load routes
        $this->loadRoutesFrom(__DIR__.'/Http/Routes/web.php');
        # load views
        $this->loadViewsFrom(__DIR__.'/resources/views', 'PRXBroadcasterAdmin');
        # publish config
        $this->publishes([
            __DIR__.'/config/' => config_path(),
        ], 'praxxys-broadcaster-admin.config');
        # publish migrations
        $this->publishes([
            __DIR__ . '/database/migrations/' => database_path('migrations/'),
        ], 'praxxys-broadcaster-admin.migrations');
        # publish public assets
        $this->publishes([
            __DIR__.'/resources/assets' => public_path('vendor/praxxys/broadcaster-admin'),
        ], 'praxxys-broadcaster-admin.assets');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        # Register commands
        $this->commands($this->commands);

    }
}
