                    <li id="notificationsMenu"  class="dropdown notifications-menu" v-cloak>
                        <!-- Menu toggle button -->
                        <a @click="readNotifications" href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning">@{{ count }}</span>
                        </a>
                        <ul class="dropdown-menu">

                            <li class="header">You have @{{ count }} unread @{{ countWord }}</li>

                            <li>
                                <!-- Inner Menu: contains the notifications -->
                                <ul class="menu">
                                    <li v-for="notif in notifications"><!-- start notification -->
                                        <a href="#">
                                         <i :class="renderNotification(notif.type).icon"></i> @{{ notif.data.message }}
                                        </a>
                                    </li>
                                    <!-- end notification -->
                                </ul>
                            </li>
                            <li class="footer"><a href="{{ route('admin.notifications.view') }}">View all</a></li>
                        </ul>
                    </li>