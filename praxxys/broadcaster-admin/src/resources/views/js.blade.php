@include('PRXBroadcaster::js', [
	'guard' => config('praxxys-broadcaster-admin.guard')
])
<script type="text/javascript" src="/vendor/praxxys/broadcaster-admin/js/praxxys-broadcaster-admin.js"></script>

{{-- Set broadcaster admin options --}}
<script type="text/javascript">
	praxxys.broadcasterAdmin.options.userID = {{ PRXBroadcasterAdmin::guard()->id() }};
	praxxys.broadcasterAdmin.options.adminModel = '{{ config('praxxys-broadcaster-admin.user-model') }}';
	praxxys.broadcasterAdmin.options.notifications = {!! json_encode(config('praxxys-broadcaster-admin.notifications')) !!};

	$('document').ready(function() {
		praxxys.broadcasterAdmin.init();
	});

</script>