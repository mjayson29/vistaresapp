@extends(config('praxxys-broadcaster-admin.views.extends'))

@section(config('praxxys-broadcaster-admin.views.yields'))
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Notifications
    </h1>
    <ol class="breadcrumb">
      <li class="active"><i class="fa fa-bell"></i> Notifications</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">

      <div class="col-xs-12">
        <div class="box">

          <div class="box-header">
            <h3 class="box-title">Notifications</h3>
          </div>

         <div class="box-body">
            <table class="table table-bordered table-striped dataTables dt-responsive nowrap" width="100%">
              <thead>
              <tr>
                <th>Type</th>
                <th>Message</th>
                <th>Date</th>
              </tr>
              </thead>
              <tbody>
              @foreach($notifications as $notification)
              <tr>
                <td>{{ PRXBroadcasterAdmin::render($notification->type)['description'] }}</td>
                <td>{{ $notification->data['message'] }}</td>
                <td>{{ $notification->created_at }}</td>
              </tr>
              @endforeach
              </tbody>
              <tfoot>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
@stop