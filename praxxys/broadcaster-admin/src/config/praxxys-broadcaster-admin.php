<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Auth
    |--------------------------------------------------------------------------
    */

    # Defaults to PRAXXYS Admin credentials:

    # 'user-model' => 'App.User',
    # 'guard' => 'web',

    'user-model' => 'PRAXXYS.Admin.Models.Admin',
    'guard' => 'praxxys-admin',


    /*
    |--------------------------------------------------------------------------
    | App Notifications
    |--------------------------------------------------------------------------
    |
    | Define your app notifications here.
    |
    */

    'notifications' => [
        'PRAXXYS\Broadcaster\Notifications\TestNotification' => [
            'icon' => 'fa fa-wrench',
            'description' => 'Test Notification'
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Assets
    |--------------------------------------------------------------------------
    */

    'sound' => '/vendor/praxxys/broadcaster-admin/notification.mp3',

    /*
    |--------------------------------------------------------------------------
    | Routes
    |--------------------------------------------------------------------------
    |
    | Sets the controller method and url of the routes
    */


    'routePrefix' => 'admin.notifications',
    'routes' => [
        'get' => '/admin/notifications/get',
        'read' => '/admin/notifications/read',
        'view' => '/admin/notifications/view',
    ],

    /*
    |--------------------------------------------------------------------------
    | Views
    |--------------------------------------------------------------------------
    |
    | Defines blade views. `Master` defines which blade template to extend.
    | `View` defines the "view all" view to be used.
    */

    'views' => [
        'extends' => 'admin.main',
        'yields' => 'content',
        'view' => 'PRXBroadcasterAdmin::view'
    ]

];
