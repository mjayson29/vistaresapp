<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Project;
use App\Unit;

class FloorLayout extends Model
{

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'project_id', 'name', 'type', 'building_layout_id',
    	'description', 'image_path', 'status'
    ];


    /**
     * Constant Variables
     * 
     */
    const COMMERCIAL = 1;
    const ZONE_1 = 2;
    const ZONE_2 = 3;
    const SKY_AMENITY = 4;



    public function project() {
    	return $this->belongsTo(Project::class);
    }

    public function units() {
      return $this->hasMany(Unit::class, 'floor_layout_id');
    }

    public function building_layout()
    {
      return $this->belongsTo(BuildingLayout::class);
    }

    public function room_layout()
    {
      return $this->belongsTo(RoomLayout::class);
    }


    /*
    |-----------------------------------------------
    | @Methods
    |-----------------------------------------------
    */


    /**
     * Get Floor Types
     * @return Collection
     */
    public static function getTypes() 
    {

    	$types = collect([
    		['name' => 'Commercial', 'value' => FloorLayout::COMMERCIAL],
    		['name' => 'Zone 1 Residential', 'value' => FloorLayout::ZONE_1],
  			['name' => 'Zone 2 Residential', 'value' => FloorLayout::ZONE_2],
  			['name' => 'Sky Amenity', 'value' => FloorLayout::SKY_AMENITY],    		
    	]);

    	return $types;

    }


    /**
     * Find type of floor layout
     * 
     * @return String
     */
    public function findType()
    {
    	foreach (FloorLayout::getTypes() as $type) {
    		if($this->type === $type['value']) {
    			return $type['name'];
    		}
    	}
    }

    /**
     * Deactivating Active status 
     * @param int $type    Floor Layout type
     * @param int $project Project ID
     */
    public static function deactivateActiveStatus($type, $project, $floorID)
    {
      $activeFloor = FloorLayout::where(['type' => $type, 'project_id' => $project])->first();
      if($activeFloor && $activeFloor->id != $floorID) {
        /** Set to InActive */
        $activeFloor->status = 0;
        $activeFloor->save();
      }
    }


    /*
    |-----------------------------------------------
    | @Render
    |-----------------------------------------------
    */
   	
   	/**
   	 * Rendering short description
   	 * 
   	 * @return String
   	 */
   	public function renderShortDescription()
   	{

   		if($this->description) {
   			return str_limit($this->description, 100);
   		}

   	}

   	/**
   	 * Rendering Image Path
   	 * 
   	 * @return String
   	 */
   	public function renderImagePath()
   	{
   		if($this->image_path) {
   			return '/storage/' . $this->image_path;
   		}
   	}

   	/**
   	 * Rendering status of Floor layout
   	 * @return String
   	 */
   	public function renderStatus()
   	{
   		if($this->status) {
   			return '<label class="label label-primary">Active</label>';
   		} else {
   			return '<label class="label label-default">In Active</label>';
   		}
   	}




    /*
    |-----------------------------------------------
    | @Checker
    |-----------------------------------------------
    */

    /**
     * Check if has floor layout
     * @param  int $project Project ID
     * @param  int $type Floor Layout Type
     * @return boolean
     */
    public static function hasFloorLayout($project, $type) {

    	if(count(FloorLayout::where(['project_id' => $project, 'type' => $type])->get())) {
    		return true;
    	} else {
    		return false;
    	}

    	return false;

    }

}