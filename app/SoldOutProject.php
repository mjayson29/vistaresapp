<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoldOutProject extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'name', 'description', 'image_path'
	];


	/*
    |-----------------------------------------------
    | @Render
    |-----------------------------------------------
    */

    /**
     * Rendering Image Path
     * 
     * @return string
     */
    public function renderImagePath()
    {

    	if($this->image_path) {
    		return '/storage/'. $this->image_path;

    	}

    }

}
