<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomLayout extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'unit_id', 'name', 
		'x_coordinates', 'y_coordinates'
	];

	public function unit()
	{
		return $this->belongsTo(Unit::class);
	}
}
