<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Amenity;
use App\CommercialArea;
use App\Room;
use App\Gallery;

class Project extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'name', 'description', 'address',
        'image_path','longitude', 'latitude', 
        'footer', 'front_outside_image', 'back_outside_image',
        'landmark'
	];

    /*
    |-----------------------------------------------
    | @Relationships
    |-----------------------------------------------
    */

    public function rooms() 
    {
    	return $this->hasMany(Room::class);
    }

    public function amenities() 
    {
        return $this->hasMany(Amenity::class);
    }
    
    public function commercial_areas() 
    {
        return $this->hasMany(CommercialArea::class);
    }

    public function gallery_images() 
    {
        return $this->hasMany(Gallery::class);
    }

    public function building_layouts()
    {
        return $this->hasMany(BuildingLayout::class);
    }

    public function renderImagePath()
    {
        if($this->image_path) {
            return '/storage/' . $this->image_path;
        }
    }

    public function renderOutsideFrontView()
    {
        if($this->front_outside_image) {
            return '/storage/' . $this->front_outside_image;
        }
    }

    public function renderOutsideBackView()
    {
        if($this->back_outside_image) {
            return '/storage/' . $this->back_outside_image;
        }
    }

}
