<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\FloorLayout;

class Unit extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   	protected $fillable = [
   		'floor_layout_id', 'unit_number', 'name',
   	  'x_coordinates', 'y_coordinates',
   		'description', 'room_type_id', 'image_path',
      'available_360',
   	];


    public function floor() {
      return $this->belongsTo(FloorLayout::class, 'floor_layout_id');      
    }

    public function room_type() {
      return $this->belongsTo(RoomType::class);      
    }

    public function room_layout() {
      return $this->hasMany(RoomLayout::class);      
    }

  	/**
  	 * Constant Variables
  	 * 
  	 */
  	const STUDIO = 1;
  	const BEDROOM_1 = 2;
  	const BEDROOM_2 = 3;
    
    /*
    |-----------------------------------------------
    | @Methods
    |-----------------------------------------------
    */
	
	  /**
  	 * Get unit types
  	 * 
  	 * @return collection
  	 */
   	public static function getTypes()
   	{

   		$types = collect([
			['name' => 'Studio', 'value' => Unit::STUDIO],
			['name' => '1 Bed Room', 'value' => Unit::BEDROOM_1],
			['name' => '2 Bed Room', 'value' => Unit::BEDROOM_2],
   		]);

   		return $types;

   	}

    /**
     * Rendering Image Path
     * 
     * @return String
     */
    public function renderImagePath()
    {
      if($this->image_path) {
        return '/storage/' . $this->image_path;
      }
    }

}

