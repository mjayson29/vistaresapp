<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Project;

class Gallery extends Model
{
	/**
	 * The attributes that are mass assignable.
	 * 
	 * @var array
	 */
	protected $fillable = [
		'project_id', 'description', 'image_path'
	];

    /*
    |-----------------------------------------------
    | @Relationships
    |-----------------------------------------------
    */
   	public function project() 
   	{
   		return $this->belongsTo(Project::class);
   	}


    /*
    |-----------------------------------------------
    | @Render
    |-----------------------------------------------
    */

    public function renderImagePath()
    {
    	if($this->image_path) {
    		return '/storage/' . $this->image_path;
    	}
    }

}
