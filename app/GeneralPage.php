<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\GeneralPageItem;

class GeneralPage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
  	protected $fillable = [
  		'name', 'description', 'slug_code'
  	];

    /*
    |-----------------------------------------------
    | @Relationships
    |-----------------------------------------------
    */
   	public function page_item() 
   	{
   		return $this->hasMany(GeneralPageItem::class);
   	}

}
