<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\GeneralPage;

class GeneralPageItem extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'general_page_id', 'content'
	];

    /*
    |-----------------------------------------------
    | @Relationships
    |-----------------------------------------------
    */
    public function page()
   	{
   		return $this->belongsTo(GeneralPage::class, 'general_page_id');
   	}

}
