<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeBanner extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'title', 'image_path', 'description'
    ];


    /*
    |-----------------------------------------------
    | @Render
    |-----------------------------------------------
    */
   	
   	/**
   	 * Rendering image path
   	 * 
   	 * @return string
   	 */
    public function renderImagePath()
    {
    	if($this->image_path) {
    		return '/storage/'. $this->image_path;
    	}
    }

    /**
     * Rendering banner title
     * 
     * @return string
     */
    public function renderBannerTitle() 
    {

    	if($this->title) {
    		return $this->title;
    	}

    	return 'Home Banner';

    }


}
