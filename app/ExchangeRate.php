<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use PRAXXYS\Admin\Traits\ImplementsModelAdmin;

class ExchangeRate extends Model
{

    use ImplementsModelAdmin;

    public static $summary_fields = [
        'name' => 'Name',
        'exchange_rate_name' => 'Displayed Name',
        'amount' => 'Amount',
    ];

    /**
     * Set editable fields
     *
     * @var array
     */ 
    public static $editable_fields = [
        'name' => [
            'type' => 'text',
            'label' => 'Name',
            'required' => true,
        ],

        'exchange_rate_name' => [
            'type' => 'text',
            'label' => 'Displayed Name',
            'required' => true,
        ],

        'amount' => [
            'type' => 'number',
            'label' => 'Amount',
            'required' => true,
        ],
    ];


    /**
     * Has one fields
     *
     * @var array
     */ 

   /* * * * * * * * * * * * * * * * * *
    * Sample belongs_to implementation   *
    * * * * * * * * * * * * * * * * * * 

    public static function belongs_to() {
        return [
            'level_id' => [
                'label' => 'Level',
                'collection' => Level::all(),
                'type' => 'select',
            ],
        ];

    }


    public function level() {
        return $this->belongsTo(Level::class);
    }
    */

   /**
     * Has many fields
     *
     * @var array
     */ 

   /* * * * * * * * * * * * * * * * * *
    * Sample has_many implementation   *
    * * * * * * * * * * * * * * * * * * 

    public static function has_many() {
        return [
            'positions' => [
                'label' => 'Positions',
                'class' => Position::class
            ]
        ];
    }

    public function positions() {
        return $this->hasMany(Position::class);
    }
    */


}
