<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Project;

class Room extends Model
{
    	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'project_id', 'name', 'description',
		'image_path'
	];


    /*
    |-----------------------------------------------
    | @Relationships
    |-----------------------------------------------
    */
    public function project()
    {
    	return $this->belongsTo(Project::class);
    }



    /*
    |-----------------------------------------------
    | @Render
    |-----------------------------------------------
    */

    /**
     * Rendering short description
     * 
     * @return string
     */
    public function renderShortDescription()
    {
        if($this->description) {
            return str_limit($this->description, 100);
        }

    }

    /**
     * Rendering Image path
     * 
     * @return string
     */
    public function renderImagePath() 
    {
        if($this->image_path) {
            return '/storage/'. $this->image_path;
        }
    }



}
