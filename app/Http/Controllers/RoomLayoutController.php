<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\RoomLayout;
use App\FloorLayout;
use App\Unit;

class RoomLayoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(FloorLayout $floor, $name)
    {
        return view('pages.admin.projects.room-layout.index', [
            'rooms' => RoomLayout::all(),
            'floor' => $floor,
            'units' => $floor->units,

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(FloorLayout $floor, $name)
    {
        $units =  Unit::all();
        $available = [];
        foreach ($units as $unit) {
             array_push($available, RoomLayout::where('unit_id', $unit->id)->get());           
        }
        return view('pages.admin.projects.room-layout.create',[
            'floor' => $floor,
            // 'units' => Unit::all(),
            'rooms' => RoomLayout::all(),
            'available' => $available,
            'units' => $floor->units,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vars = $request->all();
        $vars['unit_id'] = json_decode($vars['unit_id'])->id;
        /** Start Transaction */
        \DB::beginTransaction();
            /** Create unit */
            $room = RoomLayout::create($vars);

        /** End Transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash($room->name .' has been successfully created!', 'Success!', 'success');        

        /** Return to unit index */
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RoomLayout  $roomLayout
     * @return \Illuminate\Http\Response
     */
    public function show(RoomLayout $roomLayout)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RoomLayout  $roomLayout
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $unit)
    {
        $room = RoomLayout::find($id);
        $layouts = RoomLayout::where('unit_id', $room->unit_id)
                        ->whereNotIn('id', [$room->id])->get();
        return view('pages.admin.projects.room-layout.edit', [
            'room' => $room,
            'layouts' => $layouts
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RoomLayout  $roomLayout
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vars = $request->all();
        $roomLayout = RoomLayout::find($id);
        /** Start Transaction */
        \DB::beginTransaction();

            /** Create unit */
            $roomLayout->update($vars);

        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash($roomLayout->name .' has been successfully updated!', 'Success!', 'success');        

        /** Return to unit index */
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RoomLayout  $roomLayout
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        /** Begin Transaction */
        \DB::beginTransaction();

            /** Create unit */
            RoomLayout::destroy($id);

        /** End transaction */
        \DB::commit();


        /** Flash message */
        \PRXAdmin::view()->flash('Room Layout has been successfully removed!', 'Success!', 'success');        

        /** Return to unit index */
        return back();        
    }
}
