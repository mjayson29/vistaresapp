<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RoomStorePost;

use App\Project;
use App\Room;

class RoomController extends Controller
{

    public static $guardResource = 'rooms';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Room::get();
            
        return view('pages.admin.rooms.index', [
            'rooms' => $rooms
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project = null, $name = null)
    {
        $projects = Project::get();

        return view('pages.admin.rooms.create', [
            'project' => $project,
            'projects' => $projects
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoomStorePost $request)
    {
        $vars = $request->except(['image_path']);

        if($request->hasFile('image_path')) {
            $vars['image_path'] = $request->file('image_path')->store('rooms-image', 'public');
        }

        /** Begins transaction */
        \DB::beginTransaction();

            /** Create room */
            $room =  Room::create($vars);

        /** End transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash($room->name .' has been successfully created!', 'Success!', 'success');        

        /** Return to project index */
        return redirect()->route('admin.project.view', [$room->project->id, $room->project->name]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $room = Room::find($id);

        $projects = Project::get();

        return view('pages.admin.rooms.edit', [
            'room' => $room,
            'projects' => $projects
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoomStorePost $request, $id)
    {
        /** Find  Room*/
        $room = Room::find($id);
        $vars = $request->except(['image_path']);

        if($request->hasFile('image_path')) {

            if($room->image_path) {
                /** Delete file/image in storage */
                \Storage::delete('public/'. $room->image_path);                
            }
           
            $vars['image_path'] = $request->file('image_path')->store('rooms-image', 'public');
        }

        /** Begin transaction */
        \DB::beginTransaction();

            /** Update room */
            $room->update($vars);

        /** Commit transaction */
        \DB::commit();


        /** Flash message */
        \PRXAdmin::view()->flash($room->name .' has been successfully updated!', 'Success!', 'success');                

        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $room = Room::find($id);

        /** Begin Transaction */
        \DB::beginTransaction();

            /** Remove from storage */
            \Storage::delete('public/'. $room->image_path);

            /** Soft Delete room */
            $room->delete();        

        /** End Transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash('A room has been successfully archived!', 'Success!', 'success');                

        return back();


    }
}
