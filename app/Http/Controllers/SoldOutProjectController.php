<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SoldOutProjectStorePost;

use App\SoldOutProject;

class SoldOutProjectController extends Controller
{
    public static $guardResource = 'sold-out-projects';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $soldOutProjects = SoldOutProject::get();

        return view('pages.admin.sold-out-projects.index', [
            'soldOutProjects' => $soldOutProjects
        ]);      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.sold-out-projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SoldOutProjectStorePost $request)
    {
        $vars = $request->except(['image_path']);

        if($request->hasFile('image_path')) {
            $vars['image_path'] = $request->file('image_path')->store('sold-out-projects-image', 'public');
        }

        /** Begins transaction */
        \DB::beginTransaction();

            /** Create Sold out project */
            $soldOutProject =  SoldOutProject::create($vars);

        /** End transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash($soldOutProject->name .' has been successfully created!', 'Success!', 'success');        

        /** Return to sold out project index */
        return redirect()->route('admin.sold-out-projects.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SoldOutProject $project, $name)
    {
        return view('pages.admin.sold-out-projects.show', [
            'project' => $project
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = SoldOutProject::find($id);

        return view('pages.admin.sold-out-projects.edit', [
            'project' => $project
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SoldOutProjectStorePost $request, $id)
    {
        /** Find Soldoutproject */
        $project = SoldOutProject::find($id);
        $vars = $request->except(['image_path']);

        if($request->hasFile('image_path')) {

            if($project->image_path) {

                /** Delete file/image in storage */
                \Storage::delete('public/'. $project->image_path);                
            }
           
            $vars['image_path'] = $request->file('image_path')->store('sold-out-projects-image', 'public');
        }

        /** Begin transaction */
        \DB::beginTransaction();

            /** Update sold out project */
            $project->update($vars);

        /** Commit transaction */
        \DB::commit();


        /** Flash message */
        \PRXAdmin::view()->flash($project->name .' has been successfully updated!', 'Success!', 'success');                

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = SoldOutProject::find($id);

        /** Begin transaction */
        \DB::beginTransaction();

            /** Remove from storage */
            \Storage::delete('public/'. $project->image_path);

            /** Soft delete sold out project */
            $project->delete();

        /** End transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash('Project has been successfully archived!', 'Success!', 'success');        

        return redirect()->route('admin.sold-out-projects.index');
    }
}
