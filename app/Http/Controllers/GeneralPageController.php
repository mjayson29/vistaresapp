<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GeneralPageStorePost;

use App\GeneralPage;

class GeneralPageController extends Controller
{
    public static $guardResource = 'general-page-item';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = GeneralPage::get();
        return view('pages.admin.general-pages.index', [
            'pages' => $pages
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(GeneralPage $page, $name)
    {
        return view('pages.admin.general-pages.show', [
            'page' => $page
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = GeneralPage::find($id);
        return view('pages.admin.general-pages.edit', [
           'page' => $page
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GeneralPageStorePost $request, $id)
    {   
        $vars = $request->all();

        /** Find General Page */
        $page = GeneralPage::find($id);

        /** Begin transaction */
        \DB::beginTransaction();

            $page->update($vars);

        /** End transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash($page->name .' has been successfully updated!', 'Success!', 'success');        

        return back();

    }

}
