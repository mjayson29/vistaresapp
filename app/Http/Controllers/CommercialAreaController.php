<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CommercialAreaStorePost;
use Intervention\Image\Facades\Image;

use App\CommercialArea;
use App\Project;

class CommercialAreaController extends Controller
{
    public static $guardResource = 'commercial-areas';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commercialAreas = CommercialArea::get();

        return view('pages.admin.commercial-areas.index', [
            'commercialAreas' => $commercialAreas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project = null, $name = null)
    {
        $projects = Project::get();

        return view('pages.admin.commercial-areas.create', [
            'project' => $project,
            'projects' => $projects
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommercialAreaStorePost $request)
    {

        $vars = $request->except(['image_path']);

        if($request->hasFile('image_path')) {
           $vars['image_path'] = $request->file('image_path')->store('commercial-areas-image', 'public');
        }

        /** Begins transaction */
        \DB::beginTransaction();

            /** Create amenity */
            $commercialArea =  CommercialArea::create($vars);

        /** End transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash($commercialArea->name .' has been successfully created!', 'Success!', 'success');        

        /** Return to project index */
        return redirect()->route('admin.project.view', [$commercialArea->project->id, $commercialArea->project->name]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $commercialArea = CommercialArea::find($id);

        $projects = Project::get();

        return view('pages.admin.commercial-areas.edit', [
            'commercialArea' => $commercialArea,
            'projects' => $projects
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommercialAreaStorePost $request, $id)
    {
        /** Find Commercial Area */
        $commercialArea = CommercialArea::find($id);
        $vars = $request->except(['image_path']);

        if($request->hasFile('image_path')) {

            if($commercialArea->image_path) {
                /** Delete file/image in storage */
                \Storage::delete('public/'. $commercialArea->image_path);                
            }
           
            $image = $request->file('image_path');
            $hash = $image->hashName('commercial-areas-image');
            $compress = Image::make($image)->resize(500, 500);
            $vars['image_path'] = $hash;
            \Storage::put('public/'. $hash, (string) $compress->encode());
        }

        /** Begin transaction */
        \DB::beginTransaction();

            /** Update commercial Area */
            $commercialArea->update($vars);

        /** Commit transaction */
        \DB::commit();


        /** Flash message */
        \PRXAdmin::view()->flash($commercialArea->name .' has been successfully updated!', 'Success!', 'success');                

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $commercialArea = CommercialArea::find($id);

        /** Begin Transaction */
        \DB::beginTransaction();

            /** Remove from storage */
            \Storage::delete('public/'. $commercialArea->image_path);

            /** Soft Delete Commercial Area */
            $commercialArea->delete();        

        /** End Transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash('A commercial area has been successfully archived!', 'Success!', 'success');                

        return back();
    }
}
