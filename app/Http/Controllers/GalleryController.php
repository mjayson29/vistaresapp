<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GalleryStorePost;

use App\Gallery;
use App\Project;

class GalleryController extends Controller
{
    
    public static $guardResource = 'gallery';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project, $name)
    {
        $images = $project->gallery_images()->paginate(6);

        return view('pages.admin.projects.gallery.index', [
            'project' => $project,
            'images' => $images
        ]);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project, $name)
    {

        return view('pages.admin.projects.gallery.create', [
            'project' => $project
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GalleryStorePost $request, Project $project)
    {
        $vars = $request->except(['image_path']);

        $vars['project_id'] = $project->id;

        if($request->hasFile('image_path')) {
            $vars['image_path'] = $request->file('image_path')->store('gallery', 'public');
        }

        /** Start transaction */
        \DB::beginTransaction();

            /** Create Gallery */
            Gallery::create($vars);

        /** End transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash('Image has been successfully added to gallery!', 'Success!', 'success');        

        /** Return to gallery index */
        return redirect()->route('admin.gallery.index', [$project->id, $project->name]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Project $project, $name)
    {
        $image = Gallery::find($id);

        return view('pages.admin.projects.gallery.edit', [
            'project' => $project,
            'image' => $image
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GalleryStorePost $request, $id)
    {
        /** Find image*/
        $image = Gallery::find($id);
        $vars = $request->except(['image_path']);

        if($request->hasFile('image_path')) {

            if($image->image_path) {
                /** Delete file/image in storage */
                \Storage::delete('public/'. $image->image_path);                
            }
           
            $vars['image_path'] = $request->file('image_path')->store('gallery', 'public');
        }

        /** Begin transaction */
        \DB::beginTransaction();

            /** Update Image */
            $image->update($vars);

        /** Commit transaction */
        \DB::commit();


        /** Flash message */
        \PRXAdmin::view()->flash('Image has been successfully updated!', 'Success!', 'success');                

        return back();


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = Gallery::find($id);

        /** Begin Transaction */
        \DB::beginTransaction();

            /** Remove from storage */
            \Storage::delete('public/'. $image->image_path);

            /** Removed image */
            $image->delete();

        /** End Transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash('An image has been successfully removed!', 'Success!', 'success');                

        return back();
    }
}
