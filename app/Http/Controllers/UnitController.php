<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UnitStorePost;

use App\FloorLayout;
use App\Unit;
use App\RoomType;

class UnitController extends Controller
{
    public static $guardResource = 'units';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(FloorLayout $floor, $name)
    {
        return view('pages.admin.projects.units.index', [
            'floor' => $floor
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(FloorLayout $floor, $name)
    {
        $types = Unit::getTypes();

        return view('pages.admin.projects.units.create', [
            'floor' => $floor,
            'room_types' => RoomType::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UnitStorePost $request, FloorLayout $floor)
    {
        $vars = $request->except(['image_path']);
        $vars['floor_layout_id'] = $floor->id;
        
        if($request->hasFile('image_path')) {

            $vars['image_path'] = $request->file('image_path')->store('room-layouts', 'public');

        }


        /** Start Transaction */
        \DB::beginTransaction();
            /** Create unit */
            $unit = Unit::create($vars);


        /** End Transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash($unit->name .' has been successfully created!', 'Success!', 'success');        

        /** Return to unit index */
        return redirect()->route('admin.units.index', [$floor->id, $floor->name]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($floor, Unit $unit)
    {   

        $types = Unit::getTypes();        


// dd($unit->unit_number);
        return view('pages.admin.projects.units.edit', [
            'unit' => $unit,
            'room_types' => RoomType::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UnitStorePost $request, Unit $unit)
    {
        $vars = $request->except(['image_path']);

        if($request->hasFile('image_path')) {

            /** Remove Image from Storage */
            if($unit->image_path) {
                \Storage::delete('public/'. $unit->image_path);
            }

            $vars['image_path'] = $request->file('image_path')->store('room-layouts', 'public');

        }

        /** Start Transaction */
        \DB::beginTransaction();

            /** Create unit */
            $unit->update($vars);

        /** End Transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash($unit->name .' has been successfully updated!', 'Success!', 'success');        

        /** Return to unit index */
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($unit)
    {
        /** Find Unit */
        $unit = Unit::find($unit);

        /** Begin Transaction */
        \DB::beginTransaction();

            /** Create unit */
            $unit->delete();

        /** End transaction */
        \DB::commit();


        /** Flash message */
        \PRXAdmin::view()->flash('Unit has been successfully removed!', 'Success!', 'success');        

        /** Return to unit index */
        return back();        

    }
}
