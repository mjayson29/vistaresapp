<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProjectStorePost;

use App\Project;
use Image;

class ProjectController extends Controller
{
    public static $guardResource = 'project';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $projects = Project::get();

        return view('pages.admin.projects.index', [
            'projects' => $projects
        ]);

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project, $name)
    {
        return view('pages.admin.projects.show', [
            'project' => $project
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project, $name)
    {
        return view('pages.admin.projects.edit', [
            'project' => $project
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectStorePost $request, Project $project)
    {

        $vars = $request->except(['image_path', 'front_outside_image', 'back_outside_image']);

        /** Begin transaction */
        \DB::beginTransaction();

            if($request->hasFile('image_path')) {
                $vars['image_path'] = $request->file('image_path')->store('projects', 'public');
            }

            if($request->hasFile('front_outside_image')) {
                $vars['front_outside_image'] = $request->file('front_outside_image')->store('outside-view', 'public');
            }

            if($request->hasFile('back_outside_image')) {
                $vars['back_outside_image'] = $request->file('back_outside_image')->store('outside-view', 'public');
            }

            /** Update Project */
            $project->update($vars);


        /** Commit transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash($project->name. ' has been successfully updated!', 'Success!', 'success');

        return back();

    }
}
