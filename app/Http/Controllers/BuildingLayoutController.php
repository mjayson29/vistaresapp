<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Project;
use App\BuildingLayout;

class BuildingLayoutController extends Controller
{

    public static $guardResource = 'building-layout';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project)
    {
        $layouts = BuildingLayout::where('project_id', $project->id)->get();

        return view('pages.admin.projects.building-layout.index', [
            'project' => $project,
            'layouts' => $layouts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project)
    {
        $types = BuildingLayout::getTypes();
        $layouts = BuildingLayout::where('project_id', $project->id)->get();
        return view('pages.admin.projects.building-layout.create', [
            'project' => $project,
            'types' => $types,
            'layouts' => $layouts
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Project $project)
    {
        // dd($request->all());
        $vars = $request->except([]);
        $vars['project_id'] = $project->id;

        /** Start transaction */
        \DB::beginTransaction();

            /** Store Building Layout */
            $building = BuildingLayout::create($vars);

        /** End Transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash($building->floor .' has been successfully created!', 'Success!', 'success');     

        return redirect()->route('admin.building-layouts.index', [$project->id, $project->name]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BuildingLayout  $buildingLayout
     * @return \Illuminate\Http\Response
     */
    public function show(BuildingLayout $buildingLayout)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BuildingLayout  $buildingLayout
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project, BuildingLayout $layout)
    {
        $types = BuildingLayout::getTypes();
        $layouts = BuildingLayout::where('project_id', $project->id)->get();

        return view('pages.admin.projects.building-layout.edit', [
            'project' => $project,
            'types' => $types,
            'layouts' => $layouts,
            'layout' => $layout
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BuildingLayout  $buildingLayout
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vars = $request->except([]);

        $buildingLayout = BuildingLayout::find($id);

        /** Start Transaction */
        \DB::beginTransaction();

            /** Create unit */
            $buildingLayout->update($vars);

        /** End Transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash($buildingLayout->floor .' has been successfully updated!', 'Success!', 'success');        

        /** Return to unit index */
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BuildingLayout  $buildingLayout
     * @return \Illuminate\Http\Response
     */
    public function destroy($buildingLayout)
    {
        /** Find Building Layout */
        $buildingLayout = BuildingLayout::find($buildingLayout);

        /** Begin Transaction */
        \DB::beginTransaction();

            /** Create Building Layout */
            $buildingLayout->delete();

        /** End transaction */
        \DB::commit();


        /** Flash message */
        \PRXAdmin::view()->flash('Floor has been successfully removed!', 'Success!', 'success');        

        /** Return to Building Layout index */
        return back();
    }
}
