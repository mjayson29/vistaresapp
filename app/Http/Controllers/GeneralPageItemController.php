<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\GeneralPageItem;

class GeneralPageItemController extends Controller
{

    public static $guardResource = 'general-page-item';
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pageItem = GeneralPageItem::find($id);

        return view('pages.admin.general-pages.general-page-item.edit', [
            'pageItem' => $pageItem
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pageItem = GeneralPageItem::find($id);

        $vars = $request->all();

        /** Begin Transaction */
        \DB::beginTransaction();

            /** Update page item */
            $pageItem->update($vars);

        /** End transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash('General page item has been successfully updated!', 'Success!', 'success');        

        return back();

    }
}
