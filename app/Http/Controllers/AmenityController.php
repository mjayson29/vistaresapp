<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AmenityStorePost;
use Intervention\Image\Facades\Image;

use App\Amenity;
use App\Project;

class AmenityController extends Controller
{
    public static $guardResource = 'amenities';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $amenities = Amenity::get();

        return view('pages.admin.amenities.index', [
            'amenities' => $amenities
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project = null, $name = null)
    {
        $projects = Project::get();

        return view('pages.admin.amenities.create', [
            'project' => $project,
            'projects' => $projects
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AmenityStorePost $request)
    {

        $vars = $request->except(['image_path']);

        if($request->hasFile('image_path')) {
            $vars['image_path'] = $request->file('image_path')->store('amenities-image', 'public');
        }

        /** Begins transaction */
        \DB::beginTransaction();

            /** Create amenity */
            $amenity =  Amenity::create($vars);

        /** End transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash($amenity->name .' has been successfully created!', 'Success!', 'success');        

        /** Return to project index */
        return redirect()->route('admin.project.view', [$amenity->project->id, $amenity->project->name]);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $amenity = Amenity::find($id);

        $projects = Project::get();

        return view('pages.admin.amenities.edit', [
            'amenity' => $amenity,
            'projects' => $projects
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AmenityStorePost $request, $id)
    {
        /** Find Amenity*/
        $amenity = amenity::find($id);
        $vars = $request->except(['image_path']);

        if($request->hasFile('image_path')) {

            if($amenity->image_path) {
                /** Delete file/image in storage */
                \Storage::delete('public/'. $amenity->image_path);                
            }
            $vars['image_path'] = $request->file('image_path')->store('amenities-image', 'public');
        }

        /** Begin transaction */
        \DB::beginTransaction();

            /** Update amenity */
            $amenity->update($vars);

        /** Commit transaction */
        \DB::commit();


        /** Flash message */
        \PRXAdmin::view()->flash($amenity->name .' has been successfully updated!', 'Success!', 'success');                

        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /** Find Amenity */
        $amenity = Amenity::find($id);

        /** Begin transaction */
        \DB::beginTransaction();

            /** Soft Deletes */
            $amenity->delete();

        /** Commit transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash('Amenity has been successfully archived!', 'Success!', 'success');

        return back();


    }
}
