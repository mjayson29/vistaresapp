<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Project;
use App\FloorLayout;
use App\BuildingLayout;

class ProjectPageController extends Controller
{
    public function index() 
    {
    	return response()->json([
    		'projects' => Project::all()
    	]);
    }

    public function view($id) 
    {
    	$item = Project::find($id);
        $floor = FloorLayout::where('project_id', $id)->with('units', 'building_layout')->get();
        $item->types = BuildingLayout::getTypes();
    	$item->rooms = $item->rooms;
    	$item->amenities = $item->amenities;
        $item->commercial_areas = $item->commercial_areas;
    	$item->floors = $floor;
        $item->layouts = $item->building_layouts;

    	return response()->json([
    		'item' => $item,
    	]);
    }
}
