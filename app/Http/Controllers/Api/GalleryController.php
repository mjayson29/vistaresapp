<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Project;

class GalleryController extends Controller
{

	/**
	 * Fetch Gallery Images
	 * 
	 * @param  int $id
	 * @return Illuminate\Http\Response
	 */
	public function fetch($id)
	{
		/** Find Project */
		$project = Project::find($id);

		return response()->json([
			'gallery' => $project->gallery_images, 
		]);

	}

}
