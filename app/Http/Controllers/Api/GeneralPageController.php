<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\GeneralPage;
use App\ExchangeRate;

class GeneralPageController extends Controller
{

	/**
	 * Getting Content for Terms and condition
	 * 
	 * @return Illuminate\Http\Response 
	 */
	public function terms() 
	{
		$page = GeneralPage::where('slug_code', 'terms_and_condition')->first();

		return response()->json([
			'content' => $page->page_item->first()
		]);

	}

	/**
	 * Getting Content for Privacy Policy
	 * 
	 * @return Illuminate\Http\Response
	 */
	public function privacy() 
	{
		$page = GeneralPage::where('slug_code', 'privacy_policy')->first();

		return response()->json([
			'content' => $page->page_item->first()
		]);

	}

	/**
	 * Getting Content for Introduction
	 * 
	 * @return Illuminate\Http\Response
	 */
	public function introduction() 
	{
		$page = GeneralPage::where('slug_code', 'introduction')->first();

		return response()->json([
			'content' => $page->page_item
		]);

	}

	/**
	 * Getting Exchange Rate for Calculator
	 * 
	 * @return Illuminate\Http\Response
	 */
	
	public function calculator()
	{
		return response()->json([
			'exchange_rates' => ExchangeRate::all()
		]);	
	}

}
