<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\HomeBanner;
use App\Project;
use App\SoldOutProject;

class HomePageController extends Controller
{
    public function index() 
    {
    	return response()->json([
    		'banner' => HomeBanner::first(),
    		'projects' => Project::all(),
    		'sold_outs' => SoldOutProject::all()
    	]);
    }
}
