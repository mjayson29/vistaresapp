<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;

class AuthController extends Controller {


	/**
	 * Get Client Secret
	 * 
	 * @return \Illuminate\Http\Response
	 */
	public function getClientSecret() {
		
		/** Get secret from oauth_clients table */
		$client = DB::table('oauth_clients')->
            	whereName(config('app.name').' Password Grant Client')->first();

        return response()->json([
            'secret' => ['id' => $client->id, 'secret' => $client->secret],
        ]);
	
	}

}