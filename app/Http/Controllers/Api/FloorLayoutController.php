<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Project;
use App\FloorLayout;
use App\BuildingLayout;
use App\Unit;
use App\RoomType;

class FloorLayoutController extends Controller
{
    public function index($id)
    {
    	$item = BuildingLayout::find($id);
        $unit_arr = [];
        foreach($item->floor_layout->units as $unit) {
            foreach ($unit->room_layout as $value) {
                $_unit = array_push($unit_arr, $value);
            }
        }
    
    	return response()->json([
    		'floor' => $item->floor_layout,
            'units'  => $item->floor_layout->units,
            'unit' => $unit_arr,
            'types'  => RoomType::all()
    	]);
    }

    /**
     * Filter unit/floor type
     * 
     * @return \Illuminate\Http\Response
     */
    public function filterByType($floor, $type) 
    {

        /** Get units within specific unit type */
        if($type != 'null') {
            $units = Unit::where(['room_type_id' => $type, 'floor_layout_id' => $floor])->get();            
        } else {
            $units = Unit::where(['floor_layout_id' => $floor])->get();            
        }


        return response()->json([
            'units' => $units
        ]);

    }
}
