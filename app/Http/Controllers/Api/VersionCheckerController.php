<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VersionCheckerController extends Controller
{

	/**
	 * Get Android, iOS version
	 * 
	 * @return \Illuminate\Http\Response
	 */
	public function getVersion() 
	{

		$android = Config('versionchecker.android_version');
		$ios = Config('versionchecker.ios_version');

		return response()->json([
			'android' => $android,
			'ios' => $ios
		]);


	}

}
