<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PRAXXYS\Admin\Http\Controllers\ResourceController;

use App\RoomType;

class RoomTypeController extends ResourceController
{
    # This line enables the resource authentication system:
    public static $guardResource = 'room_types';

    # Link the Class Model so the controller can reference it:
    public $model = RoomType::class;

}
