<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PRAXXYS\Admin\Http\Controllers\ResourceController;

use App\ExchangeRate;

class ExchangeRateController extends ResourceController
{
    # This line enables the resource authentication system:
    public static $guardResource = 'exchange_rates';

    # Link the Class Model so the controller can reference it:
    public $model = ExchangeRate::class;

}
