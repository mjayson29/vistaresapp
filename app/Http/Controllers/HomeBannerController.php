<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\HomeBannerStorePost;

use App\HomeBanner;

class HomeBannerController extends Controller
{
    public static $guardResource = 'home-banner';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = HomeBanner::first();
        return view('pages.admin.home-banner.index', [
            'banner' => $banner
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.home-banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HomeBannerStorePost $request)
    {
        $vars = $request->except(['image_path']);

        if($request->hasFile('image_path')) {
            $vars['image_path'] = $request->file('image_path')->store('banner-image', 'public');
        }

        /** Begins transaction */
        \DB::beginTransaction();

            /** Create Home banner */
            $banner =  HomeBanner::create($vars);

        /** End transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash('Home banner has been successfully created!', 'Success!', 'success');        

        /** Return to project index */
        return redirect()->route('admin.home-banner.index');

        
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = HomeBanner::find($id);

        return view('pages.admin.home-banner.edit', [
            'banner' => $banner
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HomeBannerStorePost $request, $id)
    {

        /** Find Banner*/
        $banner = HomeBanner::find($id);
        $vars = $request->except(['image_path']);

        if($request->hasFile('image_path')) {

            if($banner->image_path) {
                /** Delete file/image in storage */
                \Storage::delete('public/'. $banner->image_path);                
            }
           
            $vars['image_path'] = $request->file('image_path')->store('banner-image', 'public');
        }

        /** Begin transaction */
        \DB::beginTransaction();

            /** Update banner */
            $banner->update($vars);

        /** Commit transaction */
        \DB::commit();


        /** Flash message */
        \PRXAdmin::view()->flash($banner->title .' has been successfully updated!', 'Success!', 'success');                

        return back();        

    }
}
