<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FloorLayoutStorePost;

use App\Project;
use App\FloorLayout;


class FloorLayoutController extends Controller
{
    public static $guardResource = 'floor-layout';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project, $name)
    {

        $floors = FloorLayout::where('project_id', $project->id)->get();

        return view('pages.admin.projects.floor-layout.index', [
            'project' => $project,
            'floors' => $floors,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project, $name)
    {

        $types = FloorLayout::getTypes();

        return view('pages.admin.projects.floor-layout.create', [
            'project' => $project,
            'types' => $types,
            'floors' => $project->building_layouts
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FloorLayoutStorePost $request, Project $project)
    {
        $vars = $request->except([]);
        $vars['project_id'] = $project->id;

        if(FloorLayout::hasFloorLayout($project->id, $request->type)) {
            $vars['status'] = 0;
        } else {
            $vars['status'] = 1;
        }

        if($request->hasFile('image_path')) {
            $vars['image_path'] = $request->file('image_path')->store('floor-layouts', 'public');
        }

        /** Start transaction */
        \DB::beginTransaction();

            /** Store Floor Layout */
            $floor = FloorLayout::create($vars);

        /** End Transaction */
        \DB::commit();


        /** Flash message */
        \PRXAdmin::view()->flash($floor->name .' has been successfully created!', 'Success!', 'success');        

        /** Return to project index */
        return redirect()->route('admin.floor-layout.index', [$project->id, $project->name]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($project, FloorLayout $floor)
    {
        $types = FloorLayout::getTypes();
        $project = Project::find($project);

        return view('pages.admin.projects.floor-layout.edit', [
            'floor' => $floor,
            'types' => $types,
            'layouts' => $project->building_layouts
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FloorLayoutStorePost $request, $project, FloorLayout $floor)
    {   
        $vars = $request->except(['image_path', 'status']);

        if(FloorLayout::hasFloorLayout($project, $request->type, $floor->id)) {
            $vars['status'] = 0;
        } else {
            $vars['status'] = 1;
        }

        if($request->hasFile('image_path')) {

            /** Remove Image from Storage */
            if($floor->image_path) {
                \Storage::delete('public/'. $floor->image_path);
            }

            $vars['image_path'] = $request->file('image_path')->store('floor-layouts', 'public');

        }

        /** Start Transaction */
        \DB::beginTransaction();

            if($request->status) {
                /** Set floor layout to Active */
                FloorLayout::deactivateActiveStatus($floor->type, $floor->project_id, $floor->id);
                $vars['status'] = 1;
            }

            $floor->update($vars);

        /** End Transaction */
        \DB::commit();


        /** Flash message */
        \PRXAdmin::view()->flash($floor->name .' has been successfully updated!', 'Success!', 'success');        

        return back();

    }   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $floor = FloorLayout::find($id);

        /** Begin Transaction */
        \DB::beginTransaction();

            /** Remove from storage */
            \Storage::delete('public/'. $floor->image_path);

            /** Soft Delete floor */
            $floor->delete();        

        /** End Transaction */
        \DB::commit();

        /** Flash message */
        \PRXAdmin::view()->flash('A floor layout has been successfully archived!', 'Success!', 'success');                

        return back();        

    }
}
