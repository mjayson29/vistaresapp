<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AmenityStorePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $validate = '';

        if($this->request->get('_method') != 'PUT') {
            $validate = 'required';
        }

        $post = [];

        $post = [
            'project_id' => 'required',
            'name' => 'required',
            'image_path' => $validate . '|image|max:5000'
        ];

        return $post;

    }

    /**
     * Return array of messages
     * 
     * @return Array
     */
    public function messages()
    {

        $messages = [];

        $messages = [
            'project_id.required' => 'Project is required',
            'name.required' => 'Name is required',
            'image_path.required' => 'Image is required',
            'image_path.max' => 'Image must be less than 5mb',
            'image_path.image' => 'Image must be jpeg, jpg, bmp or png',
        ];

        return $messages;

    }
}
