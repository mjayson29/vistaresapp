<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectStorePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $post = [];

        $post = [
            'name' => 'required',
            'description' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'footer' => 'required',
            'image_path' => 'required|image|max:5000',
            'front_outside_image' => 'required|image|max:5000',
            'back_outside_image' => 'required|image|max:5000'            
        ];

        return $post; 
    }

    /**
     * Return array of messages
     * 
     * @return Array
     */
    public function messages() 
    {
        $messages = [];

        $messages = [
            'name.required' => 'Project name is required',
            'description.required' => 'Project description is required',
            'address' => 'Project address is required',
            'latitude' => 'Latitude is required',
            'longitude' => 'Longitude is required',
            'footer' => 'Footer is required',
          
            'image_path.required' => 'Project Image is required',
            'image_path.max' => 'Project Image must be less than 5mb',
            'image_path.image' => 'Project Image must be jpeg, jpg, bmp or png',

            'front_outside_image.required' => 'Outside view (Front View) image is required',
            'front_outside_image.max' => 'Outside view (Front View) image must be less than 5mb',
            'front_outside_image.image' => 'Outside view (Front View) image must be jpeg, jpg, bmp or png',

            'back_outside_image.required' => 'Outside view (Bront View) image is required',
            'back_outside_image.max' => 'Outside view (Bront View) image must be less than 5mb',
            'back_outside_image.image' => 'Outside view (Bront View) image must be jpeg, jpg, bmp or png',


        ];

        return $messages;
    }
}
