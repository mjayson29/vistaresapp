<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FloorLayoutStorePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $validate = '';

        if($this->request->get('_method') != 'PUT' || $this->request->get('image_path') != null) {
            $validate = 'required';
        }

        $post = [
            'name' => 'required',
            'type' => 'required',
            'image_path' => $validate. '|image|max:5000'
        ];

        return $post;
    }
    /**
     * Return array of messages
     * 
     * @return Array
     */
    public function messages()
    {

        $messages = [];

        $messages = [
            'project_id.required' => 'Project is required',
            'name.required' => 'Floor Name is required',
            'type' => 'Floor Type is required',
            'image_path.required' => 'Floor Layout image is required',
            'image_path.max' => 'Floor Layout image must be less than 5mb',
            'image_path.image' => 'Floor Layout image must be jpeg, jpg, bmp or png',
        ];

        return $messages;
    }
}
