<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UnitStorePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->request->get('_method') != 'PUT') {
            $validate = 'required';
        }

        $post = [
            'unit_number' => 'required',
            'name' => 'required',
            'room_type_id' => 'required',
            'x_coordinates' => 'required',
            'y_coordinates' => 'required',
            'description' => 'required',
            'image_path' => $validate. '|image|max:5000'
        ];

        return $post;

    }

    /**
     * Return array of messages
     * 
     * @return Array
     */
    public function messages() 
    {

        $messages = [
            'unit_number.required' => 'Unit number is required',
            'name.required' => 'Unit Name is request',
            'room_type_id.required' => 'Unit Type is Required',
            'x_coordinates.required' => 'Please select from the floor layout',
            'y_coordinates.required' => 'Please select from the floor layout',
            'description.required' => 'Unit description is required',
            'image_path.required' => 'Image is required',
            'image_path.max' => 'Image must be less than 5mb',
            'image_path.image' => 'Image must be jpeg, jpg, bmp or png',
        ];

        return $messages;

    }

}
