<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GeneralPageStorePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $post = [];

        $post = [
            'name' => 'required',
        ];

        return $post; 
    }

    /**
     * Return array of messages
     * 
     * @return Array
     */
    public function messages() 
    {
        $messages = [];

        $messages = [
            'name.required' => 'General page name is required',
        ];

        return $messages;
    }
}
