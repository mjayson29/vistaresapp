<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuildingLayout extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'project_id', 'floor', 'type',
        'width', 'height', 'description',
    	 'x_coordinates', 'y_coordinates'
    ];

     /**
     * Constant Variables
     * 
     */
    const COMMERCIAL = 1;
    const PARKING = 2;
    const RESIDENTIAL = 3;

    public function project() {
    	return $this->belongsTo(Project::class);
    }

    public function floor_layout()
    {
      return $this->hasOne(FloorLayout::class);
    }


     /**
     * Get Floor Types
     * @return Collection
     */
    public static function getTypes() 
    {

        $types = collect([
            ['name' => 'Commercial', 'value' => BuildingLayout::COMMERCIAL],
            ['name' => 'Parking', 'value' => BuildingLayout::PARKING],
            ['name' => 'Residential', 'value' => BuildingLayout::RESIDENTIAL],
        ]);

        return $types;

    }
}
