-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: uat-db1.praxxys.ph    Database: vista_db
-- ------------------------------------------------------
-- Server version	10.1.26-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_avatars`
--

DROP TABLE IF EXISTS `admin_avatars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_avatars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_avatars_admin_id_index` (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_avatars`
--

LOCK TABLES `admin_avatars` WRITE;
/*!40000 ALTER TABLE `admin_avatars` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_avatars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_logs`
--

DROP TABLE IF EXISTS `admin_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_logs_admin_id_index` (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_logs`
--

LOCK TABLES `admin_logs` WRITE;
/*!40000 ALTER TABLE `admin_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_password_resets`
--

DROP TABLE IF EXISTS `admin_password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `admin_password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_password_resets`
--

LOCK TABLES `admin_password_resets` WRITE;
/*!40000 ALTER TABLE `admin_password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable` tinyint(1) NOT NULL DEFAULT '1',
  `is_verified` tinyint(1) NOT NULL DEFAULT '0',
  `verify_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`),
  UNIQUE KEY `admins_verify_token_unique` (`verify_token`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'Default','Admin','admin@praxxys.ph','$2y$10$hsbW6WRmIZs/3x0Js9sN2.sAJmENpMtT8zXOSkLFi.cf0qtWMntQW',1,1,'d7346be6f6706d84141f1fd82869b57b34b20ef0c496618c24f2461a682a157894f06530a1a6265e34ee9ae1642781c0cf44c6510b5b7453f063cff89b4cbdeb',NULL,NULL,'2019-03-20 05:46:47','2019-03-20 05:46:47'),(7,'John','Doe','daryl@praxxys.ph','$2y$10$tvWn4/NFoZUeaXWNWXcQ1ONYzaceM1qThqeIqWFREns5QUMuYURG.',1,0,NULL,'QTrZbxU23a2SseDihNeWFqRh0TcRLIsyEKFWAjTxGwEQslmJra7aAXFgFVZM',NULL,'2019-04-02 03:02:53','2019-04-02 03:04:30');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amenities`
--

DROP TABLE IF EXISTS `amenities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amenities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `amenities_project_id_index` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amenities`
--

LOCK TABLES `amenities` WRITE;
/*!40000 ALTER TABLE `amenities` DISABLE KEYS */;
INSERT INTO `amenities` VALUES (1,1,'Gym','Plumeria Heights Gym','amenities/Bqot41MeA116guJYT0J3vjZO78WVIwm1Urf6KNrc.jpeg',NULL,'2019-03-20 05:53:56','2019-03-22 04:04:57'),(2,1,'Study Hall','Plumeria Heights Study Hall','amenities-image/VTZTVLWgWNDG6LpXbCDfxl8fX0dIrCv8ZNGQnDzf.jpeg',NULL,'2019-03-20 05:54:11','2019-03-31 06:15:26'),(3,1,'Pool','Plumeria Heights Pool Area','amenities-image/1YDPMyXL2axFvf40lM6mE7V6rCbm2YbBlFUoLLzV.jpeg',NULL,'2019-03-20 05:54:25','2019-03-22 04:05:04'),(4,1,'Function Room','Plumeria Heights Function Room Plumeria Heights Function Room Plumeria Heights Function Room Plumeria Heights Function Room Plumeria Heights Function Room','amenities-image/zqtfn2rxu6LDMGbVG47VlKDvrz6TETPz5NuMVnvV.jpeg',NULL,'2019-03-20 05:54:40','2019-04-29 03:52:06'),(5,2,'Gym','Bradbury Heights Gym','amenities-image/XpYakO2xv3HDPh4SmWuGYeFP97Nt0MbP6lsgmpFC.jpeg',NULL,'2019-03-20 05:56:21','2019-03-22 05:18:59'),(6,2,'Pool','Bradbury Heights Pool','amenities-image/ZJgC4qliKqFEuMk64QcC0Erfuc6wfG5uZpXnV8Il.jpeg',NULL,'2019-03-20 05:56:31','2019-03-22 05:17:38'),(7,3,'Gym','Hawthrone Heights Gym','amenities-image/U2BdnXPbuhlSsRtKKjSV64Sxay7tUGP5qPNb9tYW.jpeg',NULL,'2019-03-20 05:57:13','2019-03-22 05:18:48'),(8,3,'Pool','Hawthrone Heights Pool','amenities-image/FQmcy5k6uzq5okYKT2geQ23f8RO7fVVSN4YWQ6kX.jpeg',NULL,'2019-03-20 05:57:38','2019-03-22 05:17:54');
/*!40000 ALTER TABLE `amenities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `building_layouts`
--

DROP TABLE IF EXISTS `building_layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `building_layouts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `floor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL,
  `width` tinyint(4) NOT NULL,
  `height` tinyint(4) NOT NULL,
  `x_coordinates` double(8,2) NOT NULL,
  `y_coordinates` double(8,2) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `building_layouts_project_id_index` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `building_layouts`
--

LOCK TABLES `building_layouts` WRITE;
/*!40000 ALTER TABLE `building_layouts` DISABLE KEYS */;
INSERT INTO `building_layouts` VALUES (2,1,'14th - 40th',3,13,50,40.37,13.19,'Plumeria Heights Typical Residential Floors','2019-03-20 06:07:40','2019-04-01 10:50:27'),(5,2,'9th - 36th',3,14,54,38.02,11.50,'Residential Floors','2019-03-20 06:20:39','2019-03-31 05:37:29'),(6,3,'11th-42nd',3,12,55,40.37,14.88,'Residential Floors','2019-03-20 06:24:10','2019-03-31 07:44:35');
/*!40000 ALTER TABLE `building_layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commercial_areas`
--

DROP TABLE IF EXISTS `commercial_areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commercial_areas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `commercial_areas_project_id_index` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commercial_areas`
--

LOCK TABLES `commercial_areas` WRITE;
/*!40000 ALTER TABLE `commercial_areas` DISABLE KEYS */;
INSERT INTO `commercial_areas` VALUES (7,1,'Coffee Project',NULL,'commercial-areas-image/JSsZURdLx0PUPM73Xj1ct0PHKMt30bQ6TuGbQmZw.jpeg',NULL,'2019-04-22 06:26:26','2019-04-22 06:29:11'),(8,1,'All Day Convenience Store',NULL,'commercial-areas-image/5GWGh8EKvV5ufDyMTo21zvvjWktOLPgmoFcdlUCl.jpeg',NULL,'2019-04-22 07:28:05','2019-04-22 07:28:05'),(9,2,'Coffee Project',NULL,'commercial-areas-image/RlAsoePCpbRJ3fuYHlS9U4Jyx1H8qvJaWdgGwtVG.jpeg',NULL,'2019-04-22 07:28:26','2019-04-22 07:28:26'),(10,2,'All Day Convenience Store',NULL,'commercial-areas-image/RmZIM776VxL1tp236wSo5mKCj9nxL9bM7LtlMYz7.jpeg',NULL,'2019-04-22 07:28:38','2019-04-22 07:28:38'),(11,3,'Coffee Project',NULL,'commercial-areas-image/gvpJxnbIPVqkdI2FLkRcRKzUs4dXTGuK211zyQk0.jpeg',NULL,'2019-04-22 07:29:03','2019-04-22 07:29:03'),(12,3,'All Day Convenience Store',NULL,'commercial-areas-image/w3kIa6qcgFrUrv01rB5EZZxFqmhGQEc2k2fPIMpv.jpeg',NULL,'2019-04-22 07:29:14','2019-04-22 07:29:14');
/*!40000 ALTER TABLE `commercial_areas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exchange_rates`
--

DROP TABLE IF EXISTS `exchange_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exchange_rates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exchange_rate_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(9,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exchange_rates`
--

LOCK TABLES `exchange_rates` WRITE;
/*!40000 ALTER TABLE `exchange_rates` DISABLE KEYS */;
INSERT INTO `exchange_rates` VALUES (1,'US DOLLAR','USD',52.00,'2019-03-20 05:46:47','2019-03-20 05:46:47',NULL),(2,'EURO','EURO',59.00,'2019-03-20 05:46:47','2019-03-20 05:46:47',NULL),(3,'JAPAN','YEN',0.47,'2019-03-20 05:46:47','2019-03-20 05:46:47',NULL),(4,'SRI LANKA','RUPEE',0.30,'2019-03-20 05:46:47','2019-03-20 05:46:47',NULL),(5,'SINGAPORE','SGD',39.00,'2019-03-31 04:27:24','2019-03-31 04:27:46',NULL),(6,'HONGKONG','HKD',7.00,'2019-03-31 04:28:20','2019-03-31 04:28:20',NULL);
/*!40000 ALTER TABLE `exchange_rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `floor_layouts`
--

DROP TABLE IF EXISTS `floor_layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `floor_layouts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `building_layout_id` int(10) unsigned NOT NULL,
  `type` tinyint(4) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `floor_layouts_project_id_index` (`project_id`),
  KEY `floor_layouts_building_layout_id_index` (`building_layout_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `floor_layouts`
--

LOCK TABLES `floor_layouts` WRITE;
/*!40000 ALTER TABLE `floor_layouts` DISABLE KEYS */;
INSERT INTO `floor_layouts` VALUES (2,1,2,2,'14th - 40th Floor','Plumeria Heights Typical Residential Floors','floor-layouts/vrnHiZCEn9S7LHRmw1Wm5gVc1q1fOpLEIIeajSLB.jpeg',1,'2019-03-20 06:15:48','2019-04-01 10:51:02'),(3,2,5,2,'Residential Floors','Typical Floor Layout','floor-layouts/lQxxQNytDRntFX8abCNw4Gk0CWvoelH0J4nmQQZP.jpeg',1,'2019-03-20 06:21:12','2019-03-31 06:42:23'),(4,3,6,2,'11th - 42nd Floor','Residential','floor-layouts/DOgFoRvwatcf4hkU6onvIGhGr6jVdyB4Guoeh7Ar.jpeg',1,'2019-03-20 06:25:05','2019-03-31 07:45:29');
/*!40000 ALTER TABLE `floor_layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `galleries_project_id_index` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
INSERT INTO `galleries` VALUES (19,1,'Plumeria Heights_room','gallery/KQ5T07XPDZSdQukIPU9qLEu31C4dpDk4FlAB05hY.jpeg','2019-04-22 06:34:47','2019-04-22 06:34:47'),(20,1,'Plumeria Heights_Coffee Project','gallery/qDVQuQyZswPdiZ73U7ReNSu7cIEVSFFBobSyWH2P.jpeg','2019-04-22 06:35:10','2019-04-22 06:35:10'),(21,1,'Plumeria Heights_room_2','gallery/yWoJ05AOKIE1FHHirsQVWrZ3mOjqa6e6POyqJUK2.jpeg','2019-04-22 06:35:46','2019-04-22 06:35:46'),(22,1,'Plumeria Heights_room_3','gallery/iAfPIXJx4kbiYhKbW9UPJqwqFZm0kctyu9KKlTDj.jpeg','2019-04-22 06:38:22','2019-04-22 06:38:22'),(23,1,'Plumeria Heights_Coffee Project','gallery/zcCXevC0ZMW58Fuo0h1TKPcAKTwini6gDeG32gT0.jpeg','2019-04-22 06:38:58','2019-04-22 06:38:58'),(24,1,'Plumeria Heights_room_4','gallery/qh9h0EIJ4WmKqdVYNnU0m5C4TxnqHfVISz5qMxaL.jpeg','2019-04-22 06:39:17','2019-04-22 06:39:17'),(25,2,'Bradbury Heights_pool','gallery/RVVasT9qfDs6xBMwVKDmzCSRoAaWjud2dT24agAN.jpeg','2019-04-22 06:44:41','2019-04-22 06:44:41'),(26,2,'Bradbury Heights_room_1','gallery/RdDQVKc0Yece5jhuq56mJWjKBrk1AcK0UpeIDssD.jpeg','2019-04-22 06:44:58','2019-04-22 06:44:58'),(27,2,'Breadbury Heights_study area','gallery/vypMb6AjjjWyinoL9lunsewnrKvU9wcIzM9c9cXf.jpeg','2019-04-22 06:45:16','2019-04-22 06:45:16'),(28,2,'Bradbury Heights_srudy area_2','gallery/38OEZ3EFWu7llqJKVV4PB07B0qvyURg4CeIv5tDU.jpeg','2019-04-22 06:45:33','2019-04-22 06:45:33'),(29,2,'Bradbury Heights_study area_3','gallery/hzh2iRYWix4Xrr7ttLL3T0LVoN9qJvxV6X37yxEZ.jpeg','2019-04-22 06:46:26','2019-04-22 06:46:26'),(30,2,'Bradbury Heights_study area_4','gallery/QSd86IL4THWiemFaruRqWhon5BXThN8bXHSRm9QA.jpeg','2019-04-22 06:48:06','2019-04-22 06:48:06'),(31,3,'Hawthorne Heights_lobby','gallery/rQcKfSt2O8y8aTEms8Q8wt4vS942RLSgPVtM0bdl.jpeg','2019-04-22 06:51:54','2019-04-22 06:51:54'),(32,3,'Hawthorne Heights_room_1','gallery/85WT0RZWOI1YUPUUOhjSXcTYk9TfVLhnsuDcnnNr.jpeg','2019-04-22 06:52:07','2019-04-22 06:52:07'),(33,3,'Hawthorne Heights_study area_1','gallery/IGBNLf7kVpcwyQ7BCJetqbvpZ5BSw6Me14TsDpK0.jpeg','2019-04-22 06:52:22','2019-04-22 06:52:22'),(34,3,'Hawthorne Heights_roof deck_1','gallery/ZkKPa9rH5N99klBEz3TRILPQMJk03FMtbkyiRXkc.jpeg','2019-04-22 06:52:36','2019-04-22 06:52:36'),(35,3,'Hawthorne Heights_roof deck_2','gallery/HuCO25OSvKBRCkaVV64tnPLhyngEmGXe3cWuUh2E.jpeg','2019-04-22 06:52:53','2019-04-22 06:52:53'),(36,3,'Hawthorne Heights_pool','gallery/lS73hhFeoOJSqMn4teD8RcOsb2yZFl60Sa3TQd0d.jpeg','2019-04-22 06:53:08','2019-04-22 06:53:08');
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `general_page_items`
--

DROP TABLE IF EXISTS `general_page_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `general_page_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `general_page_id` int(10) unsigned NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `general_page_items_general_page_id_index` (`general_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `general_page_items`
--

LOCK TABLES `general_page_items` WRITE;
/*!40000 ALTER TABLE `general_page_items` DISABLE KEYS */;
INSERT INTO `general_page_items` VALUES (1,1,'<p><strong>CHANGES TO THIS TERMS OF USE</strong><br><br>Vista Residences, Inc.&nbsp; reserves the right to change this Terms of Use from time to time, in whole or in part, without notice to you. You should check back often so you are aware of your current rights and responsibilities. Your continued use of this app after changes to the Terms of Use have been published constitutes your binding acceptance of the updated Terms of Use. If at any time the Terms of Use are not acceptable to you, you should immediately cease all use of this app.<br><br><strong>TRADEMARKS</strong><br><br>The trademarks, logos, and service marks displayed on this app are the property of Vista Residences, Inc. &nbsp;or their respective owners. You are not permitted to use these items without the prior written consent of Vista Residences, Inc.&nbsp; or their respective owners.<br><br><strong>COPYRIGHTS</strong><br><br>Vista Residences, Inc.&nbsp; either owns the intellectual property rights in the content that is made available on this app, or has obtained the permission of the owner to make it available on this app. Vista Residences, Inc. &nbsp;strictly prohibits the redistribution or copying of this app, in whole or in part, without written permission from Vista Residences, Inc. &nbsp;Vista Residences, Inc.&nbsp; authorizes you to display on your computer, download, or print the pages from this app provided:</p><ul><li>The copyright notice appears on all printouts.</li><li>The information is intact and will not be altered in any way.</li><li>The content is used for personal, educational, or non-commercial use only.</li><li>You do not redistribute or copy the information to any other media.</li></ul><p>Vista Residences, Inc.&nbsp; respects the intellectual property of others. If you believe that your work has been copied in a way that constitutes copyright infringement, please contact us immediately at the contact information provided below.</p><p><strong>LINKS TO OTHER SITES</strong></p><p>This app contains link to other apps and are provided for convenience only. Vista Residences, Inc.&nbsp; is not responsible for the content or availability of any linked apps. The inclusion of any link to a app does not imply endorsement by Vista Residences, Inc. of the app or their owners, products or services.<br><br><strong>SUBMISSION OF IDEAS</strong><br><br>Vista Residences, Inc. or any of its employees do not accept or consider unsolicited ideas, including ideas for new advertising campaigns, new promotions, new products or technologies, processes, materials, marketing plans, or new product names. Please do not send any original creative artwork, samples, demos, or other works. The sole purpose of this policy is to avoid potential misunderstandings or disputes when Vista Residences, Inc. products, services, or marketing strategies might seem similar to ideas submitted to Vista Residences, Inc. Please do not send your unsolicited ideas to Vista Residences, Inc. or anyone at Vista Residences, Inc. If, despite our request, you still send us your ideas or materials, please understand that Vista Residences, Inc. makes no assurances that your ideas or materials will be treated as confidential or proprietary.<br><br><strong>RULES OF CONDUCT</strong><br><br>Your use of this app is subject to all applicable local, national and international laws and regulations, and you agree not to violate such laws and regulations. Any attempt by any person to deliberately damage this app is a violation of criminal and civil laws. Vista Residences, Inc. reserves the right to seek damages from any such person to the fullest extent permitted by law.<br><br>In addition, you agree not to post or transmit through this app any material or content that violates or infringes in any way the rights of others or solicits, encourages or promotes the use of illegal substances or activities, which is unlawful, threatening, abusive, harassing, defamatory, libelous, derogatory, invasive of privacy or publicity rights, vulgar, obscene, bigoted or hateful, profane, scandalous, pornographic, indecent or otherwise objectionable, gives rise to civil or criminal liability or otherwise violates any applicable law. You may not engage in any activity on this app that restricts or inhibits any other user from using or enjoying this app by \"hacking\", \"cracking\", \"spoofing\", or defacing any portions of this app.<br><br>You may not post or transmit through this app advertising or commercial solicitations; promotional materials relating to app or online services which are competitive with Vista Residences, Inc.and/or this app; software or other materials that contain viruses, worms, time bombs, Trojan horses, or other harmful or disruptive components, political campaign materials; chain letters; mass mailings, spam mail, any robot, spider, site search/retrieval application, or other manual or automatic device or process to retrieve, index, \"data mine\", or in any way reproduce or circumvent the navigational structure or presentation of this app or its contents. You may not harvest or collect information about app visitors without their express written consent.</p><p><strong>GLOBAL AVAILABILITY</strong></p><p>Vista Residences, Inc. controls this app from its Philippine offices. If you use this app from other locations you are responsible for compliance with applicable local laws. Vista Residences, Inc. makes no representation that the products and services referenced herein are appropriate, or available, worldwide and in fact certain products and services may not be available worldwide.<br><br><strong>ACCURACY, COMPLETENESS AND TIMELINESS OF INFORMATION</strong><br><br>Vista Residences, Inc. is not responsible if the information that is made available on this app is not accurate or complete. Any reliance upon the material on this app shall be at your own risk. You agree that it is your responsibility to monitor any changes to the material and the information contained on this app.<br><br><strong>DISCLAIMER</strong><br><br>THIS APP AND ALL CONTENT, MATERIALS, INFORMATION, SOFTWARE, PRODUCTS AND SERVICES ARE PROVIDED ON AN \"AS IS\" AND \"AS AVAILABLE\" BASIS. YOUR USE OF THIS APP IS AT YOUR OWN RISK. VISTA RESIDENCES INC MAKES NO WARRANTIES OR REPRESENTATIONS, EXPRESSED OR IMPLIED, AS TO THE FUNCTIONALITY OR USEFULNESS OF THIS APP OR ANY CONTENT. VISTA RESIDENCES INC DISCLAIMS ALL WARRANTIES, EXPRESSED OR IMPLIED, INCLUDING WITHOUT LIMITATION WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. VISTA RESIDENCES INC DISCLAIMS LIABILITY FOR ANY DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL, EXEMPLARY, PUNITIVE OR OTHER DAMAGES, OR LOST PROFITS, THAT MAY RESULT, DIRECTLY OR INDIRECTLY, FROM YOUR USE OF THIS APP OR ANY CONTENT, INCLUDING WITHOUT LIMITATION ANY DAMAGE TO COMPUTER SYSTEMS, HARDWARE OR SOFTWARE, LOSS OF DATA, OR ANY OTHER PERFORMANCE FAILURES, OR ANY ERRORS, BUGS, VIRUSES OR OTHER DEFECTS THAT RESULT FROM OR ARE ASSOCIATED WITH THE USE OF THIS APP OR ANY CONTENT. VISTA RESIDENCES INC MAKES NO WARRANTY THAT, (I) THE SERVICE WILL BE UNINTERRUPTED, TIMELY, SECURE, OR ERROR FREE, (II) THE RESULTS THAT MAY BE OBTAINED FROM THE USE OF THE SERVICE WILL BE ACCURATE OR RELIABLE, (III) THE QUALITY OF ANY PRODUCTS, SERVICES, CONTENT, INFORMATION, OR OTHER MATERIALS PURCHASED OR OBTAINED BY YOU THROUGH THE APP WILL MEET YOUR EXPECTATIONS, (IV) ANY ERRORS IN THE SOFTWARE WILL BE CORRECTED, OR THAT THIS APP, ITS CONTENT, AND THE SERVER ON WHICH THE APP AND CONTENT ARE AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. ANY MATERIAL (INCLUDING CONTENT) DOWNLOADED OR OBTAINED THROUGH THE USE OF THIS APP IS DONE AT YOUR OWN RISK AND YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF ANY MATERIAL. INFORMATION CREATED BY THIRD PARTIES THAT YOU MAY ACCESS ON THIS APP OR THROUGH LINKS IS NOT ADOPTED OR ENDORSED BY VISTA RESIDENCES INC AND REMAINS THE RESPONSIBILITY OF THE THIRD PARTY.</p><p><strong>LIMITATION OF LIABILITY</strong><br><br>TO THE FULLEST EXTENT PERMITTED BY LAW, VISTA RESIDENCES INC IS NOT LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, SPECIAL, INCIDENTAL, CONSEQUENTIAL, OR EXEMPLARY DAMAGES (INCLUDING, WITHOUT LIMITATION, LOSS OF BUSINESS, REVENUE, PROFITS, GOODWILL, DATA, ELECTRONICALLY TRANSMITTED ORDERS, OR OTHER ECONOMIC ADVANTAGE) ARISING OUT OF OR IN CONNECTION WITH THE APP, EVEN IF VISTA RESIDENCES INC HAS PREVIOUSLY BEEN ADVISED OF, OR REASONABLY COULD HAVE FORESEEN, THE POSSIBILITY OF SUCH DAMAGES, HOWEVER THEY ARISE, WHETHER IN BREACH OF CONTRACT OR IN TORT (INCLUDING NEGLIGENCE), INCLUDING WITHOUT LIMITATION DAMAGES DUE TO (a) THE USE OF OR THE INABILITY TO USE THE APP; (b) THE COST OF PROCUREMENT OF SUBSTITUTE GOODS AND SERVICES RESULTING FROM ANY GOODS, DATA, INFORMATION OR SERVICES PURCHASED OR OBTAINED OR MESSAGES RECEIVED OR TRANSACTIONS ENTERED INTO, THROUGH OR FROM THE APP; ( c) STATEMENTS OR CONDUCT OF ANY THIRD PARTY ON THE APP, INCLUDING WITHOUT LIMITATION UNAUTHORIZED ACCESS TO OR ALTERATION OF TRANSMISSIONS OR DATA, MALICIOUS OR CRIMINAL BEHAVIOR, OR FALSE OR FRAUDULENT TRANSACTIONS, OR (d) CONTENT OR INFORMATION YOU MAY DOWNLOAD, USE, MODIFY OR DISTRIBUTE. TO THE EXTENT ANY JURISDICTION DOES NOT ALLOW THE EXCLUSION OR LIMITATION OF DIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES, PORTIONS OF THE ABOVE LIMITATION OR EXCLUSION MAY NOT APPLY.</p><p><strong>JURISDICTION</strong></p><p>This agreement and all claims relating to the relationship between the parties are governed by the laws of the Republic of the Philippines.</p><p>If one or more of the provisions contained in this Agreement is held invalid, illegal or unenforceable in any respect by any court of competent jurisdiction, such holding will not impair the validity, legality, or enforceability of the remaining provisions.</p><p><strong>HOW TO CONTACT US</strong></p><p>If you have any questions or concerns about this Terms of Use please do not hesitate in contacting us:</p><p><strong>Address:</strong></p><p>Head Office</p><p>UGF Worldwide Corporate Centre Shaw Boulevard Mandaluyong City</p><p>&nbsp;</p>','2019-03-21 07:42:48','2019-04-08 02:20:23'),(2,2,'<p><strong>PERSONAL INFORMATION COLLECTED</strong><br>Vista Residences, Inc.. (the “Company”) collects Personal Information upon inquiry.</p><p>“Personal Information” may include, but not limited to, your name, gender, birthday, marital status, employment status/employer, social security/tax identification number, home address, e-mail address, contact information and other information from which your identity is apparent or can reasonably and direct ascertained. It may also include certain technical information, such us, but not limited to, IP addresses, internet browser used, and web pages accessed.<br><br><strong>YOUR CONSENT</strong><br>By using this mobile application (the “App”), you are consenting to the collection, use and disclosure of your Personal Information by the Company.</p><p>&nbsp;</p><p><strong>USE OF PERSONAL INFORMATION</strong><br>We use Personal Information to provide you with details and information regarding our products and services.</p><p>The Company may disclose your Personal Information to third parties under any of the following circumstances: (i) required by law or by court decisions/processes; (ii) to provide services, conduct billing processing, and other business transactions; (iii) for information, update and marketing purposes; and (iv) for research purposes.</p><p>&nbsp;</p><p><strong>OUR COMMITMENT TO CHILDREN’S ONLINE PRIVACY</strong><br>The Company does not knowingly accept, collect or solicit Personal Information from children under the age of 13. In the event that Personal Information was inadvertently collected from children under the age of 13, parents may either give consent to the collection, use and disclosure of the child’s personal information or request the removal of the child’s personal information, by contacting the Company.</p><p><br><strong>CHANGES TO THIS PRIVACY POLICY</strong><br>The Company reserves the right to change this Privacy Policy from time to time. In case of changes, the revised Privacy Policy will be posted on the App and will take effect immediately.</p>','2019-03-21 07:42:48','2019-04-03 09:13:30'),(3,3,'<p>Welcome to the Vista Residences Interactive Map!</p>','2019-03-21 07:42:48','2019-03-27 01:20:22'),(4,3,'<p>Explore this app and learn more about us. Welcome and enjoy!</p>','2019-03-21 07:42:48','2019-03-27 01:20:38'),(5,3,'<p>Discover what your future Vista Residences home has to offer.</p>','2019-03-21 07:42:48','2019-03-27 01:20:41');
/*!40000 ALTER TABLE `general_page_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `general_pages`
--

DROP TABLE IF EXISTS `general_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `general_pages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `slug_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `general_pages`
--

LOCK TABLES `general_pages` WRITE;
/*!40000 ALTER TABLE `general_pages` DISABLE KEYS */;
INSERT INTO `general_pages` VALUES (1,'Terms and Condition','','terms_and_condition','2019-03-21 07:42:48','2019-03-21 07:42:48'),(2,'Privacy Policy','','privacy_policy','2019-03-21 07:42:48','2019-03-21 07:42:48'),(3,'Introduction','','introduction','2019-03-21 07:42:48','2019-03-21 07:42:48');
/*!40000 ALTER TABLE `general_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_banners`
--

DROP TABLE IF EXISTS `home_banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `home_banners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_banners`
--

LOCK TABLES `home_banners` WRITE;
/*!40000 ALTER TABLE `home_banners` DISABLE KEYS */;
INSERT INTO `home_banners` VALUES (1,'Vista Residences','banner-image/VbPXfz7aJOeFEKZt1srWEJMptAJnDZbruKSPzi99.jpeg','Vista Residences, the high-rise condominium arm of the country’s biggest property developer, Vista Land, is tweaking the concept of the university town by building\r\nvertical versions for the country’s key universities, lifting “higher learning” even higher.',NULL,'2019-03-20 06:00:28','2019-03-31 04:56:28');
/*!40000 ALTER TABLE `home_banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_06_01_000001_create_oauth_auth_codes_table',1),(4,'2016_06_01_000002_create_oauth_access_tokens_table',1),(5,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(6,'2016_06_01_000004_create_oauth_clients_table',1),(7,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(8,'2017_09_04_000000_create_admins_table',1),(9,'2017_09_04_000001_create_admin_password_resets_table',1),(10,'2017_09_04_000002_create_roles_and_permissions_tables',1),(11,'2017_09_04_000004_create_admin_avatars_table',1),(12,'2017_10_31_000001_create_notifications_table',1),(13,'2018_07_17_082035_create_admin_logs_table',1),(14,'2019_02_27_093617_create_projects_table',1),(15,'2019_02_27_095022_create_rooms_table',1),(16,'2019_02_28_004018_create_amenities_table',1),(17,'2019_02_28_005106_create_commercial_areas_table',1),(18,'2019_02_28_005734_create_home_sliders_table',1),(19,'2019_02_28_010229_create_sold_out_projects_table',1),(20,'2019_02_28_011241_create_general_pages_table',1),(21,'2019_02_28_011256_create_general_page_items_table',1),(22,'2019_03_06_012854_create_galleries_table',1),(23,'2019_03_12_231348_create_units_table',1),(24,'2019_03_12_231411_create_floor_layouts_table',1),(25,'2019_03_14_003325_create_building_layouts_table',1),(26,'2019_03_19_001345_create_exchange_rates_table',1),(27,'2019_03_20_011939_create_room_types_table',1),(28,'2019_03_25_023420_create_room_layouts_table',2),(29,'2019_03_25_065433_update_units_table',2),(30,'2019_03_29_012231_adding_column_units_table',3),(31,'2019_03_31_233539_change_description_to_nullable_in_room_types_table',4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` VALUES (2,'PRAXXYS\\Admin\\Models\\Admin',1),(4,'PRAXXYS\\Admin\\Models\\Admin',7);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) unsigned NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('0466c15c09ce49f6175ef06ee051f227c4be80f71f16e5d1ab1bca6849a8506dd41d4b6e96da24a7',1,2,NULL,'[\"*\"]',0,'2019-03-21 03:08:01','2019-03-21 03:08:01','2020-03-21 03:08:01'),('050697c498a820ef881e786921a96c5e8a9a87cfa4d1a1310e0bdbfee6ac2f0018e9a98e47be41c0',1,2,NULL,'[\"*\"]',0,'2019-03-26 07:02:51','2019-03-26 07:02:51','2020-03-26 07:02:51'),('0bb44f80c445829b2370fd69dada31cee8e30c40b65f26f2ca39bfdfe2dc0c3db1f1498fdce0ba19',1,2,NULL,'[\"*\"]',0,'2019-03-27 02:41:38','2019-03-27 02:41:38','2020-03-27 02:41:38'),('0e0ab50ceb65cd3711d70c4bf1bc721eaf46f71a3577732a0d7f1ffa284df819c85f0820019bdf51',1,2,NULL,'[\"*\"]',0,'2019-03-28 06:25:54','2019-03-28 06:25:54','2020-03-28 06:25:54'),('0ff7d92abbeefbd152a68564934f447cfc8c7556c31ac0b27a611d6200266164d7521ce5267e9d88',1,2,NULL,'[\"*\"]',0,'2019-04-01 06:31:32','2019-04-01 06:31:32','2020-04-01 06:31:32'),('124bfbe5865e159c4f77883326b3af8ed472ab506447ee0bf741382042d682a67c03c1c7251b8170',1,2,NULL,'[\"*\"]',0,'2019-03-21 07:55:06','2019-03-21 07:55:06','2020-03-21 07:55:06'),('13b133fb3c8af3bec431e78f4aacf6a6b9473da51580ea18324b890fd452fc5ffae4ee1ec7ce501c',1,2,NULL,'[\"*\"]',0,'2019-04-01 06:20:44','2019-04-01 06:20:44','2020-04-01 06:20:44'),('14d57bffa37e8cb901bde78c04c6f9ded28875ff96004eebda5154f2d5906916379886e3eced0caa',1,2,NULL,'[\"*\"]',0,'2019-03-27 01:14:47','2019-03-27 01:14:47','2020-03-27 01:14:47'),('157a34209b807c76fa5669144da3f483f380179643b3153438e5cec8c2b8ab3728b07fa49699de91',1,2,NULL,'[\"*\"]',0,'2019-04-01 06:30:25','2019-04-01 06:30:25','2020-04-01 06:30:25'),('165ba0525c7fe40de7ca3673ae09ad1cb3a70a0635e4caedc7ba637dcae3c4987b02821eae77b7c3',1,2,NULL,'[\"*\"]',0,'2019-03-27 01:20:13','2019-03-27 01:20:13','2020-03-27 01:20:13'),('18f68253c603fb1a4a1175e1587d886632b8529c10d6898a724faaaa796d1991aee6f89f749ed91b',1,2,NULL,'[\"*\"]',0,'2019-03-28 02:00:31','2019-03-28 02:00:31','2020-03-28 02:00:31'),('197493f122c84bb009caff59981bef3c8e946c3a156a64d122f284875e4183f0a66e142e1c90cdf6',1,2,NULL,'[\"*\"]',0,'2019-03-28 05:33:43','2019-03-28 05:33:43','2020-03-28 05:33:43'),('1dcbc1a9d3b8feb0c1caffd31f784bca8fc2a7d077cfb8d24dcfa549e24af7d32ea357145be7c1b7',1,2,NULL,'[\"*\"]',0,'2019-03-22 00:14:57','2019-03-22 00:14:57','2020-03-22 00:14:57'),('201294348d82be35fefc77d843a83999ec8d2920035b51c0f9328225438dc8a9ca0bb2c52cdb7105',1,4,NULL,'[\"*\"]',0,'2019-04-08 06:18:31','2019-04-08 06:18:31','2020-04-08 06:18:31'),('24b805a3e8aca0c839af641295465edab86994aa3ef339eb452e90b7afdce231aa019f676a66096e',1,2,NULL,'[\"*\"]',0,'2019-03-27 03:00:55','2019-03-27 03:00:55','2020-03-27 03:00:55'),('298e9c36576f244c1819fb9461f35e89fb8dbb30060d47fd7d82a97c3d3d56fe93b4fc039a5bbf07',1,2,NULL,'[\"*\"]',0,'2019-03-29 01:06:25','2019-03-29 01:06:25','2020-03-29 01:06:25'),('2f56125a2a784a436829800352abe072993478f334e933fabc7d3c81bd423960af103d9c5a713200',1,2,NULL,'[\"*\"]',0,'2019-03-27 08:01:03','2019-03-27 08:01:03','2020-03-27 08:01:03'),('34edefb65ddb2f6e7a81c64e45dc27d091bfed8c4ecaa26d05c8d7caec8a4327bbf444c53bd4b521',1,2,NULL,'[\"*\"]',0,'2019-03-26 07:07:46','2019-03-26 07:07:46','2020-03-26 07:07:46'),('444ba4f4ecf7bff9938f0cd4deaaf4e77d7366a2bc846521aaa7fc6e9203776f9f6b3ce4ab1d470a',1,2,NULL,'[\"*\"]',0,'2019-03-28 02:10:05','2019-03-28 02:10:05','2020-03-28 02:10:05'),('46d4fe7c77481e5cb6fed467052e889dc7cc7eda5e823326bbd9b39a8cfb9c136894ada784dd7576',1,2,NULL,'[\"*\"]',0,'2019-04-01 06:34:51','2019-04-01 06:34:51','2020-04-01 06:34:51'),('4a6dcaaccc1de65ac7508fc9fa2de6ac6029ae6035e21e8d6fc710d192107bc6d6fcf26de816778c',1,2,NULL,'[\"*\"]',0,'2019-03-22 02:42:27','2019-03-22 02:42:27','2020-03-22 02:42:27'),('4ee9e0d7e595833f929ccc42ef7362a781d7277668eb1ea77f3dda71df1afe740e8e66dffbf98ddc',1,2,NULL,'[\"*\"]',0,'2019-03-28 03:38:22','2019-03-28 03:38:22','2020-03-28 03:38:22'),('51519e07e23f5cc943a19a5fc8f3fc5f4b3c3798be8d5bbf8f7cd6723a8543d4e2d77ee3396c9de2',1,2,NULL,'[\"*\"]',0,'2019-03-22 02:48:19','2019-03-22 02:48:19','2020-03-22 02:48:19'),('529af9eafeecbf735920950e5a187b2a4ea1424c1e74a56b84ba91966d6ad92a10dc5b8149f520b4',1,2,NULL,'[\"*\"]',0,'2019-03-26 03:24:14','2019-03-26 03:24:14','2020-03-26 03:24:14'),('56ba0685e670b3966c4b94af298132520c912595ae38e409e7f78de2b9e95352af94659f43243250',1,2,NULL,'[\"*\"]',0,'2019-03-27 01:19:11','2019-03-27 01:19:11','2020-03-27 01:19:11'),('574b4767af7cbee2ab262032b7aadefd59726b55fe7df7402ebacd3253a816c336a535f76623cb6b',1,2,NULL,'[\"*\"]',0,'2019-03-27 05:30:54','2019-03-27 05:30:54','2020-03-27 05:30:54'),('5a7ef85ca610fe4d126443bf237cd6d49c61432da4384caa4a19743ee66106be5dea25e482d36a2d',1,2,NULL,'[\"*\"]',0,'2019-03-20 06:55:39','2019-03-20 06:55:39','2020-03-20 06:55:39'),('5d8aedc1da4023cfc9c13e5d2d80a59faefb4783d143497e00c98bd4aa0de9c809b95d68c7682671',1,2,NULL,'[\"*\"]',0,'2019-03-28 05:28:13','2019-03-28 05:28:13','2020-03-28 05:28:13'),('5e5288760760451e07547c0c17ede85b6fb3c4c40441c7ce39bddcc2aa73a58d6f51a882cd2b0c92',1,2,NULL,'[\"*\"]',0,'2019-03-25 06:37:52','2019-03-25 06:37:52','2020-03-25 06:37:52'),('637a186e1987239c21b17e23196354bfb58487e254e93ea51d0777aa2983573e65a03231b7b68790',1,2,NULL,'[\"*\"]',0,'2019-04-01 06:26:56','2019-04-01 06:26:56','2020-04-01 06:26:56'),('667dc3404dbd2bf4ac6deda46e9f7d5cc79b4a1e8da11ed17cbb5f1cac1c8a12c3a999ab29efd2d6',1,2,NULL,'[\"*\"]',0,'2019-03-28 06:24:44','2019-03-28 06:24:44','2020-03-28 06:24:44'),('6c0dae411fdf188a7691bacf4a2b7c69ebfa0b774679658afede8ee6087403545b8dd174b34fea99',1,2,NULL,'[\"*\"]',0,'2019-03-27 07:58:26','2019-03-27 07:58:26','2020-03-27 07:58:26'),('6e71003626aa7ece9195df3bbe933c5117e27ce46bcbeb59f1b240ab5b3e82b8febb771c91a92424',1,2,NULL,'[\"*\"]',0,'2019-03-27 08:11:45','2019-03-27 08:11:45','2020-03-27 08:11:45'),('705172e6cb32a400c29da207690b6553f6baca74ac61bc4d3f50aee027d3ecf53cf7f85bf1dac7c9',1,2,NULL,'[\"*\"]',0,'2019-03-27 05:54:46','2019-03-27 05:54:46','2020-03-27 05:54:46'),('71fad045369b5d49b45e94bdd554913bb0bd7fdac770e11a60698555c37b66eabcb55ec277ac9e3f',1,2,NULL,'[\"*\"]',0,'2019-04-01 03:55:36','2019-04-01 03:55:36','2020-04-01 03:55:36'),('755bbc0f58b6b3fb0380fbf2bbadc74f3473acf0d7e97b7d56517fa005c64c960ac835095d62cabc',1,2,NULL,'[\"*\"]',0,'2019-03-26 02:26:04','2019-03-26 02:26:04','2020-03-26 02:26:04'),('76381a817c5edab711973127ff46c0dc1b486dbdd0240fc8ab606af965813d03994a2710ea80f68d',1,2,NULL,'[\"*\"]',0,'2019-04-01 06:28:35','2019-04-01 06:28:35','2020-04-01 06:28:35'),('77d7cf3d24c494526904fe4b71dc62a214eba218429e8b197bcb488ede7e6916d4fdc68eb6ced81c',1,2,NULL,'[\"*\"]',0,'2019-03-21 07:58:44','2019-03-21 07:58:44','2020-03-21 07:58:44'),('7ada8dcc153c812afd733c01ee9dfbc71280b48b3e2ed4ac92f514ee3bb51414e45b0b7f62b699e9',1,2,NULL,'[\"*\"]',0,'2019-03-28 03:00:05','2019-03-28 03:00:05','2020-03-28 03:00:05'),('7af054cc9340980e92b1979be4458bc5e73c1d25549353e6da1a796c11172bc37803063e45db5ce5',1,2,NULL,'[\"*\"]',0,'2019-03-26 07:49:17','2019-03-26 07:49:17','2020-03-26 07:49:17'),('7cf62cdab45f8b76028a8f48300648a67119a58c14615d1c680ff8be10f96bcb1f7de16b728c6e21',1,2,NULL,'[\"*\"]',0,'2019-03-27 00:35:34','2019-03-27 00:35:34','2020-03-27 00:35:34'),('7e22de961c30b335b5d3eea7b7e3380af3e3d9e1da6a86c2f3720e63f02131b23987ddbd34ac059a',1,2,NULL,'[\"*\"]',0,'2019-03-31 03:46:33','2019-03-31 03:46:33','2020-03-31 03:46:33'),('7f4cb42836ecefad6a7f7acb66ef5661af0c1c656e54395a0e592ac44701291e3a06402d807b3255',1,2,NULL,'[\"*\"]',0,'2019-04-01 06:24:10','2019-04-01 06:24:10','2020-04-01 06:24:10'),('8033cf31b8cbd1ea88c7e3b28867e88e88bf033e4f92d9c34039fbfd80a059707afd28e57c6e0913',1,2,NULL,'[\"*\"]',0,'2019-03-21 03:08:01','2019-03-21 03:08:01','2020-03-21 03:08:01'),('82bd8908fa6c7e9793f525c7bdabb8bf476d5d6ec7780c56c0cce2f0bef22a813669ed62acda0378',1,2,NULL,'[\"*\"]',0,'2019-03-27 02:13:11','2019-03-27 02:13:11','2020-03-27 02:13:11'),('84c09f7f5be5cc7e183e816105506a755bd11c2ba59860a42993f2d61e65da79ed6ef56ffe139547',1,2,NULL,'[\"*\"]',0,'2019-03-21 23:55:41','2019-03-21 23:55:41','2020-03-21 23:55:41'),('853a44d6b7c14a00325a5e547423c6b37636bf08f740b1622072d32228d93a743717c00cda9f693a',1,2,NULL,'[\"*\"]',0,'2019-03-28 01:47:42','2019-03-28 01:47:42','2020-03-28 01:47:42'),('8aa41c7fe162c5ee53f87fde83c42bff47c2ae44d4deb842e3a3aeae33843cc0facfd3c5668d52a9',1,2,NULL,'[\"*\"]',0,'2019-03-21 03:15:07','2019-03-21 03:15:07','2020-03-21 03:15:07'),('8c6464d8442afee21cd62366716f119580cde3e763a8b282c90050e2530afbc7336dbc413527e6d0',1,2,NULL,'[\"*\"]',0,'2019-04-01 06:33:35','2019-04-01 06:33:35','2020-04-01 06:33:35'),('8e7efa6de3165bd08e20f58a953fefaa264e40ba0ef06a78900b5f55009c144ce767f2c5e7fcaed6',1,2,NULL,'[\"*\"]',0,'2019-03-26 08:11:25','2019-03-26 08:11:25','2020-03-26 08:11:25'),('9322160ce51feb130a7cd3576a5402b2c3c69f71a220c716d59ab9cd451c08f2aeaf3b361b46613c',1,2,NULL,'[\"*\"]',0,'2019-04-01 06:07:37','2019-04-01 06:07:37','2020-04-01 06:07:37'),('953357f27eb8c0e08b407531cc91107f30033044334840b78a435b0b42baa16abd9c92136664d4ef',1,2,NULL,'[\"*\"]',0,'2019-03-22 05:47:44','2019-03-22 05:47:44','2020-03-22 05:47:44'),('96305406260ab2dbb1acd9626e167c0bacd4e033967390610fc62dc462a85ceee9b2926549ffb37e',1,2,NULL,'[\"*\"]',0,'2019-03-28 02:11:30','2019-03-28 02:11:30','2020-03-28 02:11:30'),('9aa35797f18f5a5ad3cd6aaa831b719c1f31386ace0e2c692e559792954511e632cbcf2414ce4329',1,2,NULL,'[\"*\"]',0,'2019-03-28 02:16:15','2019-03-28 02:16:15','2020-03-28 02:16:15'),('9ae4b495ea25b96b72baa94ed5a016a0cfd297089142dbf39d43ae88d04db1c274b930ea1bab10a1',1,2,NULL,'[\"*\"]',0,'2019-04-01 06:26:37','2019-04-01 06:26:37','2020-04-01 06:26:37'),('9d09a3f390b55497fa06f6bdd9548a2cf606ccc09184e4e99d45f178cf28fd61d642947305e1327f',1,2,NULL,'[\"*\"]',0,'2019-03-29 00:11:54','2019-03-29 00:11:54','2020-03-29 00:11:54'),('a1c522f19791458569516749128cbc21c5372c0ca95e234d6827390bebfb0ad3e0b6d075c84a1804',1,2,NULL,'[\"*\"]',0,'2019-03-21 09:00:50','2019-03-21 09:00:50','2020-03-21 09:00:50'),('a375a983bf30b7165a8bd3bc403a6d8238a3d4107fc722cf4b5ac94df4466698efcb3b00d2345275',1,2,NULL,'[\"*\"]',0,'2019-03-27 05:53:59','2019-03-27 05:53:59','2020-03-27 05:53:59'),('a400f21062345e9060e1cada48be232802be907f978ee72db0cf8a5b82117dd66744f148a4d47335',1,4,NULL,'[\"*\"]',0,'2019-04-08 02:41:35','2019-04-08 02:41:35','2020-04-08 02:41:35'),('a45641fdc5997df7f2b01d0f3edc9b3f583c9ded5674ef9b01bc625ba2ec98b43e8a73bc54859ad2',1,2,NULL,'[\"*\"]',0,'2019-04-01 03:57:03','2019-04-01 03:57:03','2020-04-01 03:57:03'),('a7fc4b83a0a0a348fbd101dd599b926d871c38138479aca45138e1ccda765831b5cb6430e282f091',1,2,NULL,'[\"*\"]',0,'2019-04-01 06:21:23','2019-04-01 06:21:23','2020-04-01 06:21:23'),('a818307b1a2269384aef3c0d418347ce91f6736fe55f57a984c56fd7904ee7476e83f07ea7bd20fd',1,4,NULL,'[\"*\"]',0,'2019-04-27 04:28:40','2019-04-27 04:28:40','2020-04-27 04:28:40'),('b3d79f36ff83d1f745afbef25d53e8eb21d0d5f104d25d29b494a724e9f6e89818fcf2c143c9742f',1,2,NULL,'[\"*\"]',0,'2019-04-01 06:37:51','2019-04-01 06:37:51','2020-04-01 06:37:51'),('b636f4ec9ac5728d98506b503c2376464f91e76581c8b916a2b8fbd225050a40588b24d5c91bb2c2',1,2,NULL,'[\"*\"]',0,'2019-03-26 09:34:06','2019-03-26 09:34:06','2020-03-26 09:34:06'),('b702bd5a80ffd5ab4f08c3c2b58ffaca00ec117f02b2aec6d214ad70fd4711b2ccaca3eb372f9296',1,2,NULL,'[\"*\"]',0,'2019-03-22 05:48:50','2019-03-22 05:48:50','2020-03-22 05:48:50'),('b873251f28bbe6b3a767fb1cecb18df3c383513576277fce29192bbc25c782bb4aafab7a10f693d1',1,2,NULL,'[\"*\"]',0,'2019-03-21 08:49:27','2019-03-21 08:49:27','2020-03-21 08:49:27'),('ba38027b6ce3703ff4b4cb8af5b0bbb053b4b507b593ce6f4455fcc9ed0fe0dcbe8e8a492b2528fc',1,2,NULL,'[\"*\"]',0,'2019-04-01 23:36:47','2019-04-01 23:36:47','2020-04-01 23:36:47'),('bdf30ac115b229f42b425fa7933fd12c31a91b131fa3aa9e083b085a77a149d7760798a725ac6e03',1,2,NULL,'[\"*\"]',0,'2019-03-21 07:59:20','2019-03-21 07:59:20','2020-03-21 07:59:20'),('c1708d51bfbc71dca17deaff7c22922b11e18b0b816ae3284209048fe9508dca40c4e4e8bf2b06e8',1,4,NULL,'[\"*\"]',0,'2019-04-08 06:25:57','2019-04-08 06:25:57','2020-04-08 06:25:57'),('c672fec1f48f012ccf0bed4393ec587748fbd10136a8e4a7067d72f830cb977157c2bc2cc8c21030',1,2,NULL,'[\"*\"]',0,'2019-03-27 08:01:03','2019-03-27 08:01:03','2020-03-27 08:01:03'),('c89a01c3d2f7d7b3a1ef5309fed4b808b9238eed31107a069eb4a69eed405ceeddde3d47b2028af9',1,2,NULL,'[\"*\"]',0,'2019-03-27 03:43:00','2019-03-27 03:43:00','2020-03-27 03:43:00'),('cb006fd0da79c6afdd2c618f18c949df872f18ade6600c81371f2c717c1ce6b050b6f27e83705b01',1,2,NULL,'[\"*\"]',0,'2019-03-28 01:58:46','2019-03-28 01:58:46','2020-03-28 01:58:46'),('cb6b9cd1e1bb0efc0dfbb93b11276241444b64b6f793a9308dfd565f2a6429dce4ebec0889523575',1,2,NULL,'[\"*\"]',0,'2019-03-21 23:38:56','2019-03-21 23:38:56','2020-03-21 23:38:56'),('cbd0d88750337b448dbde0b20b58731bdb7b3f6f80f9e91badad743e5d15f3c3061bcad2cc6b79e0',1,2,NULL,'[\"*\"]',0,'2019-04-01 06:38:54','2019-04-01 06:38:54','2020-04-01 06:38:54'),('cc422ceb5d16406c4b55b07b011d700121c83235d033f1d9386cf5ca04441cd4d7e323e8b4677a69',1,2,NULL,'[\"*\"]',0,'2019-03-20 06:06:32','2019-03-20 06:06:32','2020-03-20 06:06:32'),('cd68a31a9388137267d544ed5fbb2f080cf419bf75d6cf0ccf7b89a6e77797c93635cc139232f7d4',1,2,NULL,'[\"*\"]',0,'2019-03-27 06:18:02','2019-03-27 06:18:02','2020-03-27 06:18:02'),('ce808c6fc4d43d2cdfd7678939dedcecb26bef0ed5bfc69d56756297054322b83240a0d0fbd0f020',1,2,NULL,'[\"*\"]',0,'2019-03-26 08:08:24','2019-03-26 08:08:24','2020-03-26 08:08:24'),('d024cb85092b4a4c2f68e9eb113d3b0cf4d561f4f966fa9c40bedd360631f95b6c2e23a64004afa4',1,2,NULL,'[\"*\"]',0,'2019-03-21 07:57:21','2019-03-21 07:57:21','2020-03-21 07:57:21'),('d0b1915061d761a4c33cfe3ab53b0c277c860b2e25f10be1a87aebba76689823917b9d0d8b3476f9',1,2,NULL,'[\"*\"]',0,'2019-03-25 00:01:05','2019-03-25 00:01:05','2020-03-25 00:01:05'),('d2f2ca7f9c325e3b21f7de16e36e656da0274d5ad92e45e11f465338c2060c358886de2a43f96bc1',1,2,NULL,'[\"*\"]',0,'2019-03-27 04:57:56','2019-03-27 04:57:56','2020-03-27 04:57:56'),('d97af90f3a4a5d3ed9e52f8d7d4a0afa9125555f70c486e9e7e8a1aae447319b403279230e0b8b76',1,2,NULL,'[\"*\"]',0,'2019-04-01 06:19:26','2019-04-01 06:19:26','2020-04-01 06:19:26'),('d9df872961079cba5921776567ef8a0b3febb69407292589293a4944219fec7ae22f8d7f7df3e5a8',1,2,NULL,'[\"*\"]',0,'2019-03-26 05:50:41','2019-03-26 05:50:41','2020-03-26 05:50:41'),('dc8df962c700bc2e3d23b7599fe0b4186c3771b82d674ea4dc6ad71ffbbbdff55c9942792659e7eb',1,2,NULL,'[\"*\"]',0,'2019-04-01 06:25:41','2019-04-01 06:25:41','2020-04-01 06:25:41'),('de23a5d9562e10574420d7485c825661ecf8491db30912f67b8a41f845c1af7249670e588414654b',1,2,NULL,'[\"*\"]',0,'2019-04-01 06:31:23','2019-04-01 06:31:23','2020-04-01 06:31:23'),('e831070d2ff84c7a713e19ab283a476452b1a1740615e3bc63bac6dd91fc76a6c9a8f3ecb63ebedb',1,2,NULL,'[\"*\"]',0,'2019-03-27 01:11:27','2019-03-27 01:11:27','2020-03-27 01:11:27'),('e9cc2f05be0ca43b2d7f35de87a1d9f92001e7f38a65a5575cebeabf07f194e3d92a1b7a9e0a6455',1,2,NULL,'[\"*\"]',0,'2019-04-01 23:39:23','2019-04-01 23:39:23','2020-04-01 23:39:23'),('eaf0dc4c6383d413be6ff1fd07e7a36dbea4da8571378e1345bf8aca960dfa3f5027b41d3f8ed99d',1,2,NULL,'[\"*\"]',0,'2019-03-25 23:31:54','2019-03-25 23:31:54','2020-03-25 23:31:54'),('eb8e4ecc82290c1a5976866ef7bf395c2560577b79c2160a2ed017e3ba06e66ac49a6510b3ca1876',1,2,NULL,'[\"*\"]',0,'2019-03-27 07:55:42','2019-03-27 07:55:42','2020-03-27 07:55:42'),('eeae164e5aefe6fc56aa1f721754c2f28851106a94e9236eb1a441972b84002cb42d44e0d7c614cf',1,2,NULL,'[\"*\"]',0,'2019-03-27 23:04:03','2019-03-27 23:04:03','2020-03-27 23:04:03'),('f3d57e8fc3fe665ca26eeec0463a38e0540099f47ab7b8cc6bd51c9940478d61c5d21acaa2361930',1,2,NULL,'[\"*\"]',0,'2019-03-20 07:49:06','2019-03-20 07:49:06','2020-03-20 07:49:06'),('f42538c382f836f4b2efdadcf444ef6285576c0345fadbe821e961224838a92ab458153be8d86a39',1,2,NULL,'[\"*\"]',0,'2019-03-22 02:37:20','2019-03-22 02:37:20','2020-03-22 02:37:20'),('f45af934870fe885ca372eec0e22fa0b378cab1781bd537362046eee3a16691c1958f4af65ded29b',1,2,NULL,'[\"*\"]',0,'2019-04-01 01:26:47','2019-04-01 01:26:47','2020-04-01 01:26:47'),('f6be83e09bd4b78438b0afa41dfff7f9ca390c877421d35a709e86452306c9f45ce830babf7f9071',1,2,NULL,'[\"*\"]',0,'2019-04-01 06:22:19','2019-04-01 06:22:19','2020-04-01 06:22:19'),('fa9192e3c1fbdbb5916754ad1bc249cbcb592814c422f99ed49095315104c957c2485a0a06a34fbe',1,2,NULL,'[\"*\"]',0,'2019-03-26 05:32:03','2019-03-26 05:32:03','2020-03-26 05:32:03'),('ff134ae21c44ab1895ce3d36aa9945d508a9f1fa982c6e29599524591034238f06e1002160704002',1,2,NULL,'[\"*\"]',0,'2019-03-20 06:32:42','2019-03-20 06:32:42','2020-03-20 06:32:42');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (1,NULL,'Laravel Personal Access Client','7kok0Jp3pb9xdoWYdb7zgblwhswxjVgsLmNLFE3w','http://localhost',1,0,0,'2019-03-20 05:46:49','2019-03-20 05:46:49'),(2,NULL,'Laravel Password Grant Client','9PYwlyos4LC1dxO2WBdkDwycWMfzgOLL0BimklKf','http://localhost',0,1,0,'2019-03-20 05:46:49','2019-03-20 05:46:49'),(3,NULL,'Vista Personal Access Client','OAvvfbG0b921mRXWXoXvaRpFHliem0D5hSTNN1bV','http://localhost',1,0,0,'2019-04-08 02:41:02','2019-04-08 02:41:02'),(4,NULL,'Vista Password Grant Client','q9DsH56owy3tY3KRjtgvjAQKROxxLYgiaAl8bgVn','http://localhost',0,1,0,'2019-04-08 02:41:02','2019-04-08 02:41:02');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,1,'2019-03-20 05:46:49','2019-03-20 05:46:49'),(2,3,'2019-04-08 02:41:02','2019-04-08 02:41:02');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
INSERT INTO `oauth_refresh_tokens` VALUES ('024b78447735f820c18753eed00513490627414c3ccf08ea4c036dc58cc4a6944ccf8e86c3311cca','c89a01c3d2f7d7b3a1ef5309fed4b808b9238eed31107a069eb4a69eed405ceeddde3d47b2028af9',0,'2020-03-27 03:43:00'),('026a7261ae905d683150007cc4bfca8e4b9f4f781c5ddaadd7722485db84b3084dc3b5a8981b24ef','4ee9e0d7e595833f929ccc42ef7362a781d7277668eb1ea77f3dda71df1afe740e8e66dffbf98ddc',0,'2020-03-28 03:38:22'),('026ddb8b32340062130cd0a1e8c4d6e0646d9a4e22ddf916e9204d45345e41ec77d22690ec184664','a375a983bf30b7165a8bd3bc403a6d8238a3d4107fc722cf4b5ac94df4466698efcb3b00d2345275',0,'2020-03-27 05:53:59'),('0870566ac5b480697e1c37c759da754cba5976b29179b7e6fd61a2ec14165501c08c289e775097b7','d97af90f3a4a5d3ed9e52f8d7d4a0afa9125555f70c486e9e7e8a1aae447319b403279230e0b8b76',0,'2020-04-01 06:19:26'),('08f0671cc2af2fb3af6d9d46b0931a73d52b156f8aa1bb4fee14e7c68b87cc68b21d54aa4123ab62','d9df872961079cba5921776567ef8a0b3febb69407292589293a4944219fec7ae22f8d7f7df3e5a8',0,'2020-03-26 05:50:41'),('0a687e77d5c42cc4cdc6a56a17118ff17547dee54707b6944bf16ee533c30f2bb84c7e84b8821256','7f4cb42836ecefad6a7f7acb66ef5661af0c1c656e54395a0e592ac44701291e3a06402d807b3255',0,'2020-04-01 06:24:10'),('0cd5deed9d55caa62348110eaf6022b8e6e50a88d5d75deada1045318cfdc75ca4caf6f5b8ab6c74','f3d57e8fc3fe665ca26eeec0463a38e0540099f47ab7b8cc6bd51c9940478d61c5d21acaa2361930',0,'2020-03-20 07:49:06'),('0dabbd4036582cfa56c33c42f692461d2d542d30eb0e07cdb36a7226c5a4c914703eb553fa2e56a5','9322160ce51feb130a7cd3576a5402b2c3c69f71a220c716d59ab9cd451c08f2aeaf3b361b46613c',0,'2020-04-01 06:07:37'),('102921b57683b503cca94dd9c0b30909526f48755b9a4beac85418c1ce0ac0b35beb84ee818a5eb3','c1708d51bfbc71dca17deaff7c22922b11e18b0b816ae3284209048fe9508dca40c4e4e8bf2b06e8',0,'2020-04-08 06:25:57'),('1092b5894ea5a237737cc306cbe0b2f9b1876aa2f0863000593188e765bb983752b97999c0fa5cea','13b133fb3c8af3bec431e78f4aacf6a6b9473da51580ea18324b890fd452fc5ffae4ee1ec7ce501c',0,'2020-04-01 06:20:44'),('1128e0d872d8dee4c591d50fa55eb1d57969c46969f6a1c68b6ec4cad8172672647241494cd08261','77d7cf3d24c494526904fe4b71dc62a214eba218429e8b197bcb488ede7e6916d4fdc68eb6ced81c',0,'2020-03-21 07:58:44'),('127d79b0182130f2075fa096336544af0ab0f033d3197ae51e56a4c669ec8a8198489addaa365d31','71fad045369b5d49b45e94bdd554913bb0bd7fdac770e11a60698555c37b66eabcb55ec277ac9e3f',0,'2020-04-01 03:55:37'),('174ddbe214870aa4054711e50783d597a844e4c79cd6553888c717df694169163bfb8d350bbea977','1dcbc1a9d3b8feb0c1caffd31f784bca8fc2a7d077cfb8d24dcfa549e24af7d32ea357145be7c1b7',0,'2020-03-22 00:14:57'),('17df857339a1d6233c350ab261594f4dbbe16fb256cf51a13bac73629ff60d62d1aa1b29430818cb','9d09a3f390b55497fa06f6bdd9548a2cf606ccc09184e4e99d45f178cf28fd61d642947305e1327f',0,'2020-03-29 00:11:54'),('1896c98bd7f688bde7c9762e491df16bb8c195d8992c56353dca4dc49a27f08fb6db8ae12a25d3e1','eb8e4ecc82290c1a5976866ef7bf395c2560577b79c2160a2ed017e3ba06e66ac49a6510b3ca1876',0,'2020-03-27 07:55:42'),('1926a22f3d646a9cf37662e8994aca4e3f24a7c99e3fd3b82e8bd44777ca0d24bd9438c5d6c8316e','fa9192e3c1fbdbb5916754ad1bc249cbcb592814c422f99ed49095315104c957c2485a0a06a34fbe',0,'2020-03-26 05:32:03'),('1b494ea831321d26b81c01644d8c0a3e2f48ece1ae9e208c79889bd0bca4682a69d75a9220197a67','a400f21062345e9060e1cada48be232802be907f978ee72db0cf8a5b82117dd66744f148a4d47335',0,'2020-04-08 02:41:35'),('22bc2d5071d006fecf9fc3c62ec2b9589cf1160e33141c1cfb9e03ac537d6a17cadebbdc302518a3','18f68253c603fb1a4a1175e1587d886632b8529c10d6898a724faaaa796d1991aee6f89f749ed91b',0,'2020-03-28 02:00:31'),('230294f92e46ce9c846ad0224bb75c80ad4cd9a2fd426747305b5cdc716bdac40bb3d6501f84f869','d024cb85092b4a4c2f68e9eb113d3b0cf4d561f4f966fa9c40bedd360631f95b6c2e23a64004afa4',0,'2020-03-21 07:57:21'),('237726bbba40099ce7aa463d9483fe6a1cde4839d7c6bbb3786c9b2745680a2829a55eb314d5ac05','7cf62cdab45f8b76028a8f48300648a67119a58c14615d1c680ff8be10f96bcb1f7de16b728c6e21',0,'2020-03-27 00:35:34'),('272bfb2d07adfdbf87f55b69dbd82d54d62761bdadf591bdf59abca7fffb4e122186f31304aaa521','b873251f28bbe6b3a767fb1cecb18df3c383513576277fce29192bbc25c782bb4aafab7a10f693d1',0,'2020-03-21 08:49:27'),('2c8b06929a1226ff1e0fdb838c3000f707704d02d7c52e7575726d35ac88521c1f01344972622e61','a1c522f19791458569516749128cbc21c5372c0ca95e234d6827390bebfb0ad3e0b6d075c84a1804',0,'2020-03-21 09:00:50'),('302cf9b433d0ef827eb76a60d57de655ac11492ef756a9c127340ef0ab7b99316bee1cbea293311e','a45641fdc5997df7f2b01d0f3edc9b3f583c9ded5674ef9b01bc625ba2ec98b43e8a73bc54859ad2',0,'2020-04-01 03:57:03'),('31d9c086f2724d3b1f3bcd6ca406fbe152b6883f180813c0e8980c1cf56e2a0de50aa9e4cd73c429','ba38027b6ce3703ff4b4cb8af5b0bbb053b4b507b593ce6f4455fcc9ed0fe0dcbe8e8a492b2528fc',0,'2020-04-01 23:36:47'),('36f75b4c8b43a89ee284cc9a228b628d86c60d783430ac3549b11efad42e7718b75501d28b274515','a7fc4b83a0a0a348fbd101dd599b926d871c38138479aca45138e1ccda765831b5cb6430e282f091',0,'2020-04-01 06:21:23'),('36fe0d593a8bc136cfaa075576851c61ffa38cf6cb2d6f5e9d80bf6a8c5d857ed0a4a613e1262e8a','de23a5d9562e10574420d7485c825661ecf8491db30912f67b8a41f845c1af7249670e588414654b',0,'2020-04-01 06:31:23'),('386c932d5c7a19af11c78e6c9a9a197ebc1ee38665ad5766019d2f27f994528c0d022f227cf38057','b3d79f36ff83d1f745afbef25d53e8eb21d0d5f104d25d29b494a724e9f6e89818fcf2c143c9742f',0,'2020-04-01 06:37:51'),('3c53187cf5f40b33381dbcb447df7bf1be81da69564fee48e2e7bf833ad40a042bcbaa76a77ac5a0','201294348d82be35fefc77d843a83999ec8d2920035b51c0f9328225438dc8a9ca0bb2c52cdb7105',0,'2020-04-08 06:18:31'),('3d7baae0e9ae60a1bca92d51352a7c6510af9c4d20718bc802e50343d96737a198d8fd0a74d2f742','cc422ceb5d16406c4b55b07b011d700121c83235d033f1d9386cf5ca04441cd4d7e323e8b4677a69',0,'2020-03-20 06:06:32'),('3ff93654960dbe6f239003898f4ec35bd61cbb17866b6115512f39be362dd07ef7aa9bcbfed691c2','84c09f7f5be5cc7e183e816105506a755bd11c2ba59860a42993f2d61e65da79ed6ef56ffe139547',0,'2020-03-21 23:55:41'),('43ad1334cde9282073d656ec7bdf49cf3ae4a913c37c78d61db9a42ea1d7610c2cf2ea2e1aab9f26','d0b1915061d761a4c33cfe3ab53b0c277c860b2e25f10be1a87aebba76689823917b9d0d8b3476f9',0,'2020-03-25 00:01:05'),('4415e0f7a8a6ff5b60a5bc7ba99e4fd0b0c2fc8810dffbcbfbee61c72c26213ffae0183f49e917a2','34edefb65ddb2f6e7a81c64e45dc27d091bfed8c4ecaa26d05c8d7caec8a4327bbf444c53bd4b521',0,'2020-03-26 07:07:46'),('4ba3ec43901808a272e1c07d8588bdd6566603c7b94e4986ad35636d1eea90c470301b7b706adba6','2f56125a2a784a436829800352abe072993478f334e933fabc7d3c81bd423960af103d9c5a713200',0,'2020-03-27 08:01:03'),('4cea4d1f9785c7cb6167d74cc56dc234cc3c9b1c4468f56a4d9dbcfd36bc292bde82a2b092e81f34','d2f2ca7f9c325e3b21f7de16e36e656da0274d5ad92e45e11f465338c2060c358886de2a43f96bc1',0,'2020-03-27 04:57:56'),('5209539b721a2b45172b9eec8a705e34bcebea094e0f48b4d15a95e7e2764e4b8e1ed3602a35cb86','0ff7d92abbeefbd152a68564934f447cfc8c7556c31ac0b27a611d6200266164d7521ce5267e9d88',0,'2020-04-01 06:31:32'),('55f8934e137a1d7faaf5919c6c27412397d22d5f6ed04505696c1a489fb75f68c1806d5e1c24f4d0','bdf30ac115b229f42b425fa7933fd12c31a91b131fa3aa9e083b085a77a149d7760798a725ac6e03',0,'2020-03-21 07:59:20'),('59553bfd9c286d6b5eb296ed2f7f935af09af93c178a2d9bdeaaa6d9442a9dea64e1ffb84e686fca','7ada8dcc153c812afd733c01ee9dfbc71280b48b3e2ed4ac92f514ee3bb51414e45b0b7f62b699e9',0,'2020-03-28 03:00:05'),('5c895f2022924ee30f3bc6c109a2a0367f415a94a0115b3efe4cb26c01b58a968d5616a9254338b3','e831070d2ff84c7a713e19ab283a476452b1a1740615e3bc63bac6dd91fc76a6c9a8f3ecb63ebedb',0,'2020-03-27 01:11:27'),('5ca2f62e361ffcbef1edf055c5f4d978dd850ef9a9397b012b01b28561a560a65bf8e83f9133cb6c','157a34209b807c76fa5669144da3f483f380179643b3153438e5cec8c2b8ab3728b07fa49699de91',0,'2020-04-01 06:30:25'),('5d34122b42632aa218445f22a11f5b7f8478a194e7b530e8c2493945536da289a31f0819bda72aae','14d57bffa37e8cb901bde78c04c6f9ded28875ff96004eebda5154f2d5906916379886e3eced0caa',0,'2020-03-27 01:14:47'),('5e4839c6961df7c40fb3523f6de5f43e1414b18ed73181bd657c4152077677c85676286dbf949c25','529af9eafeecbf735920950e5a187b2a4ea1424c1e74a56b84ba91966d6ad92a10dc5b8149f520b4',0,'2020-03-26 03:24:14'),('5f9fe82dd221ad998c2500f707ef05dad9a9f7ba1cb554fdf4ef9f2d71020d8f05a24d7e9757db64','82bd8908fa6c7e9793f525c7bdabb8bf476d5d6ec7780c56c0cce2f0bef22a813669ed62acda0378',0,'2020-03-27 02:13:11'),('61b441d3a56f6069a0d67c0aa69ec138dff64a49ca46682c9e5ac1db49bd458b961ed3d400f08973','755bbc0f58b6b3fb0380fbf2bbadc74f3473acf0d7e97b7d56517fa005c64c960ac835095d62cabc',0,'2020-03-26 02:26:04'),('61eeccbae16485bd6d2575734d5fc6a9586e3388d9b995ebb7201209b88a9957538584e0593c397f','9aa35797f18f5a5ad3cd6aaa831b719c1f31386ace0e2c692e559792954511e632cbcf2414ce4329',0,'2020-03-28 02:16:15'),('68931c83e51fd5c73129e989b1bd3dbbd945e8a2d63a4707ecfd715e8e5ebfcdc09110e599a165c2','6c0dae411fdf188a7691bacf4a2b7c69ebfa0b774679658afede8ee6087403545b8dd174b34fea99',0,'2020-03-27 07:58:26'),('6d8fcc4adcae35d9716fa6c4848e2eacb40ef7ea4621c7523440a88519fae33fc57217940e009b6a','b702bd5a80ffd5ab4f08c3c2b58ffaca00ec117f02b2aec6d214ad70fd4711b2ccaca3eb372f9296',0,'2020-03-22 05:48:50'),('6e87cf8e415c9c807b81f90955b15954b618962bf21905d046b6a2d4d5d358f66ce296084090a2e2','cb6b9cd1e1bb0efc0dfbb93b11276241444b64b6f793a9308dfd565f2a6429dce4ebec0889523575',0,'2020-03-21 23:38:56'),('729cc6a0372bc62cfc29c80c838510a7185e47257621d0b530ca0da8e819f1bb8b6989a93a47ec63','eaf0dc4c6383d413be6ff1fd07e7a36dbea4da8571378e1345bf8aca960dfa3f5027b41d3f8ed99d',0,'2020-03-25 23:31:54'),('781234a4bd9a96413a8fd8f9fdfd221fc2fd31b62d7a1f7fd8be03868ed7f646e65bb50658283303','953357f27eb8c0e08b407531cc91107f30033044334840b78a435b0b42baa16abd9c92136664d4ef',0,'2020-03-22 05:47:44'),('796e1f60195eb5457d2f05dd831e47f6ca082fbb2dd1ab78682d1b92cf4b10247da1808bf50ed945','8c6464d8442afee21cd62366716f119580cde3e763a8b282c90050e2530afbc7336dbc413527e6d0',0,'2020-04-01 06:33:35'),('809aefb2d3e2bca40c080d5b883d40caa450b4ae702722c62512605c50e910c61bf5c7eff7e76289','050697c498a820ef881e786921a96c5e8a9a87cfa4d1a1310e0bdbfee6ac2f0018e9a98e47be41c0',0,'2020-03-26 07:02:51'),('8416f2e6e5f6a6c2f9a4c3a52abf7dbdc4478caf96248ccf51badf56b5fc3eb58a666f0f122bef5e','ce808c6fc4d43d2cdfd7678939dedcecb26bef0ed5bfc69d56756297054322b83240a0d0fbd0f020',0,'2020-03-26 08:08:24'),('8c23e50eb9eb606405f581275e97623eb44efc3dc0ab6a67b5278239113857d67088b93e8882a74b','705172e6cb32a400c29da207690b6553f6baca74ac61bc4d3f50aee027d3ecf53cf7f85bf1dac7c9',0,'2020-03-27 05:54:46'),('8d668bb20c293018d2b4dd2de6a803736f25a4d95e3a8edda68cef2e17caab6f018799a940f46117','197493f122c84bb009caff59981bef3c8e946c3a156a64d122f284875e4183f0a66e142e1c90cdf6',0,'2020-03-28 05:33:43'),('8da67d1f41e80f0d93d9788a1d4e361e41bf6488bb1d9167764aec5945a19935e83ba28e11b9d241','f45af934870fe885ca372eec0e22fa0b378cab1781bd537362046eee3a16691c1958f4af65ded29b',0,'2020-04-01 01:26:47'),('91e001caa8a7e700b4ccef27113e04736703f5ac0f4d371c1820042915208999c34b637e128a9a4a','96305406260ab2dbb1acd9626e167c0bacd4e033967390610fc62dc462a85ceee9b2926549ffb37e',0,'2020-03-28 02:11:30'),('998ec8b60f0d74f94bbaae5dee098f332e01141eb7a95d9f897dfa873b228ffefad60b2f638c0d96','8e7efa6de3165bd08e20f58a953fefaa264e40ba0ef06a78900b5f55009c144ce767f2c5e7fcaed6',0,'2020-03-26 08:11:25'),('9c35cce47923f249e21b68cc1e4198a7e983c0731c7496d1f44319ccc53accf0f1c60c067326ac4d','eeae164e5aefe6fc56aa1f721754c2f28851106a94e9236eb1a441972b84002cb42d44e0d7c614cf',0,'2020-03-27 23:04:03'),('a09484df970dcbd3e2a3fb0b97d43284c5f72a0d1f906f8e9b6f5bb575cba253fb9a469de433ac9d','dc8df962c700bc2e3d23b7599fe0b4186c3771b82d674ea4dc6ad71ffbbbdff55c9942792659e7eb',0,'2020-04-01 06:25:41'),('a0d785f97b2f2542973e478fef4cf2f276e1ac8a72f58164491a653913794bb38b4fa2f743a71df9','5a7ef85ca610fe4d126443bf237cd6d49c61432da4384caa4a19743ee66106be5dea25e482d36a2d',0,'2020-03-20 06:55:39'),('a3363a4b59654f5f8cc853d2b089ba8cc779c2e135fa840f96e37d7803fb77c99a1cab32464ddd32','a818307b1a2269384aef3c0d418347ce91f6736fe55f57a984c56fd7904ee7476e83f07ea7bd20fd',0,'2020-04-27 04:28:40'),('a6c034399e0259b192775d592d91dcc43d4fa15f923289653253b1bb974b372815fc488bb8544051','9ae4b495ea25b96b72baa94ed5a016a0cfd297089142dbf39d43ae88d04db1c274b930ea1bab10a1',0,'2020-04-01 06:26:37'),('a76429f8ff66c0a0f3c30c89fbbc1eaba567ff3230d9ee2b846291ba04c83158c129a1e831d55533','b636f4ec9ac5728d98506b503c2376464f91e76581c8b916a2b8fbd225050a40588b24d5c91bb2c2',0,'2020-03-26 09:34:06'),('a779dacc9fb20aed4bed3371f0e270c30ea50ebe5fc982f7ebcc37ff78fc9cc7730e9dcf304cad2d','7e22de961c30b335b5d3eea7b7e3380af3e3d9e1da6a86c2f3720e63f02131b23987ddbd34ac059a',0,'2020-03-31 03:46:33'),('ad1c73b3984532cf13838b3981e9c01685c17169f73f1f64af33a0f0750e3cd698bfd12d98c0832e','165ba0525c7fe40de7ca3673ae09ad1cb3a70a0635e4caedc7ba637dcae3c4987b02821eae77b7c3',0,'2020-03-27 01:20:13'),('b12c41e4b47ba5f8fc7981f501b63675cbb2f1b292c11ee846a284ffd4b04f75f23484ce4d6b79aa','5d8aedc1da4023cfc9c13e5d2d80a59faefb4783d143497e00c98bd4aa0de9c809b95d68c7682671',0,'2020-03-28 05:28:13'),('b43397a1ce12dd0d026ecce8022b7212a1f5647a098d8353936d87cfed182713522b74e7ee1f695a','8aa41c7fe162c5ee53f87fde83c42bff47c2ae44d4deb842e3a3aeae33843cc0facfd3c5668d52a9',0,'2020-03-21 03:15:07'),('b4b32c0ac0e3d222f8c260ac26f52058766ee3892ba45c7e8292b65276016fe3b115e271ad226178','cd68a31a9388137267d544ed5fbb2f080cf419bf75d6cf0ccf7b89a6e77797c93635cc139232f7d4',0,'2020-03-27 06:18:02'),('ba0ea2c714489babaf36ef1a15350409342bf2c70c11fd7ac0336f84287d5fba412ae7875687f2fa','7af054cc9340980e92b1979be4458bc5e73c1d25549353e6da1a796c11172bc37803063e45db5ce5',0,'2020-03-26 07:49:17'),('be2f6d99bbe5b2880d710d066bcbbd8c94ed34eec16e479f089ebfb2119d3df9e071f6fdd3122d2d','298e9c36576f244c1819fb9461f35e89fb8dbb30060d47fd7d82a97c3d3d56fe93b4fc039a5bbf07',0,'2020-03-29 01:06:25'),('c0a01e381c4c4255f7469f4fd98dbdf592381eb8b4ac4dafc7e5eb33f8ea506432c98b4caf9f40a0','8033cf31b8cbd1ea88c7e3b28867e88e88bf033e4f92d9c34039fbfd80a059707afd28e57c6e0913',0,'2020-03-21 03:08:02'),('c170dbc2a684dc5de703be762a626180f561a0c20fb3ed657b794d38fd2c4977831be02bd269766d','0466c15c09ce49f6175ef06ee051f227c4be80f71f16e5d1ab1bca6849a8506dd41d4b6e96da24a7',0,'2020-03-21 03:08:01'),('c57c6f47a20fa790c00c9b99f6088ad8d7244d333d5fd3740f8d63d2b9bd3d0cc67e37b75a96afcf','637a186e1987239c21b17e23196354bfb58487e254e93ea51d0777aa2983573e65a03231b7b68790',0,'2020-04-01 06:26:56'),('cb7ca9b3bb5e0413c2695c1e86a4f2c347a4663bd952f2c9b7db9e643dac7e9607538fcf8b5572c5','ff134ae21c44ab1895ce3d36aa9945d508a9f1fa982c6e29599524591034238f06e1002160704002',0,'2020-03-20 06:32:42'),('ce82cba75754bdb9db844e1eeb3c15d4de8cb4c41b01fb367014d802a90ba3e8cb67b00440c00b4e','853a44d6b7c14a00325a5e547423c6b37636bf08f740b1622072d32228d93a743717c00cda9f693a',0,'2020-03-28 01:47:42'),('cfca1b5e68e8f8c52538b7b26fd702b4bdcc3c3323f625909f5411c3e588a5bc53ea6f2e6c13373d','c672fec1f48f012ccf0bed4393ec587748fbd10136a8e4a7067d72f830cb977157c2bc2cc8c21030',0,'2020-03-27 08:01:03'),('d67729e0289a6f374aae9f76da545c6b6363b400465c693cd62b640c1b3cf6fc05aaa6e6edaf06f0','574b4767af7cbee2ab262032b7aadefd59726b55fe7df7402ebacd3253a816c336a535f76623cb6b',0,'2020-03-27 05:30:54'),('d6f8f02b36060541e90d41b95bc4a9403bd4facbf1bb8a9b3e59a2a825b3c81731ce1ea2f743f5c7','56ba0685e670b3966c4b94af298132520c912595ae38e409e7f78de2b9e95352af94659f43243250',0,'2020-03-27 01:19:11'),('d926167c09d01b3896bc58cdcb10d11e39f3a71b040f4ef2592ccff2c677d5b06941498304771673','46d4fe7c77481e5cb6fed467052e889dc7cc7eda5e823326bbd9b39a8cfb9c136894ada784dd7576',0,'2020-04-01 06:34:51'),('d9365dbad4ec6fe4ec1453425f27161e277b61a4ebd2b9523dbde085e0aa94e91666ae0ac8e1690d','76381a817c5edab711973127ff46c0dc1b486dbdd0240fc8ab606af965813d03994a2710ea80f68d',0,'2020-04-01 06:28:35'),('d9ea2e868d0dead7389b01ad423075923cb15acc371bc174224c5fcffe224f095f8d51fad30b15f7','0bb44f80c445829b2370fd69dada31cee8e30c40b65f26f2ca39bfdfe2dc0c3db1f1498fdce0ba19',0,'2020-03-27 02:41:38'),('db78c1f267058b077671b8481d75d4fdc2ef86bfaf8329fd0736ff68da73c83acd9c7250209149fd','6e71003626aa7ece9195df3bbe933c5117e27ce46bcbeb59f1b240ab5b3e82b8febb771c91a92424',0,'2020-03-27 08:11:45'),('dba1fcfb751ca040f756548370f5e7693b5a14d3b5546c1b724e2b59c8aeaa0740b045a311b16ca8','667dc3404dbd2bf4ac6deda46e9f7d5cc79b4a1e8da11ed17cbb5f1cac1c8a12c3a999ab29efd2d6',0,'2020-03-28 06:24:44'),('e00a7c583e159351939a8c392fdd94d8ad4a0f53078b473ee756ce8af545a9ba0ab3948edbc07a84','0e0ab50ceb65cd3711d70c4bf1bc721eaf46f71a3577732a0d7f1ffa284df819c85f0820019bdf51',0,'2020-03-28 06:25:54'),('e390e2ae53b3ed31052c821cb4a698849deebd26d835d1bbbee75fd780339895f1921fd8ad072275','444ba4f4ecf7bff9938f0cd4deaaf4e77d7366a2bc846521aaa7fc6e9203776f9f6b3ce4ab1d470a',0,'2020-03-28 02:10:05'),('e9d65945ae1f8fa784cfd0ed89f05dd9b0f6830442b25c364be811760b0272cc6bf27bbfb0d0a68a','5e5288760760451e07547c0c17ede85b6fb3c4c40441c7ce39bddcc2aa73a58d6f51a882cd2b0c92',0,'2020-03-25 06:37:52'),('ec20fecb020b48041faa11ed1574719fc1024b1a73b58f0085a30a40e11475ab95235459c93c17f7','f42538c382f836f4b2efdadcf444ef6285576c0345fadbe821e961224838a92ab458153be8d86a39',0,'2020-03-22 02:37:20'),('f0f2b64fcc418b5417c007be8e398e97964386146ee9a7fb2572bbded337cd4260e0c73fc356c2bd','24b805a3e8aca0c839af641295465edab86994aa3ef339eb452e90b7afdce231aa019f676a66096e',0,'2020-03-27 03:00:55'),('f45f50924c3ad211a2e287ca721370681881858cd46576b201a13cf4a6855984e59e9bb74a07162e','e9cc2f05be0ca43b2d7f35de87a1d9f92001e7f38a65a5575cebeabf07f194e3d92a1b7a9e0a6455',0,'2020-04-01 23:39:23'),('f92925a2fa646ecbfdbd99d376704c13376a904166e1b8538b60755c1ee817175bc0f350a363cb28','f6be83e09bd4b78438b0afa41dfff7f9ca390c877421d35a709e86452306c9f45ce830babf7f9071',0,'2020-04-01 06:22:19'),('fbf0e92b4e47af32aa560c24d566b34fc647e28a7419c5df7995f23c72f5b8b615300eda2c8b596e','cb006fd0da79c6afdd2c618f18c949df872f18ade6600c81371f2c717c1ce6b050b6f27e83705b01',0,'2020-03-28 01:58:46'),('fc4df02e9e8fff7961be58beeab82912deffd4c52c3c46fc4ba14c631cf2c15504b36396a7e42432','51519e07e23f5cc943a19a5fc8f3fc5f4b3c3798be8d5bbf8f7cd6723a8543d4e2d77ee3396c9de2',0,'2020-03-22 02:48:19'),('fd9d58710fe41957727a41da64a644261d17ed8a9fbacebd1bac8dd3a676a9a24f526068f723c867','cbd0d88750337b448dbde0b20b58731bdb7b3f6f80f9e91badad743e5d15f3c3061bcad2cc6b79e0',0,'2020-04-01 06:38:54'),('fef0d83ae3c022f57ad3ccd9cb0a780b7b0b7415323c57e147e17c23390a74c303919b89e6eac36d','4a6dcaaccc1de65ac7508fc9fa2de6ac6029ae6035e21e8d6fc710d192107bc6d6fcf26de816778c',0,'2020-03-22 02:42:27'),('ffdb8775885f60c1162092b601b463ca0f59324d000e175b183ece6777620efcd9517ac21a82de6a','124bfbe5865e159c4f77883326b3af8ed472ab506447ee0bf741382042d682a67c03c1c7251b8170',0,'2020-03-21 07:55:06');
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `system` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'admin-users.index','praxxys-admin','View all admin-users',1,'2019-03-20 05:46:47','2019-03-20 05:46:47'),(2,'admin-users.show','praxxys-admin','View individual admin-users',1,'2019-03-20 05:46:47','2019-03-20 05:46:47'),(3,'admin-users.create','praxxys-admin','Create new admin-users',1,'2019-03-20 05:46:47','2019-03-20 05:46:47'),(4,'admin-users.edit','praxxys-admin','Edit admin-users',1,'2019-03-20 05:46:47','2019-03-20 05:46:47'),(5,'admin-users.destroy','praxxys-admin','Delete admin-users',1,'2019-03-20 05:46:47','2019-03-20 05:46:47'),(6,'admin-users.restore','praxxys-admin','Restore admin-users',1,'2019-03-20 05:46:47','2019-03-20 05:46:47'),(7,'rooms.index','praxxys-admin','Index Rooms',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(8,'rooms.create','praxxys-admin','Create Rooms',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(9,'rooms.store','praxxys-admin','Store Rooms',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(10,'rooms.edit','praxxys-admin','Edit Rooms',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(11,'rooms.show','praxxys-admin','Show Rooms',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(12,'rooms.destroy','praxxys-admin','Destroy Rooms',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(13,'exchange_rates.index','praxxys-admin','Index Exchange Rates',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(14,'exchange_rates.create','praxxys-admin','Create Exchange Rates',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(15,'exchange_rates.store','praxxys-admin','Store Exchange Rates',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(16,'exchange_rates.edit','praxxys-admin','Edit Exchange Rates',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(17,'exchange_rates.show','praxxys-admin','Show Exchange Rates',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(18,'exchange_rates.destroy','praxxys-admin','Destroy Exchange Rates',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(19,'amenities.index','praxxys-admin','Index Amenities',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(20,'amenities.create','praxxys-admin','Create Amenities',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(21,'amenities.store','praxxys-admin','Store Amenities',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(22,'amenities.edit','praxxys-admin','Edit Amenities',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(23,'amenities.show','praxxys-admin','Show Amenities',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(24,'amenities.destroy','praxxys-admin','Destroy Amenities',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(25,'sold-out-projects.index','praxxys-admin','Index Sold-Out-Projects',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(26,'sold-out-projects.create','praxxys-admin','Create Sold-Out-Projects',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(27,'sold-out-projects.store','praxxys-admin','Store Sold-Out-Projects',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(28,'sold-out-projects.edit','praxxys-admin','Edit Sold-Out-Projects',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(29,'sold-out-projects.show','praxxys-admin','Show Sold-Out-Projects',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(30,'sold-out-projects.destroy','praxxys-admin','Destroy Sold-Out-Projects',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(31,'commercial-areas.index','praxxys-admin','Index Commercial-Areas',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(32,'commercial-areas.create','praxxys-admin','Create Commercial-Areas',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(33,'commercial-areas.store','praxxys-admin','Store Commercial-Areas',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(34,'commercial-areas.edit','praxxys-admin','Edit Commercial-Areas',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(35,'commercial-areas.show','praxxys-admin','Show Commercial-Areas',1,'2019-04-02 02:33:12','2019-04-02 02:33:12'),(36,'commercial-areas.destroy','praxxys-admin','Destroy Commercial-Areas',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(37,'home-banner.index','praxxys-admin','Index Home-Banner',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(38,'home-banner.create','praxxys-admin','Create Home-Banner',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(39,'home-banner.store','praxxys-admin','Store Home-Banner',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(40,'home-banner.edit','praxxys-admin','Edit Home-Banner',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(41,'home-banner.show','praxxys-admin','Show Home-Banner',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(42,'home-banner.destroy','praxxys-admin','Destroy Home-Banner',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(43,'general-page-item.index','praxxys-admin','Index General-Page-Item',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(44,'general-page-item.create','praxxys-admin','Create General-Page-Item',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(45,'general-page-item.store','praxxys-admin','Store General-Page-Item',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(46,'general-page-item.edit','praxxys-admin','Edit General-Page-Item',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(47,'general-page-item.show','praxxys-admin','Show General-Page-Item',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(48,'general-page-item.destroy','praxxys-admin','Destroy General-Page-Item',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(49,'project.index','praxxys-admin','Index Project',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(50,'project.create','praxxys-admin','Create Project',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(51,'project.store','praxxys-admin','Store Project',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(52,'project.edit','praxxys-admin','Edit Project',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(53,'project.show','praxxys-admin','Show Project',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(54,'project.destroy','praxxys-admin','Destroy Project',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(55,'building-layout.index','praxxys-admin','Index Building-Layout',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(56,'building-layout.create','praxxys-admin','Create Building-Layout',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(57,'building-layout.store','praxxys-admin','Store Building-Layout',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(58,'building-layout.edit','praxxys-admin','Edit Building-Layout',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(59,'building-layout.show','praxxys-admin','Show Building-Layout',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(60,'building-layout.destroy','praxxys-admin','Destroy Building-Layout',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(61,'floor-layout.index','praxxys-admin','Index Floor-Layout',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(62,'floor-layout.create','praxxys-admin','Create Floor-Layout',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(63,'floor-layout.store','praxxys-admin','Store Floor-Layout',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(64,'floor-layout.edit','praxxys-admin','Edit Floor-Layout',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(65,'floor-layout.show','praxxys-admin','Show Floor-Layout',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(66,'floor-layout.destroy','praxxys-admin','Destroy Floor-Layout',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(67,'gallery.index','praxxys-admin','Index Gallery',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(68,'gallery.create','praxxys-admin','Create Gallery',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(69,'gallery.store','praxxys-admin','Store Gallery',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(70,'gallery.edit','praxxys-admin','Edit Gallery',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(71,'gallery.show','praxxys-admin','Show Gallery',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(72,'gallery.destroy','praxxys-admin','Destroy Gallery',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(73,'units.index','praxxys-admin','Index Units',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(74,'units.create','praxxys-admin','Create Units',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(75,'units.store','praxxys-admin','Store Units',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(76,'units.edit','praxxys-admin','Edit Units',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(77,'units.show','praxxys-admin','Show Units',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(78,'units.destroy','praxxys-admin','Destroy Units',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(79,'projects.index','praxxys-admin','Index Projects',1,'2019-04-02 02:33:13','2019-04-02 02:33:13'),(80,'building-layouts.index','praxxys-admin','Index Building-Layouts',1,'2019-04-02 02:33:13','2019-04-02 02:33:13');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` double NOT NULL DEFAULT '0',
  `longitude` double NOT NULL DEFAULT '0',
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `front_outside_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `back_outside_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landmark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (1,'Plumeria Heights','Rising above the heart of Taft Avenue, Plumeria Heights Is a sanctuary designed for the students. It Is not just a place to stay, but a place to grow. A stones throw away from the Taft-Quirino-San Andres Intersection. the tower allows easy access to schools, hospitals, malIs and businesses.','TAFT 2128 TAFT AVENUE. MALATE, MANILA',14.576377,121.085114,'projects/uB0g0PG2cIwnOO7bcoD3xCvP9W2bjQeDkmesXXJ1.jpeg','outside-view/Ax488KFMemo3CkOk8beoEMPmYw4Iv2DCCpiDimEF.png','outside-view/zzzScL84Zg2r1GwvLpZQddAazgxDFAzMiYF9MHQq.png','Taft','TAFT 2128 TAFT AVENUE. MALATE, MANILA','2019-03-20 05:46:47','2019-03-29 01:46:26'),(2,'Bradbury Heights','Conveniently located along the university belt, near transportation terminals, and\r\nsurrounded by food hubs, this residential tower will soon be a home to students and individuals who are\r\nthe future of this country.','A MENDOZA ST., CORNER LAONG LAAN ROAD, SAMPALOC, MANILA',14.576377,121.085114,'projects/7ElFNw1iBxzPiaMhYf9voCBlVOqjYE088P2LpVvE.jpeg','outside-view/3NmfW2MQ4ZWC7pX4eV3TF3kqKl7K00wSvQmS3SYX.png','outside-view/w8yyOlAbX7ighqTr0QKU0ifqBQufi4tch1NCl162.png','MANILA','A MENDOZA ST., CORNER LAONG LAAN ROAD, SAMPALOC, MANILA','2019-03-20 05:46:47','2019-03-29 01:46:39'),(3,'Hawthorne Heights','Vista Residences third project in Katipunan offers space-efficient units that are sure to meet all the needs of a student.\r\n\r\nIt is strategically located near major universities and from key commercial establishments. It is also a stone\'s throw away commercial establishments. It is also a stone\'s throw away from the LRT 2 Katipunan station.','267 KATIPUNAN AVENUE, QUEZON CITY',14.576377,121.085114,'projects/ByxOKAyrIoO1TrKeivHEY8QRJuXMOwfdTI5wUV8W.jpeg','outside-view/bvlCifrTR0CNI7C9ifwEzrRvqcBaigSgGc00AkSQ.png','outside-view/Dw6UMYNXZ8IacMhcdRbmjp29Bn3btqRYmqctBwDV.png','KATIPUNAN','267 KATIPUNAN AVENUE, QUEZON CITY','2019-03-20 05:46:47','2019-03-31 03:49:46');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,4),(8,4),(9,4),(10,4),(11,4),(12,4),(13,4),(14,4),(15,4),(16,4),(17,4),(18,4),(19,4),(20,4),(21,4),(22,4),(23,4),(24,4),(37,4),(38,4),(39,4),(40,4),(41,4),(42,4),(43,4),(44,4),(45,4),(46,4),(47,4),(48,4),(49,4),(50,4),(51,4),(52,4),(53,4),(54,4),(55,4),(56,4),(57,4),(58,4),(59,4),(60,4),(61,4),(62,4),(63,4),(64,4),(65,4),(66,4),(67,4),(68,4),(69,4),(70,4),(71,4),(72,4),(73,4),(74,4),(75,4),(76,4),(77,4),(78,4),(79,4),(80,4);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `system` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin-users','praxxys-admin','Manages admin-users',1,'2019-03-20 05:46:47','2019-03-20 05:46:47'),(2,'super-admin','praxxys-admin','Omnipotent. Omniscient',1,'2019-03-20 05:46:47','2019-03-20 05:46:47'),(3,'user','praxxys-admin','Regular user',1,'2019-03-20 05:46:47','2019-03-20 05:46:47'),(4,'admin-cms','praxxys-admin','Handle CMS functions',0,'2019-04-02 03:06:29','2019-04-02 03:06:29');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_layouts`
--

DROP TABLE IF EXISTS `room_layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room_layouts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `unit_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `x_coordinates` double(8,2) NOT NULL,
  `y_coordinates` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `room_layouts_unit_id_index` (`unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_layouts`
--

LOCK TABLES `room_layouts` WRITE;
/*!40000 ALTER TABLE `room_layouts` DISABLE KEYS */;
INSERT INTO `room_layouts` VALUES (1,1,'cr',50.37,65.46,'2019-03-26 05:45:53','2019-03-26 05:45:53'),(2,1,'window',49.98,75.01,'2019-03-26 05:46:24','2019-03-26 05:46:24'),(4,11,'cr',35.14,34.31,'2019-03-27 00:12:53','2019-03-27 00:12:53'),(5,11,'window',58.84,44.43,'2019-03-27 00:14:12','2019-03-27 00:14:12');
/*!40000 ALTER TABLE `room_layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_types`
--

DROP TABLE IF EXISTS `room_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_types`
--

LOCK TABLES `room_types` WRITE;
/*!40000 ALTER TABLE `room_types` DISABLE KEYS */;
INSERT INTO `room_types` VALUES (1,'Studio','2019-03-20 06:13:16','2019-03-31 04:14:52',NULL),(2,'1 Bedroom','2019-03-20 06:13:25','2019-03-31 04:15:48',NULL);
/*!40000 ALTER TABLE `room_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rooms_project_id_index` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (4,1,'Room 1','Model Unit','rooms-image/DR6U570Ocsi6W2AsCWLeY48dIHEwxR8eY0VEOgym.jpeg',NULL,'2019-03-22 05:07:23','2019-03-31 06:06:24'),(5,1,'Room 2','Model Unit 2','rooms-image/bYBenuhiycvlOgTPiMFxZWUo305hY6GWcPBFvMUh.jpeg',NULL,'2019-03-31 06:08:54','2019-03-31 07:27:22');
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sold_out_projects`
--

DROP TABLE IF EXISTS `sold_out_projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sold_out_projects` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sold_out_projects`
--

LOCK TABLES `sold_out_projects` WRITE;
/*!40000 ALTER TABLE `sold_out_projects` DISABLE KEYS */;
INSERT INTO `sold_out_projects` VALUES (1,'878 Espana',NULL,'sold-out-projects-image/i10i9oTorXUmAX8qlshmQrjomCcEPtsIoUEMSwvq.jpeg',NULL,'2019-03-20 06:10:15','2019-04-15 03:47:38'),(2,'Vista GL Taft',NULL,'sold-out-projects-image/ox6JO3TGB4MWS0fDC6qIKDKFkUkJp7bM3AOIQXo9.jpeg',NULL,'2019-03-20 06:10:52','2019-03-20 06:10:52'),(3,'Orwell Heights',NULL,'sold-out-projects-image/iYWkGAZqYy6pYEDKx4N8mCBT1M1y5pwz3BHO6ddk.jpeg',NULL,'2019-03-20 06:11:13','2019-03-20 06:11:13'),(4,'Vista Heights',NULL,'sold-out-projects-image/YkHprjllvMLy4PGzIZ8ckwJ5m5Jm5lI7nwIyEog3.jpeg',NULL,'2019-03-20 06:11:32','2019-03-20 06:11:32'),(5,'Vista Taft',NULL,'sold-out-projects-image/IxhqkQYRHUBwJl1NA6BA4fokDLhu2SgyNGSqM9aS.jpeg',NULL,'2019-03-20 06:11:47','2019-03-20 06:11:47'),(6,'309 Katipunan',NULL,'sold-out-projects-image/hfUhPr9geTsCHqI3ggheo56UkiCmEo1HLAuOFpcf.jpeg',NULL,'2019-03-20 06:12:00','2019-03-20 06:12:00'),(7,'Vista Ponte',NULL,'sold-out-projects-image/ThAi8OgHrmkrur1GFj7gPBHM2nh97btlDFx9jGFU.jpeg',NULL,'2019-03-20 06:12:17','2019-03-20 06:12:17'),(8,'Crown Tower University Belt',NULL,'sold-out-projects-image/6iwr0BkLxpGLGyj3xEz97DPbjPw9Fr55Bf1uF6I4.jpeg',NULL,'2019-03-20 06:12:39','2019-03-20 06:12:39');
/*!40000 ALTER TABLE `sold_out_projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS `units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `floor_layout_id` int(10) unsigned NOT NULL,
  `room_type_id` int(10) unsigned NOT NULL,
  `unit_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `x_coordinates` double(8,2) NOT NULL,
  `y_coordinates` double(8,2) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `available_360` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units`
--

LOCK TABLES `units` WRITE;
/*!40000 ALTER TABLE `units` DISABLE KEYS */;
INSERT INTO `units` VALUES (9,3,1,'A','A',37.24,35.57,'Floor Area: 20.15 sqm','2019-03-20 06:21:38','2019-03-31 07:03:32','room-layouts/gonjxrCmyuSPk7cjVOVYBBtlRGsWU1RMxVde16y2.png','1'),(10,3,1,'B','B',90.18,36.67,'Floor Area: 21 SQM','2019-03-20 06:22:03','2019-03-31 07:03:58','room-layouts/FhNq0BhSrwmyor0G3esLZsLKzA9I0n1YYjLpNAaw.png','1'),(11,3,1,'D','D',87.63,29.22,'Floor Area: 24.75 SQM','2019-03-20 06:22:25','2019-03-31 06:53:24','room-layouts/w5UfR8OFlRGyMWdO3jkA8tet1jsIU9OJlXhh5Zoq.png','1'),(18,3,1,'B','B',89.98,44.41,'Floor Area: 21 SQM','2019-03-31 06:51:11','2019-03-31 07:04:40','room-layouts/dHfodntflMXQ7GuXA2cdGgxQMv6fs84kW32bPr7x.png','1'),(19,3,1,'B','B',89.78,52.97,'Floor Area: 21 SQM','2019-03-31 06:51:43','2019-03-31 07:05:18','room-layouts/sk6X8qR0qSlHhbUxWzdluAHsv4tuy8271M6LSfRo.png','1'),(20,3,1,'B','B',90.18,61.54,'Floor Area: 21 SQM','2019-03-31 06:52:27','2019-03-31 06:52:27','room-layouts/H9kOcHfmIr1TG3F2fzGCG2J2HP8GMpYreM1nh5Et.png','1'),(21,3,1,'C','C',9.39,54.63,'Floor Area: 24.07 SQM','2019-03-31 07:00:31','2019-03-31 07:00:31','room-layouts/9hwwpKtdJwu6HohWULytGv0TOrVpouVWLT543AtK.png','1'),(22,3,1,'C','C',9.39,61.26,'Floor Area: 24.07 SQM','2019-03-31 07:01:23','2019-03-31 07:01:23','room-layouts/lylpbEo9DSdinbInQASoYBQGSvujMEsnJFw0JW64.png','1'),(23,3,1,'A','A',31.75,35.57,'Floor Area: 20.15 SQM','2019-03-31 07:02:50','2019-03-31 07:02:50','room-layouts/L2y2GpC3wrot9CEeBLUOeAhY8jqzrMvci33Que6d.png','1'),(24,3,1,'A','A',42.33,35.57,'Floor Area: 20.15 SQM','2019-03-31 07:05:58','2019-03-31 07:05:58','room-layouts/pkJI7KaRNTfPtyC6hgrgu8dvh9CCLmeMjOVpSzLO.png','1'),(25,3,1,'A','A',47.43,35.85,'Floor Area: 20.15 SQM','2019-03-31 07:06:22','2019-03-31 07:06:22','room-layouts/8ZcVgFm5yIx7Ud4QvrFhTF9nCrp7gmSong38PGsB.png','1'),(26,3,1,'A','A',52.53,35.57,'Floor Area: 20.15 SQM','2019-03-31 07:07:14','2019-03-31 07:07:14','room-layouts/Ql4foggYH98ZpO1G1BFCSOPHbQGaRDiSMK8TzftK.png','1'),(27,3,1,'A','A',57.82,35.57,'Floor Area: 20.15 SQM','2019-03-31 07:08:06','2019-03-31 07:08:06','room-layouts/crMdPo6hqxLslxDqFwZxV1lMhFkCh37AVYMW0BYF.png','1'),(28,3,1,'A','A',62.92,35.57,'Floor Area: 20.15 SQM','2019-03-31 07:08:30','2019-03-31 07:08:30','room-layouts/tmOy2SLwwZ9kJoFTCNk9Nk9J2yuFpYTxuEjQtQuv.png','1'),(29,3,1,'A','A',68.22,35.57,'Floor Area: 20.15 SQM','2019-03-31 07:08:58','2019-03-31 07:08:58','room-layouts/JGOK6Nx7K1W2XjdtIiOP6ALcddqL2detAsZM4z5l.png','1'),(30,3,1,'A','A',73.12,35.85,'Floor Area: 20.15 SQM','2019-03-31 07:09:23','2019-03-31 07:09:23','room-layouts/tCq8AFvdXN0iJqeLeDFmhUp7WdraPBsSQvW5dYsv.png','1'),(31,3,1,'A','A',78.61,35.85,'Floor Area: 20.15 SQM','2019-03-31 07:09:52','2019-03-31 07:09:52','room-layouts/NBgDi1FOHZ0BQFRZx2EGZjcebctYWAkjPQfbty6H.png','1'),(32,3,1,'A','A',21.55,59.88,'Floor Area: 20.15 SQM','2019-03-31 07:11:00','2019-03-31 07:11:00','room-layouts/Mwto4YXXJXsgO9veV2oFCFrwiXFnrqvDQYqRGnWn.png','1'),(33,3,1,'A','A',97.24,85.02,'Floor Area: 20.15 SQM','2019-03-31 07:11:24','2019-03-31 07:11:24','room-layouts/HQHZzB7Phb2Mfa5SNhywOxm3WKLpbW3GRSHokvR1.png','1'),(34,3,1,'A','A',26.65,59.88,'Floor Area: 20.15 SQM','2019-03-31 07:19:45','2019-03-31 07:19:45','room-layouts/7ETIUejRJl3ADPPK7wyttxpxxJagfYUGUoUvHPwB.png','1'),(35,3,1,'A','A',32.14,59.88,'Floor Area: 20.15 SQM','2019-03-31 07:20:03','2019-03-31 07:20:03','room-layouts/MrySgkm0vPDUJYBPXTPy6NP4tnowBe72ER4aUDpx.png','1'),(36,3,1,'A','A',37.24,60.15,'Floor Area: 20.15 SQM','2019-03-31 07:20:33','2019-03-31 07:20:33','room-layouts/nddjSNbSfQYO8S6KhqGQM1grzvYKTHSO1AdPFHct.png','1'),(37,3,1,'A','A',42.53,59.88,'Floor Area: 20.15 SQM','2019-03-31 07:21:12','2019-03-31 07:21:12','room-layouts/I0eafIOfP59SEvHIzwJyYNYECePtyH8ku5LLKld4.png','1'),(38,3,1,'A','A',47.63,60.15,'Floor Area: 20.15 SQM','2019-03-31 07:21:38','2019-03-31 07:21:38','room-layouts/QNyFl6sz6ajG4RWQC5JJVDY6rj8RJBSTwkIDyZF9.png','1'),(39,3,1,'A','A',52.53,60.15,'Floor Area: 20.15 SQM','2019-03-31 07:22:04','2019-03-31 07:22:04','room-layouts/NUq9hlwyXkYhFTbBDq7LBicayZh58yHrTl6c7UNJ.png','1'),(40,3,1,'A','A',57.82,60.15,'Floor Area: 20.15 SQM','2019-03-31 07:23:18','2019-03-31 07:23:18','room-layouts/9LKOGhWhcn2RRccBdhwHEsKEHaWOwJichGwfTHkJ.png','1'),(41,3,1,'A','A',63.12,60.15,'Floor Area: 20.15 SQM','2019-03-31 07:23:42','2019-03-31 07:23:42','room-layouts/1b6KjWFkP5ndWL1Pm2sCeTBVl9h8yjXpkHiGI9HM.png','1'),(42,3,1,'A','A',68.41,60.15,'Floor Area: 20.15 SQM','2019-03-31 07:24:10','2019-03-31 07:24:10','room-layouts/7c3CJd9SldajOuCYhOdtecOPS3ZJzslojHa5mtwF.png','1'),(43,3,1,'A','A',73.31,60.15,'Floor Area: 20.15 SQM','2019-03-31 07:24:46','2019-03-31 07:24:46','room-layouts/QKJi5QzraCQwxHTOz2NPDno6qFfYtCnuLFvrucnZ.png','1'),(44,4,2,'E','E',7.24,5.22,'Floor Area: 43.14','2019-03-31 07:50:46','2019-03-31 07:53:06','room-layouts/zs3mAWqhTtzN5DtRtl3cvT4EwMMQdMah4MKm3YUw.png','1'),(45,4,1,'B','B',34.10,4.96,'Floor Area: 21 SQM','2019-03-31 07:54:08','2019-03-31 07:54:08','room-layouts/h7z5s90TEsmVHkRzER99FIM6goswTM6riutAtSO7.png','1'),(46,4,1,'B','B',39.39,4.96,'Floor Area: 21 SQM','2019-03-31 07:54:37','2019-03-31 07:54:37','room-layouts/xmYngzyNoO29MzKLoFboqjy2B1a7a4YM4Z0ZqfnQ.png','1'),(47,4,1,'B','B',44.88,5.22,'Floor Area: 21 SQM','2019-03-31 08:06:41','2019-03-31 08:06:41','room-layouts/Ww5osxOeDxpEifmAHNGlzSdvqRSLIJuKjbeUvq18.png','1'),(48,4,1,'B','B',50.57,4.96,'Floor Area: 21 SQM','2019-03-31 08:08:16','2019-03-31 08:08:16','room-layouts/mY4ayZGQIOgLhRzfSzvyY0LBxQL0xiLMWfALZbbO.png','1'),(49,4,1,'B','B',56.06,4.96,'Floor Area: 21 SQM','2019-03-31 08:09:06','2019-03-31 08:09:06','room-layouts/pVOu2M4h1i3eXaGhJJrceTYr2Wpxee2S6PF95iIF.png','1'),(50,4,1,'B','B',61.94,5.22,'Floor Area: 21 SQM','2019-03-31 08:10:02','2019-03-31 08:10:02','room-layouts/WJt6dYxGYXSLG9yfVxZCCGIxTqD1fSCyojXhGOa7.png','1'),(51,4,1,'B','B',33.71,28.67,'Floor Area: 21 SQM','2019-03-31 08:10:42','2019-03-31 08:10:42','room-layouts/Z1iQ4MCjuMfzEt7bKCstV6aQFlF2uexC6n1nAGL4.png','1'),(52,4,1,'B','B',39.39,28.67,'Floor Area: 21 SQM','2019-03-31 08:11:48','2019-03-31 08:11:48','room-layouts/4u9AgrgVgnlJhldY6qxSRKnfVQTYJGGFuEG42kR5.png','1'),(53,4,1,'B','B',50.76,28.67,'Floor Area: 21 SQM','2019-03-31 08:12:56','2019-03-31 08:12:56','room-layouts/wQejCi3BvoThlquoZeTRkC04ScdswpMPYylEgbSE.png','1'),(54,4,1,'B','B',44.88,28.67,'Floor Area: 21 SQM','2019-03-31 08:13:39','2019-03-31 08:13:39','room-layouts/g9zTPDVPRHTHdeYmByJThjam1KhCnwLIn5qqfvxR.png','1'),(55,4,1,'B','B',56.06,28.67,'Floor Area: 21 SQM','2019-03-31 08:14:17','2019-03-31 08:14:17','room-layouts/hJ3VIKlvOOKfFYC4d5nFC1CYxmmtfKft3NkNNZXF.png','1'),(56,2,1,'C','C',5.86,50.44,'Floor Area: 26.71 SQM','2019-03-31 08:15:29','2019-03-31 08:15:29','room-layouts/BucfPk0iT8B0PEqVSmrUj7wHz01LinTncCCMwE91.png','1'),(57,4,1,'B','B',74.10,25.84,'Floor Area: 21 SQM','2019-03-31 08:15:30','2019-03-31 08:15:30','room-layouts/dtRYQPXr0P427tqbLu0ilD1QRGZwkAALSufvc8hO.png','1'),(58,2,1,'B','B',75.67,34.99,'Floor Area: 24.88 SQM','2019-03-31 08:16:17','2019-03-31 08:16:17','room-layouts/pTi4fFylaZlS3AjYuD3V4PN2HvUX51YeCLo7br0U.png','1'),(59,4,1,'B','B',74.10,33.05,'Floor Area: 21 SQM','2019-03-31 08:16:28','2019-03-31 08:16:28','room-layouts/U1K9OABQ4ZDk708l54XWJ2UAdHWfSjlkoZk8OqAI.png','1'),(60,2,1,'D','D',93.12,69.14,'Floor Area: 29.02 SQM','2019-03-31 08:17:31','2019-03-31 08:17:31','room-layouts/0AfqRERehXjTr07cG5bYUtmjKGETSGB3VhqmnjjV.png','1'),(61,4,1,'B','B',74.10,40.78,'Floor Area: 21 SQM','2019-03-31 08:17:35','2019-03-31 08:17:35','room-layouts/vK4u810nbO49V8n3dFAYmyrJnzvMbJCIQpdep1JK.png','1'),(62,4,1,'B','B',73.90,47.74,'Floor Area: 21 SQM','2019-03-31 08:18:28','2019-03-31 08:18:28','room-layouts/b9soz7fvcOpLzbbriczGQP9TfJJvPKqZfXRH5aSC.png','1'),(63,2,1,'A','A',4.29,13.85,'Floor Area: 21 SQM','2019-03-31 08:18:43','2019-03-31 08:18:43','room-layouts/MsHToMMxTqOzrwtER0ttwCkCMx6XvJR1tuuMdfDP.png','1'),(64,4,1,'B','B',73.90,55.22,'Floor Area: 21 SQM','2019-03-31 08:19:18','2019-03-31 08:19:18','room-layouts/YDKlF1SLcgLetnr0otv2OwhPifBLStdrDy6dkEj8.png','1'),(65,2,1,'A','A',4.10,23.61,'Floor Area: 21 SQM','2019-03-31 08:19:31','2019-03-31 08:19:31','room-layouts/n272PgGFqlkfZpLKZvKu7TqLIKBlPUOrrlrS56u7.png','1'),(66,4,1,'B','B',73.90,61.92,'Floor Area: 21 SQM','2019-03-31 08:19:50','2019-03-31 08:19:50','room-layouts/uUXfSy58ubC2mvygzPcf22s8Kwb04zDvVZAxPz2M.png','1'),(67,2,1,'A','A',4.29,33.37,'Floor Area: 21 SQM','2019-03-31 08:20:12','2019-03-31 08:20:12','room-layouts/Fbrlcs8TgW56q02J4FxsvIsFB3QDPwmpD95b2apz.png','1'),(68,2,1,'A','A',4.29,42.72,'Floor Area: 21 SQM','2019-03-31 08:20:31','2019-03-31 08:20:31','room-layouts/wpZBy5qHTmvpazUMbbAEPHiaTWrk5SMiEkFegNSP.png','1'),(69,4,1,'B','B',91.94,18.10,'Floor Area: 21 SQM','2019-03-31 08:20:36','2019-03-31 08:20:36','room-layouts/K9lz7qbdlnraE8HG6zr77cXbStqNcMVZvkCDlMnY.png','1'),(70,2,1,'A','A',76.45,44.75,'Floor Area: 21 SQM','2019-03-31 08:20:54','2019-03-31 08:20:54','room-layouts/rNgsAD5o1FctoZh4ZFzTvOI06BXGyfpX6xeO0AaY.png','1'),(71,4,1,'B','B',91.94,25.58,'Floor Area: 21 SQM','2019-03-31 08:21:10','2019-03-31 08:21:10','room-layouts/48UzVHiuvG2hsNSi5HAH6Rg8bbkCva2TJOND8HUZ.png','1'),(72,2,1,'A','A',76.84,54.10,'Floor Area: 21 SQM','2019-03-31 08:21:36','2019-03-31 08:21:36','room-layouts/ku7z2aFDCMKF5ctkSaityFmhWdhPxqSxoa8nsOrD.png','1'),(73,2,1,'A','A',78.02,63.85,'Floor Area: 21 SQM','2019-03-31 08:21:57','2019-03-31 08:21:57','room-layouts/hi6AH9apkMv8GBESKkRTsWv2nLIS10J0cBtLBw0u.png','1'),(74,2,1,'A','A',78.80,73.20,'Floor Area: 21 SQM','2019-03-31 08:22:15','2019-03-31 08:22:15','room-layouts/GOpSakMHrM4IVAJSKPCPwh5VSPABDf5QcYYudAIV.png','1'),(75,4,1,'B','B',91.75,32.79,'Floor Area: 21 SQM','2019-03-31 08:22:17','2019-03-31 08:22:17','room-layouts/OgbQRDrqNl1zE5J2ddhvhZxz4xt5VmU7sVpU7fIK.png','1'),(76,4,1,'B','B',91.94,40.01,'Floor Area: 21 SQM','2019-03-31 08:22:42','2019-03-31 08:22:42','room-layouts/x2uRQv0pT3Tu4KfmjjjPkK597Nyh2tn7upqVO3Ou.png','1'),(77,2,1,'A','A',89.20,13.04,'Floor Area: 21 SQM','2019-03-31 08:22:43','2019-03-31 08:22:43','room-layouts/fozk6QZBQM6XyGdZDHuM2rplPOhG3Ps9kygeM36i.png','1'),(78,4,1,'B','B',91.94,47.74,'Floor Area: 21 SQM','2019-03-31 08:23:08','2019-03-31 08:23:08','room-layouts/lmKBqUDM5iDW6ikOISbkkE8S8Yq3XNSrlHklnrPE.png','1'),(79,4,1,'B','B',92.14,54.96,'Floor Area: 21 SQM','2019-03-31 08:23:27','2019-03-31 08:23:27','room-layouts/0g5EIWC3FZiv5bYkgHZRmT6zWFOFsVEStRWPnQbY.png','1'),(80,2,1,'A','A',89.39,22.80,'Floor Area: 21 SQM','2019-03-31 08:23:29','2019-03-31 08:23:29','room-layouts/gjGviLbAXW50mRFG8ju7i3WEeCEe6uqoSemztUMv.png','1'),(81,4,1,'B','B',92.14,62.95,'Floor Area: 21 SQM','2019-03-31 08:24:01','2019-03-31 08:24:01','room-layouts/0sdTvdDo1qV0pdPJVAWmPodzI2o14risCA9r4lTD.png','1'),(82,4,1,'B','B',92.14,70.16,'Floor Area: 21 SQM','2019-03-31 08:24:27','2019-03-31 08:24:27','room-layouts/jF0cfFrvfiLsm3NXuAV4UC3UvhUTGLZdo2SW4FgG.png','1'),(83,2,1,'A','A',89.59,32.55,'Floor Area: 21 SQM','2019-03-31 08:24:55','2019-03-31 08:24:55','room-layouts/txj4jE60F2BF5XAj45i23usg7nEgR9BL7jJBnYxd.png','1'),(84,4,1,'B','B',91.94,77.38,'Floor Area: 21 SQM','2019-03-31 08:24:55','2019-03-31 08:24:55','room-layouts/0oD20IVucYBSNmOmriNFNGxiNp1kK1RT3PTlqpF2.png','1'),(85,2,1,'A','A',89.98,41.90,'Floor Area: 21 SQM','2019-03-31 08:25:19','2019-03-31 08:25:19','room-layouts/QO3bzMrnh9o4q2xiHWDKtYL5VedHSW9q2JuaYykc.png','1'),(86,2,1,'A','A',90.57,51.25,'Floor Area: 21 SQM','2019-03-31 08:26:13','2019-03-31 08:26:13','room-layouts/t0i1JtTrZ8gsRZH38vNOSIJaxsL0JRz4HonpJOX4.png','1'),(87,2,1,'A','A',91.55,60.20,'Floor Area: 21 SQM','2019-03-31 08:26:28','2019-03-31 08:26:28','room-layouts/tsoFiqE0zFrlHkdPb5U3NdMevYtDKMaJ2rv7QHbY.png','1'),(88,2,1,'A','A',15.27,40.68,'Floor Area: 21 SQM','2019-03-31 09:59:44','2019-03-31 09:59:44','room-layouts/sR6L4A8sFr9kmwKEw68C7VZj9wGgwOQiQL3CwjaQ.png','1'),(89,2,1,'A','A',20.57,41.09,'Floor Area: 21 SQM','2019-03-31 10:00:05','2019-03-31 10:00:05','room-layouts/wodQEXZgh4v4ovO9Lf8Z8IEQRAO1E08BATAzrNuO.png','1'),(90,2,1,'A','A',25.86,41.50,'Floor Area: 21 SQM','2019-03-31 10:00:24','2019-03-31 10:00:24','room-layouts/8uRuLggFn4RegRh8RSwzYxc9u3VrziKsAV8yZ2OD.png','1'),(91,2,1,'A','A',30.96,41.50,'Floor Area: 21 SQM','2019-03-31 10:00:51','2019-03-31 10:00:51','room-layouts/fBTsOdne3y7eKkKAurvnN66hBkTSHn7hEtk2f5uf.png','1'),(92,2,1,'A','A',36.25,41.50,'Floor Area: 21 SQM','2019-03-31 10:01:05','2019-03-31 10:01:05','room-layouts/aKNZsBO94xJPSRZNMVINWwT5UeDBi9Ojru8ltcx0.png','1'),(93,2,1,'A','A',41.16,42.72,'Floor Area: 21 SQM','2019-03-31 10:01:23','2019-03-31 10:01:23','room-layouts/BYntqAgDCpPJErJnHR0RxTyXAOaxEIUKvN1LVpOX.png','1'),(94,2,1,'A','A',45.67,42.31,'Floor Area: 21 SQM','2019-03-31 10:01:38','2019-03-31 10:01:38','room-layouts/hGDjKKV1DxXOYBhaEYXAv8Zk1NVOypjfmkFnaI4l.png','1'),(95,2,1,'A','A',50.18,43.12,'Floor Area: 21 SQM','2019-03-31 10:01:59','2019-03-31 10:01:59','room-layouts/4GISndaTsDSWwexenw1oFuTT3v6d1joaHbiUhHMQ.png','1'),(96,2,1,'A','A',20.76,10.20,'Floor Area: 21 SQM','2019-03-31 10:02:22','2019-03-31 10:02:22','room-layouts/PHy1k8xNys1Y1h5rRLcQX6mICdPrnFZUEz3XfbTn.png','1'),(97,2,1,'A','A',26.06,10.20,'Floor Area: 21 SQM','2019-03-31 10:02:48','2019-03-31 10:02:48','room-layouts/qtoaJkzThi5zLEQGztVFYPbeJGOGCRvUZfEKVYRs.png','1'),(98,2,1,'A','A',30.76,9.79,'Floor Area: 21 SQM','2019-03-31 10:03:13','2019-03-31 10:03:13','room-layouts/VWZQFnmRmelcnBPoEixhETliFBTRZiizaM9vlIPB.png','1'),(99,2,1,'A','A',36.45,9.79,'Floor Area: 21 SQM','2019-03-31 10:03:51','2019-03-31 10:03:51','room-layouts/D7UUjtgvs9wOiJbvl2L3KJskK2EH7Q9LP6xwXrqR.png','1'),(100,2,1,'A','A',40.96,10.20,'Floor Area: 21 SQM','2019-03-31 10:04:23','2019-03-31 10:04:23','room-layouts/EokCH0KG7PuPYdvPEb7rLNkvieDMoogeGlE2dZ7V.png','1'),(101,2,1,'A','A',45.86,10.20,'Floor Area: 21 SQM','2019-03-31 10:04:58','2019-03-31 10:04:58','room-layouts/Spzjh9W4ZkgHoCuKc7ekjrFNzepGdBlTuO1SItbt.png','1'),(102,2,1,'A','A',50.18,10.60,'Floor Area: 21 SQM','2019-03-31 10:05:20','2019-03-31 10:05:20','room-layouts/JSorx2Lz7tojkw0oh4vW7jYsiGB7jTdEXI1MzCg2.png','1'),(103,2,1,'A','A',54.49,9.79,'Floor Area: 21 SQM','2019-03-31 10:06:12','2019-03-31 10:06:12','room-layouts/RTGYetGug3bU5vUD6KO26u9vGRqC22hkBNOymLHg.png','1'),(104,2,1,'A','A',59.00,9.79,'Floor Area: 21 SQM','2019-03-31 10:06:37','2019-03-31 10:06:37','room-layouts/yfbL3sFIjedZghTbQf47Rl3XMyplFM6zwIdH0Osm.png','1'),(105,2,1,'A','A',63.71,10.60,'Floor Area: 21 SQM','2019-03-31 10:07:06','2019-03-31 10:07:06','room-layouts/rvJW6JhqMmdcZQB3xHsTabpX5QmeYX61qDR9AKVp.png','1'),(106,2,1,'A','A',68.02,10.60,'Floor Area: 21 SQM','2019-03-31 10:07:28','2019-03-31 10:07:28','room-layouts/GkUZl8dIMKRXN4C5swZHmBPgpfd1v9O50xrfY8GV.png','1'),(107,2,1,'A','A',72.92,11.41,'Floor Area: 21 SQM','2019-03-31 10:07:47','2019-03-31 10:07:47','room-layouts/9hubBo5qhPXqLQph6MdRTZuE2TdN6jtz793twHRI.png','1'),(108,4,1,'A','A',16.65,4.96,'Floor Area: 19.5 SQM','2019-04-01 10:28:04','2019-04-01 10:28:04','room-layouts/jAXmu3wt7nQPPHm2HhcWoeUqjfepCJpXWAvugwhp.png','1'),(109,4,1,'A','A',22.73,4.96,'Floor Area: 19.5 SQM','2019-04-01 10:28:27','2019-04-01 10:28:27','room-layouts/Wl0C3ckXHW1Wqb53G45IXMS0fKLocjQ7DUxsxLAd.png','1'),(110,4,1,'A','A',28.22,4.96,'Floor Area: 19.5 SQM','2019-04-01 10:28:46','2019-04-01 10:28:46','room-layouts/rlTxlqW4NvM55gZGKNnOAadGKzc0b35PX7r9BViC.png','1'),(111,4,1,'D','D',16.45,48.00,'Floor Area: 41.17 SQM','2019-04-01 10:33:33','2019-04-01 10:33:33','room-layouts/jtBL4RMtwrXvuXQQutJ7gCFAh35qeEWy5d2UjUUs.png','1'),(112,4,2,'D','D',92.14,11.66,'Floor Area: 41.17 SQM','2019-04-01 10:34:16','2019-04-01 10:34:16','room-layouts/NyLJPRjKitkreDec3zeYNm4mtUZ5Xt39FFNW7AgI.png','1'),(113,4,1,'C','C',91.75,86.14,'Floor Area: 37.6 SQM','2019-04-01 10:35:21','2019-04-01 10:35:21','room-layouts/bfc2jycv2cyQEjaHaiWmp1TvZ4xJHKVYM20dCHHU.png','1'),(114,2,1,'E','E',78.02,12.23,'Floor Area: 21.81 SQM','2019-04-01 10:38:19','2019-04-01 10:38:19','room-layouts/NROOCU2WEiw76b0qeD17VwhFJSwYs1mIyzZv7blY.png','1'),(115,2,1,'F','F',87.63,2.88,'Floor Area: 28.55 SQM','2019-04-01 10:38:54','2019-04-01 10:38:54','room-layouts/LXztAGl9vLIE1r8IflzZeRitu1NFR8nyEAKrfNpF.png','1'),(116,2,1,'G','G',4.49,4.91,'Floor Area: 23.25 SQM','2019-04-01 10:39:18','2019-04-01 10:39:18','room-layouts/zJ1RcFQbODgpoAtrvqzJupEPfat1P8qy7xJHvzE3.png','1'),(117,1,1,'G','G',4.69,4.07,'Floor Area: 23.25 SQM','2019-04-01 10:42:40','2019-04-01 10:42:40','room-layouts/Eeh2OzwX6Dh88W88QsPB3emS8c0pjMnyxkPW1zz6.png','1');
/*!40000 ALTER TABLE `units` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Shared Credentials','vista-shared-credentials@praxxys.ph',NULL,'$2y$10$XzAke62Gbjen/o7ZjiM28ei6Wi/UGEkpZ7FX47aWz84p35lNvmTUG',NULL,'2019-03-20 05:46:47','2019-03-20 05:46:47');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-29 13:47:09
