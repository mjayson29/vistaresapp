
if (typeof praxxys === 'undefined') {
	var praxxys = {};
}

praxxys.broadcasterAdmin = {

	// Set default options
	options: {
		userID: 0,
		sound: '/vendor/praxxys/broadcaster-admin/notification.mp3',
		adminModel: 'SET THIS IN js.blade.php!!!',
		routes: {
			getNotifications: '/admin/notifications/get',
			readNotifications: '/admin/notifications/read',
		},
		notifications: []
	},

	init: function() {
		this.websocket.init();
	},

	websocket: {
		app: null,
		notificaitonSound: null,
		init: function() {

			// set sound from config
			this.notificationSound = new Audio(praxxys.broadcasterAdmin.options.sound);
			


			this.app = new Vue({
			    el: '#notificationsMenu',

			    data: function() {
			        return {
			            notifications: [],
			        };
			    },

			    mounted: function() {
			    	console.log('vue ready');
					this.$nextTick(function () {
				        this.notify();
				        this.getNotifications();		    
					});
			    },

			    computed: {
		    		count: function() {
		    			return this.notifications.length;
		    		},
		    		countWord: function() {
		    			return this.notifications.length !== 1 ? 'notifications' : 'notification';
		    		}
			    },

			    methods: {

			        notify: function() {
			            echo.private(praxxys.broadcasterAdmin.options.adminModel + '.' + praxxys.broadcasterAdmin.options.userID)
			            .notification(
			            	function(notif) {
			                	praxxys.broadcasterAdmin.websocket.app.notifications.unshift(
			                		{
			                			id: notif.id,
			                			type: notif.type,
			                			data: {
			                				message: notif.message
			                			}
			                		});

								praxxys.broadcasterAdmin.websocket.notificationSound.play();
			            	}
			            );
			        },

			        getNotifications: function() {
			        	axios.get(praxxys.broadcasterAdmin.options.routes.getNotifications)
			        	.then(function(response) {
							praxxys.broadcasterAdmin.websocket.app.notifications = response.data;
						})
			        	.catch(function(e) {
			        		console.log(e);
			        	});
			        },

			        readNotifications: function() {
			        	axios.get(praxxys.broadcasterAdmin.options.routes.readNotifications)
			        	.catch(function(e) {
			        		console.log(e);
			        	});

			        },

			        renderNotification: function(type) {
	        			return praxxys.broadcasterAdmin.options.notifications[type];
			        },
			    }
			});
		}
	}
};