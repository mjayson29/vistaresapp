$(document).ready(function() {
	main.init();
});

var main = {

	init: function () {

		var setup = this.setup;
		setup.pinUnit();
	},

	setup: {

		pinUnit: function() {


		    function FindPosition(oElement)
		    {

		        if(typeof( oElement.offsetParent ) != "undefined")
		        {
		            for(var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent)
		            {
		                posX += oElement.offsetLeft;
		                posY += oElement.offsetTop;
		            }
		            return [ posX, posY ];
		        }
		        else
		        {   
		            return [ oElement.x, oElement.y ];
		        }
		    }

			function getCoordinates(e) {
				
				var unitID = $('#unitID');

				var PosX = 0;
				var PosY = 0;
				var x = $('#x_coordinates');
				var y = $('#y_coordinates');
				var unitNumber = $('#unitNumber').val();

				// if(!unitNumber) {
				// 	swal('Set Unit/Room number', 'Please set Unit/Room number to proceed', 'info');
				// 	return;
				// }

				var ImgPos;

              	ImgPos = FindPosition(image);
				if (!e) var e = window.event;
              	if (e.pageX || e.pageY) {
	                PosX = e.pageX;
	                PosY = e.pageY;
              	}
              	else if (e.clientX || e.clientY) {
					PosX = e.clientX + document.body.scrollLeft
						+ document.documentElement.scrollLeft;
					PosY = e.clientY + document.body.scrollTop
						+ document.documentElement.scrollTop;
                }

				PosX = PosX - ImgPos[0];
				PosY = PosY - ImgPos[1];

				y.val((PosY / image.clientHeight) * 100 - 2);
				x.val((PosX / image.clientWidth) * 100 - 1);


				// $('.tag').remove();
				// $('#unit' + '' + unitID.val()).remove();
				// $('#imageHolder').prepend('\
				// 	<div class="tag" style="position: absolute; top: '+ y.val() +'%; left: '+ x.val() +'%; color: #fff; font-weight: bold;font-size:18px">'+ unitNumber +'</div>\
				// ');
				
				if(!unitNumber) {
					/**
					* Building Layout
					 */
					var width = $('#width');
					var height = $('#height');
					$('.tag').remove();
					$('#unit' + '' + unitID.val()).remove();

					$('#width').keyup(function(e) {
		                $('.tag').css('width', width.val() + '%')
		            })

		            $('#height').keyup(function(e) {
		                $('.tag').css('height', height.val() + '%')
		            })


					$('#imageHolder').prepend('\
						<div class="tag" style="position: absolute; top: '+ y.val() +'%; left: '+ x.val() +'%; height: ' + height.val() +'%; width: ' + width.val() +'%; background: #00000085"></div>\
					');

					$('#imageHolder360').prepend('\
						<div class="tag" style="position: absolute; top: '+ y.val() +'%; left: '+ x.val() +'%; height: ' + height.val() +'%; width: ' + width.val() +'%; background: #000; border-radius: 50%;"></div>\
					');
				} else {
					/**
					* Units Layout
					 */
					$('.tag').remove();
					$('#unit' + '' + unitID.val()).remove();
					$('#imageHolder').prepend('\
						<div class="tag" style="position: absolute; top: '+ y.val() +'%; left: '+ x.val() +'%; color: #fff; font-weight: bold;font-size:18px">'+ unitNumber +'</div>\
					');
				}
				

			}

		
            var image = document.getElementById("image");
            image.onmousedown = getCoordinates;

		},

	}

};


