
$('document').ready(function() {
	main.init();
});

var main = {
	init: function() {
		this.alertStatusMessage();
	},

	alertStatusMessage: function() {
		if(systemVars.status.title !== null) {

			swal({
			  	title: systemVars.status.title,
			  	text: systemVars.status.message,
			  	type: systemVars.status.type,
				confirmButtonColor: '#E0C34C',
			});

		}
	},
};
