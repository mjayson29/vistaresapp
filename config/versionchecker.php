<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Version Checker
    |--------------------------------------------------------------------------
    */

   	/** ANDROID VERSION */
    'android_version' => env('ANDROID_MINIMUM_VERSION', ''),
    
    /** IOS VERSIONS */
    'ios_version' => env('IOS_MINIMUM_VERSION', '')

];