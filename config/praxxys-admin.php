<?php

return [

   /*
    |--------------------------------------------------------------------------
    | General Settings
    |--------------------------------------------------------------------------
    */

    'title' => 'VISTA Admin',
    'app-id' => 'app', 
    'theme' => 'skin-theme',
    'header' => [
        'main' => 'VISTA ',
        'sub' => 'Admin',
        'abbr' => 'VST',
        'link' => '/',
    ],
    
    'meta-tags' => [
        '<link rel="apple-touch-icon" href="apple-touch-icon.png">',
        '<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">',
    ],

    'footer' => [
        'copyrights' => \Carbon\Carbon::now()->year,
        'caption' => 'Make it Happen.',
        'link' => '/',
    ],

    'avatars' => [
        'background' => '#f39c12',
        'color' => '#fff',
        'border' => 0,
    ],

   /*
    |--------------------------------------------------------------------------
    | Admin Navigation Sections
    |--------------------------------------------------------------------------
    */

    'navigation' => [
        'CMS' => [

            'Manage' => [
                'icon' => 'fa fa-tasks',

                'items' => [
                    'Projects' => [
                        'route' => 'admin.project.index',
                        'icon' => '',
                    ],
                    'Rooms' => [
                        'route' => 'admin.rooms.index',
                        'icon' => '',
                    ],
                    'Units/Rooms Types' => [
                        'route' => 'admin.room_types.index',
                        'icon' => '',
                    ],
                    'Amenities' => [
                        'route' => 'admin.amenities.index',
                        'icon' => '',
                    ],

                    'Commercial Areas' => [
                        'route' => 'admin.commercial-areas.index',
                        'icon' => '',
                    ],

                    'Sold Out Projects' => [
                        'route' => 'admin.sold-out-projects.index',
                        'icon' => '',
                    ],

                    'Home Banner' => [
                        'route' => 'admin.home-banner.index',
                        'icon' => '',
                    ],

                    'Exchange Rates' => [
                        'route' => 'admin.exchange_rates.index',
                        'icon' => ''
                    ],

                    'General Pages' => [
                        'route' => 'admin.general-pages.index',
                        'icon' => '',
                    ],

                ],
            ],
        ],

        
        'Security' => [

            'Access Control' => [
                'icon' => 'fa fa-shield-alt',

                'items' => [
                    'Administrators' => [
                        'route' => 'admin-users.index',
                        'icon' => 'fa fa-user-circle',
                    ],
                    'Roles' => [
                        'route' => 'roles.index',
                        'icon' => 'fa fa-address-card',
                    ],
                    'Permissions' => [
                        'route' => 'permissions.index',
                        'icon' => 'fa fa-shield-alt',
                    ],
                ],
            ],

            'Logs' => [
                'icon' => 'fa fa-clipboard-list',
                'route' => 'admin-logs.index',
            ]

        ],
    ],

   /*
    |--------------------------------------------------------------------------
    | Default Pages
    |--------------------------------------------------------------------------
    | 
    | Sets the default redirect page after admin login based on user role.
    |
    | Note: The array is read sequentially. Make sure to list the highest role 
    | (e.g., super-admin) first.
    |
    */

    'default-redirects' => [
        'super-admin' => 'admin-users.index',
    ],

    /*
    |--------------------------------------------------------------------------
    | Logging
    |--------------------------------------------------------------------------
    */

    'enable_log' => false,

    /*
    |--------------------------------------------------------------------------
    | Security
    |--------------------------------------------------------------------------
    */

    'security' => [
        'default-routes' => true,
        'file-protection' => true,
        'storage-path' => 'praxxys/admin',
        'guard' => 'praxxys-admin',
        'resources' => [
            'admin-users' => \PRAXXYS\Admin\Models\Admin::class
        ],
        'admin-model' => \PRAXXYS\Admin\Models\Admin::class
    ],

    /*
    |--------------------------------------------------------------------------
    | Storage
    |--------------------------------------------------------------------------
    */

    'storage' => [
        'driver' => 'local'
    ],

    /*
    |--------------------------------------------------------------------------
    | Images
    |--------------------------------------------------------------------------
    */

    'images' => [
        'storage-path' => 'praxxys/admin/images',
    ],

    /*
    |--------------------------------------------------------------------------
    | Caching
    |--------------------------------------------------------------------------
    */
    'cache' => [
        'images' => [
            'store' => 'praxxys-admin-file',
            'route' => 'images/cache'
        ],
        'paths' => [
            'admin-avatar',
            'uploads'
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Broadcasting
    |--------------------------------------------------------------------------
    */
    'broadcast' => env('BROADCASTER_APP_ENABLED', false),
    
    /*
    |--------------------------------------------------------------------------
    | Custom Assets
    |--------------------------------------------------------------------------
    */

    'assets' => [
        'path' => '/vendor/praxxys/admin/',
        'css' => [
            'remote' => [
                [
                    'uri' => 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',
                    'description' => 'Google Fonts'
                ],
            ],

            'local' => [
                [
                    'uri' => 'assets/css/admin.min.css',
                    'description' => 'Main Admin Stylesheet'
                ],
            ],
        ],
        'js' => [
            'remote' => [
                [
                    'tag' => '<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>',
                    'description' => 'Font Awesome JS',
                ],
            ],

            'local' => [
                [
                    'uri' => 'assets/js/app.min.js',
                    'description' => 'Compiled Node Dependencies'
                ],
                [
                    'uri' => 'assets/js/main.min.js',
                    'description' => 'Main Admin Scripts'
                ],

                [
                    'uri' => 'assets/js/custom.js',
                    'description' => 'Custom Js'
                ],  

                [
                    'uri' => 'assets/js/image-view.js',
                    'description' => 'Custom Js'
                ],                
            ],

        ],
    ],




    /*
    |--------------------------------------------------------------------------
    | Custom Guards
    |--------------------------------------------------------------------------
    |
    | Override existing guards in order to utilize custom Admin model
    | authentication.
    |
    | This section defines a custom guard, custom password broker, and auth 
    | provider.
    */

    'laravel' => [

        'auth' => [
            'guards' => [
                'praxxys-admin' => [
                    'driver' => 'session',
                    'provider' => 'admins'
                ],
            ],
            'passwords' => [
                'praxxys-admin' => [
                    'provider' => 'admins',
                    'table' => 'admin_password_resets',
                    'expire' => 60,
                ],
            ],
            'providers' => [
                'admins' => [
                    'driver' => 'eloquent',
                    'model' => \PRAXXYS\Admin\Models\Admin::class
                ],
            ],
        ],

        'cache' => [
            'stores' => [
                'praxxys-admin-file' => [
                    'driver' => 'file',
                    'path' => storage_path('framework/cache/praxxys-admin'),
                ],
            ]
        ],

        'praxxys-broadcaster' => [
            'channels' => [
                'private' => [
                    'admins' => \PRAXXYS\Admin\Models\Admin::class
                ]

            ]
        ],

        'imagecache' => [
            'templates' => [
                'square-tiny' => 'PRAXXYS\Admin\ImageFilters\SquareTiny',
                'square-medium' => 'PRAXXYS\Admin\ImageFilters\SquareMedium',
                'square-large' => 'PRAXXYS\Admin\ImageFilters\SquareLarge',
                'normal' => 'PRAXXYS\Admin\ImageFilters\Normal',
            ]
        ],
    ]
];