<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function() {

	Route::group(['middleware' => ['cors']], function () {
		/*
		|--------------------------------------------------------------------------
		| @Laravel Passport
		|--------------------------------------------------------------------------
		*/
		Route::get('fetch/secret', 'Api\AuthController@getClientSecret');	

		Route::get('check/version', 'Api\VersionCheckerController@getVersion');

	});

	Route::group(['middleware' => ['auth:api']], function() {

		Route::get('/home', 'Api\HomePageController@index');
		Route::get('/projects', 'Api\ProjectPageController@index');
		Route::get('/view/project/{id}', 'Api\ProjectPageController@view');
		Route::get('/floorlayout/{id}', 'Api\FloorLayoutController@index');

		Route::get('/floor/type/{floor}/{type}', 'Api\FloorLayoutController@filterByType');


		/*
		|--------------------------------------------------------------------------
		| @General Pages
		|--------------------------------------------------------------------------
		*/
		Route::get('/terms', 'Api\GeneralPageController@terms');
		Route::get('/privacy', 'Api\GeneralPageController@privacy');
		Route::get('/calculator', 'Api\GeneralPageController@calculator');
		Route::get('/introduction', 'Api\GeneralPageController@introduction');


		/*
		|--------------------------------------------------------------------------
		| @Gallery
		|--------------------------------------------------------------------------
		*/
		Route::get('/gallery/{id}', 'Api\GalleryController@fetch');

	});

});




