<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/admin/login');
});

Route::get('/home', function () {
    return redirect('/admin/login');
});


/*
|--------------------------------------------------------------------------
| @PRX Admin Routes
|--------------------------------------------------------------------------
*/
Route::name('admin.')
    ->prefix('admin')
    ->middleware('prxadmin')
    ->group(function() { 


		/*
		|--------------------------------------------------------------------------
		| @Room
		|--------------------------------------------------------------------------
		*/
		Route::resource('rooms', 'RoomController')->except([
			'create',
			'show'
		]);
		Route::get('rooms/create/{project?}/{name?}', 'RoomController@create')->name('rooms.create');

		/*
		|--------------------------------------------------------------------------
		| @Amenities
		|--------------------------------------------------------------------------
		*/
		Route::resource('amenities', 'AmenityController')->except([
			'create',
			'show'
		]);
		Route::get('amenities/create/{project?}/{name?}', 'AmenityController@create')->name('amenities.create');	


		/*
		|--------------------------------------------------------------------------
		| @Commercial Areas
		|--------------------------------------------------------------------------
		*/
		Route::resource('commercial-areas', 'CommercialAreaController')->except([
			'create',
			'show'
		]);
		Route::get('commercial-areas/create/{project?}/{name?}', 'CommercialAreaController@create')->name('commercial-areas.create');	


		/*
		|--------------------------------------------------------------------------
		| @Sold Out Project
		|--------------------------------------------------------------------------
		*/
		Route::resource('sold-out-projects', 'SoldOutProjectController')->except([
			'show'
		]);
		Route::get('sold-out-projects/{project}/{name}', 'SoldOutProjectController@show')->name('sold-out-projects.show');		

		/*
		|--------------------------------------------------------------------------
		| @Room Layout
		|--------------------------------------------------------------------------
		*/
		// Route::resource('room-layout', 'RoomLayoutController')->except([
		// 	'create',
		// 	'show'
		// ]);
		// Route::get('room-layout/create/{floor_layout?}/{name?}', 'RoomLayoutController@create')->name('room-layout.create');

		/*
		|--------------------------------------------------------------------------
		| @Home page banner
		|--------------------------------------------------------------------------
		*/
		Route::resource('home-banner', 'HomeBannerController')->except([
			'show',
			'destroy'
		]);

		/*
		|--------------------------------------------------------------------------
		| @General Page
		|--------------------------------------------------------------------------
		*/
		Route::resource('general-pages', 'GeneralPageController')->except([
			'create',
			'store',
			'show',
			'destroy'
		]);
		Route::get('general-pages/{page}/{name}', 'GeneralPageController@show')->name('general-pages.show');

		/*
		|--------------------------------------------------------------------------
		| @General Page item
		|--------------------------------------------------------------------------
		*/
		Route::resource('general-page-item', 'GeneralPageItemController')->except([
			'create',
			'store',
			'show',
			'destroy'
		]);

		Route::resource('exchange_rates', 'ExchangeRateController');

		Route::resource('room_types', 'RoomTypeController');


		Route::group(['prefix' => 'projects'], function() {

			/*
			|--------------------------------------------------------------------------
			| @Project
			|--------------------------------------------------------------------------
			*/
			Route::get('/', 'ProjectController@index')->name('project.index');
			Route::get('{project}/{name}', 'ProjectController@show')->name('project.view');
			Route::get('edit/{project}/{name}', 'ProjectController@edit')->name('project.edit');
			Route::put('update/{project}', 'ProjectController@update')->name('project.update');

			Route::get('building-layout/{project}/{name}', 'BuildingLayoutController@index')->name('building-layouts.index');
			Route::get('/building-layout/store/{project}/{name}', 'BuildingLayoutController@create')->name('project.layout.create');
			Route::post('/building-layouts/store/{project}', 'BuildingLayoutController@store')->name('building-layout.store');
			Route::get('building-layout/{project}/{layout}/edit', 'BuildingLayoutController@edit')->name('building-layout.edit');
			Route::put('building-layout/{layout}/update', 'BuildingLayoutController@update')->name('building-layout.update');	
			Route::delete('building-layout/{layout}/destroy', 'BuildingLayoutController@destroy')->name('building-layout.destroy');		



			/*
			|--------------------------------------------------------------------------
			| @Gallery
			|--------------------------------------------------------------------------
			*/	
			Route::get('gallery/{project}/{name}', 'GalleryController@index')->name('gallery.index');
			Route::get('gallery/create/{project}/{name}', 'GalleryController@create')->name('gallery.create');
			Route::get('gallery/edit/{id}/{project}/{name}', 'GalleryController@edit')->name('gallery.edit');
			Route::post('gallery/store/{project}', 'GalleryController@store')->name('gallery.store');
			Route::put('gallery/update/{id}', 'GalleryController@update')->name('gallery.update');
			Route::delete('gallery/destroy/{id}', 'GalleryController@destroy')->name('gallery.destroy');

			/*
			|--------------------------------------------------------------------------
			| @Floor Layout
			|--------------------------------------------------------------------------
			*/
			Route::get('{project}/{name}/floor-layouts/', 'FloorLayoutController@index')->name('floor-layout.index');
			Route::get('{project}/{name}/floor-layoust/create', 'FloorLayoutController@create')->name('floor-layout.create');
			Route::post('{project}/floor-layouts/store', 'FloorLayoutController@store')->name('floor-layout.store');
			Route::get('/{project?}/{floor}/floor-layouts/edit', 'FloorLayoutController@edit')->name('floor-layout.edit');

			Route::put('{project?}/{floor}/floor-layouts/update', 'FloorLayoutController@update')->name('floor-layout.update');

			Route::delete('floor-layouts/{floor}/destroy', 'FloorLayoutController@destroy')->name('floor-layout.destroy');


			/*
			|--------------------------------------------------------------------------
			| @Floor Layout
			|--------------------------------------------------------------------------
			*/
			Route::get('units/{floor}/{name}', 'UnitController@index')->name('units.index');
			Route::get('units/{floor}/{name}/create', 'UnitController@create')->name('units.create');
			Route::post('units/{floor}/store', 'UnitController@store')->name('units.store');
			Route::get('units/{floor}/{unit}/edit', 'UnitController@edit')->name('units.edit');
			Route::put('units/{unit}/update', 'UnitController@update')->name('units.update');	
			Route::delete('units/{unit}/destroy', 'UnitController@destroy')->name('units.destroy');	

			/*
			|--------------------------------------------------------------------------
			| @Room Layout
			|--------------------------------------------------------------------------
			*/
			// Route::get('room-layouts/{floor}/{name}', 'RoomLayoutController@index')->name('room-layouts.index');	
			// Route::get('room-layouts/{floor}/{name}/create', 'RoomLayoutController@create')->name('room-layouts.create');	
			// Route::post('room-layouts/store', 'RoomLayoutController@store')->name('room-layouts.store');	
			// Route::get('room-layouts/{id}/{unit}/edit', 'RoomLayoutController@edit')->name('room-layouts.edit');	
			// Route::put('room-layouts/{roomlayout}/update', 'RoomLayoutController@update')->name('room-layouts.update');
			// Route::delete('room-layouts/{roomlayout}/destroy', 'RoomLayoutController@destroy')->name('room-layouts.destroy');			
		});

});