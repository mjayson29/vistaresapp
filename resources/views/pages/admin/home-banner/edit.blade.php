@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ $banner->renderBannerTitle() }}
        <small>Update {{ $banner->renderBannerTitle() }}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-image"></i> {{ $banner->renderBannerTitle() }}</a></li>
        <li class="active">Update</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title">Update {{ $banner->renderBannerTitle() }}</h3>
                </div>
                <div class="box-body">
                    
                    @include('PRXAdmin::includes.form-errors')

                    <form role="form" method="post" action="{{ route('admin.home-banner.update', $banner->id) }}"
                        enctype="multipart/form-data">
                        
                        {{ csrf_field() }}

                        {{ method_field('PUT') }}

                        <div class="box-body">
                            <div class="row">


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input name="title" type="title" class="form-control" placeholder="Title" value="{{ $banner->title }}">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea id="description" name="description" rows="3" class="form-control" required>{{ $banner->description }}</textarea>
                                    </div> 
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        
                                        <label for="image_path">Image</label>

                                        <div class="form-group">
                                            <img src="{{ $banner->renderImagePath() }}" width="300" height="200">
                                        </div>

                                        <input type="file" name="image_path" id="image_path" class="form-control">
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer text-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop