@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Home Banner
        <small>List of Home Banner</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-image"></i> Home Banner</a></li>
        <li class="active">Index</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row margin-bottom">
        <div class="col-md-12">
         
            @if(!$banner)
                <a href="{{ route('admin.home-banner.create') }}" type="button" class="btn btn-success"><i class="fa fa-plus-circle"></i> Create Home Banner</a>
            @else
                <a href="{{ route('admin.home-banner.edit', $banner->id) }}" type="button" class="btn btn-primary"><i class="fa fa-edit"></i> Update Home Banner</a>
            @endif
        </div>

        <!-- /.col -->
    </div>
    <!-- /.row -->      

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Home Banner</h3>
                </div>
                <div class="box-body">
                    @if($banner)
                            <div class="text-center">
                                <div class="form-group">
                                    <label>{{ $banner->renderBannerTitle() }}</label>
                                </div>
                                <center>
                                    <div class="col-md-12">
                                        <img class="img-responsive"
                                        src="{{ $banner->renderImagePath() }}"
                                        >
                                    </div>
                                </center>

                            </div>

                    @else
                        <div class="text-center form-group">
                            <label>No banner. <a href="{{ route('admin.home-banner.create') }}">Create banner?</a></label>
                        </div>

                    @endif
                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop