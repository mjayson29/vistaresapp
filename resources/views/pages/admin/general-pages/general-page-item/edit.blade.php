@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Page item of <a href="{{ route('admin.general-pages.show', [$pageItem->page->id, $pageItem->page->name]) }}">{{ $pageItem->page->name }}</a>
        <small>Update page item of {{ $pageItem->page->name }}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-file-alt"></i> {{ $pageItem->page->name }} page item</a></li>
        <li class="active">Update</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-body">
                    
                    @include('PRXAdmin::includes.form-errors')

                    <form role="form" method="post" action="{{ route('admin.general-page-item.update', $pageItem->id) }}">
                        
                        {{ csrf_field() }}

                        {{ method_field('PUT') }}

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="content">Content</label>
                                        <textarea id="content" name="content" placeholder="content" class="form-control editorField" rows="5">{{ $pageItem->content }}</textarea>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer text-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop