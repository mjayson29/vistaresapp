@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ $page->name }}
        <small>Show {{ $page->name }}</small>
        
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-file-alt"></i> {{ $page->name }}</a></li>
        <li class="active">Show</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row margin-bottom">
        <div class="col-md-12">
                
            <a href="{{ route('admin.general-pages.edit', [$page->id, $page->name]) }}" type="button" class="btn btn-primary"><i class="fa fa-edit"></i> Edit {{ $page->name }}</a>

        </div>

        <!-- /.col -->
    </div>
    <!-- /.row -->      

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    
                    <!-- PageItems -->                    
                    <div class="form-group">
                        <h3>Page Item</h3>
                    </div>

                    <table id="roomTable" class="dataTables table table-bordered">
                        <thead>
                            <th>Content</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($page->page_item as $page_item)
                                <tr>
                                    <td>{!! $page_item->content !!}</td>
                                    <td>
                                        <a href="{{ route('admin.general-page-item.edit', [$page_item->id]) }}" class="btn btn-sm btn-primary">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                   
                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop