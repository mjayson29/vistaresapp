@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        General Pages
        <small>List of General Pages</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-file-alt"></i> General Pages</a></li>
        <li class="active">Index</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row margin-bottom">
        <div class="col-md-12">
         
        </div>

        <!-- /.col -->
    </div>
    <!-- /.row -->      

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">General Pages</h3>
                </div>
                <div class="box-body">
                    
                    @foreach($pages as $page)

                        <div class="col-xs-4">
                            <a 
                            href="{{ route('admin.general-pages.show', [$page->id, $page->name]) }}"
                            type="button" class="btn btn-large btn-lg btn-block btn-primary"><i class="fa fa-file-alt"></i> {{ $page->name }}</a>
                        </div>

                    @endforeach

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop