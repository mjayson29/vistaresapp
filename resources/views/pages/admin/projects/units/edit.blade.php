@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Unit
        <small>Update {{ $unit->name }}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-building"></i> Update {{ $unit->name }}</a></li>
        <li class="active">Update</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title">Update {{ $unit->name }}</h3>
                </div>
                <div class="box-body">
                    
                    @include('PRXAdmin::includes.form-errors')

                    <!-- form start -->
                    <form role="form" method="post" action="{{ route('admin.units.update', $unit->id) }}" enctype="multipart/form-data">

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div id="imageHolder" style="width: 100%; height: 100%; position: relative; text-align: center;">

                                        <!-- Stored units -->
                                        @foreach($unit->floor->units as $unitInfo)
                                            <div id="unit{{ $unitInfo->id }}" style="position: absolute; top: {{ $unitInfo->y_coordinates  }}%; left: {{ $unitInfo->x_coordinates  }}%; color: #fff; font-weight: bold;font-size:18px">{{ $unitInfo->unit_number }}</div>
                                        @endforeach

                                        <img id="image" src="{{ $unit->floor->renderImagePath() }}" width="100%" height="100%">
                                    </div>
                                </div> 

                                <div class="col-md-6">
                                    <!-- Hidden Variable -->
                                    <input type="hidden" id="unitID" class="form-control" value="{{ $unit->id }}" required>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Unit/Room Number: </label>
                                            <input type="text" id="unitNumber" name="unit_number" placeholder="Unit Number" class="form-control" value="{{ $unit->unit_number }}" required>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Name: </label>
                                            <input type="text" id="name" name="name" placeholder="Name" class="form-control" value="{{ $unit->name }}" required>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Type: </label>
                                            <select class="form-control" name="room_type_id" id="type">
                                                @foreach($room_types as $type)
                                                    <option value="{{ $type->id }}"
                                                        @if($unit->type === $type->id)
                                                            selected 
                                                        @endif
                                                    >{{ $type->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea id="description" name="description" rows="3" class="form-control" required>{{ $unit->description }}</textarea>
                                        </div> 
                                    </div>     

                                    <div class="col-md-12">
                                        
                                        <div class="form-group">
                                            <img class="img-responsive" src="{{ $unit->renderImagePath() }}">
                                        </div>

                                        <div class="form-group">
                                            <label>Room <small>(room layout)</small></label>
                                            <input type="file" name="image_path" id="image_path" class="form-control">
                                        </div> 
                                    </div>    

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Availability for 360 view</label><br>
                                            <select class="form-control" name="available_360">
                                                <option value="1" selected="{{ $unit->available_360 ? 'selected' : '' }}">Yes, make it available.</option>
                                                <option value="0" {{ !$unit->available_360 ? 'selected' : '' }}>No for now.</option>
                                            </select>
                                        </div>
                                    </div>                            

                                    <!-- hidden inputs -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" id="x_coordinates" name="x_coordinates" placeholder="" class="form-control" value="{{ $unit->x_coordinates }}">
                                            <input type="hidden" id="y_coordinates" name="y_coordinates" placeholder="" class="form-control" value="{{ $unit->y_coordinates }}">                                            
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>

                        <!-- /.box-body -->
                        <div class="box-footer text-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop