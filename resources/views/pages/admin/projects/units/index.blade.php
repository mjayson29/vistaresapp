@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Units
        <small>List of Units of {{ $floor->name }}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-building"></i> Units</a></li>
        <li class="active">Index</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row margin-bottom">
        <div class="col-md-12">
            <a href="{{ route('admin.units.create', [$floor->id, $floor->name]) }}" type="button" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add Unit</a>            
            {{-- <a href="{{ route('admin.room-layouts.index', [$floor->id, $floor->name]) }}" type="button" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Room Layout</a> --}}            
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->      

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Units</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <div id="imageHolder" style="width: 100%; height: 100%; position: relative; text-align: center;">

                                <!-- Stored units -->
                                @foreach($floor->units as $unit)
                                    <div style="position: absolute; top: {{ $unit->y_coordinates  }}%; left: {{ $unit->x_coordinates  }}%; color: #fff; font-weight: bold;font-size:18px">{{ $unit->unit_number }}</div>
                                @endforeach


                                <img id="image" src="{{ $floor->renderImagePath() }}" width="100%" height="100%">
                            </div>
                        </div>
                    <div class="col-md-3"></div> 
                

                    <div class="row">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <table id="roomTable" class="dataTables table table-bordered">
                                    <thead>
                                        <th>Project</th>
                                        <th>Floor Layout</th>
                                        <th>Name</th>
                                        <th>Room Number</th>
                                        <th>Actions</th>
                                    </thead>

                                    <tbody>
                                        @foreach($floor->units as $unit)
                                            <tr>
                                                <td>{{ $unit->floor->project->name }}</td>
                                                <td>{{ $unit->floor->name }}</td>
                                                <td>{{ $unit->name}}</td>
                                                <td>{{ $unit->unit_number }}</td>
                                                <td>
                                                    <a href="{{ route('admin.units.edit', [$unit->floor->id, $unit->id]) }}" class="btn btn-sm btn-primary">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a class="btn btn-sm btn-danger deleteBtn"
                                                        data-action="{{ route('admin.units.destroy', $unit->id) }}"
                                                        data-message="Are you sure you want to archived {{ $unit->name }} ?">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop