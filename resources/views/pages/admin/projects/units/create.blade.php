@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Unit
        <small>Add Unit to {{ $floor->name }}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-building"></i> Add Unit to {{ $floor->name }}</a></li>
        <li class="active">Create</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title">Add Unit to {{ $floor->name }}</h3>
                </div>
                <div class="box-body">
                    
                    @include('PRXAdmin::includes.form-errors')

                    <!-- form start -->
                    <form role="form" method="post" action="{{ route('admin.units.store', $floor->id) }}" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div id="imageHolder" style="width: 100%; height: 100%; position: relative; text-align: center;">

                                        <!-- Stored units -->
                                        @foreach($floor->units as $unit)
                                            <div style="position: absolute; top: {{ $unit->y_coordinates  }}%; left: {{ $unit->x_coordinates  }}%; color: #fff; font-weight: bold;font-size:18px">{{ $unit->unit_number }}</div>
                                        @endforeach
                                        <img id="image" src="{{ $floor->renderImagePath() }}" width="100%" height="100%">
                                    </div>
                                </div> 

                                <div class="col-md-6">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Unit/Room Number: </label>
                                            <input type="text" id="unitNumber" name="unit_number" placeholder="Unit Number" class="form-control" value="{{ old('unit_number') }}" required>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Name: </label>
                                            <input type="text" id="name" name="name" placeholder="Name" class="form-control" value="{{ old('name') }}" required>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Type: </label>
                                            <select class="form-control" name="room_type_id" id="type">
                                                @foreach($room_types as $type)
                                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea id="description" name="description" rows="3" class="form-control" required>{{ old('description') }}</textarea>
                                        </div> 
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Room <small>(room layout)</small></label>
                                            <input type="file" name="image_path" id="image_path" class="form-control" required>
                                        </div> 
                                    </div>    

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Availability for 360 view</label><br>
                                            <select class="form-control" name="available_360">
                                                <option value="1">Yes, make it available.</option>
                                                <option value="0">No for now.</option>
                                            </select>
                                        </div>
                                    </div>                                

                                    <!-- hidden inputs -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" id="x_coordinates" name="x_coordinates" placeholder="" class="form-control">
                                            <input type="hidden" id="y_coordinates" name="y_coordinates" placeholder="" class="form-control">                                            
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>

                        <!-- /.box-body -->
                        <div class="box-footer text-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop