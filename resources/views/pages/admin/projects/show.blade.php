@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ $project->name }}
        <small>Show {{ $project->name }}</small>
        
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-building"></i> {{ $project->name }}</a></li>
        <li class="active">Show</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row margin-bottom">
        <div class="col-md-12">
                
            <a href="{{ route('admin.project.edit', [$project->id, $project->name]) }}" type="button" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>

            <a href="{{ route('admin.gallery.index', [$project->id, $project->name]) }}" type="button" class="btn btn-info"><i class="fa fa-image"></i> View Gallery</a>

            <a href="{{ route('admin.building-layouts.index', [$project->id, $project->name]) }}" type="button" class="btn btn-danger"><i class="fas fa-eye"></i> View Building Layout</a>

            <a href="{{ route('admin.floor-layout.index', [$project->id, $project->name]) }}" type="button" class="btn btn-success"><i class="fas fa-eye"></i> View Floor Layout</a>

        </div>

        <!-- /.col -->
    </div>
    <!-- /.row -->      

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">{{ $project->name }}</h3>
                </div>
                <div class="box-body">
                    
                    <!-- Room -->                    
                    <div class="col-xs-12">
                        <div class="form-group">
                            <h3>Rooms List</h3>

                            <a href="{{ route('admin.rooms.create', [$project->id, $project->name]) }}" type="button" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add Room</a>
                        </div>

                        <table id="roomTable" class="dataTables table table-bordered">
                            <thead>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Image</th>
                                <th>Action</th>
                            </thead>

                            <tbody>
                                @foreach($project->rooms as $room)
                                    <tr>
                                        <td>{{ $room->name }}</td>
                                        <td>{{ $room->renderShortDescription() }}</td>
                                        <td>
                                            <img src="{{ $room->renderImagePath() }}" width="50" height="50" alt="{{ $room->name }}">
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.rooms.edit', [$room->id]) }}" class="btn btn-sm btn-primary">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a class="btn btn-sm btn-danger deleteBtn"
                                                data-action="{{ route('admin.rooms.destroy', $room->id) }}"
                                                data-message="Are you sure you want to archived {{ $room->name }} ?">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div> <!-- Rooms ./end -->


                    <!--  Amenities -->
                    <div class="col-xs-12">
                        <div class="form-group">
                            <h3>Amenities List</h3>

                            <a href="{{ route('admin.amenities.create', [$project->id, $project->name]) }}" type="button" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add Amenities</a>
                        </div>

                        <table id="amenitiesTable" class="dataTables table table-bordered">
                            <thead>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Image</th>
                                <th>Action</th>
                            </thead>

                            <tbody>
                                @foreach($project->amenities as $amenity)
                                    <tr>
                                        <td>{{ $amenity->name }}</td>
                                        <td>{{ $amenity->renderShortDescription() }}</td>
                                        <td>
                                            <img src="{{ $amenity->renderImagePath() }}" width="50" height="50" alt="{{ $amenity->name }}">
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.amenities.edit', [$amenity->id]) }}" class="btn btn-sm btn-primary">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a class="btn btn-sm btn-danger deleteBtn"
                                                data-action="{{ route('admin.amenities.destroy', $amenity->id) }}"
                                                data-message="Are you sure you want to archived {{ $amenity->name }} ?">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                    <!-- Commercial Areas -->
                    <div class="col-xs-12">
                        <div class="form-group">
                            <h3>Commercial Areas List</h3>

                            <a href="{{ route('admin.commercial-areas.create', [$project->id, $project->name]) }}" type="button" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add Amenities</a>
                        </div>

                        <table id="commercialAreaTable" class="dataTables table table-bordered">
                            <thead>
                                <th>Name</th>
                                {{-- <th>Description</th> --}}
                                <th>Image</th>
                                <th>Action</th>
                            </thead>

                            <tbody>
                                @foreach($project->commercial_areas as $commercial_area)
                                    <tr>
                                        <td>{{ $commercial_area->name }}</td>
                                        {{-- <td>{{ $commercial_area->renderShortDescription() }}</td> --}}
                                        <td>
                                            <img src="{{ $commercial_area->renderImagePath() }}" width="50" height="50" alt="{{ $commercial_area->name }}">
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.commercial-areas.edit', [$commercial_area->id]) }}" class="btn btn-sm btn-primary">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a class="btn btn-sm btn-danger deleteBtn"
                                                data-action="{{ route('admin.commercial-areas.destroy', $commercial_area->id) }}"
                                                data-message="Are you sure you want to archived {{ $commercial_area->name }} ?">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>


                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop