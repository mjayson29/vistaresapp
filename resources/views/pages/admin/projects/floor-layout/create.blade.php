@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Floor Layout
        <small>Add Floor Layout</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-image"></i> Add Floor Layout</a></li>
        <li class="active">Create</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title">Add Floor Layout for {{ $project->name }}</h3>
                </div>
                <div class="box-body">
                    
                    @include('PRXAdmin::includes.form-errors')

                    <!-- form start -->
                    <form role="form" method="post" action="{{ route('admin.floor-layout.store', $project->id) }}" 
                        enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="box-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" id="name" class="form-control" required>
                                    </div> 
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Type</label>
                                        <select class="form-control" id="type" name="type">
                                            @foreach($types as $type)

                                                <option value="{{ $type['value'] }}">{{ $type['name'] }}</option>

                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Floor</label>
                                        <select class="form-control" id="building_layout_id" name="building_layout_id">
                                            @foreach($floors as $floor)

                                                <option value="{{ $floor->id }}">{{ $floor->floor }}</option>

                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea id="description" name="description" rows="3" class="form-control">{{ old('description') }}</textarea>
                                    </div> 
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Image</label>
                                        <input type="file" name="image_path" id="image_path" class="form-control" required>
                                    </div> 
                                </div>

                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer text-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop