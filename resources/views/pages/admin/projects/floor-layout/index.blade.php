@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Floor Layout
        <small>Floor Layout of {{ $project->name }}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-image"></i> Floor Layout of {{ $project->name }}</a></li>
        <li class="active">Index</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row margin-bottom">
        <div class="col-md-12">
         
            <a href="{{ route('admin.floor-layout.create', [$project->id, $project->name]) }}" type="button" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add Floor Layout</a>

        </div>

        <!-- /.col -->
    </div>
    <!-- /.row -->      

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Floor Layout</h3>
                </div>
                <div class="box-body">

                    <!-- Unit -->                    
                    <div class="col-xs-12">
                        <table id="roomTable" class="dataTables table table-bordered">
                            <thead>
                                <th>Project</th>
                                <th>Type</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Action</th>
                            </thead>

                            <tbody>
                                @foreach($floors as $floor)
                                    <tr>
                                        <td>{{ $floor->project->name }}</td>
                                        <td>{{ $floor->findType() }}</td>
                                        <td>{{ $floor->name }}</td>
                                        <td>{{ $floor->renderShortDescription() }}</td>
                                        <td>
                                            <img src="{{ $floor->renderImagePath() }}" width="50" height="50" alt="{{ $floor->name }}">
                                        </td>
                                        <td>{!! $floor->renderStatus() !!}</td>
                                        <td>
                                            <a href="{{ route('admin.floor-layout.edit', [$floor->project_id, $floor->id]) }}" class="btn btn-sm btn-primary">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a class="btn btn-sm btn-danger deleteBtn"
                                                data-action="{{ route('admin.floor-layout.destroy', $floor->id) }}"
                                                data-message="Are you sure you want to archived {{ $floor->name }} ?">
                                                <i class="fa fa-trash"></i>
                                            </a>

                                            <a href="{{route('admin.units.index', [$floor->id, $floor->name]) }}" class="btn btn-sm btn-success"><i class="fa fa-building"></i> Manage Units</a>

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div> <!-- Unit ./end -->                

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop