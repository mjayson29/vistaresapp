@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Floor Layout
        <small>Update Floor Layout</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-edit"></i> Update {{ $floor->name }}</a></li>
        <li class="active">Create</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title">Update {{ $floor->name }}</h3>
                </div>
                <div class="box-body">
                    
                    @include('PRXAdmin::includes.form-errors')

                    <!-- form start -->
                    <form role="form" method="post" action="{{ route('admin.floor-layout.update', [$floor->project_id, $floor->id]) }}" 
                        enctype="multipart/form-data">

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="box-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" id="name" class="form-control" required value="{{ $floor->name }}">
                                    </div> 
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Type</label>
                                        <select class="form-control" id="type" name="type">
                                            @foreach($types as $type)

                                                <option value="{{ $type['value'] }}" 
                                                    @if($floor->type === $type['value'])
                                                        selected 
                                                    @endif
                                                >
                                                {{ $type['name'] }}</option>

                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Floor</label>
                                        <select class="form-control" id="building_layout_id" name="building_layout_id">
                                            @foreach($layouts as $layout)

                                                <option value="{{ $layout->id }}"
                                                    @if($floor->building_layout_id === $layout->id)
                                                        selected 
                                                    @endif
                                                >{{ $layout->floor }}</option>

                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea id="description" name="description" rows="3" class="form-control">{{ $floor->description }}</textarea>
                                    </div> 
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <img class="img-responsive" src="{{ $floor->renderImagePath() }}">
                                    </div>
                                </div>

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label>Image</label>
                                        <input type="file" name="image_path" id="image_path" class="form-control">
                                    </div> 
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <br />
                                        <input name="status" type="checkbox" class="form-control bootstrap-toggle" data-toggle="toggle" data-on="Active" data-off="In Active" data-width="100" data-height="35" {{ $floor->status ? 'checked' : '' }}>
                                    </div>                                
                                </div>  

                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer text-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop