@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Building Layout
        <small>Add Building Layout</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-image"></i> Add Building Layout</a></li>
        <li class="active">Create</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title">Add Building Layout for {{ $project->name }} </h3>
                </div>
                <div class="box-body">
                    
                    @include('PRXAdmin::includes.form-errors')

                    <!-- form start -->
                    <form role="form" method="post" action="{{ route('admin.building-layout.store', $project->id) }}" 
                        enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div id="imageHolder" style="width: 100%; height: 100%; position: relative; text-align: center;">

                                        <!-- Stored units -->

                                        @foreach($layouts as $plot)
                                            <div style="position: absolute; top: {{ $plot->y_coordinates  }}%; left: {{ $plot->x_coordinates  }}%; background: #00000085; width: {{ $plot->width }}%; height: {{ $plot->height }}%"></div>
                                        @endforeach


                                        <img id="image" src="{{ $project->renderImagePath() }}" width="100%" height="100%">
                                    </div>
                                </div> 

                                <div class="col-md-6">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Width % <small>(Highlight)</small>: </label>
                                            <input type="number" id="width" name="width" placeholder="Width %" class="form-control" value="{{ old('width') }}" required step="any">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Height % <small>(Highlight)</small>: </label>
                                            <input type="number" id="height" name="height" placeholder="Height %" class="form-control" value="{{ old('height') }}" required step="any">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Floor: </label>
                                            <input type="text" id="floor" name="floor" placeholder="Floor" class="form-control" value="" required>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Type: </label>
                                            <select class="form-control" name="type" id="type">
                                                @foreach($types as $type)
                                                    <option value="{{ $type['value'] }}">{{ $type['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea id="description" name="description" rows="3" class="form-control" required>{{ old('description') }}</textarea>
                                        </div> 
                                    </div>                                    

                                    <!-- hidden inputs -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" id="x_coordinates" name="x_coordinates" placeholder="" class="form-control">
                                            <input type="hidden" id="y_coordinates" name="y_coordinates" placeholder="" class="form-control">                                            
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer text-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop