@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Floor
        <small>List of Floor of {{ $project->name }}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-building"></i> Floor</a></li>
        <li class="active">Index</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row margin-bottom">
        <div class="col-md-12">
            <a href="{{ route('admin.project.layout.create', [$project->id, $project->name]) }}" type="button" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Add Building Layout</a>          
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->      

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Units</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <div id="imageHolder" style="width: 100%; height: 100%; position: relative; text-align: center;">

                                <!-- Stored units -->

                                @foreach($layouts as $plot)
                                    <div style="position: absolute; top: {{ $plot->y_coordinates  }}%; left: {{ $plot->x_coordinates  }}%; background: #00000085; width: {{ $plot->width }}%; height: {{ $plot->height }}%; color:white">{{ $plot->floor }}</div>
                                @endforeach


                                <img id="image" src="{{ $project->renderImagePath() }}" width="100%" height="100%">
                            </div>
                        </div>
                    <div class="col-md-3"></div> 
                

                    <div class="row">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <table id="roomTable" class="dataTables table table-bordered">
                                    <thead>
                                        <th>Project</th>
                                        <th>Floor</th>
                                        <th>Actions</th>
                                    </thead>

                                    <tbody>
                                        @foreach($layouts as $layout)
                                            <tr>
                                                <td>{{ $layout->project->name }}</td>
                                                <td>{{ $layout->floor }}</td>
                                                <td>
                                                    <a href="{{ route('admin.building-layout.edit', [$project->id, $layout->id]) }}" class="btn btn-sm btn-primary">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a class="btn btn-sm btn-danger deleteBtn"
                                                        data-action="{{ route('admin.building-layout.destroy', $layout->id) }}"
                                                        data-message="Are you sure you want to archived {{ $layout->floor }} ?">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop