@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Rooms
        <small>List of Rooms of </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-building"></i> Rooms</a></li>
        <li class="active">Index</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row margin-bottom">
        <div class="col-md-12">
            <a href="{{ route('admin.room-layouts.create', [$floor->id, $floor->name]) }}" type="button" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add Room Layout</a>            
            {{-- <a href="{{ route('admin.units.create', [$floor->id, $floor->name]) }}" type="button" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Room Layout</a>  --}}           
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->      

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Units</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-xs-12">
                              
                                <table id="roomTable" class="dataTables table table-bordered">
                                    <thead>
                                        <th>Room</th>
                                        <th>Layout Name</th>
                                        <th>Actions</th>
                                    </thead>

                                    <tbody>
                                        @foreach($units as $unit)
                                            @foreach($unit->room_layout as $room)
                                                <tr>
                                                    <td>{{ $unit->unit_number }}</td>
                                                    <td>{{ $room->name }}</td>
                                                    <td>
                                                        <a href="{{ route('admin.room-layouts.edit', [$room->id, $room->unit->name]) }}" class="btn btn-sm btn-primary">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        <a class="btn btn-sm btn-danger deleteBtn"
                                                            data-action="{{ route('admin.room-layouts.destroy', $room->id) }}"
                                                            data-message="Are you sure you want to archived {{ $room->name }} ?">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop