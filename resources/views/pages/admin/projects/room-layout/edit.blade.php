@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Room Layout
        <small>Update </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-building"></i> Room Layout</a></li>
        <li class="active">Update</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title">Update Room Layout</h3>
                </div>
                <div class="box-body">
                    
                    @include('PRXAdmin::includes.form-errors')

                    <!-- form start -->
                    <form role="form" method="post" action="{{ route('admin.room-layouts.update', $room->id) }}" enctype="multipart/form-data">

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">

                                    <div id="imageHolder" style="width: 100%; height: 100%; position: relative; text-align: center;">
                                        <!-- Stored units -->
                                            <div id="unit{{ $room->id }}" style="position: absolute; top: {{ $room->y_coordinates  }}%; left: {{ $room->x_coordinates  }}%; color: #fff; font-weight: bold;font-size:18px">{{ $room->name }}</div>
                                            @foreach($layouts as $layout)
                                                <div id="unit{{ $layout->id }}" style="position: absolute; top: {{ $layout->y_coordinates  }}%; left: {{ $layout->x_coordinates  }}%; color: #fff; font-weight: bold;font-size:18px">{{ $layout->name }}</div>
                                            @endforeach
                    
                                        <img id="image" src="{{ $room->unit->renderImagePath() }}" width="50%" height="100%">
                                    </div>
                                </div> 

                                <div class="col-md-6">
                                    <!-- Hidden Variable -->
                                    <input type="hidden" id="unitID" class="form-control" value="{{ $room->id }}" required>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Name: </label>
                                            <input type="text" id="unitNumber" name="name" placeholder="Name" class="form-control" value="{{ $room->name }}" required>
                                        </div>
                                    </div>

                                    <!-- hidden inputs -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" id="x_coordinates" name="x_coordinates" placeholder="" class="form-control" value="{{ $room->x_coordinates }}">
                                            <input type="hidden" id="y_coordinates" name="y_coordinates" placeholder="" class="form-control" value="{{ $room->y_coordinates }}">                                            
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>

                        <!-- /.box-body -->
                        <div class="box-footer text-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop