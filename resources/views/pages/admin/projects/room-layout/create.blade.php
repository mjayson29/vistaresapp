@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Floor Layout
        <small>Add Floor Layout</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-image"></i> Add Floor Layout</a></li>
        <li class="active">Create</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title">Add Room Layout for {{ $floor->name }}</h3>
                </div>
                <div class="box-body">
                    
                    @include('PRXAdmin::includes.form-errors')

                    <!-- form start -->
                    <form role="form" method="post" action="{{ route('admin.room-layouts.store') }}" 
                        enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="box-body">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group image-preview">
                                        <div id="imageHolder" style="width: 100%; height: 100%; position: relative; text-align: center;">
                                            <img src="#" id="image" width="50%">
                                        </div>
                                    </div> 
                                </div>

                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Room</label>
                                            <select class="form-control" id="imageSelection" name="unit_id" required>
                                                <option disabled selected>Select Room</option>
                                                @foreach($units as $unit)
                                                    {{-- @foreach($unit->room_layout as $room) --}}
                                                        @if(count($unit->room_layout) < 3) 
                                                            <option value='{"image": "{{ $unit->image_path }}", "id": {{ $unit->id }} }'>{{ $unit->unit_number }}</option>
                                                        @endif
                                                    {{-- @endforeach --}}
                                                @endforeach
                                            </select>
                                        </div> 
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <select class="form-control" id="unitNumber" name="name">
                                                <option value="cr" selected>CR</option>
                                                <option value="window">Window</option>
                                                <option value="door">Door</option>
                                                <option value="center">Center</option>
                                             </select>
                                            {{-- <input type="text" name="name" id="unitNumber" class="form-control" required> --}}
                                        </div>
                                    </div> 
                                </div>

                                <!-- hidden inputs -->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="hidden" id="x_coordinates" name="x_coordinates" placeholder="" class="form-control">
                                        <input type="hidden" id="y_coordinates" name="y_coordinates" placeholder="" class="form-control">                                            
                                    </div>
                                </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer text-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop