@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ $project->name }}
        <small>Update {{ $project->name }}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-building"></i> {{ $project->name }}</a></li>
        <li class="active">Update</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title">Update {{ $project->name }}</h3>
                </div>
                <div class="box-body">
                    
                    @include('PRXAdmin::includes.form-errors')

                    <form role="form" method="post" action="{{ route('admin.project.update', $project->id) }}" enctype="multipart/form-data">
                        
                        {{ csrf_field() }}

                        {{ method_field('PUT') }}

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input name="name" type="name" class="form-control" placeholder="Name" required value="{{ $project->name }}">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea id="description" name="description" placeholder="Description" class="form-control" rows="5">{{ $project->description }}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="address">Address</label>
                                        <input type="text" id="address" name="address" class="form-control" required value="{{ $project->address }}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="landmark">Landmark</label>
                                        <input type="text" id="landmark" name="landmark" class="form-control" required value="{{ $project->landmark }}">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="latitude">Latitude</label>
                                        <input type="text" id="latitude" name="latitude" class="form-control" required value="{{ $project->latitude }}">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="longitude">Longitude</label>
                                        <input type="text" id="longitude" name="longitude" class="form-control" required value="{{ $project->longitude }}">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="footer">Footer <small>(Description)</small></label>
                                        <textarea id="footer" name="footer" placeholder="Footer (Description)" class="form-control" rows="5">{{ $project->footer }}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="form-group"> 
                                            <label for="image_path">Building Image <i>(Should be a landscape image)</i></label> 
                                            <a @if($project->image_path) href="{{ $project->renderImagePath() }}" @endif target="_blank" class="btn @if($project->image_path) btn-info @else btn-danger @endif btn-sm">@if($project->image_path) View Building Image @else No Building image @endif</a></a>
                                        </div>
                                        <input type="file" id="image_path" name="image_path" class="form-control" required>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label>Outside View <small>(Front view)</small></label>
                                            <a @if($project->front_outside_image) href="{{ $project->renderOutsideFrontView() }}" @endif target="_blank" class="btn @if($project->front_outside_image) btn-info @else btn-danger @endif btn-sm">@if($project->front_outside_image) View Outside Image @else No Outside image (Front view) @endif</a>
                                        </div>
                                        <input type="file" name="front_outside_image" id="front_outside_image" class="form-control" required>
                                    </div> 
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label>Outside View <small>(Back view)</small></label>
                                            <a @if($project->back_outside_image) href="{{ $project->renderOutsideBackView() }}" @endif target="_blank" class="btn @if($project->back_outside_image) btn-info @else btn-danger @endif btn-sm">@if($project->back_outside_image) View Outside Image @else No Outside image (Back view) @endif</a>
                                        </div>
                                        <input type="file" name="back_outside_image" id="back_outside_image" class="form-control" required>
                                    </div> 
                                </div>
                            </div>
                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer text-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop

@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script type="text/javascript">
        function renderImage(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();

                reader.onload = function(e){
                    $('#image_file').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#image_path').change(function(){
            $('#image_file').show();
            if($('#image_path').val() === ''){
                $('#image_file').hide();
            }
            renderImage(this);
        });
    </script>
@endsection