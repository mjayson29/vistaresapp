@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Gallery
        <small>Gallery of {{ $project->name }}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-image"></i> Gallery of {{ $project->name }}</a></li>
        <li class="active">Index</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row margin-bottom">
        <div class="col-md-12">
         
            <a href="{{ route('admin.gallery.create', [$project->id, $project->name]) }}" type="button" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add Image</a>

        </div>

        <!-- /.col -->
    </div>
    <!-- /.row -->      

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Images</h3>
                </div>
                <div class="box-body">

                    @foreach($images as $image)
                        <div class="form-group col-md-4">
                            <a href="{{ route('admin.gallery.edit', [$image->id, $image->project->id, $image->project->name]) }}">
                                <img class="img-responsive" style="height: 200px;" src="{{ $image->renderImagePath()  }}">
                            </a>

                            <div class="col-md-12 text-center">
                                <br />
                                <div class="form-group">
                                    <a type="button" href="{{ route('admin.gallery.edit', [$image->id, $image->project->id, $image->project->name]) }}" class="btn btn-success"><i class="fa fa-edit"></i></a>
                                    <a type="button" class="btn btn-danger deleteBtn"
                                        data-action="{{ route('admin.gallery.destroy', $image->id) }}"
                                        data-message="Are you sure you want to removed this image?"
                                    ><i class="fa fa-trash"></i></a>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="form-group col-md-12 text-right">
                        {{ $images->links() }}
                    </div>

                    @if(!count($images))
                        <div class="text-center">
                            <label>No images</label>
                        </div>
                    @endif

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop