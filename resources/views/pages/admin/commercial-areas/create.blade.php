@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Commercial Area
        <small>Create Commercial Area</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-building"></i> Commercial Area</a></li>
        <li class="active">Create</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title">Create Commercial Area @if($project)for {{ $project->name }}@endif</h3>
                </div>
                <div class="box-body">
                    
                    @include('PRXAdmin::includes.form-errors')

                    <!-- form start -->
                    <form role="form" method="post" action="{{ route('admin.commercial-areas.store') }}" 
                        enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="box-body">
                            <div class="row">
                                
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Project Name</label>
                                        
                                        @if($project)
                                            <!-- Hidden ID -->
                                            <input type="hidden" name="project_id" id="project_id" class="form-control" readonly value="{{ $project->id }}" required>

                                            <input id="project_name" type="text" class="form-control" placeholder="Project Name" readonly value="{{ $project->name }}" required>

                                        @else
                                            <select id="project_id" name="project_id" class="form-control">
                                                @foreach($projects as $project)
                                                    <option value="{{ $project->id }}">
                                                    {{ $project->name }}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </div> 
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" id="name" class="form-control" placeholder="Name" value="{{ old('name') }}" required>
                                    </div> 
                                </div>

                                {{-- <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea id="description" name="description" rows="3" class="form-control">{{ old('description') }}</textarea>
                                    </div> 
                                </div> --}}


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Image</label>
                                        <input type="file" name="image_path" id="image_path" class="form-control" required>
                                    </div> 
                                </div>

                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer text-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop