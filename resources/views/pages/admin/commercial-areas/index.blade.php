@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Commercial Areas
        <small>List of Commercial Areas</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-building"></i> Commercial Areas</a></li>
        <li class="active">Index</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row margin-bottom">
        <div class="col-md-12">
         
            <a href="{{ route('admin.commercial-areas.create') }}" type="button" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add Commercial Areas</a>

        </div>

        <!-- /.col -->
    </div>
    <!-- /.row -->      

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Commercial Areas</h3>
                </div>
                <div class="box-body">

                    <table id="commercialAreasTable" class="dataTables table table-bordered">
                        <thead>
                            <th>Name</th>
                            <th>Project Name</th>
                            {{-- <th>Description</th> --}}
                            <th>Image</th>
                            <th>Action</th>
                        </thead>

                        <tbody>
                            @foreach($commercialAreas as $commercialArea)
                                <tr>
                                    <td>{{ $commercialArea->name }}</td>
                                    <td>{{ $commercialArea->project->name }}</td>
                                    {{-- <td>{{ $commercialArea->renderShortDescription() }}</td> --}}
                                    <td>
                                        <img src="{{ $commercialArea->renderImagePath() }}" width="50" height="50" alt="{{ $commercialArea->name }}">
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.commercial-areas.edit', [$commercialArea->id]) }}" class="btn btn-sm btn-primary">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a class="btn btn-sm btn-danger deleteBtn"
                                            data-action="{{ route('admin.commercial-areas.destroy', $commercialArea->id) }}"
                                            data-message="Are you sure you want to archived {{ $commercialArea->name }} ?">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop