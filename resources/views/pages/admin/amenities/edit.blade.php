@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ $amenity->name }}
        <small>Update {{ $amenity->name }}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-building"></i> {{ $amenity->name }}</a></li>
        <li class="active">Update</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title">Update {{ $amenity->name }}</h3>
                </div>
                <div class="box-body">
                    
                    @include('PRXAdmin::includes.form-errors')

                    <form role="form" method="post" action="{{ route('admin.amenities.update', $amenity->id) }}"
                        enctype="multipart/form-data">
                        
                        {{ csrf_field() }}

                        {{ method_field('PUT') }}

                        <div class="box-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="project_id">Project Name</label>
                                        <select id="project_id" name="project_id" class="form-control">
                                            @foreach($projects as $project)
                                                <option value="{{ $project->id }}"
                                                    @if($amenity->project_id === $project->id) selected @endif
                                                >
                                                {{ $project->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input name="name" type="name" class="form-control" placeholder="Name" required value="{{ $amenity->name }}">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea id="description" name="description" placeholder="Description" class="form-control" rows="5">{{ $amenity->description }}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        
                                        <label for="image_path">Image</label>

                                        <div class="form-group">
                                            <img src="{{ $amenity->renderImagePath() }}" width="300" height="200">
                                        </div>

                                        <input type="file" name="image_path" id="image_path" class="form-control">
                                    </div>
                                </div>                                


                            </div>
                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer text-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop