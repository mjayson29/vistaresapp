@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Sold Out Projects
        <small>List of Sold Out Projects</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-building"></i> Sold Out Projects</a></li>
        <li class="active">Index</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row margin-bottom">
        <div class="col-md-12">

            <a href="{{ route('admin.sold-out-projects.create') }}" type="button" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add Sold Out Project</a>

        </div>

        <!-- /.col -->
    </div>
    <!-- /.row -->      

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Sold Out Projects</h3>
                </div>
                <div class="box-body">
                    

                    @if(!count($soldOutProjects))
                        <div class="form-group text-center">
                            <label>No Sold out Projects. <a href="{{ route('admin.sold-out-projects.create') }}">Add now?</a></label>
                        </div>
                    @endif


                    @foreach($soldOutProjects as $project)

                        <div class="col-xs-4">
                            <div class="form-group">
                                <a type="button" href="{{ route('admin.sold-out-projects.show', [$project->id, $project->name]) }}" class="btn btn-large btn-lg btn-block btn-primary"><i class="fa fa-building"></i> {{ $project->name }}</a>
                            </div>
                        </div>

                    @endforeach


                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop