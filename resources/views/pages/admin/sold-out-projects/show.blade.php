@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ $project->name }}
        <small>Show {{ $project->name }}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-building"></i> {{ $project->name }}</a></li>
        <li class="active">Show</li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row margin-bottom">
        <div class="col-md-12">

            <a href="{{ route('admin.sold-out-projects.edit', $project->id) }}" type="button" class="btn btn-primary"><i class="fa fa-edit"></i> Edit {{ $project->name }}</a>

            <a type="button" class="btn btn-danger deleteBtn"
                data-action="{{ route('admin.sold-out-projects.destroy', $project->id) }}"
                data-message="Are you sure you want to archived {{ $project->name }} ?"
            ><i class="fa fa-trash"></i> Archive {{ $project->name }}</a>

        </div>

        <!-- /.col -->
    </div>
    <!-- /.row -->      

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">{{ $project->name }}</h3>
                </div>
                <div class="box-body">
                    <div class="col-xs-6">
                        
                        <img class="img-responsive"
                        src="{{ $project->renderImagePath() }}">

                    </div>

                    <div class="col-xs-6">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Project Name: <span class="label label-primary">{{ $project->name }}</span></label>
                            </div>
                        </div>

                     {{--    <div class="col-md-12">
                            <div class="form-group">
                                <label>Description: </label>
                                <span>{{ $project->description }}</span>
                            </div>
                        </div> --}}


                    </div>
                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop