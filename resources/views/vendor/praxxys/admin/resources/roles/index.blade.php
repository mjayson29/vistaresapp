@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Roles
        <small>Manages Roles and Permission of Administrators</small>
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="{{ route('roles.index') }}"><i class="fa fa-address-card"></i> Roles</a></li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row margin-bottom">
        <div class="col-md-12">
            <a href="{{ route('roles.create') }}" class="btn btn-primary">
                <i class="fa fa-plus"></i> Add Role
            </a>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->      

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table class="dataTables table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($roles as $role)
                            <tr>
                                <td>{{ $role->id }}</td>
                                <td>{{ $role->name }}</td>
                                <td>{{ $role->description }}</td>
                                <td>
                                    <center>
                                        <a href="{{ route('roles.show', $role->id) }}" id="" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a>
                                    </center>
                                </td>
                            </tr>
                            @endforeach
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
             </div>
            <!-- /.box -->

        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop