<table id="" class="dataTables table table-bordered table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Avatar</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($lists as $admin)
        <tr>
            <td>{{ $admin->id }}</td>
            <td>
                <center>
                    <img class="thumbnail" src="{{ $admin->avatar() }}" alt="">
                </center>
            </td>
            <td>{{ $admin->renderFullname() }}</td>
            <td>{{ $admin->email }}</td>
            <td>{{ $admin->renderRoles() }}</td>
            <td>
                <center>
                    <a class="btn btn-primary btn-xs" href="{{ route('admin-users.edit', $admin->id) }}"><i class="fa fa-eye"></i></a>

                    @if ($admin->trashed())
                        <a class="btn btn-warning btn-xs actionBtn" href="#"
                            data-action="{{ route('admin-users.restore', $admin->id) }}"
                            data-message="Are you sure you want to restore the Admin {{ $admin->name }}?"
                            ><i class="fa fa-check"></i>
                        </a>
                    @else
                        <a class="btn btn-danger btn-xs deleteBtn" href="#"
                            data-action="{{ route('admin-users.destroy', $admin->id) }}"
                            data-message="Are you sure you want to delete the Admin {{ $admin->name }}?"
                            ><i class="fa fa-times"></i>
                        </a>
                    @endif
                </center>
            </td>
        </tr>
        @endforeach
        </tr>
    </tbody>
</table>