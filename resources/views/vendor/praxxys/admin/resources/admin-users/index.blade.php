@extends('PRXAdmin::master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Administrators
        <small>Manage Administrator Accounts</small>
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="{{ route('admin-users.index') }}"><i class="fa fa-user-circle"></i> Administrators</a></li>
    </ol>

</section>

<!-- Main content -->
<section class="content">

    <div class="row margin-bottom">
        <div class="col-md-12">
            <a href="{{ route('admin-users.create') }}" class="btn btn-primary">
                <i class="fa fa-plus"></i> Add Admin
            </a>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-widget nav-tabs-custom table-responsive">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#users" data-toggle="tab"><h5><b>Admins</b></h5></a>
                    </li>
                    <li>
                        <a href="#users-archive" data-toggle="tab"><h5><b>Archive</b></h5></a>
                    </li>                                                   
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="users">
                        
                        @include('PRXAdmin::resources.admin-users.lists', [
                            'lists' => $admins,
                        ])

                    </div>
                    <div class="tab-pane" id="users-archive">
                        
                        @include('PRXAdmin::resources.admin-users.lists', [
                            'lists' => $archiveAdmins,
                        ])

                    </div>                  
                </div>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->    
</section>
<!-- /.content -->
@stop