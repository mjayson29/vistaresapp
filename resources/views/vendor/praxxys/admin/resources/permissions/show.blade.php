@extends('PRXAdmin::master')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Roles and Permissions
        <small>{{ $role->name }} {{ $role->description }}</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

       <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Permissions</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST"
              action="{{ route('permissions.update', $role->id) }}"
              >
              {{ csrf_field() }}

              @include('PRXAdmin::includes.form-errors')

              <div class="box-body">
                <div class="box-group" id="accordion">
                  <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                  @foreach ($permissionGroups as $permissions)
                  <div class="panel box box-primary">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a class="text-capitalize" data-toggle="collapse" data-parent="#accordion" href="#{{ $permissions->first()->id }}">
                          {{ str_replace('-', '', str_replace('_', ' ', str_replace('admin', '', explode('.', $permissions->first()->name)[0]))) }}
                        </a>
                      </h4>
                    </div>
                    <div id="{{ $permissions->first()->id }}" class="panel-collapse collapse in">
                      <div class="box-body">
                        @foreach ($permissions as $permission)
                        <div class="col col-sm-4">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" name="permissions[]" value="{{ $permission->id }}" 
                                {{ in_array($role->id, $permission->roles()->pluck('id')->toArray()) ? 'checked' : '' }}>
                                {{ $permission->description }}
                            </label>
                          </div>
                        </div>
                        @endforeach
                      </div>
                    </div>
                  </div>
                  @endforeach
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save Changes</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

          </div>
          <!-- /.col -->

        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
@endsection