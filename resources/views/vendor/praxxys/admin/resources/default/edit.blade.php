    @extends('PRXAdmin::master')
    @section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $title }}
            <small>{{ $subtitle }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route("admin.$name.index") }}"><i class="far fa-edit"></i> {{ $title }}</a></li>
            <li class="active">{{ $subtitle }}</li>
        </ol>
    </section>

<!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ $subtitle }} {{ $title }}</h3>
                    </div>
                    <div class="box-body">

                        @include('PRXAdmin::includes.form-errors')

                        <!-- form start -->
                        <form role="form" method="post" action="{{ route("admin.$name.update", $item->id) }}" 
                            enctype="multipart/form-data">

                            {{ csrf_field() }}
                            {{ method_field('PUT') }}

                            <div class="box-body">


                                @foreach($belongs_to as $name => $data)

                                    @switch($data['type'])

                                    @case('select')

                                    <div class="form-group">
                                        <label for="{{ $name }}">{{ $data['label'] }}</label><br>
                                        <select class="select2" name="{{ $name }}" required>
                                            <option selected value="{{ $item->$name }}">{{ $item::getBelongsToByField($name, $item)}}</option>
                                            @foreach($data['collection'] as $collection)
                                            <option value="{{ $collection->id }}">{{ $collection->getName()}}</option>
                                            @endforeach
                                        </select>

                                    </div>

                                        @break

                                    @endswitch

                                @endforeach

                                {{-- Render Enum Fields --}}
                                @foreach($enum_fields as $name => $data)

                                    <div class="form-group">
                                        <label for="{{ $name }}">{{ $data['label'] }}</label><br>
                                        <select class="select2" name="{{ $name }}" required>
                                            @foreach($data['collection'] as $collection_item)
                                            <option value="{{ $collection_item['id'] }}"
                                            @if($item->$name == $collection_item['id'])
                                            selected="selected" 
                                            @endif
                                            >{{ $collection_item['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                @endforeach

                                @foreach($fields as $name => $data)
                                    
                                    @switch($data['type'])
                                    
                                    @case('text')
                                    @case('email')

                                    <div class="form-group">
                                        <label for="{{ $name }}">{{ $data['label'] }}</label>
                                        @if(array_key_exists('readonly', $data) && $data['readonly'])
                                        <p>{{ $item->$name }}</p>
                                        @else
                                        <input name="{{ $name }}" type="{{ $data['type'] }}" class="form-control" id="{{ $name }}" value="{{ $item->$name }}" 
                                        @if(isset($data['required'])) required @endif
                                        >
                                        @endif
                                    </div>


                                        @break
                                    
                                    @case('number')

                                    <div class="form-group">
                                        @if(array_key_exists('readonly', $data) && $data['readonly'])
                                        <p>{{ $item->$name }}</p>
                                        @else
                                        <label for="{{ $name }}">{{ $data['label'] }}</label>
                                        <input name="{{ $name }}" type="{{ $data['type'] }}" class="form-control" id="{{ $name }}" value="{{ $item->$name }}"
                                        @if(isset($data['required'])) required @endif
                                        >
                                        @endif
                                    </div>


                                        @break

                                    @case('decimal')

                                    <div class="form-group">
                                        @if(array_key_exists('readonly', $data) && $data['readonly'])
                                        <p>{{ $item->$name }}</p>
                                        @else
                                        <label for="{{ $name }}">{{ $data['label'] }}</label>
                                        <input name="{{ $name }}" type="number" class="form-control" id="{{ $name }}" value="{{ $item->$name }}"
                                        @if(isset($data['required'])) required @endif
                                        step="0.01"
                                        >
                                        @endif
                                    </div>


                                        @break

                                    @case('date')

                                    <div class="form-group">
                                        @if(array_key_exists('readonly', $data) && $data['readonly'])
                                        <p>{{ $item->$name }}</p>
                                        @else
                                        <label for="{{ $name }}">{{ $data['label'] }}</label>
                                        <input name="{{ $name }}" type="{{ $data['type'] }}" class="form-control @if($data['type'] === 'date') datePicker @endif" id="{{ $name }}" placeholder="{{ $data['label'] }}" value="{{ $item->$name }}"
                                        @if(isset($data['required'])) required @endif
                                        >
                                        @endif
                                    </div>


                                        @break

                                    @case('time')

                                    <div class="form-group">
                                        @if(array_key_exists('readonly', $data) && $data['readonly'])
                                        <p>{{ $item->$name }}</p>
                                        @else
                                        <label for="{{ $name }}">{{ $data['label'] }}</label>
                                        <input name="{{ $name }}" type="{{ $data['type'] }}" class="form-control @if($data['type'] === 'time') timePicker @endif" id="{{ $name }}" placeholder="{{ $data['label'] }}" value="{{ $item->$name }}"
                                        @if(isset($data['required'])) required @endif
                                        >
                                        @endif
                                    </div>


                                        @break

                                    @case('datetime')

                                    <div class="form-group">
                                        @if(array_key_exists('readonly', $data) && $data['readonly'])
                                        <p>{{ $item->$name }}</p>
                                        @else
                                        <label for="{{ $name }}">{{ $data['label'] }}</label>
                                        <input name="{{ $name }}" type="{{ $data['type'] }}" class="form-control @if($data['type'] === 'datetime') dateTimePicker @endif" id="{{ $name }}" placeholder="{{ $data['label'] }}" 
                                        @if(isset($data['required'])) required @endif
                                        value="{{ $item->$name }}"
                                        >
                                        @endif
                                    </div>


                                        @break

                                    @case('textarea')

                                    <div class="form-group">
                                        @if(array_key_exists('readonly', $data) && $data['readonly'])
                                        <p>{{ $item->$name }}</p>
                                        @else
                                        <label for="{{ $name }}">{{ $data['label'] }}</label>
                                        <textarea class="form-control" name="{{ $name }}" id="" rows="8">{{ $item->$name }}</textarea>
                                        @endif
                                    </div>


                                        @break

                                    @case('image')
                                        @if(array_key_exists('readonly', $data) && $data['readonly'])
                                        @else
                                        <div class="form-group">
                                            <label for="{{ $name }}">{{ $data['label'] }}</label>
                                            <input type="file" id="{{ $name }}" name="{{ $name }}">

                                            {{-- Placeholder for existing image --}}
                                            <input type="hidden" name="{{ $name }}" value="{{ $item->$name }}">

                                        </div>
                                        @endif
                                        <div class="form-group">
                                            <img src="{{ PRXAdmin::storage()->getCachedImage('square-medium',basename($item->$name)) }}" alt="">
                                        </div>

                                        @break

                                    @case('publicImage')
                                        <div class="form-group">
                                            <label for="{{ $name }}">{{ $data['label'] }}</label>
                                            <input type="file" id="{{ $name }}" name="{{ $name }}">

                                            {{-- Placeholder for existing image --}}
                                            <input type="hidden" name="{{ $name }}" value="{{ $item->$name }}">

                                        </div>

                                        <div class="form-group">
                                            <img src="/storage/{{ $item->$name }}" alt="No Image" width="130">
                                        </div>
                                        @break;

                                    @case('boolean')
                                        <div class="form-group">
                                            <p><b>{{ $data['label'] }}</b></p>
                                            <input name="{{ $name}}" type="checkbox" class="form-control bootstrap-toggle" data-toggle="toggle" data-on="Enabled" data-off="Disabled" data-width="100" data-height="35" @if($item->$name) checked @endif> 
                                        </div>

                                        @break

                                    @case('ckeditor')

                                        <div class="form-group">
                                            @if(array_key_exists('readonly', $data) && $data['readonly'])
                                            <p>{{ $item->$name }}</p>
                                            @else
                                            <label for="{{ $name }}">{{ $data['label'] }}</label>
                                            <textarea class="form-control editorField" name="{{ $name }}" id="" rows="8">{{ $item->$name }}</textarea>
                                            @endif
                                        </div>

                                        @break

                                    @case('locationPicker')

                                        <div class="form-group locationPicker">

                                            <label for="{{ $name . '-location-picker-search' }}">{{ $data['label'] }}</label>
                                            
                                            <input 
                                                name="{{ $name . '_address' }}"
                                                type="text"
                                                class="form-control locationPicker__search"
                                                id="{{ $name . '-location-picker-search' }}"
                                                placeholder="Search address..." 
                                                @if(isset($data['required'])) required @endif
                                                value="{{ $item->{$name . '-address'} }}">

                                            <div class="locationPicker__map"></div>

                                            <input 
                                                name="{{ $name . '_latitude' }}"
                                                class="locationPicker__lat"
                                                type="text"
                                                value="{{ $item->{$name . '_latitude'} }}"
                                                placeholder="Longitude" 
                                                readonly>

                                            <input 
                                                name="{{ $name . '_longitude' }}"
                                                class="locationPicker__long"
                                                type="text"
                                                value="{{ $item->{$name . '_longitude'} }}"
                                                placeholder="Longitude" 
                                                readonly>


                                        </div>
                                        
                                        @break



                                    @endswitch

                                @endforeach

                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- /.content -->

    @stop
