    @extends('PRXAdmin::master')
    @section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Logs
            <small>Index</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-clipboard"></i> Logs</a></li>
            <li class="active">Index</li>
        </ol>

    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Logs</h3>
                    </div>
                    <div class="box-body">
					    <table data-name="logs" 
					    class="dataTables table table-bordered table-hover"></table>
                    </div>
                    <!-- /.box-body -->
                 </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->    
    </section>
    <!-- /.content -->
    @stop