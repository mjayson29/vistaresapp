    <table data-name="{{ $name }}" 
    data-route="{{ route("admin.{$name}.index") }}"
    class="dataTables table__actions table table-bordered table-hover @admincan("$name.show") table__viewable @endadmincan @admincan("$name.edit") table__editable @endadmincan @admincan("$name.destroy") table__deletable @endadmincan"></table>