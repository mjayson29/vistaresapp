<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        {{ config('praxxys-admin.footer.caption') }}
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {{ config('praxxys-admin.footer.copyrights') }} 
    	<a href="{{ config('praxxys-admin.footer.link') }}">{{ config('praxxys-admin.title') }}</a>.
    </strong> All rights reserved.
</footer>