@foreach(config('praxxys-admin.navigation') as $section => $items)
    <li class="header">{{ $section }}</li>

    @foreach($items as $title => $item)

        {{-- Check if nav has sub items --}}
        @if(array_key_exists('items', $item))
            
            {{-- Render items with sub menu --}}
            <li class="treeview nav-item">
                <a><i class="{{ $item['icon'] }}"></i> <span>{{ $title }}</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    {{-- Check user permission --}}
                
                    @foreach($item['items'] as $title => $sub_item)

                        @if(PRXAdmin::auth()->authorizeRoute($sub_item['route']))

                            <li><a class="nav-link" href="{{ PRXAdmin::router()->getURLByName($sub_item['route']) }}"><i class="{{ $sub_item['icon'] }}"></i> {{ $title }}</a></li>

                        @endif

                    @endforeach

                </ul>

            </li>

        @else

            {{-- Check user permission --}}
            @if(PRXAdmin::auth()->authorizeRoute($item['route']))

                {{-- Render single-level nav items --}}
                <li class="nav-item">
                    <a class="nav-link" href="{{ PRXAdmin::router()->getURLByName($item['route']) }}">
                        <i class="{{ $item['icon'] }}"></i> <span>{{ $title }}</span>
                    </a>
                </li>

            @endif

        @endif

    @endforeach
@endforeach
