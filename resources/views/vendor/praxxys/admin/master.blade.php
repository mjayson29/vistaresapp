<!DOCTYPE html>
<html>

<head>
@include('PRXAdmin::includes.head')
@include('PRXAdmin::includes.css')
</head>

<body class="{{ config('praxxys-admin.theme') }} hold-transition sidebar-mini fixed">
    <div id="{{ config('praxxys-admin.app-id') }}" class="wrapper">
	
	@include('PRXAdmin::includes.header')
	@include('PRXAdmin::includes.sidebar')
	@include('PRXAdmin::includes.content')
	@include('PRXAdmin::includes.footer')
	@include('PRXAdmin::includes.settings-bar')

	</div>
	<!-- ./wrapper -->

@include('PRXAdmin::includes.js')

</body>
</html>
