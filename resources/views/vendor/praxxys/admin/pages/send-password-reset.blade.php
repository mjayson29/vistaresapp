@extends('PRXAdmin::user-form')
@section('content')

<div class="login-box">
    <div class="login-logo">
        <b>{{ config('praxxys-admin.titleStyled.main') }}</b>{{ config('praxxys-admin.titleStyled.sub') }}
    </div>

    <!-- /.login-logo -->
    <div class="login-box-body">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

        <h3 class="login-box-msg">Password Reset</h3>

        <p class="login-box-msg">Enter your email to send the password reset link.</p>
        <form action="{{ route('admin.password.email') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">

                <input type="email" class="form-control" placeholder="Email" value="{{ old('email') }}" required name="email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif

            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Reset</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
    </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

@stop